<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700|Roboto:400,700" rel="stylesheet">
<link rel="shortcut icon" type="image/png" href="/assets/img/favicon.png" />
<?php wp_head(); ?>
	
<script src='https://www.google.com/recaptcha/api.js'></script>
	
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	
<div id="topnav-outer">
<div id="topnav-inner">
	<a href="/">&larr; Eversmart homepage</a>	
</div>	
</div>

<div id="blog-banner">
	<a href="/blog/"><img class="blog-logo" src="/blog/images/eversmart-blog.png" alt="Eversmart Blog" /></a>
</div>
	
	<?php

	/*
	 * If a regular post or page, and not the front page, show the featured image.
	 * Using get_queried_object_id() here since the $post global may not be set before a call to the_post().
	 */
	if ( ( is_single() || ( is_page() && ! twentyseventeen_is_frontpage() ) ) && has_post_thumbnail( get_queried_object_id() ) ) :
		echo '<div class="single-featured-image-header">';
		echo get_the_post_thumbnail( get_queried_object_id(), 'twentyseventeen-featured-image' );
		echo '<div class="gradient"></div>';
	    echo '<div class="title-wrapper"><h1 class="new-entry-title">', get_the_title(), '</h1></div>';
	    echo '</div><!-- .single-featured-image-header -->';
	endif;
	?>
		
<div id="wrapper-outer">
<div id="wrapper-inner">
	
	<div class="site-content-contain">
		<div id="content" class="site-content">
