<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>

</div>
</div><!--/wrappers-->

</div><!-- #content -->

<div class="clear"></div>

<div id="footer-outer">
<div id="footer-inner">

<div class="grid50 tablet50 mobile100">
	<p>&copy; <?php echo date("Y"); ?> Eversmart Energy Ltd</p>
</div>
	
<div class="grid50 tablet50 mobile100 social-icons">
<a href="https://www.facebook.com/eversmartenergy" target="_blank" rel="nofollow"><img src="/blog/images/facebook-white.svg" alt="Facebook" height="25" width="25" /></a>
<a href="https://twitter.com/eversmartenergy" target="_blank" rel="nofollow"><img src="/blog/images/twitter-white.svg" alt="Twitter" height="25" width="25" /></a>
<a href="https://www.instagram.com/eversmartenergy/" target="_blank" rel="nofollow"><img src="/blog/images/instagram-white.svg" height="25" width="25" alt="Instagram" /></a>
</div>
	
<div class="clear"></div>
	
<a style="display:none" href="http://en.paperblog.com/" rel="paperblog eversmart" title="Paperblog : The best blog articles around" >
<img style="display:none" src="https://m5.paperblog.com/assets/images/logos/minilogo.png" border="0" alt="Paperblog" />
</a>
	
</div>
</div><!--/footer wrappers-->

</div><!-- .site-content-contain -->
</div><!-- #page -->

<!----- Scripts ----->

<?php wp_footer(); ?> 

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-79295711-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-79295711-1');
</script>


</body>
</html>
