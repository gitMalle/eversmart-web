<div id="landing-page">
    <section data-type="video-hero">
        <div class="full-width-video">
            <div class="video-overlay"></div>
            
            <div class="video-overlay-text text-left">
                <h1>Switch in April</h1>
                <p>Receive FREE heating and boiler insurance .</p>
                <div class="button-wrap">
                    <form class="get-quote-form" method="get" action="/quotation">
                        <input type="text" name="postcode" placeholder="Enter your postcode..." />
                        <button class="btn btn-in-input">Get Quote</button>
                    </form>
                </div>
                <br />
                <img src="/assets/images/new-world/trustpilot-5star-white.png" alt="" style="width: auto; height: 48px;"/>
                <p><small>*100% renewable energy</small></p>
            </div>
            <video class="hidden-xs" width="100%" height="100%" playsinline autoplay muted loop>
                <source src="https://s3.eu-west-2.amazonaws.com/website-cdn-assets/a992155f-7da1-41c4-ba75-8cbf234be975.MP4" type="video/mp4">
                Your browser does not support the video tag.
            </video>
        </div>
    </section>

    <section class="hero hero-fullwidth background-white">
        <div class="hidden-xs layout-50-lg layout-50-md layout-50-sm layout-100-xs text-center">
            <img src="/assets/images/new-world/homepage/windbg.png" alt="" style="width: 100%; height: 100%; object-fit: cover;" />
        </div>
        <div class="layout-50-lg layout-50-md layout-50-sm layout-100-xs hero-padding content-vertical-center">
            <br />
            <h1 class="text-left">Ensure your home is protected 365 days a year!</h1>
            <p class="text-left">
                Receive 12 months of <strong>FREE</strong> boiler and central heating insurance worth £160 when you switch to any Eversmart tariff throughout April 2019.       
            </p>
        </div>

        <div class="hidden-sm hidden-md hidden-lg layout-50-lg layout-50-md layout-50-sm layout-100-xs text-center">
            <img src="/assets/images/new-world/homepage/windbg.png" alt="" style="width: 100%; height: 100%; object-fit: cover;" />
        </div>
    </section>

    <section class="hero hero-fullwidth">
        <div class="layout-50-lg layout-50-md layout-50-sm layout-100-xs background-white hero-padding content-vertical-center">
            <h1>Switch your friends for dividends</h1>
            <p>Receive <strong>£40 cash</strong> when you refer a friend and they get £10 too!</p>
            <p>Simply head to your Eversmart account and share your unique code.</p>
        </div>

        <div class="layout-50-lg layout-50-md layout-50-sm layout-100-xs">
            <img src="/assets/images/new-world/homepage/refer.png" alt="" style="width: 100%; height: 100%; object-fit: cover;"/>
        </div>
    </section>

    <section id="sponsors-hero" class="hero hero-fullwidth background-grey hero-tall-padding">
        <div class="layout-25-lg layout-25-md layout-50-sm layout-100-xs">
            <img src="/assets/images/new-world/homepage/sponsors/utilityweek.png" alt="" style="max-width: 100%; height: 60px;" />
        </div>
        <div class="layout-25-lg layout-25-md layout-50-sm layout-100-xs">
            <img src="/assets/images/new-world/homepage/sponsors/dailymail.png" alt="" style="max-width: 100%; height: 60px;" />
        </div>
        <div class="layout-25-lg layout-25-md layout-50-sm layout-100-xs">
            <img src="/assets/images/new-world/homepage/sponsors/which.png" alt="" style="max-width: 100%; height: 60px;" />
        </div>
        <div class="layout-25-lg layout-25-md layout-50-sm layout-100-xs">
            <img src="/assets/images/new-world/homepage/sponsors/thetimes.png" alt="" style="max-width: 100%; height: 60px;" />
        </div>
    </section>

    <section class="hero hero-fullwidth background-white">
        <div class="layout-50-lg layout-50-md layout-50-sm layout-100-xs text-center">
            <img src="/assets/images/new-world/homepage/windbg.png" alt="" style="width: 100%; height: 100%;" />
        </div>
        <div class="layout-50-lg layout-50-md layout-50-sm layout-100-xs hero-padding content-vertical-center">
            <div class="testimonial-hero" style="width: 100%; display: inline-block;">
                <div class="testimonial-text">
                    Been with Eversmart for months now, went from payG to direct debit, no hassle, decent price, would recommend.
                </div>
                <div class="testimonial-information">
                    <div class="testimonial-name">David Trallis</div>
                    <div class="testimonial-position">CHELMSFORD</div>
                </div>
            </div>
        </div>
    </section>

    <section class="hero hero-fullwidth background-white">
        <div class="layout-50-lg layout-50-md layout-50-sm layout-100-xs hero-padding content-vertical-center">
            <img src="/assets/images/new-world/homepage/windicon.png" alt="" style="width: 64px; height: 64px;"/>
            <br />
            <h1 class="text-left">
                100% Renewable Energy Sources
            </h1>
            <p class="text-left">
            Be part of our environmentally conscious family and protect the planet with us. Have peace of mind in our 100% certified renewable electricity sources from local wind farms. Building partnerships to counterbalance our carbon footprint, aiming at being gas carbon neutral.  
            </p>
        </div>
        <div class="layout-50-lg layout-50-md layout-50-sm layout-100-xs text-center">
            <img src="/assets/images/new-world/homepage/testimonialbg.png" alt="" style="width: 100%; height: 100%;" />
        </div>
    </section>

    <section class="hero hero-fullwidth background-red hero-tall-padding">
        <h2 class="text-center">Switching is easy — we do all the hard work for you!</h2>
        <div class="layout-33-lg layout-33-md layout-100-sm layout-100-xs">
            <div class="hero-iconarea">
                <form class="get-quote-form" method="get" action="/quotation">
                    <input type="text" name="postcode" placeholder="Enter your postcode..." />
                    <button class="btn btn-in-input" type="submit">&#8594;</button>
                </form>
            </div>
            <h5>Get a quote</h5>
            <p class="text-center">
                Enter your postcode.
            </p>
        </div>
        <div class="layout-33-lg layout-33-md layout-100-sm layout-100-xs">
            <div class="hero-iconarea">
                <img src="/assets/images/new-world/homepage/pigicon.png" alt="" />
        
            </div>
            <h5>Sign up</h5>
            <p class="text-center">
                Find out how much you could save with Eversmart.
            </p>
        </div>
        <div class="layout-33-lg layout-33-md layout-100-sm layout-100-xs">
            <div class="hero-iconarea">
                <img src="/assets/images/new-world/homepage/checkicon.png" alt="" />
            </div>
            <h5>You're all done</h5>
            <p class="text-center">
                Sit back and let us take care of the rest.
            </p>
        </div>
    </section>

    <section class="hero hero-fullwidth background-white">
        <div class="layout-50-lg layout-50-md layout-50-sm layout-100-xs text-center hero-padding">
            <img src="/assets/images/new-world/homepage/phone.png" alt="" style="width: auto; height: 100%;"/>
        </div>

        <div class="layout-50-lg layout-50-md layout-50-sm layout-100-xs hero-padding hero-padding content-vertical-center">
            <h1 class="text-left">Technology that gives you control and transparency. </h1>
            <p class="text-left">Easy, efficient and enjoyable energy management with our new Eversmart app.</p>
            <p class="text-left">Manage your account online, send us meter readings and pay your bills on the go!</p>
        </div>
    </section>


    <section>
        <h3>Got a question?</h3>
       
    </section>

    <!--<section data-type="switching">
        <h2>Switching is easy – we do all the hard work for you!</h2>
        <ul>
            <li>
                <div class="img-wrap">
                    <img src="assets/images/new-world/icons/file-content.svg" alt="Step One" />
                </div>
                <h3>Step 1</h3>
                <h4>Register Your Details</h4>
                <p>Enter your postcode and tell us a little bit about your energy usage.</p>
            </li>
            <li>
                <div class="img-wrap">
                    <img src="assets/images/new-world/icons/like.svg" alt="Step Two" />
                </div>
                <h3>Step 2</h3>
                <h4>Get a quote</h4>
                <p>Find out how much you could save with Eversmart.</p>
            </li>
            <li>
                <div class="img-wrap">
                    <img src="assets/images/new-world/icons/handout.svg" alt="Step Three" />
                </div>
                <h3>Step 3</h3>
                <h4>You're all done </h4>
                <p>Sit back and let us take care of the rest. After 21 days you’ll be part of the Eversmart family.</p>
            </li>
        </ul>
        <div class="button-wrap">
            <a href="/quotation" title="Get Quote">Switch Now</a>
        </div>
    </section>

    <section data-type="switching">
        <h2>Switching is easy – we do all the hard work for you!</h2>
        <ul>
            <li>
                <div class="img-wrap">
                    <img src="assets/images/new-world/icons/file-content.svg" alt="Step One" />
                </div>
                <h3>Step 1</h3>
                <h4>Register Your Details</h4>
                <p>Enter your postcode and tell us a little bit about your energy usage.</p>
            </li>
            <li>
                <div class="img-wrap">
                    <img src="assets/images/new-world/icons/like.svg" alt="Step Two" />
                </div>
                <h3>Step 2</h3>
                <h4>Get a quote</h4>
                <p>Find out how much you could save with Eversmart.</p>
            </li>
            <li>
                <div class="img-wrap">
                    <img src="assets/images/new-world/icons/handout.svg" alt="Step Three" />
                </div>
                <h3>Step 3</h3>
                <h4>You're all done </h4>
                <p>Sit back and let us take care of the rest. After 21 days you’ll be part of the Eversmart family.</p>
            </li>
        </ul>
        <div class="button-wrap">
            <a href="/quotation" title="Get Quote">Switch Now</a>
        </div>
    </section>

    <section data-type="tech">
        <div class="img-wrap">
            <img src="assets/images/new-world/landing-page/all_device_assest.png" alt="Devices We Support" />
        </div>
        <div class="info-wrap">
            <h2>Tech that makes life easier</h2>
            <p>Manage your account online or with the Eversmart app. Send us meter readings and pay your bills on the go!</p>
        </div>
    </section>

    <section data-type="faqs">
        <span>customer Care</span>
        <h2>Guides &amp; FAQs</h2>
        <ul>
            <li>
                <a href="https://consumer.paypoint.com/" title="Where is my nearest paypoint?">
                    <div class="img-wrap">
                        <img src="assets/images/new-world/landing-page/btmimage.png" alt="Nearest Paypoint" />
                    </div>
                    <span>Where is my nearest paypoint?</span>
                </a>
            </li>
            <li>
                <a href="https://www.eversmartenergy.co.uk/index.php/Faqs?m=Switching" title="How long does the switch take?">
                    <div class="img-wrap">
                        <img src="assets/images/new-world/landing-page/btmimageone.png" alt="How long does the switch take?" />
                    </div>
                    <span>How long does the switch take?</span>
                </a>
            </li>
            <li>
                <a href="https://www.eversmartenergy.co.uk/index.php/Faqs?m=Topping%20Up" title="What payment options do I have?">
                    <div class="img-wrap">
                        <img src="assets/images/new-world/landing-page/btmimagetree.png" alt="What payment options do I have?" />
                    </div>
                    <span>What payment options do I have?</span>
                </a>
            </li>
        </ul>
    </section> -->
</div>