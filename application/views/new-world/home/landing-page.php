<script>
    function openBannerVideoFullscreen() {
        var video = document.getElementById("landingvideo");

        video.pause();
        video.currentTime = 0;

        if (video.requestFullscreen) {
            video.requestFullscreen();
        } else if (video.mozRequestFullScreen) {
            /* Firefox */
            video.mozRequestFullScreen();
        } else if (video.webkitRequestFullscreen) {
            /* Chrome, Safari and Opera */
            video.webkitRequestFullscreen();
        } else if (video.msRequestFullscreen) {
            /* IE/Edge */
            video.msRequestFullscreen();
        }


        video.setAttribute("controls", "controls");
        video.removeAttribute("loop");
        video.load();
        video.volume = 1.0;
    }

    document.addEventListener("fullscreenchange", function (e) {
        var video = document.getElementById("landingvideo");
        var state = document.fullScreen || document.mozFullScreen || document.webkitIsFullScreen;
        var event = state ? 'FullscreenOn' : 'FullscreenOff';

        if (event === 'FullscreenOff') {
            video.setAttribute("loop", "loop");
            video.removeAttribute("controls");
            video.volume = 0.0;
        } else {
            video.volume = 0.8;
        }
    });


    /* namespace QuoteSwitcher { */ (function () { 
        let currentQuote = 0;
        let quotes = [
            {
                text: "I have been very happy with the friendly and helpful customer services and how fast I have been able to reach the staff without all the automated services unlike some other companies",
                name: "Emma Kerr",
                position: ""
            },
            {
                text: "Been with Eversmart for months now, went from payG to direct debit, no hassle, decent price, would recommend.",
                name: "David Trallis",
                position: "CHELMSFORD"
            },
            {
                text: "This is the best energy company. Their customer services surpassed every other company I have ever used.",
                name: "Mrs Esther",
                position: ""
            },
            {
                text: "Facebook customer service chat was really quick and helpful. Nicola was very informative and went out of her way to send my statement despite the customer portal being down for maintenance.",
                name: "Eleanor",
                position: ""
            }
        ];
        let totalQuotes = quotes.length;

        let fadeQuoteIn = function(quote) {
            $("#landing-testimonial").removeClass("fadeIn");
            $("#landing-testimonial").removeClass("fadeOut");
            $("#landing-testimonial").removeClass("animated");
            $("#landing-testimonial").addClass("animated fadeIn");

            setTimeout( RotateQuotes, 9000);
        }

        let changeQuote = function() {
            let quote = quotes[currentQuote];

            if (quote) {
                $("#landing-testimonial .testimonial-text").text(quote.text);
                $("#landing-testimonial .testimonial-name").text(quote.name);
                $("#landing-testimonial .testimonial-position").text(quote.position);
            }
            else {
                console.error("Quote is undefined at position: " + currentQuote);
            }

            fadeQuoteIn(quote);
        }

        let RotateQuotes = function() {
            currentQuote++;
            if (currentQuote >= totalQuotes) {
                currentQuote = 0;
            }

            $("#landing-testimonial").removeClass("fadeIn");
            $("#landing-testimonial").removeClass("fadeOut");
            $("#landing-testimonial").removeClass("animated");
            $("#landing-testimonial").addClass("animated fadeOut");

            setTimeout( changeQuote, 600);
        }


        window.addEventListener("load", function() {
            RotateQuotes();
        });
    })() /* } */
</script>

<div id="landing-page">
    <section data-type="video-hero">
        <div class="full-width-video">
            <div class="video-overlay"></div>

            <div class="video-overlay-text">
                <h1>Receive <strong>FREE</strong> boiler and heating insurance</h1>
                <p class="subTag">When you switch to our cheap energy throughout May</p>
                <div class="button-wrap">
                    <form class="get-quote-form" method="get" action="/quotation">
                        <input type="text" name="postcode" placeholder="Enter your postcode..." />
                        <button class="btn btn-in-input">Get Quote</button>
                    </form>
                </div>

                <div class="img-wrap text-center">
                    <img class="trustPilot" src="/assets/images/new-world/trustpilot-4star-white.png" alt="" />
                </div>
                <ul class="smallPrint">
                    <li>100% renewable electricity</li>
                    <li>Free boiler cover on selected tariffs</li>
                    <li>Terms & Conditions apply</li> 
                </ul>

                <p class="hidden-xs" style="margin-top: 48px;"><a class="btn btn-eversmart-transparent-outline" href="#"
                        onclick="event.preventDefault(); openBannerVideoFullscreen(); return false;">Watch Video</a></p>
                <p class="hidden-sm hidden-md hidden-lg" style="margin-top: 48px;"><a
                        class="btn btn-eversmart-transparent-outline"
                        href="https://s3.eu-west-2.amazonaws.com/website-cdn-assets/a992155f-7da1-41c4-ba75-8cbf234be975.MP4">Watch
                        Video</a></p>
            </div>
            <div class="hidden-sm hidden-md hidden-lg mobileStill">
            </div>
            <!-- <img class="hidden-sm hidden-md hidden-lg" src="/assets/images/new-world/homepage/videobg.jpg" alt="" style="width: auto; min-height: 530px; object-fit: cover;"/> -->
            <video id="landingvideo" class="hidden-xs" width="100%" height="100%" playsinline autoplay loop
                onloadstart="this.volume=0.0">
                <source
                    src="https://s3.eu-west-2.amazonaws.com/website-cdn-assets/a992155f-7da1-41c4-ba75-8cbf234be975.MP4"
                    type="video/mp4">
                Your browser does not support the video tag.
            </video>
        </div>
    </section>

    <section class="hero hero-fullwidth background-white">
        <div class="hidden-xs layout-50-lg layout-50-md layout-50-sm layout-100-xs text-center">
            <img src="/assets/images/new-world/homepage/engineer.jpg" alt=""
                style="width: 100%; height: 100%; object-fit: cover;" />
        </div>
        <div
            class="layout-50-lg layout-50-md layout-50-sm layout-100-xs hero-padding content-vertical-left-center text-left">
            <h1 class="text-left">Ensure your home is protected all year round!</h1>
            <p class="">
                Switch to Eversmart energy <strong> throughout April</strong> for fair tariff rates, and free boiler
                cover for the next 12 months on our silver package.
            </p>
            <ul class="homeBullets">
                <li>Free cover worth £89</li>
                <li>Alternative accommodation</li>
                <li>Free Smart Meter</li>
            </ul>
            <a href="/quotation" class="btn btn-in-input ctaButton">Get Covered</a>

        </div>

        <div class="hidden-sm hidden-md hidden-lg layout-50-lg layout-50-md layout-50-sm layout-100-xs text-center">
            <img src="/assets/images/new-world/homepage/engineer.jpg" alt=""
                style="width: 100%; height: 100%; object-fit: cover;" />
        </div>
    </section>

    <section id="sponsors-hero" class="hero hero-fullwidth background-grey hero-tall-padding">
        <div class="quoteWrap">
            <blockquote id="which">
                "energy customers getting a good deal"
            </blockquote>
            <blockquote id="mirror" class="active">
                "It's the best energy deal on the market"
            </blockquote>
            <blockquote id="lm">
                "Eversmart Energy: the cheap energy supplier that pays you"
            </blockquote>
        </div>
        <div class="logoWrap" id="quoteSwitch">
            <div class="pressLogo" data-quote="which">
                <img src="/assets/images/new-world/homepage/sponsors/which.png" alt="" />
            </div>
            <div class="pressLogo active" data-quote="mirror">
                <img src="/assets/images/new-world/homepage/sponsors/mirror.png" alt="" />
            </div>
            <div class="pressLogo" data-quote="lm">
                <img src="/assets/images/new-world/homepage/sponsors/love-money.png" alt="" />
            </div>
        </div>
    </section>

    <section class="hero hero-fullwidth">
        <div
            class="layout-50-lg layout-50-md layout-50-sm layout-100-xs background-white hero-padding content-vertical-left-center text-left">
            <h1 class='text-left'>Switch your friends for dividends</h1>
            <p>Receive <strong>£40 cash</strong> everytime you refer a friend and they'll get £10 too!</p>
            <p>Simply head to your <a href="http://portal.eversmartenergy.co.uk" target="_blank"
                    class='contentLink'>Eversmart account</a> and share your unique code.</p>
            <p>Over £200 cheaper than the price gap</p>
            <a href="/quotation" class="btn btn-in-input ctaButton">Get your referral code</a>
        </div>

        <div class="layout-50-lg layout-50-md layout-50-sm layout-100-xs">
            <video autoplay playsinline muted loop poster="/assets/images/new-world/homepage/refer.png"
                style="object-fit: cover; width: 100%; height: 100%;">
                <source
                    src="https://s3.eu-west-2.amazonaws.com/website-cdn-assets/videos/Eversmart+Referral+Bonus+WEBSITE.mp4"
                    type="video/mp4">
                Your browser does not support the video tag.
            </video>
        </div>
    </section>

    <section class="hero hero-fullwidth background-white">
        <div class="layout-50-lg layout-50-md layout-50-sm layout-100-xs text-center">
            <img class="hero-image" src="https://s3.eu-west-2.amazonaws.com/website-cdn-assets/homepage/testimonialbg.jpg" alt=""/>
        </div>
        <div class="layout-50-lg layout-50-md layout-50-sm layout-100-xs hero-padding content-vertical-center">
            <div id="landing-testimonial" class="testimonial-hero" style="width: 100%; display: inline-block;">
                <div class="testimonial-text"></div>
                <div class="testimonial-information">
                    <div class="testimonial-name"></div>
                    <div class="testimonial-position"></div>
                </div>
            </div>
        </div>
    </section>

    <section class="hero hero-fullwidth background-white">
        <div class="layout-50-lg layout-50-md layout-50-sm layout-100-xs hero-padding content-vertical-center">
            <img src="/assets/images/new-world/homepage/windicon.png" alt="" style="width: 64px; height: 64px;" />
            <br />
            <h1 class="text-left">
                100% Renewable Energy Sources
            </h1>
            <p class="text-left">
                Be part of our environmentally conscious family and protect the planet with us. Have peace of mind in
                our 100% certified renewable electricity sources from local wind farms. Building partnerships to
                counterbalance our carbon footprint, aiming at being gas carbon neutral.
            </p>
        </div>
        <div class="layout-50-lg layout-50-md layout-50-sm layout-100-xs text-center">
            <img src="/assets/images/new-world/homepage/windbg.jpg" alt=""
                style="width: 100%; height: 100%; object-fit: cover;" />
        </div>
    </section>

    <section class="hero hero-fullwidth background-red hero-tall-padding">
        <h2 class="text-center">Switching is easy — we do all the hard work for you!</h2>
        <div class="layout-33-lg layout-33-md layout-100-sm layout-100-xs">
            <div class="hero-iconarea">
                <form class="get-quote-form" method="get" action="/quotation">
                    <input type="text" name="postcode" placeholder="Enter your postcode..." />
                    <button class="btn btn-in-input" type="submit">&#8594;</button>
                </form>
            </div>
            <h5>Get a quote</h5>
            <p class="text-center">
                Enter your postcode.
            </p>
        </div>
        <div class="layout-33-lg layout-33-md layout-100-sm layout-100-xs">
            <div class="hero-iconarea">
                <img src="/assets/images/new-world/homepage/pigicon.png" alt="" />

            </div>
            <h5>Sign up</h5>
            <p class="text-center">
                Find out how much you could save with Eversmart.
            </p>
        </div>
        <div class="layout-33-lg layout-33-md layout-100-sm layout-100-xs">
            <div class="hero-iconarea">
                <img src="/assets/images/new-world/homepage/checkicon.png" alt="" />
            </div>
            <h5>You're all done</h5>
            <p class="text-center">
                Sit back and let us take care of the rest.
            </p>
        </div>
    </section>

    <section class="hero hero-fullwidth background-white">
        <div class="layout-50-lg layout-50-md layout-50-sm layout-100-xs text-center hero-padding">
            <img src="/assets/images/new-world/homepage/phone.png" alt=""
                style="width: auto; height: 100%; object-fit: cover;" />
        </div>

        <div
            class="layout-50-lg layout-50-md layout-50-sm layout-100-xs hero-padding hero-padding content-vertical-center">
            <h1 class="text-left">Technology that gives you control and transparency. </h1>
            <p class="text-left">Easy, efficient and enjoyable energy management with our new Eversmart app.</p>
            <p class="text-left">Manage your account online, send us meter readings and pay your bills on the go!</p>
            <a href="/quotation" disabled class="btn btn-in-input ctaButton">Coming soon</a>
        </div>
    </section>



    <!--<section data-type="switching">
        <h2>Switching is easy – we do all the hard work for you!</h2>
        <ul>
            <li>
                <div class="img-wrap">
                    <img src="assets/images/new-world/icons/file-content.svg" alt="Step One" />
                </div>
                <h3>Step 1</h3>
                <h4>Register Your Details</h4>
                <p>Enter your postcode and tell us a little bit about your energy usage.</p>
            </li>
            <li>
                <div class="img-wrap">
                    <img src="assets/images/new-world/icons/like.svg" alt="Step Two" />
                </div>
                <h3>Step 2</h3>
                <h4>Get a quote</h4>
                <p>Find out how much you could save with Eversmart.</p>
            </li>
            <li>
                <div class="img-wrap">
                    <img src="assets/images/new-world/icons/handout.svg" alt="Step Three" />
                </div>
                <h3>Step 3</h3>
                <h4>You're all done </h4>
                <p>Sit back and let us take care of the rest. After 21 days you’ll be part of the Eversmart family.</p>
            </li>
        </ul>
        <div class="button-wrap">
            <a href="/quotation" title="Get Quote">Switch Now</a>
        </div>
    </section>

    <section data-type="switching">
        <h2>Switching is easy – we do all the hard work for you!</h2>
        <ul>
            <li>
                <div class="img-wrap">
                    <img src="assets/images/new-world/icons/file-content.svg" alt="Step One" />
                </div>
                <h3>Step 1</h3>
                <h4>Register Your Details</h4>
                <p>Enter your postcode and tell us a little bit about your energy usage.</p>
            </li>
            <li>
                <div class="img-wrap">
                    <img src="assets/images/new-world/icons/like.svg" alt="Step Two" />
                </div>
                <h3>Step 2</h3>
                <h4>Get a quote</h4>
                <p>Find out how much you could save with Eversmart.</p>
            </li>
            <li>
                <div class="img-wrap">
                    <img src="assets/images/new-world/icons/handout.svg" alt="Step Three" />
                </div>
                <h3>Step 3</h3>
                <h4>You're all done </h4>
                <p>Sit back and let us take care of the rest. After 21 days you’ll be part of the Eversmart family.</p>
            </li>
        </ul>
        <div class="button-wrap">
            <a href="/quotation" title="Get Quote">Switch Now</a>
        </div>
    </section>

    <section data-type="tech">
        <div class="img-wrap">
            <img src="assets/images/new-world/landing-page/all_device_assest.png" alt="Devices We Support" />
        </div>
        <div class="info-wrap">
            <h2>Tech that makes life easier</h2>
            <p>Manage your account online or with the Eversmart app. Send us meter readings and pay your bills on the go!</p>
        </div>
    </section>

    <section data-type="faqs">
        <span>customer Care</span>
        <h2>Guides &amp; FAQs</h2>
        <ul>
            <li>
                <a href="https://consumer.paypoint.com/" title="Where is my nearest paypoint?">
                    <div class="img-wrap">
                        <img src="assets/images/new-world/landing-page/btmimage.png" alt="Nearest Paypoint" />
                    </div>
                    <span>Where is my nearest paypoint?</span>
                </a>
            </li>
            <li>
                <a href="https://www.eversmartenergy.co.uk/index.php/Faqs?m=Switching" title="How long does the switch take?">
                    <div class="img-wrap">
                        <img src="assets/images/new-world/landing-page/btmimageone.png" alt="How long does the switch take?" />
                    </div>
                    <span>How long does the switch take?</span>
                </a>
            </li>
            <li>
                <a href="https://www.eversmartenergy.co.uk/index.php/Faqs?m=Topping%20Up" title="What payment options do I have?">
                    <div class="img-wrap">
                        <img src="assets/images/new-world/landing-page/btmimagetree.png" alt="What payment options do I have?" />
                    </div>
                    <span>What payment options do I have?</span>
                </a>
            </li>
        </ul>
    </section> -->
</div>