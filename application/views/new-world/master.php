<!doctype html>
<html class="no-js" lang="en">

<head>
    <!-- META TAGS -->
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Switching Energy Suppliers Made Easy - Eversmart</title>
    <meta name="description" content="">
    <meta name="keywords" content="youswitch,eversmart,electricity,gas">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=yes">

    <!-- FAVICON -->
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url(); ?>/assets/images/new-world/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url(); ?>/assets/images/new-world/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>/assets/images/new-world/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo base_url(); ?>/assets/images/new-world/favicon/site.webmanifest">
    <link rel="mask-icon" href="<?php echo base_url(); ?>/assets/images/new-world/favicon/safari-pinned-tab.svg" color="#000000">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">

    <!-- FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:500" rel="stylesheet"/>
    <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet"/>

    <!-- CSS FILES -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/hamburgers/1.1.3/hamburgers.min.css"
          integrity="sha256-JPv5igMSqm2ztJxtjiup0q6NMWu7L3MbMn9FqyPgfdo=" crossorigin="anonymous"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/new-world/everywhere/normalize.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/new-world/everywhere/main.css"/>

    <!-- PAGE SPECIFIC -->
    <?php if (isset($stylesheets)) { ?>
        <?php foreach ($stylesheets as $stylesheet) { ?>
<link rel="stylesheet" href="<?php echo $stylesheet; ?>"/>
        <?php } ?>
    <?php } ?>

    <!-- BOOTSTRAP CSS -->
    <?php if (isset($bol_bootstrap_enabled)) { ?>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
              integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
              crossorigin="anonymous">
    <?php } ?>


    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-79295711-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());
        gtag('config', '<?= $this->config->item('universal_id'); ?>');
    </script>
</head>

<body class="<?= isset($bodyClasses) ? $bodyClasses : '' ?>">
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T5RBGJ9" height="0" width="0"
            style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->
<link rel="stylesheet" type="text/css"
      href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.css"/>
<!--[if lte IE 9]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade
    your browser</a> to improve your experience and security.</p>
<![endif]-->

<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T44QPZV" height="0" width="0"
            style="display:none;visibility:hidden"></iframe>
</noscript>

<header id="MAIN-header">
    <?php $this->load->view('new-world/header'); ?>
</header>
<section id="MAIN-content">
    <!-- **********************************
                DYNAMIC BODY VIEW LOADED BELOW
            ********************************** !-->

    <?php $this->load->view($content); ?>

    <!-- **********************************
                DYNAMIC BODY VIEW LOADED END
            ********************************** !-->
    <div id="preloader-wrap">
        <div class="inner">
            <div class="backdrop"></div>
            <div id="background-wrap">
                <div class="x1">
                    <div class="cloud"></div>
                </div>

                <div class="x2">
                    <div class="cloud"></div>
                </div>

                <div class="x3">
                    <div class="cloud"></div>
                </div>

                <div class="x4">
                    <div class="cloud"></div>
                </div>

                <div class="x5">
                    <div class="cloud"></div>
                </div>
            </div>
            <!-- <div class="loader">
                        <div class="box">
                        <div class="spin one"></div>
                        <div class="spin two"></div>
                        <div class="spin three"></div>
                    </div> -->

            <!-- <img id="turbineSvg" src="/assets/images/wind.svg" /> -->
            <div class='turbines'>
                <div class="loading">
                    <h2>Loading</h2>
                </div>
                <div id="t1" class="turbine">
                    <div class="pilot">
                        <div class="prop-container">
                            <div class="prop"></div>
                            <div class="prop"></div>
                            <div class="prop"></div>
                        </div>
                    </div>
                    <div class="pole"></div>
                </div>
                <div id="t2" class="turbine">
                    <div class="pilot">
                        <div class="prop-container">
                            <div class="prop"></div>
                            <div class="prop"></div>
                            <div class="prop"></div>
                        </div>
                    </div>
                    <div class="pole"></div>
                </div>
                <div id="t3" class="turbine">
                    <div class="pilot">
                        <div class="prop-container">
                            <div class="prop"></div>
                            <div class="prop"></div>
                            <div class="prop"></div>
                        </div>
                    </div>
                    <div class="pole"></div>
                </div>
            </div>
        </div>
        <!-- <div class="body">
                        <div class="preloader">
                            <div class="spinner"></div>
                        </div>
                    </div> -->
    </div>
    </div>
    <div id="errors-wrap" class="hidden">
        <a href="#" class="close-all-link">Close All</a>
        <ul>
            <!-- DYNAMICLY LOADED -->
        </ul>
    </div>
    <div id="modal-wrap">
        <a class="backdrop" href="#"></a>
        <div class="body">
            <div class="inner">
                <!-- DYNAMICLY LOADED -->
            </div>
        </div>
    </div>
</section>
<footer id="MAIN-footer">
    <!-- INTERCOM -->
    <script>
        window.intercomSettings = {
            app_id: "qjs70gbf",
            custom_launcher_selector: ".open_intercom"
            <?php if(isset($this->session->userdata('login_data')['intercom_user_id'])){ ?>,
            user_id: "<?=$this->session->userdata('login_data')['intercom_user_id'];?>"
            <?php } ?>
            <?php if(isset($this->session->userdata('login_data')['intercom_id'])){ ?>,
            id: "<?=$this->session->userdata('login_data')['intercom_id'];?>"
            <?php } ?>
            <?php if(isset($this->session->userdata('login_data')['intercom_name'])){ ?>,
            name: "<?=$this->session->userdata('login_data')['intercom_name'];?>"
            <?php } ?>
            <?php if(isset($this->session->userdata('login_data')['intercom_email'])){ ?>,
            email: "<?=$this->session->userdata('login_data')['intercom_email'];?>"
            <?php } ?>
        };
        (function () {
            var w = window;
            var ic = w.Intercom;
            if (typeof ic === "function") {
                ic('reattach_activator');
                ic('update', intercomSettings);
            } else {
                var d = document;
                var i = function () {
                    i.c(arguments)
                };
                i.q = [];
                i.c = function (args) {
                    i.q.push(args)
                };
                w.Intercom = i;

                function l() {
                    var s = d.createElement('script');
                    s.type = 'text/javascript';
                    s.async = true;
                    s.src = 'https://widget.intercom.io/widget/qjs70gbf';
                    var x = d.getElementsByTagName('script')[0];
                    x.parentNode.insertBefore(s, x);
                }

                if (w.attachEvent) {
                    w.attachEvent('onload', l);
                } else {
                    w.addEventListener('load', l, false);
                }
            }
        })()
    </script>
    <?php if (isset($footer_options)) { ?>
        <?php $this->load->view('new-world/footer', $footer_options); ?>
    <?php } else { ?>
        <?php $this->load->view('new-world/footer'); ?>
    <?php }; ?>
</footer>

<!-- START Rakuten Marketing Tracking -->
<script type="text/javascript">
    (function (url) {
        if (!window.DataLayer) {
            window.DataLayer = {};
        }

        if (!DataLayer.events) {
            DataLayer.events = {};
        }

        DataLayer.events.SiteSection = "1";

        var loc, ct = document.createElement("script");
        ct.type = "text/javascript";
        ct.async = true;
        ct.src = url;
        loc = document.getElementsByTagName('script')[0];
        loc.parentNode.insertBefore(ct, loc);
    }(document.location.protocol + "//intljs.rmtag.com/115780.ct.js"));
</script>
<!-- END Rakuten Marketing Tracking -->

<script>
    window.onload = function () {
        videos = document.getElementsByTagName("video");
        window.addEventListener('scroll', checkScroll, false);
        window.addEventListener('resize', checkScroll, false);

        var header = document.getElementById("MAIN-header");

        var sticky = header.offsetTop;

        window.onscroll = function () {
            if (!document.body.classList.contains("menu-open")) {
                if (window.pageYOffset > sticky) {
                    header.classList.add("sticky");
                } else {
                    header.classList.remove("sticky");
                }
            }
        };
    }
</script>

<!-- HELPERS -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/vendors/modernizr-3.6.0.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.js"></script>

<!-- POLYFILLS -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/polyfills/es5-shim.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/polyfills/promise-6.1.0.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/polyfills/fetch.umd.js"></script>

<!-- 3RD PARTY LIBARIES -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/vendors/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/vendors/handlebars-v4.0.12.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/vendors/TimelineMax.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/vendors/TweenMax.min.js"></script>

<!-- HELPERS -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/new-world/everywhere/plugins.js"></script>

<!-- SITEWIDE FUNCTIONALITY -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/new-world/everywhere/menu.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/new-world/everywhere/global.js"></script>

<!-- TRUST PILOT -->
<script type="text/javascript" src="//widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js" async></script>

<!-- Facebook Pixel Code -->
<script>
    !function (f, b, e, v, n, t, s) {
        if (f.fbq) return;
        n = f.fbq = function () {
            n.callMethod ?
                n.callMethod.apply(n, arguments) : n.queue.push(arguments)
        };
        if (!f._fbq) f._fbq = n;
        n.push = n;
        n.loaded = !0;
        n.version = '2.0';
        n.queue = [];
        t = b.createElement(e);
        t.async = !0;
        t.src = v;
        s = b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t, s)
    }(window, document, 'script',
        'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '199077700881076');
    fbq('track', 'PageView');
</script>
<!-- End Facebook Pixel Code -->
<!-- Hotjar Tracking Code for staging.eversmartenergy.com -->
<script>
    (function (h, o, t, j, a, r) {
        h.hj = h.hj || function () {
            (h.hj.q = h.hj.q || []).push(arguments)
        };
        h._hjSettings = {
            hjid: 1282739,
            hjsv: 6
        };
        a = o.getElementsByTagName('head')[0];
        r = o.createElement('script');
        r.async = 1;
        r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
        a.appendChild(r);
    })(window, document, 'https://static.hotjar.com/c/hotjar-', '.js?sv=');
</script>
<script type="text/javascript">
    adroll_adv_id = "HLJA5ZFXUZHGTFLAOUWQOT";
    adroll_pix_id = "S6AXUXZNXBBXNICHNJE2XK";

    let _onload = function () {
        if (document.readyState && !/loaded|complete/.test(document.readyState)) {
            setTimeout(_onload, 10);
            return
        }
        if (!window.__adroll_loaded) {
            __adroll_loaded = true;
            setTimeout(_onload, 50);
            return
        }
        var scr = document.createElement("script");
        var host = (("https:" == document.location.protocol) ? "https://s.adroll.com" :
            "http://a.adroll.com");
        scr.setAttribute('async', 'true');
        scr.type = "text/javascript";
        scr.src = host + "/j/roundtrip.js";
        ((document.getElementsByTagName('head') || [null])[0] ||
            document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
    };
    if (window.addEventListener) {
        window.addEventListener('load', _onload, false);
    } else {
        window.attachEvent('onload', _onload)
    }
</script>
<script>
    var commandQueue = [];
    var cmp = function(command, parameter, callback) {
        commandQueue.push({
            command: command,
            parameter: parameter,
            callback: callback
        });
    };
    cmp.commandQueue = commandQueue;
    cmp.config = {
        layout: "modal",
        digitrust: {redirects: false},
        blockBrowsing: true,
        storePublisherData: false,
        storeConsentGlobally: true,
        logging: false,
        localization: {
            en: {
                intro: {
                    title: 'Welcome to Eversmart!',
                    domain: '',
                    description: 'When you use our site selected companies may access and use information on your device for various purposes including to serve relevant ads or personalised content.',
                    deviceInformationPopover: `
                    <strong>Information that may be used:</strong>
                    <ul>
                        <li>Type of browser and its settings</li>
                        <li>Information about the device's operating system</li>
                        <li>Cookie information</li>
                        <li>Information about other identifiers assigned to the device</li>
                        <li>The IP address from which the device accesses a client's website or mobile application</li>
                        <li>Information about the user's activity on that device, including web pages and mobile apps visited or used</li>
                        <li>Information about the geographic location of the device when it accesses a website or mobile application</li>
                    </ul>
                    `,
                    purposesPopover: `
                    <strong>Purposes for storing information:</strong>
                    <ul>
                        <li>Storage and access of information</li>
                        <li>Ad selection and delivery</li>
                        <li>Content selection and delivery</li>
                        <li>Personalisation</li>
                        <li>Traffic / Event Measurement</li>
                    </ul>
		            `,
                    acceptAll: 'Yes, that\'s fine',
                    showPurposes: 'Who can see my info?'
                },
                details: {
                    title: 'Privacy preferences',
                    back: 'Back',
                    save: 'OK, Continue to site',
                    showVendors: 'Show all companies',
                    enableAll: 'Enable all',
                    disableAll: 'Disable all'
                },
                purposes: {
                    active: 'Active',
                    inactive: 'Inactive',
                    disclaimer: 'We and selected companies may access and use information for the purposes outlined. You may customise your choice or continue using our site if you are OK with the purposes. You can see the ',
                    disclaimerVendorLink: 'complete list of companies here.',
                    showVendors: 'Show companies',
                    hideVendors: 'Hide companies',
                    featureHeader: 'This will include the following features:',
                    company: 'Company'
                },
                vendors: {
                    company: 'Company',
                    offOn: 'Allow',
                    description: 'Companies carefully selected by us will use your information. Depending on the type of data they collect, use, process and other factors, certain companies rely on your consent while others require you to opt-out. For information on each partner and to exercise your choices, see below. Or to opt-out, visit the ',
                    or: 'or ',
                    sites: ' sites.',
                    description2: 'Customise how these companies use data on the ',
                    description2Link: 'previous page.',
                    description3: 'You can control the preferences for all companies by ',
                    description3Link: 'clicking here.'
                },
                footer: {
                    message: 'Read more about access and use of information on your device for various purposes',
                    deviceInformationHeader: 'Information that may be used:',
                    deviceInformation: `
                    <ul>
                        <li>Type of browser and its settings</li>
                        <li>Information about the device's operating system</li>
                        <li>Cookie information</li>
                        <li>Information about other identifiers assigned to the device</li>
                        <li>The IP address from which the device accesses a client's website or mobile application</li>
                        <li>Information about the user's activity on that device, including web pages and mobile apps visited or used</li>
                        <li>Information about the geographic location of the device when it accesses a website or mobile application</li>
                    </ul>
                    `,
                    purposesHeader: 'Purposes for storing information:',
                    purposes: `
                    <ul>
                        <li>Storage and access of information</li>
                        <li>Ad selection and delivery</li>
                        <li>Content selection and delivery</li>
                        <li>Personalisation</li>
                        <li>Measurement</li>
                    </ul>
                    `
                }
            },
        },
        forceLocale: null,
        gdprAppliesGlobally: false,
        repromptOptions: {
            fullConsentGiven: 360,
            someConsentGiven: 30,
            noConsentGiven: 30
        },
        geoIPVendor: 'https://cdn.digitrust.mgr.consensu.org/1/geoip.json',
        testingMode: 'normal',
        showFooterAfterSubmit: true,
        logoUrl: "/assets/images/new-world/logos/logo-2018-red.png",
        css: {
            "color-primary": "rgb(236, 71, 89)",
            "color-secondary": "#eaeaea",
            "color-border": "#eaeaea",
            "color-background": "#ffffff",
            "color-text-primary": "#333333",
            "color-text-secondary": "#0a82be",
            "color-linkColor": "#0a82be",
            "color-table-background": "#f7f7f7",
            "font-family": "'Noto Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif",
            "custom-font-url": "https://fonts.googleapis.com/css?family=Noto+Sans",
        }
    };
    window.__cmp = cmp;
    setTimeout(function () {
        if(window.__cmp) {
            $('.popup_content--2JBXA').find('.intro_acceptAll--23PPA').on('click', function () {
                window.__rmcp = [1, 2, 3, 4, 5];
                console.log('Consent tracking - Rakuten [FIRED]');
            })
        }
    }, 4000);
</script>
<script src = "https://cdn.digitrust.mgr.consensu.org/1/cmp.complete.bundle.js" async> </script>

<!-- PAGE SPECIFIC -->
<?php if (isset($scripts)) { ?>
    <?php foreach ($scripts as $script) { ?>
        <script type="text/javascript" src='<?php echo $script; ?>'></script>
    <?php }; ?>
<?php }; ?>
</body>
</html>