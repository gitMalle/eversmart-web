<section class="hero hero-header container">
    <h1>Terms &amp; Conditions</h1>
</section>

<div id="terms-and-conditions-page-wrap" class="page container">
    <section class="hero hero-fullwidth background-white">
        <a class="btn btn-eversmart-jumbo" href="https://s3.eu-west-2.amazonaws.com/website-cdn-assets/Terms%26Conditions-12thApr.pdf" title="Go to T&C's" target="_blank">
            <div class="wrap">
                <div class="btn-contents">Energy T &amp; C's</div>
            </div>
        </a>
        <a class="btn btn-eversmart-jumbo" href="http://eversm.art/hppolicy" title="Go to T&C's" rel="noopener" target="_blank">
            <div class="wrap">
                <div class="btn-contents">Home Services T &amp; C's</div>
            </div>
        </a>
    </section>
</div>