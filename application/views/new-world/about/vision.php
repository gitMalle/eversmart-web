<section class="hero hero-img">
    <h1>Our Vision</h1>
</section>
<div class="page container">

    <section class="hero hero-fullwidth background-white">
        <div class="layout-50-lg layout-50-md layout-50-sm layout-100-xs text-center">
            <img src="/assets/images/new-world/about/vision-one.png" alt="" style="width: 100%; height: 100%; object-fit: cover;" />
        </div>
        <div class="layout-50-lg layout-50-md layout-50-sm layout-100-xs hero-padding content-vertical-center">
            <br />
            <h3 class="text-left">A family affair from the beginning</h3>
            <p class="text-left">Both as consumers, and people who’ve worked in the sector, we know first-hand what it feels like to be let down by the energy industry.</p>
            <p class="text-left">That’s why our CEO, Barney Cook, set out to repair the relationship between people and their energy suppliers.</p>
            <p class="text-left">Eversmart has been a family affair right from the start and we’re here to protect our customers from high prices and poor service.</p>
            <h2 class="text-left">Ensure your home is protected 365 days a year!</h2>
        </div>
    </section>

    <section class="hero hero-fullwidth background-white">
        <div class="layout-50-lg layout-50-md layout-50-sm layout-100-xs hero-padding content-vertical-center">
            <br />
            <h3 class="text-left">Finding a better way</h3>
            <p class="text-left">We can proudly say that we’re one of the cheapest energy suppliers out there. But great prices are just the start – we want to take better care of people.</p>
            <p class="text-left">That’s because our customers are more than just a number on a piece of paper; they are someone’s mum, dad, brother, sister, son, daughter, nan, grandad or best friend. We promise to take care of them as we would our own family.</p>
        </div>
        <div class="layout-50-lg layout-50-md layout-50-sm layout-100-xs text-center">
            <img src="/assets/images/new-world/about/vision-two.png" alt="" style="width: 100%; height: 100%; object-fit: cover;" />
        </div>
    </section>

    <section class="hero hero-fullwidth background-white">
        <div class="layout-50-lg layout-50-md layout-50-sm layout-100-xs text-center">
            <img src="/assets/images/new-world/about/vision-three.png" alt="" style="width: 100%; height: 100%; object-fit: cover;" />
        </div>
        <div class="layout-50-lg layout-50-md layout-50-sm layout-100-xs hero-padding content-vertical-center">
            <br />
            <h3 class="text-left">Join the family</h3>
            <p class="text-left">Over 40,000 people have already joined the Eversmart family – and we’re committed to giving those people the service and care they deserve.</p>
            <p class="text-left">We’re always looking for new ways to make life a little easier for people. Whether that’s giving you £50 credit when you refer a friend, or adding interest to your credit balance when you pay in advance.</p>
            <p class="text-left">If you’re ready to join us simply give us a call, drop us a line on social media or get a quote online. We can’t wait to hear from you!</p>
        </div>
    </section>
</div>