<section class="hero hero-img">
    <h1>Careers</h1>
</section>

<div id="careers-page-wrap" class="page container">
    <section class="hero hero-fullwidth background-white">
        <div class="links-wrap" style="width: 100%;">
            <h2>We are growing</h2>
            <p>We are a growing organisation with many opportunities. If you would like to join us then please get in touch.</p>
            <div class="layout-50-lg layout-50-md layout-50-sm layout-100-xs text-center" style="width: 50% !important; float: left">
                <a href="/vision" title="Go to Our Vision" class="nav-link">
                    <div class="img-wrap">
                        <img src="/assets/images/new-world/about/vision.png" alt="Eversmart Vision" />
                    </div>
                    <span>Eversmart Vision</span>
                </a>
            </div>
            <div class="layout-50-lg layout-50-md layout-50-sm layout-100-xs text-center" style="width: 50% !important; float: left">
                <a href="/blog/whats-it-like-working-for-eversmart" title="Go to Our Blog" class="nav-link">
                    <div class="img-wrap">
                        <img src="/assets/images/new-world/about/benefits.png" alt="Eversmart Benefits" />
                    </div>
                    <span>Eversmart Benefits</span>
                </a>
            </div>
        </div>
        <br />
    </section>

    <section class="hero hero-fullwidth background-white">
        <div class="layout-50-lg layout-50-md layout-50-sm layout-100-xs hero-padding content-vertical-center">
            <br />
            <h2 class="text-left">Careers at Eversmart</h2>
            <p class="text-left">Are you looking for an exciting new role in a modern, fast-moving company? Then you’ve come to the right place!</p>
            <p class="text-left">Eversmart is on a mission to shake-up the energy sector, and we need the right people to help us get there. If you have the right mixture of skills, flexibility and positive attitude, then we want to hear from you.</p>
        </div>
        <div class="layout-50-lg layout-50-md layout-50-sm layout-100-xs text-center">
            <img src="/assets/images/new-world/about/office.png" alt="" style="width: 100%; height: 100%; object-fit: cover;" />
        </div>
    </section>

    <section class="hero hero-fullwidth background-white">
        <div class="layout-100-lg layout-100-md layout-100-sm layout-100-xs hero-padding content-vertical-center">
            <br />
            <h2>Do we have a role for you?</h2>
            <p>Eversmart is truly revolutionising the energy and smart home industry</p>
            <div id="job-roles-wrap">
                <!-- DYNAMICLY ADDED CONTENT -->
            </div>
        </div>
    </section>
</div>
