<section id="signupSuccess" class="hero hero-img">
    <div id="quotation-page">
        <div id="quote-step-wrap">
            <div id="quote-step">
                <script>
                    fbq('track', 'CompleteRegistration');
                </script>
                <div class="inner" data-type="welcome">

                    <h1>Welcome to the family!<br /> We'll start supplying your home from {supplyStart}</h1>

                    <?php if(isset($ref) && $ref !='undefined' && $ref !='null') { ?>

                        <p>Start earning cash now by referring friends to cheap green energy! Simply share the code, £40 cash per referral</p>

                        <div class='tooltip'>
                            <span class="tooltiptext" id="myTooltip">Click to copy</span>
                            <input type="text" id="referral-link" value="<?=$ref?>" />
                        </div>

                    <?php } ?>

                </div>
            </div>
        </div>
    </div>
</section>
