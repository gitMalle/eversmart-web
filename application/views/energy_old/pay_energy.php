<div class="container">

							<div class="row">

								<div class="col-md-10 mx-auto text-center">
									<h4 class="post_box_heading4">I pay for my energy via?</h4>

								</div>
							</div>

								<div class="switch_box_main">
								<div class="row">
									<span class="back_step"><a href="javascript:void(0)" id="back_address" class="material-icons" style="top:-1px;"><img src="<?php echo base_url() ?>assets/images/red-back-button.svg"/></a> </span>
										<div class="col-md-4 main_card_box">
											<figure class="effect-sadie">
												<figcaption>
													<a href="javascript:void(0)" class="gas_card pay_energy" data-valuepayenergy="prepay">
														<div class="gas_box_icon gap_icon">
															<span class="switch_icon"><img src="<?php echo base_url() ?>assets/images/energy/prepay.svg" alt=""/></span>
														</div>

														<div class="content">
															<span class="gas_box_text dark_heading">
															Prepay</span>
														</div>
														<p class="text-center">If you have a gas meter. your boiler uses Prepay.</p>
														<span class="click_select_edit">SELECT</span>
													</a>

												</figcaption>
											</figure>
										</div><!-------------End Prepay----------------->

									<div class="col-md-4 main_card_box">
											<figure class="effect-sadie">
												<figcaption>
													<a href="javascript:void(0)" class="gas_card pay_energy" data-valuepayenergy="directdebit">
														<div class="gas_box_icon gap_icon">
															<span class="switch_icon"><img src="<?php echo base_url() ?>assets/images/energy/direct-debit-logo-symbol.svg" alt=""/></span>
														</div>

														<div class="content">
															<span class="gas_box_text dark_heading">
															Direct Debit</span>
														</div>
														<p class="text-center">If you have a gas meter. your boiler uses Direct Debit</p>
														<span class="click_select_edit">SELECT</span>

													</a>

												</figcaption>
											</figure>
										</div><!-------------End Debit----------------->

										<div class="col-md-4 main_card_box">
											<figure class="effect-sadie">
												<figcaption>
													<a href="javascript:void(0)" class="gas_card pay_energy" data-valuepayenergy="paperbill">
														<div class="gas_box_icon gap_icon">
															<span class="switch_icon"><img src="<?php echo base_url() ?>assets/images/energy/paper.svg" alt=""/></span>
														</div>

														<div class="content">
															<span class="gas_box_text dark_heading">
															Paper Bill</span>
														</div>
														<p class="text-center">If you have a gas meter. your boiler uses Paper Bill.</p>
														<span class="click_select_edit">SELECT</span>

													</a>

												</figcaption>
											</figure>
										</div><!-------------End Paper Bill----------------->

									</div><!-------------------End Row------------->


								</div><!------------------End switch_box_main----------------->
							</div><!------------------End Container----------------->
