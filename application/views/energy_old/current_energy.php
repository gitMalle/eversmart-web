<div class="container">

							<div class="row">

								<div class="col-md-10 mx-auto text-center">
									<h4 class="post_box_heading4">Do you know your current energy usage ?</h4>

								</div>
							</div>

								<div class="switch_box_main">
								<div class="row justify-content-center">
								<span class="back_step"><a href="javascript:void(0)" class="material-icons backtohearabout" style="top:1px;">
									<img src="<?php echo base_url(); ?>assets/images/red-back-button.svg"/></a></span>

									<div class="col-md-4 main_card_box">
											<figure class="effect-sadie">
												<figcaption>
													<a href="javascript:void(0)" class="gas_card know_current_usage" data-know_current_usage="yes">
														<div class="gas_box_icon gap_icon">
															<span class="switch_icon"><img src="<?php echo base_url(); ?>assets/images/energy/flash.svg" alt=""/></span>
														</div>

														<div class="content">
															<span class="gas_box_text dark_heading">
															Yes, I know</span>
														</div>
														<span class="click_select_edit">SELECT</span>

													</a>

												</figcaption>
											</figure>
										</div><!-------------End flash----------------->


									<div class="col-md-4 main_card_box">
											<figure class="effect-sadie">
												<figcaption>
													<a href="javascript:void(0)" class="gas_card know_current_usage" data-know_current_usage="no">
														<div class="gas_box_icon gap_icon">
															<span class="switch_icon"><img src="<?php echo base_url(); ?>assets/images/energy/ic_help_black_24px.svg" alt=""/></span>
														</div>

														<div class="content">
															<span class="gas_box_text dark_heading">
															 Don't know</span>
														</div>

														<span class="click_select_edit">SELECT</span>
													</a>

												</figcaption>
											</figure>
										</div><!-------------End NO Box----------------->



							</div>
							</div>
							</div>
