<div class="container">

  <div class="row">
    <div class="col-md-10 mx-auto text-center">
    <h4 class="post_box_heading4">Please enter your post code below and a quotation <br>will be provided</h4>

    </div>
  </div>
  <div class="row">
    <div class="col-md-6 mx-auto mtop50">
    <span class="back_step"><a href="<?php echo $back_link; ?>" class="material-icons" style="top:-102px;"><img src="<?php echo base_url(); ?>assets/images/red-back-button.svg"/></a></span>
      <div class="card card-raised2 card-form-horizontal wow fadeInUp" data-wow-delay="0ms" style="border-radius:35px;">
        <div class="content">
          <form id="form-postcode-simple" class="switch-form" name="">
            <div class="row wow fadeInUp" data-wow-delay="400ms" style="padding:12px 0px">
              <div class="form-group postCodeBox" id="rounder-b-energy">

                <div class="input-group">
                  <div class="md-form" id="p-s-code" >
                      <input  id="form-postcode" class="form-control" type="text" placeholder="Postcode:" required>
                      
                    </div>
                  <span class="input-group-btn btnNew">
                  <button id="redd-btn-pc"  class=" red-btn btn btn-md btn-eversmart btn-round weight-300 text-center wow fadeInUp switchButton waves-effect waves-light " type="submit">Get Quote</button>
                  </span>
                </div>
              </div>
            </div>
            <div class="error_msg" style="display:none">
              <div class="alert alert-danger" role="alert" id="error_message"></div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
