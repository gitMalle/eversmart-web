<div class="container">

							<div class="row">

								<div class="col-md-6 mx-auto text-center">
									<h4 class="post_box_heading4">Which of these best describes your home?</h4>

								</div>
							</div>

								<div class="switch_box_main">
								<div class="row">
								<span class="back_step"><a href="javascript:void(0)" class="material-icons backknowusage" style="top:1px;">
									<img src="<?php echo base_url(); ?>assets/images/red-back-button.svg"/></a></span>
										<div class="col-md-3 home_type" data-usage="100" data-interval="month">
											<div class="gas_card ">
												<div class="gas_box_icon">
													<span class="switch_icon"><img src="<?php echo base_url(); ?>assets/images/energy/Detached.svg" alt=""/></span>
												</div>
												<div class="content">
													<span class="gas_box_text">
													<a href="javascript:void()" target="_blank">Detached</a>
													</span>
												</div>
											</div>
										</div>

							<div class="col-md-3 home_type" data-usage="200" data-interval="month">
								<div class="gas_card">
									<div class="gas_box_icon">
										<span class="switch_icon"><img src="<?php echo base_url(); ?>assets/images/energy/Semi.svg" alt=""/></span>
									</div>
									<div class="content">

										<span class="gas_box_text">
                                        <a href="#" target="_blank">Semi Detached</a></span>
									</div>
								</div>
							</div>
							<div class="col-md-3 home_type" data-usage="300" data-interval="month">
								<div class="gas_card">
									<div class="gas_box_icon">
										<span class="switch_icon"><img src="<?php echo base_url(); ?>assets/images/energy/Flats.svg" alt=""/></span>
									</div>
									<div class="content">
										<span class="gas_box_text"><a href="javascript:void()" target="_blank">Flats</a></span>
									</div>
								</div>
							</div>

							<div class="col-md-3 home_type" data-usage="400" data-interval="month">
								<div class="gas_card">
									<div class="gas_box_icon">
										<span class="switch_icon"><img src="<?php echo base_url(); ?>assets/images/energy/Bunglow.svg" alt=""/></span>
									</div>
									<div class="content">
										<span class="gas_box_text"><a href="javascript:void()" target="_blank">Bunglow</a></span>
									</div>
								</div>
							</div>


								</div>


								</div>
							</div>
