<?php require_once 'header.php';
   // debug($profile,1);
 ?>

 
 <div class="col-sm-12">
		<div class="row">
			<div class="col-md-12 ">
				<div class="gas_card">
														
				<div class="gas_box_icon gap_icon_dashboard">
					<span class="switch_icon_dashboard"><img src="<?php echo base_url() ?>images/dashboard/Overview/myaccount-black.svg" alt=""></span>
					<span class="dash-over-text title">Account Info</span>
					<span class="dash-over-text account"> Your Details</span>

				</div>
				
				<form class=" well_box form-horizontal" action=" " method="post"  id="updateCustomerDetail">
					<fieldset class="col-sm-12">

						<div >
							<span class="dash-over-text line"> My Details </span>
						</div>
						
						<div class="form-group row mar-bottom">
							<label for="colFormLabel" class="col-sm-2 col-form-label myaccount-label">First Name</label>
							<div class="col-sm-4 myaccount-input">
								<input type="text" class="form-control input-style" id="colFormLabel" placeholder="<?php echo ucfirst($user_info['forename']); ?>" disabled>
							</div>
							
							<label for="colFormLabel" class="col-sm-2 col-form-label myaccount-label">Last Name</label>
							<div class="col-sm-4 myaccount-input">
								<input type="text" class="form-control input-style" id="colFormLabel" placeholder="<?php echo ucfirst($user_info['surname']); ?>" disabled>
							</div>
						</div>

						<div class="form-group row" style="margin-bottom: 2.3rem;">
							<label for="colFormLabel" class="col-sm-2 col-form-label myaccount-label">Email</label>
							<div class="col-sm-4 myaccount-input">
								<input type="email" class="form-control input-style" id="colFormLabel" placeholder="<?php echo $user_info['email']; ?>" disabled>
							</div>
							
							<label for="phonenum" class="col-sm-2 col-form-label myaccount-label">Phone Number</label>
							<div class="col-sm-4 myaccount-input">

								<input type="text" class="form-control input-style" name="phonenum" id="phonenum" placeholder="<?php echo $user_info['phone1']; ?>" onkeypress="return isNumberKey(event)" value="<?php echo $user_info['phone1']; ?>" maxlength="13" disabled>

								<!---<div class="edit-num-btn" id="edit-num">
									<img src="<?php //echo base_url(); ?>images/pencil.svg" class="edit-num-img editaccount">
									<img src="<?php// echo base_url(); ?>images/dashboard/right-tick.png" class="edit-num-img updateaccount">
									<p class="editaccount">Edit</p>
									<p class="updateaccount">Done</p>
									
								</div>-->
							</div>
						</div>
						
						<div >
							<span class="dash-over-text line"> Address </span>
						</div>

						<div class="form-group row mar-bottom">
							<label for="colFormLabel" class="col-sm-2 col-form-label myaccount-label">Street Address</label>
							<div class="col-sm-10 myaccount-input">
								<input type="text" class="form-control input-style" id="colFormLabel" placeholder="<?php echo ucfirst($user_info['first_line_address']); ?>" disabled>
							</div>
						</div>

						<div class="form-group row mar-bottom">
							<label for="colFormLabel" class="col-sm-2 col-form-label myaccount-label">City<br></label>
							<div class="col-sm-10 myaccount-input">
								<input type="text" class="form-control input-style" id="colFormLabel" placeholder="<?php echo ucfirst($user_info['town_address']); ?>" disabled>
							</div>
						</div>

						<div class="form-group row mar-bottom">
							<label for="colFormLabel" class="col-sm-2 col-form-label myaccount-label">County<br></label>
							<div class="col-sm-10 myaccount-input">
								<input type="text" class="form-control input-style" id="colFormLabel" placeholder="<?php echo $user_info['city_address']; ?>" disabled>
							</div>
						</div>

						<div class="form-group row mar-bottom">
							<label for="colFormLabel" class="col-sm-2 col-form-label myaccount-label">Postcode<br></label>
							<div class="col-sm-10 myaccount-input">
								<input type="text" class="form-control input-style" id="colFormLabel" placeholder="<?php echo $user_info['address_postcode']; ?>" disabled>
							</div>
						</div>

						<input type="submit" style="display: none" name="submit" value="submit_form">
					</fieldset>
				</form>

				<div class="row">
					<div class="col-sm-12 col-md-6 col-lg-6 ">
						<div class="tariff-box">
							<span class="tariff-h-c-p text-center line">Your Tariff</span>
							<table class="table product-table usage-table-tariff"id="tariff-table">

								<tr>
									<td>Tariff:</td>
									<td><?php if( $user_info['tariff_tariffName'] ) { echo $user_info['tariff_tariffName']; }else{ echo 'N/A'; } ?></td>
								</tr>
								<tr>
									<td>Exit Fees:</td>
									<td><?php if( $user_info['exitFeeElec'] ) { echo '&#163;' . $user_info['exitFeeElec']; }else{ echo 'N/A'; } ?></td>
								</tr>
								<tr>
									<td>Estimate annual cost:</td>
									<td><?php if( $user_info['tariff_newspend'] ) { echo '&#163;' . $user_info['tariff_newspend']; }else{ echo 'N/A'; } ?></td>
								</tr>

							</table>

						</div>
					</div>

					<div class="col-sm-12 col-md-6 col-lg-6 ">
						<div class="tariff-box">
							<span class="tariff-h-c-p text-center line">Elec | Gas</span>
								<table class="table product-table usage-table-tariff " id="tariff-table">

									<tr>
										<td>Payment Method:</td>
										<td><?php 
										if( $user_info['billingMethod'] ) 
										{ 
											if($user_info['billingMethod'] == "directdebit")
											{
												echo "Direct Debit";
											}
											else
											{ 
												echo "Prepay";
											} 
										}
										?> </td>
									</tr>

									<tr>
										<td>Rate / Unit</td>
										<td><?php if( $user_info['tariff_unitRate1Elec'] ) { echo $user_info['tariff_unitRate1Elec']; }else{ echo 'N/A'; } ?> p/kwh</td>
									</tr>
									<tr>
										<td>Standing Elec Charge</td>
										<td><?php if( $user_info['tariff_unitRate1Elec'] ) { echo $user_info['tariff_unitRate1Elec']; }else{ echo 'N/A'; } ?> p/kwh</td>
									</tr>

								</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	

       
      
      
	  
      
	  
	  
	  
<?php require_once 'footer.php'; ?>
