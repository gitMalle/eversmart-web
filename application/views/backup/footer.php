<div class="footer">
            <!-- <div class="footerNav">
                <ul>
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Switch Now</a></li>
                    <li><a href="#">Home Service</a></li>
                    <li><a href="#">About</a></li>
                    <li><a href="#">Help</a></li>
                </ul>
            </div> -->
            <div class="copyright"> © Copyright <?= date('Y'); ?>, Eversmart Energy. All Rights Reserved </div>
      </div>
        
    </div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="<?= base_url(); ?>js/jquery-1.11.2.min.js"></script>

<script src="<?= base_url(); ?>js/jquery.validate.min.js"></script>
<script src="<?= base_url(); ?>js/common.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed --> 
<script src="<?= base_url(); ?>js/bootstrap.js"></script>

<script type="text/javascript">
$(document).ready(function() {
	$('.burgerMenu').click( function() {
		$(".leftSide").toggle("slide");
		$(".leftSide").toggleClass("active");
	});
	
	$('.cancelButton').click( function() {
		$(".leftSide").toggle("slide");
		$(".leftSide").toggleClass("active");	
	});
	
	
	$('.dropdownList1').on('click', function() {
		 $('.dropdownList1 .listInner1').toggleClass("collapse1"); 
		 if( $('.clickArrow2').is(':hidden') )
         {
            $('.clickArrow2').css('display', 'inline-block');
            $('.clickArrow1').css('display', 'none'); 
         }
         else
         {
            $('.clickArrow1').css('display', 'inline-block');
            $('.clickArrow2').css('display', 'none');  
         }
	});
	
	$('.dropdownList2').on('click', function() {
		 $('.dropdownList2 .listInner2').toggleClass("collapse1"); 
		 if( $('.clickArrow4').is(':hidden') )
         {
            $('.clickArrow4').css('display', 'inline-block');
            $('.clickArrow3').css('display', 'none'); 
         }
         else
         {
            $('.clickArrow3').css('display', 'inline-block');
            $('.clickArrow4').css('display', 'none');  
         }
	});
	
	$('.dropdownList3').on('click', function() {
		 $('.dropdownList3 .listInner3').toggleClass("collapse1"); 
		 if( $('.clickArrow6').is(':hidden') )
         {
            $('.clickArrow6').css('display', 'inline-block');
            $('.clickArrow5').css('display', 'none'); 
         }
         else
         {
            $('.clickArrow5').css('display', 'inline-block');
            $('.clickArrow6').css('display', 'none');  
         }
	});

});
</script>
</body>
</html>
