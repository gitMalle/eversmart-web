<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Eversmart | Registrations</title>
    <!-- Font material icon -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:500" rel="stylesheet">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="<?php echo base_url(); ?>assets/css/mdb.min.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="<?php echo base_url(); ?>assets/css/responsive.css" rel="stylesheet">
    <!--- Font Family Add---------------->
    <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/aos.css" />
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>assets/img/favicon.png" />
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/prefixfree/1.0.7/prefixfree.min.js"></script>

    <style type="text/css">
        body{font-family: 'Quicksand', sans-serif!important; }
        #redd-btn-pc{padding:18px 29px!important}

        .loading-page {
            margin: 24% auto;
            width: 100px
        }

        .loader {
            border: 8px solid #fda1b8;
            border-radius: 50%;
            border-top: 8px solid #f64d76;
            width: 90px;
            height: 90px;
            -webkit-animation: spin 2s linear infinite; /* Safari */
            animation: spin 2s linear infinite;
        }

        /* Safari */
        @-webkit-keyframes spin {
            0% { -webkit-transform: rotate(0deg); }
            100% { -webkit-transform: rotate(360deg); }
        }

        @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }
        .regis-email-y{ font-size: 17px; color:#aeaeae; padding-bottom:20px; float:left; width:100%;line-height: 1px;}

        .mobileonly.dashbaordmobilemenu {
            position: absolute;
            top: 8px;
            font-size: 45px;
            left: 0px;
            padding: 5px;
            cursor: pointer;
            z-index: 9999;
            color: white;
        }
    </style>


    <script type="text/javascript">

        function validatePassword(Password) {
            var pattern = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$_.!#%*?&~|`¬+=])[A-Za-z\d$@$_.!#%*?&~|`¬+=]{8,}$/;
            return $.trim(Password).match(pattern) ? true : false;
        }

    </script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-79295711-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-79295711-1');
    </script>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-T44QPZV');</script>
    <!-- End Google Tag Manager -->

    <!-- Hotjar Tracking Code for www.eversmartenergy.co.uk -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:1074765,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>

</head>

<div class="cd-panel cd-panel--from-left js-cd-panel-main">
    <header class="cd-panel__header">
        <a class="logo" id="logo-dash" href="<?php echo base_url(); ?>">
            <img class="evrmenu-log" src="<?php echo base_url(); ?>/assets/images/logo-eversmart-new.png"/>
        </a>
        <a href="#0" class="cd-panel__close js-cd-close"> </a>
    </header>

    <div class="cd-panel__container">
        <div class="cd-panel__content">
            <nav id="userslideoutjs" class="user slideout-menu slideout-menu-right">
                <section class="menu-section">
                    <ul class="menu-section-list">

                        <?php if (empty($this->session->userdata('login_data'))) { ?>
                            <li><a href="<?php echo base_url(); ?>index.php/user/login">Login</a></li>
                        <?php } ?>

                        <?php if ($this->session->userdata('login_data')) { ?>
                            <li><a href="<?php echo base_url(); ?>index.php/user/account">My Account</a></li>
                        <?php } ?>
                        <li><a href="<?php echo base_url(); ?>index.php/quotation">Energy</a></li>
                        <!--<li><a href="<?php echo base_url(); ?>index.php/FamilySaver">Family Saver</a></li>-->
                        <li><a href="<?php echo base_url(); ?>index.php/Vision">Our Vision</a></li>
                        <li><a href="<?php echo base_url(); ?>index.php/Career">Careers</a></li>
                        <li><a href="<?php echo base_url(); ?>index.php/Helpfaqs">Help & FAQs</a></li>
                        <li><a href="<?php echo base_url(); ?>index.php/contact_us">Contact us</a></li>
                        <li><a href="<?php echo base_url(); ?>index.php/Policies">Privacy</a></li>
                        <li><a href="<?php echo base_url(); ?>index.php/Terms">T&Cs</a></li>
                        <?php if ($this->session->userdata('login_data')) { ?>
                            <li><a href="#" id="logout_user">Log out</a></li>
                        <?php } ?>
                    </ul>
                </section>
            </nav>
        </div>
    </div>
</div>

<body class="index-page main-page">

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T44QPZV" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->


<div class="loading">
    <div class="finger finger-1">
        <div class="finger-item">
            <span></span><i></i>
        </div>
    </div>
    <div class="finger finger-2">
        <div class="finger-item">
            <span></span><i></i>
        </div>
    </div>
    <div class="finger finger-3">
        <div class="finger-item">
            <span></span><i></i>
        </div>
    </div>
    <div class="finger finger-4">
        <div class="finger-item">
            <span></span><i></i>
        </div>
    </div>
    <div class="last-finger">
        <div class="last-finger-item"><i></i></div>
    </div>
</div>



<!-- Start your project here-->
<div class="cd-main-content" >

    <div class="red-rounded-wave">

        <div class="topbar homemobile">

            <div class="container mobileonly">

                <div class="row">

                        <span class="col-sm-12 col-md-4 col-lg-6 px-0 ">
                            <a class="logo" href="<?php echo base_url(); ?>"><img class="evrmenu" src="<?php echo base_url(); ?>/assets/images/logo-eversmart-new.png"/></a>
                        </span>

                    <div class="col-sm-12 col-md-4 col-lg-4  pull-right no-padding mobile_menu text-right hideonmobile" id="mobile_menu" style="position: relative; float:left; padding-top:10px"></div>

                    <div class="mobileonly dashbaordmobilemenu">
                        <button class="toggle-button-menu-left cd-btn js-cd-panel-trigger" data-panel="main">
                            <img class="responsive_bar_icon" src="<?php echo base_url(); ?>assets/images/menu_bar_responsive.svg" alt=""/>
                        </button>
                    </div>

                </div>

            </div>

            <?php $this->load->view('layout/menu'); ?>

        </div>

        <section class="postcode_top mt0 ">

            <div class="" id="login-section">

                <div class="container">

                    <div class="row">

                        <div class="col-md-6 mx-auto mtop60 posi-rele">

                            <div class="col-md-12 mb-4 pb-5">

                                <div class="card">

                                    <div class="card-body">

                                        <!--Header-->
                                        <?php if ($page === 'reset') { ?>
                                            <span class="loginheading">Reset Password</span>
                                        <?php } else { ?>
                                            <span class="loginheading">Activate Account</span>
                                        <?php } ?>

                                        <span class="regis-email-y text-center">Please enter your new password</span>

                                        <!--Body-->
                                        <form id="reset_form_data">

                                            <div class="login-form">

                                                <div class="md-form">

                                                    <?php if ($error == '0'){
                                                        echo '<input id="user_id" value="' . $user_id . '" name="user_id" class="form-control" type="hidden" >';
                                                        echo '<input id="email" value="' . $email . '" name="email" class="form-control" type="hidden" >';
                                                        echo '<input id="t" value="' . $_REQUEST['t'] . '" name="t" class="form-control" type="hidden" >';
                                                        if (isset($_REQUEST['ev']) && $_REQUEST['ev'] != '') {
                                                            echo '<input id="ev" value="' . $_REQUEST['ev'] . '" name="ev" class="form-control" type="hidden" >';
                                                        }
                                                        else {

                                                            echo '<input id="ev" value="" name="ev" class="form-control" type="hidden" >';
                                                        }
                                                        echo '<input id="page" value="'. $page .'" name="page" class="form-control" type="hidden" >';
                                                    }
                                                    ?>

                                                    <input id="password" name="password" required="" class="form-control" type="password" placeholder="Enter Password">

                                                    <div class="input-group-addon">
                                                        <a href="javascript:void(0)" id="show_password">
                                                            <img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/energy/show-pw-icon.svg">
                                                        </a>
                                                    </div>

                                                </div>

                                                <div class="md-form">

                                                    <input id="confirm_password" name="confirm_password" required="" class="form-control" type="password"  placeholder="Confirm Password">

                                                </div>


                                                <div id="error_msg"></div>


                                                <?php if ($error > 0) { ?>

                                                    <center>
                                                        <div class="alert alert-danger" role="alert">
                                                            <strong>Oops!</strong> <?php echo $msg; ?>
                                                        </div>
                                                    </center>

                                                <?php } ?>

                                                <?php if ($error == '0') { ?>

                                                    <div class="text-center">
                                                        <button style="text-transform:capitalize;font-size: 18px !important;" class="btn btn-default waves-effect waves-light red-btn rounded-login-btn" type="submit">Submit</button>
                                                    </div>

                                                <?php } ?>

                                                <?php if ($error == '2') { ?>
                                                    <span class="forgot-password" style="text-align:center; margin-top:50px"><a href="<?=base_url().'index.php/Reset_one/resetone'?>">Forgotten password?</a></span>
                                                <?php } ?>

                                            </div>

                                        </form>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </section>

    </div>

</div>



<!----------------Start Footer-------------------------->
<script type="text/javascript">
    $(document).ready(function(){
        $("#show_password").on('click', function() {
            //event.preventDefault();
            if($('#password').attr("type") == "text"){
                $('#password').attr('type', 'password');
                $('#show_hide_password i').addClass( "fa-eye-slash" );
                $('#show_hide_password i').removeClass( "fa-eye" );
            }else if($('#password').attr("type") == "password"){
                $('#password').attr('type', 'text');
                $('#show_hide_password i').removeClass( "fa-eye-slash" );
                $('#show_hide_password i').addClass( "fa-eye" );
            }
        });
        $('#reset_form_data').submit(function(e){
            e.preventDefault();

            if( $('#password').val() != $('#confirm_password').val() ){
                $('#error_msg').empty();
                $('#error_msg').html('<center><div class="alert alert-danger" role="alert"><strong>Error!</strong> Password did not match</div></center>');
                return false;
            }

            if( !validatePassword( $('#password').val() ) )
            {
                $('#error_msg').empty();
                $('#error_msg').html('<div class="alert alert-danger" role="alert"><strong>Error!</strong> Passwords must contain the following:<ul><li>Uppercase & lowercase letters</li><li>Numbers</li><li>Symbols</li><li>Eight characters or more</li></ul></div>');
                return false;
            }


            // Are we resetting or activating?


            <?php if ($page === 'reset') { ?>
            var end_point = "<?php echo base_url() ?>index.php/user/customer_reset_password/";
            <?php } else { ?>
            var end_point = "<?php echo base_url() ?>index.php/user/activate_account/";
            <?php } ?>

            $.ajax({
                url: end_point,
                type: 'post',
                dataType: 'json',
                data: $('#reset_form_data').serialize(),
                beforeSend:function(){
                    $('.loading-page').fadeIn();
                    $('#main_content').fadeOut();
                },
                success:function(response){
                    $('.loading-page').fadeOut();
                    $('#main_content').fadeIn();
                    $('#error_msg').empty();
                    console.log(response);
                    if( response.error =='0' ){
                        window.location.href="<?php echo base_url() ?>index.php/user/reset_confirmation";
                    }
                    if( response.error == '1' ){
                        $('#error_msg').html('<div class="alert alert-danger" role="alert"><strong>Error!</strong> '+response.msg+' </div>');
                    }
                }
            });


        });
    });
</script>



<?php $this->load->view('layout/common_footer'); ?>