<?php require_once 'header.php'; ?>

        
        <div class="box">
            <!-- <div class="box1">
            <div class="boxUpper">
                <div class="boxInner"><img src="<?= base_url(); ?>img/electricityIcon.png"></div>
                <div class="text"><b>Electricity</b></br> <?= $this->session->userdata('elec_mpan'); ?></div>
            </div>  
                <div class="greyLine"></div>
                <div class="boxText"><a href="#">View Statement</a></div>
            </div> -->
            
            
            <div class="box1 box2">
            <div class="boxUpper">
                <div class="boxInner"><img src="<?= base_url(); ?>img/gas.svg"></div>
                <div class="text"><b>Gas</b></br><?= $this->session->userdata('gas_mprn'); ?></div>
            </div> 
                <div class="greyLine"></div>
                <div class="boxText" style="visibility: hidden;"><a href="#">View Statement</a></div>
            </div>
            
        </div>
        
        <div class="dashboardTable">
            <div class="imageTop"> <img width="30px" src="<?= base_url(); ?>img/gas.svg"> </div>
            <div class="tableHeading">Gas Bill Statement</div>
            
            <table cellpadding="1" cellspacing="1" width="100%">
                <thead></thead>
                <tr>
                    <th width="30%">Bill Name</th>
                    <th width="55%">Bill Date</th>
                    <th width="15%">Details</th>
                </tr>
                <tr>
                	<td>June Bill</td>
                    <td>06/07/2017</td>
                    <td><img src="<?= base_url(); ?>img/eyeIcon.png"><img src="<?= base_url(); ?>img/downloadIcon.png"></td>
                </tr>
                <tr>
                	<td>May Bill</td>
                    <td>05/07/2017</td>
                    <td><img src="<?= base_url(); ?>img/eyeIcon.png"><img src="<?= base_url(); ?>img/downloadIcon.png"></td>
                </tr>
                 <tr>
                	<td>April Bill</td>
                    <td>04/07/2017</td>
                    <td><img src="<?= base_url(); ?>img/eyeIcon.png"><img src="<?= base_url(); ?>img/downloadIcon.png"></td>
                </tr>
                <tr>
                	<td>March Bill</td>
                    <td>03/07/2017</td>
                    <td><img src="<?= base_url(); ?>img/eyeIcon.png"><img src="<?= base_url(); ?>img/downloadIcon.png"></td>
                </tr>
                <tr>
                	<td>February Bill</td>
                    <td>02/07/2017</td>
                    <td><img src="<?= base_url(); ?>img/eyeIcon.png"><img src="<?= base_url(); ?>img/downloadIcon.png"></td>
                </tr>
                <tr>
                	<td>January Bill</td>
                    <td>01/07/2017</td>
                    <td><img src="<?= base_url(); ?>img/eyeIcon.png"><img src="<?= base_url(); ?>img/downloadIcon.png"></td>
                </tr>
           
            </table>
        </div>
<?php require_once 'footer.php'; ?>
<script type="text/javascript">
	$(document).ready(function()
	{
		$('.dashboard').removeClass('dashboard3');
	});
</script>