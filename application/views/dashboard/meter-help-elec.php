<a href="#" id="back_button" onclick="back(); return false;"><span style="font-size:24px">&#8592;</span> Back</a>

<div class="container meter-img" id="help-index">
    <div class="row">
        <div class="col-sm-12 col-md-4 meter-mob-fix " id="dm-sr" onclick="show_detail('#meter-detail-1'); return false;">
            <img class="meter-images" src="<?php echo base_url(); ?>dashboard/images/dashboard/meter-1-main.png" alt="">
        </div>
        <div class="col-sm-12 col-md-4 meter-mob-fix" id="dm-tr1" onclick="show_detail('#meter-detail-2'); return false;">
            <img class="meter-images" src="<?php echo base_url(); ?>dashboard/images/dashboard/meter-2-main.png" alt="">
        </div>
        <div class="col-sm-12 col-md-4 meter-mob-fix" id="dm-tr2" onclick="show_detail('#meter-detail-3'); return false;">
            <img class="meter-images" src="<?php echo base_url(); ?>dashboard/images/dashboard/meter-3-main.png" alt="">
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-4 meter-mob-fix" id="md-sr" onclick="show_detail('#meter-detail-4'); return false;">
            <img class="meter-images" src="<?php echo base_url(); ?>dashboard/images/dashboard/meter-4-main.png" alt="">
        </div>
        <div class="col-sm-12 col-md-4 meter-mob-fix" id="md-tr" onclick="show_detail('#meter-detail-5'); return false;">
            <img class="meter-images" src="<?php echo base_url(); ?>dashboard/images/dashboard/meter-5-main.png" alt="">
        </div>
        <div class="col-sm-12 col-md-4 meter-mob-fix" id="md-meter" onclick="show_detail('#meter-detail-6'); return false;">
            <img class="meter-images" src="<?php echo base_url(); ?>dashboard/images/dashboard/meter-6-main.png" alt="">
        </div>
    </div>
</div>

<div class="container meter-help" id="meter-detail-1">
    <div class="row">
        <div class="col-md-6">
            <img class="meter-images" src="<?php echo base_url(); ?>dashboard/images/dashboard/meter-1-detail.png" alt="">
        </div>
        <div class="col-md-6 detail">
            <h4>Digital Meter - Single Rate</h4>
            <p>This meter will have the words <b>"single phase"</b> or <b>"single rate"</b> written on it.<br><br>

            Simply write down all the numbers from left to right, including any zeros at the beginning.<br><br></p>
        </div>
    </div>
</div>

<div class="container meter-help" id="meter-detail-2">
    <div class="row">
        <div class="col-md-6">
            <img class="meter-images" src="<?php echo base_url(); ?>dashboard/images/dashboard/meter-2-detail.png" alt="">
        </div>
        <div class="col-md-6 detail">
            <h4>Digital Meter - Two Rate, Single Display</h4>
            <p>This meter will have the words <b>"multi single phase"</b> or <b>"multi rate"</b> written on it.<br><br>

            You will need to take two readings from this meter. Some displays will alternate every few seconds, as indicated by the small 1 or 2 on the left. Some meters will have a button that lets you flip between the two figures.<br><br></p>
        </div>
    </div>
</div>

<div class="container meter-help" id="meter-detail-3">
    <div class="row">
        <div class="col-md-6">
            <img class="meter-images" src="<?php echo base_url(); ?>dashboard/images/dashboard/meter-3-detail.png" alt="">
        </div>
        <div class="col-md-6 detail">
            <h4>Digital Meter - Two Rate, Single Display</h4>
            <p>This meter will have the words <b>"multi single phase"</b> or <b>"multi rate"</b> written on it.<br><br>

            You will need to take two readings from this meter. Press the button to switch between the two figures.<br><br></p>
        </div>
    </div>
</div>

<div class="container meter-help" id="meter-detail-4">
    <div class="row">
        <div class="col-md-6">
            <img class="meter-images" src="<?php echo base_url(); ?>dashboard/images/dashboard/meter-4-detail.png" alt="">
        </div>
        <div class="col-md-6 detail">
            <h4>Mechanical Digital Meter - Single Rate</h4>
            <p>Write down all the numbers from left to right. Include any zeros at beginning, but don't include the red number at the end.<br><br></p>
        </div>
    </div>
</div>

<div class="container meter-help" id="meter-detail-5">
    <div class="row">
        <div class="col-md-6">
            <img class="meter-images" src="<?php echo base_url(); ?>dashboard/images/dashboard/meter-5-detail.png" alt="">
        </div>
        <div class="col-md-6 detail">
            <h4>Mechanical Digital Meter - Two Rate</h4>
            <p>You will need to submit two readings with the type of meter.<br><br>
        
            Write down all the numbers from left to right. Include any zeros at beginning, but don't include the red number at the end.<br><br></p>
        </div>
    </div>
</div>

<div class="container meter-help" id="meter-detail-6">
    <div class="row">
        <div class="col-md-6">
            <img class="meter-images" src="<?php echo base_url(); ?>dashboard/images/dashboard/meter-6-detail.png" alt="">
        </div>
        <div class="col-md-6 detail">
            <h4>Mechanical Dial Meter</h4>
            <p>Write down the number on each dial, including any zeros. You can ignore the red dial at the end.<br>

            <ul>
                <li>If a dial is pointing between two numbers, write down the lowest number.</li><br>
                <li>If the pointer is between 9 and 0, always write down 9.</li><br>
                <li>If the pointer sits directly on top of a number, underline that number.</li><br>
                <li>If an underlined number is followed directly by a 9, you should subtract 1 from the underlined number.</li><br>
            </ul>
        
            <p>Note that some of the dials might go clockwise and some might go anti-clockwise.<br><br></p>
        </div>
    </div>
</div>

<script>

    
    function hide_all_detail()
    {
        $("#back_button").hide();
        $("#help-index").hide();
        $("#meter-detail-1").hide();
        $("#meter-detail-2").hide();
        $("#meter-detail-3").hide();
        $("#meter-detail-4").hide();
        $("#meter-detail-5").hide();
        $("#meter-detail-6").hide();
        $("#meter-detail-gas").hide();
    }

    // show specific detaikl
    function show_detail(id){
        hide_all_detail();
        $('#meter-info').text("Information about your meter");
        $(id).show();
        $('#back_button').show();
        
    }

    // show specific detail
    function back(){
        hide_all_detail();
        $('#help-index').show();
        $('#meter-info').text("Pick the meter that looks most like yours...");
    }


</script>