<script type="text/javascript" src="<?php echo base_url(); ?>dashboard/js/common.js"></script>
<script>
var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the current tab
</script>

<div class="col-sm-12">
    <div class="row">
        <div class="col-md-12 ">
        <a href="#" class="gas_card">
            <div class="gas_box_icon gap_icon_dashboard">
                <span class="switch_icon_dashboard"><img src="<?php echo base_url() ?>dashboard/images/dashboard/Overview/movehome.svg" alt=""></span>
                <span class="dash-over-text">Moving Home</span>

            </div>
        </a>
        </div>

        <div class="col-md-12">
            <div class="gas_card">
            <div class="container-fluid">
                <br /><br />
                <ul class="list-unstyled multi-steps">
                    <li id="1">Start</li>
                    <li id="2" class="is-active">Select Date</li>
                    <li id="3">Enter Readings</li>
                    <li id="4">Enter Address</li>
                    <li id="5">Finish</li>
                </ul>
                </div>
            </div>
        </div>

        <div class="en_select">

            <?php if( $session_data['mpancore'] != 'na' ) { ?>
                    <div class="col-md-6">
                        <a href="#" onclick="return false;" id="select_elec" class="gas_card white-blue">
                            <div class="gas_box_icon gap_icon_dashboard">
                                <span class="switch_icon_dashboard"><img src="<?php echo base_url() ?>dashboard/images/dashboard/Reading/Elec/elec.svg" alt=""></span>
                                <span class="dash-over-text-blue">Electricity</span>
                                <span class="dash-over-read"><?php if($session_data['mpancore'] !=null) { echo $session_data['mpancore']; }else{ echo '-'; } ?></span>

                            </div>
                        </a>

                        <a href="#" onclick="return false;" style="display:none" id="select_gas" class="gas_card white-border">
                            <div class="gas_box_icon gap_icon_dashboard">
                                <span class="switch_icon_dashboard"><img src="<?php echo base_url() ?>dashboard/images/dashboard/Reading/gas/elec-grey.svg" alt=""></span>
                                <span class="dash-over-text-blue light-grey">Electricity</span>
                                <span class="dash-over-read light-grey"><?php if( $session_data['mpancore'] !=null ) { echo $session_data['mpancore']; }else{ echo '-'; } ?></span>

                            </div>
                        </a>
                    </div>
                <?php }
                if( $session_data['user_mprn'] != 'na' ) { ?>
                    <div class="col-md-6">
                        <a href="#" onclick="return false;" id="gas_card" class="gas_card white-border">
                        <div class="gas_box_icon gap_icon_dashboard">
                            <span class="switch_icon_dashboard"><img src="<?php echo base_url() ?>dashboard/images/dashboard/Reading/Elec/gas.svg" alt=""></span>
                            <span class="dash-over-text-blue light-grey">Gas</span>
                            <span class="dash-over-read light-grey"><?php if( $session_data['user_mprn'] !=null ) { echo $session_data['user_mprn']; }else{ echo '-'; } ?></span>

                        </div>
                        </a>
                        <a href="#" onclick="return false;" style="display:none" id="gas_card_selected" class="gas_card white-gold">
                        <div class="gas_box_icon gap_icon_dashboard">
                            <span class="switch_icon_dashboard"><img src="<?php echo base_url() ?>dashboard/images/dashboard/Reading/gas/gas-gold.svg" alt=""></span>
                            <span class="dash-over-text-blue">Gas</span>
                            <span class="dash-over-read"><?php if( $session_data['user_mprn'] !=null ) { echo $session_data['user_mprn']; }else{ echo '-'; } ?></span>

                        </div>
                        </a>
                    </div>
                <?php } ?>

            </div>

        <div class="col-md-12">
            <div class="gas_card">
                <form id="moveHomeForm" action="" method="post">

                    <div class="tab">

                        <span class="dash-over-text fw500" style="padding:30px 0px 20px;text-align:center;">Move out date</span>
                        <span class="tenant-move">Please select the date of your move:</span>

                        <div class=" col-md-10 col-lg-5 col-sm-12 mx-auto ">
                            <input style="border-bottom:1px solid #d8d8d8!important; padding-bottom:10px; background:none;" name="date" id="datepicker" class="move-date-picker" placeholder="DD/MM/YY"/>
                            <div id="error_msg_date"></div>
                        </div>
                    </div>

                    <div class="tab">
                        
                        <div class="col-md-9 col-sm-12 mx-auto ">
                            <div class="gas_box_icon gap_icon_dashboard">
                                <div class="blue-box-top-c move">
                                <span class="dash-over-text account"><span id="heading">Electricity</span> Meter Reading</span>
                                    <span class="meter-re-h-s">
                                    <?php if( $session_data['address'] || $session_data['town_address'] || $session_data['city_address'] || $session_data['address_postcode'] != null ) 
                                    { 
                                        echo $session_data['address'].', '.$session_data['town_address'].', '.$session_data['city_address'].', ';
                                        echo strtoupper($session_data['address_postcode']);
                                    }
                                    else
                                    { 
                                        echo '-';
                                    } ?></span>
                                </div>
                                <div id="main_electricity_card">
                                    <div class="blue-reading-box " id="move-blue-box">
                                        <div class="blue-reading-box-inner text-center">
                                            <span class="w-elec-icon text-center"><img src="<?php echo base_url(); ?>dashboard/images/dashboard/Reading/Elec/elecwite.svg" alt=""></span>
                                            <span class="meter-serial-txt text-center">Meter Serial Number: <?php if( $session_data['elec_meter_serial'] !=null ) { echo $session_data['elec_meter_serial']; }else{ echo 'N/A'; } ?></span>

                                            <ul class="reading-input-class">
                                            <div class="meter">
                                                <li style="margin-left:0px;"><input type="text" name="elec_meter_submit1" onkeypress="return isNumberKey(event)" class="form-control " maxlength="1" placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="Dial_Moving_First" required></></li>

                                                <li><input type="text" name="elec_meter_submit2" onkeypress="return isNumberKey(event)" class="form-control " maxlength="1" max="1" placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="Dial_Moving_Second" required></></li>
                                                    
                                                <li ><input type="text" name="elec_meter_submit3" onkeypress="return isNumberKey(event)" class="form-control " maxlength="1" max="1" placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="Dial_Moving_Third" required></></li>

                                                <li><input type="text" name="elec_meter_submit4" onkeypress="return isNumberKey(event)" class="form-control " maxlength="1" max="1" placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="Dial_Moving_Fourth" required></ ></li>

                                                <li><input type="text" name="elec_meter_submit5" onkeypress="return isNumberKey(event)" class="form-control " maxlength="1" max="1" placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="Dial_Moving_Five" required></></li>

                                                <li><input type="text" name="elec_meter_submit6" onkeypress="return isNumberKey(event)" class="form-control " maxlength="1" max="1" placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="Dial_Moving_Sixth" required></></li>

                                                <li><input type="text" name="elec_meter_submit7" onkeypress="return isNumberKey(event)" class="form-control redzz" maxlength="1" max="1" placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="Dial_Moving_Seventh" required></></li>

                                                <li style="margin-right:0px;"><input type="text" name="elec_meter_submit8" onkeypress="return isNumberKey(event)" required="" class="form-control redzz" maxlength="1" max="1" placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="Dial_Moving_Eight" required></></li>
                                            </div>

                                            </ul>
                                            <input style="display:none"  class="form-control" maxlength="6" placeholder="elec_submit_reds" type="text" name="elec_submit_reds" id="elec_submit_reds">

                                            <span class="meter-serial-txt text-center grey-meter">
											<img src="<?php echo base_url() ?>dashboard/images/dashboard/Reading/Elec/timer.svg" alt="">
											Last Reading: <?php if( !empty($last_reading['cumulative']) ) { echo $last_reading['cumulative']; }else{ echo 'N/A'; } ?>
											</span>
                                        </div>
                                    </div>
                                </div>

                                <div id="main_gas_card" style="display: none">
                                    <div class="blue-reading-box " id="move-blue-box">
                                        <div class="blue-reading-box-inner text-center gold_bg">
											<span class="w-elec-icon text-center"><img src="<?php echo base_url() ?>dashboard/images/dashboard/Reading/gas/gas-white.svg" alt=""></span>
											<span class="meter-serial-txt text-center">Meter Serial Number: <?php if( $session_data['gas_meter_serial'] !=null ) { echo $session_data['gas_meter_serial']; }else{ echo 'N/A'; } ?></span>

                                            <ul class="reading-input-class">
                                            <div class="meter">
                                            
                                                <li style="margin-left:0px;"><input type="text" name="gas_meter_submit1" onkeypress="return isNumberKey(event)" class="form-control " maxlength="1" placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="Dial_Gas_Moving_First"></></li>

                                                <li><input type="text" name="gas_meter_submit2" onkeypress="return isNumberKey(event)" class="form-control " maxlength="1" max="1" placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="Dial_Gas_Moving_Second" required></></li>
                                                    
                                                <li ><input type="text" name="gas_meter_submit3" onkeypress="return isNumberKey(event)" class="form-control " maxlength="1" max="1" placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="Dial_Gas_Moving_Third" required></></li>

                                                <li><input type="text" name="gas_meter_submit4" onkeypress="return isNumberKey(event)" class="form-control " maxlength="1" max="1" placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="Dial_Gas_Moving_Fourth" required></ ></li>

                                                <li><input type="text" name="gas_meter_submit5" onkeypress="return isNumberKey(event)" class="form-control " maxlength="1" max="1" placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="Dial_Gas_Moving_Five" required></></li>

                                                <li><input type="text" name="gas_meter_submit6" onkeypress="return isNumberKey(event)" class="form-control " maxlength="1" max="1" placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="Dial_Gas_Moving_Sixth" required></></li>

                                                <li><input type="text" name="gas_meter_submit7" onkeypress="return isNumberKey(event)" class="form-control redzz" maxlength="1" max="1" placeholder="0" onclick="this.select();" onkeydown="this.value=''"  id="Dial_Gas_Moving_Seventh" required></></li>

                                                <li style="margin-right:0px;"><input type="text" name="gas_meter_submit8" onkeypress="return isNumberKey(event)" required="" class="form-control redzz" maxlength="1" max="1" placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="Dial_Gas_Moving_Eight" required></></li>
                                            </div>

                                            </ul>
                                            <input style="display:none"  class="form-control" maxlength="6" placeholder="gas_submit_reds" type="text" name="gas_submit_reds" id="gas_submit_reds">

                                            <span class="meter-serial-txt text-center grey-meter">
                                            <img src="<?php echo base_url(); ?>dashboard/images/dashboard/Reading/Elec/timer.svg" alt="">
                                            Last Reading: <?php if( !empty($last_reading['cumulative']) ) { echo $last_reading['cumulative']; }else{ echo 'N/A'; } ?>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </div>
                    </div>

                    <div class="tab">
                        
                            <span class="dash-over-text account" style="float: none;position: relative;left: 35%;"> Enter Your New Address</span>
                            <div class="well form-horizontal move">

                            <!-- Address Line 1 
                            <div class="row address">	
                                <div class="form-group address">
                                    <label class="col-md-4 control-label">Address Line 1</label>  
                                    <div class="col-md-4 inputGroupContainer">
                                        <div class="input-group">
                                            <input name="add1" id="add1" class="form-control new" style="text-transform:capitalize"  required>
                                        </div>
                                    </div>
                                </div>
                            </div> -->

                            <div class="form-group row">
                                <label for="colFormLabel" class="col-sm-2 col-form-label myaccount-label text-center">Street Address</label>
                                <div class="col-sm-10 myaccount-input">
                                    <input name="add1" id="add1" class="form-control input-style" style="text-transform:capitalize"  required>  
                                </div>
                            </div>

                            <!-- Address Line 2 
                            <div class="row address">
                                <div class="form-group address">
                                    <label class="col-md-4 control-label">Address Line 2</label>  
                                    <div class="col-md-4 inputGroupContainer">
                                        <div class="input-group">
                                            <input name="add2" id="add2" class="form-control new" style="text-transform:capitalize"  required>		 
                                        </div>
                                    </div>
                                </div>	
                            </div>	-->

                            <div class="form-group row">
                                <label for="colFormLabel" class="col-sm-2 col-form-label myaccount-label text-center">City</label>
                                <div class="col-sm-10 myaccount-input">
                                    <input name="add2" id="add2" class="form-control input-style" style="text-transform:capitalize"  required>  
                                </div>
                            </div>

                            <!-- Address Line 3 
                            <div class="row address">	
                                <div class="form-group address">
                                    <label class="col-md-4 control-label">Address Line 3</label>  
                                    <div class="col-md-4 inputGroupContainer">
                                        <div class="input-group">
                                            <input name="add3" id="add3" class="form-control new" style="text-transform:capitalize" required>
                                        </div>
                                    </div>
                                </div>
                            </div>-->

                            <div class="form-group row">
                                <label for="colFormLabel" class="col-sm-2 col-form-label myaccount-label text-center">County</label>
                                <div class="col-sm-10 myaccount-input">
                                    <input name="add3" id="add3" class="form-control input-style" style="text-transform:capitalize"  required>  
                                </div>
                            </div>

                            <!-- Postcode 
                            <div class="row address">
                                <div class="form-group address">
                                    <label class="col-md-4 control-label">Postcode</label>  
                                    <div class="col-md-4 inputGroupContainer">
                                        <div class="input-group">
                                            <input name="add4" id="add4" class="form-control new" style="text-transform:uppercase" required>		 
                                        </div>
                                    </div>
                                </div>	
                            </div>-->

                            <div class="form-group row">
                                <label for="colFormLabel" class="col-sm-2 col-form-label myaccount-label text-center">Postcode</label>
                                <div class="col-sm-10 myaccount-input">
                                    <input name="add4" id="add4" class="form-control input-style" style="text-transform:uppercase" required>  
                                </div>
                            </div>
                        </div>  
                        <input style="display:none"  class="form-control" maxlength="6" placeholder="new_address" type="text" name="new_address" id="new_address">
                    </div>

                    <div class="tabend">

                        <span class="dash-over-text account"> Thank you for letting us know!</span>
                        <div id="mov_response"></div>
                        <span class="tenant-move">You will be sent a confirmation email.</span>

                    </div>
                                
                    <div id="btns">
                        <div style="clear: both;">
                            <div id="error_msg_elec"></div>
                            <div id="error_msg_gas"></div>
                            <div id="error_msg_add"></div>
                            <button class="get-started-btn-c-btn" style="float:left;cursor:pointer;" type="button" id="prevBtn" onclick="nextPrev(-1)">Back</button>
                            <button class="get-started-btn-c-btn" style="float:right;cursor:pointer;" type="button" id="nextBtn" onclick="validateForm()">Next</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>

<script>
$(document).ready(function () {

    $('#Dial_Moving_First').change(function() {
        $('#elec_submit_reds').val($('#Dial_Moving_First').val() + $('#Dial_Moving_Second').val() + $('#Dial_Moving_Third').val() + $('#Dial_Moving_Fourth').val() + $('#Dial_Moving_Five').val() + $('#Dial_Moving_Sixth').val());
    });

    $('#Dial_Moving_Second').change(function() {
        $('#elec_submit_reds').val($('#Dial_Moving_First').val() + $('#Dial_Moving_Second').val() + $('#Dial_Moving_Third').val() + $('#Dial_Moving_Fourth').val() + $('#Dial_Moving_Five').val() + $('#Dial_Moving_Sixth').val());
    });


    $('#Dial_Moving_Third').change(function() {
        $('#elec_submit_reds').val($('#Dial_Moving_First').val() + $('#Dial_Moving_Second').val() + $('#Dial_Moving_Third').val() + $('#Dial_Moving_Fourth').val() + $('#Dial_Moving_Five').val() + $('#Dial_Moving_Sixth').val());
    });

    $('#Dial_Moving_Fourth').change(function() {
        $('#elec_submit_reds').val($('#Dial_Moving_First').val() + $('#Dial_Moving_Second').val() + $('#Dial_Moving_Third').val() + $('#Dial_Moving_Fourth').val() + $('#Dial_Moving_Five').val() + $('#Dial_Moving_Sixth').val());
    });
    $('#Dial_Moving_Five').change(function() {
        $('#elec_submit_reds').val($('#Dial_Moving_First').val() + $('#Dial_Moving_Second').val() + $('#Dial_Moving_Third').val() + $('#Dial_Moving_Fourth').val() + $('#Dial_Moving_Five').val() + $('#Dial_Moving_Sixth').val());
    });

    $('#Dial_Moving_Sixth').change(function() {
        $('#elec_submit_reds').val($('#Dial_Moving_First').val() + $('#Dial_Moving_Second').val() + $('#Dial_Moving_Third').val() + $('#Dial_Moving_Fourth').val() + $('#Dial_Moving_Five').val() + $('#Dial_Moving_Sixth').val());
    });

    ///gas

    $('#Dial_Gas_Moving_First').change(function() {
        $('#gas_submit_reds').val($('#Dial_Gas_Moving_First').val() + $('#Dial_Gas_Moving_Second').val() + $('#Dial_Gas_Moving_Third').val() + $('#Dial_Gas_Moving_Fourth').val() + $('#Dial_Gas_Moving_Five').val() + $('#Dial_Gas_Moving_Sixth').val());
    });

    $('#Dial_Gas_Moving_Second').change(function() {
        $('#gas_submit_reds').val($('#Dial_Gas_Moving_First').val() + $('#Dial_Gas_Moving_Second').val() + $('#Dial_Gas_Moving_Third').val() + $('#Dial_Gas_Moving_Fourth').val() + $('#Dial_Gas_Moving_Five').val() + $('#Dial_Gas_Moving_Sixth').val());
    });


    $('#Dial_Gas_Moving_Third').change(function() {
        $('#gas_submit_reds').val($('#Dial_Gas_Moving_First').val() + $('#Dial_Gas_Moving_Second').val() + $('#Dial_Gas_Moving_Third').val() + $('#Dial_Gas_Moving_Fourth').val() + $('#Dial_Gas_Moving_Five').val() + $('#Dial_Gas_Moving_Sixth').val());
    });

    $('#Dial_Gas_Moving_Fourth').change(function() {
        $('#gas_submit_reds').val($('#Dial_Gas_Moving_First').val() + $('#Dial_Gas_Moving_Second').val() + $('#Dial_Gas_Moving_Third').val() + $('#Dial_Gas_Moving_Fourth').val() + $('#Dial_Gas_Moving_Five').val() + $('#Dial_Gas_Moving_Sixth').val());
    });
    $('#Dial_Gas_Moving_Five').change(function() {
        $('#gas_submit_reds').val($('#Dial_Gas_Moving_First').val() + $('#Dial_Gas_Moving_Second').val() + $('#Dial_Gas_Moving_Third').val() + $('#Dial_Gas_Moving_Fourth').val() + $('#Dial_Gas_Moving_Five').val() + $('#Dial_Gas_Moving_Sixth').val());
    });

    $('#Dial_Gas_Moving_Sixth').change(function() {
        $('#gas_submit_reds').val($('#Dial_Gas_Moving_First').val() + $('#Dial_Gas_Moving_Second').val() + $('#Dial_Gas_Moving_Third').val() + $('#Dial_Gas_Moving_Fourth').val() + $('#Dial_Gas_Moving_Five').val() + $('#Dial_Gas_Moving_Sixth').val());
    });

    $('#add1').change(function() {
        $('#new_address').val($('#add1').val() + ', ' + $('#add2').val() + ', ' + $('#add3').val() + ', ' + $('#add4').val());
    });
    $('#add2').change(function() {
        $('#new_address').val($('#add1').val() + ', ' + $('#add2').val() + ', ' + $('#add3').val() + ', ' + $('#add4').val());
    });
    $('#add3').change(function() {
        $('#new_address').val($('#add1').val() + ', ' + $('#add2').val() + ', ' + $('#add3').val() + ', ' + $('#add4').val());
    });
    $('#add4').change(function() {
        $('#new_address').val($('#add1').val() + ', ' + $('#add2').val() + ', ' + $('#add3').val() + ', ' + $('#add4').val());
    });
});
</script>