<footer class="dash_footer">
    <div class="container">

        <div class="social-footer-dash text-center">
            <ul class="social-buttons socialButtonHome">
                <li><a class="btn btn-just-icon btn-simple btn-twitter dash" href="https://twitter.com/eversmartenergy" target="_blank"><i class="fa fa-twitter"></i></a></li>
                <li><a class="btn btn-just-icon btn-simple btn-facebook dash" href="https://www.facebook.com/eversmartenergy" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                <li><a class="btn btn-just-icon btn-simple btn-twitter dash" href="https://www.instagram.com/eversmartenergy/" target="_blank"><i class="fa fa-instagram"></i></a></li>

            </ul>
        </div>

        <div class="copyright text-center dash">
                &copy; 2018 Eversmart Energy Ltd - 09310427
            </div>

    </div>

</footer>