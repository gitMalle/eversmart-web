<form action="" id="webToCaseAdmin">
    <input type="hidden" data-webtocase-name="org_id" name="orgid" id="orgid" value="00D0Y000000YWqc">
    <!-- Customer Name -->
    <input id="00N1n00000SB9PS" maxlength="255" data-webtocase-name="name" data-map-name="address_careOf" name="00N1n00000SB9PS" size="20" type="hidden" value="<?=(isset($first_name)?$first_name:'');?> <?=(isset($last_name)?$last_name:'');?>" />
    <!-- Email -->
    <input id="wtc_email" maxlength="80" data-webtocase-name="email" data-map-name="email" name="email" size="20" type="hidden" value="<?=(isset($email)?$email:'');?>" />
    <!-- Phone -->
    <input id="phone" maxlength="40" data-webtocase-name="phone" data-map-name="phone1" name="phone" size="20" type="hidden" value="<?=(isset($phone1)?$phone1:'');?>" />
    <!-- Customer Date of Birth -->
    <input id="00N1n00000SB9PN" maxlength="50" data-webtocase-name="dob" data-map-name="dateOfBirth" name="00N1n00000SB9PN" size="20" type="hidden" value="<?=(isset($dob)?$dob:'')?>" />
    <!-- Address Line 1 -->
    <input id="00N1n00000SB9PI" maxlength="255" data-webtocase-name="address" data-map-name="address_address1" name="00N1n00000SB9PI" size="20" type="hidden" value="<?=(isset($address)?$address:'');?>" />
    <!-- Postcode -->
    <input id="00N1n00000SB9Ph" maxlength="255" data-webtocase-name="postcode" data-map-name="address_postcode" name="00N1n00000SB9Ph" size="20" type="hidden" value="<?=(isset($postcode)?$postcode:'');?>" />
    <!-- Subject -->
    <input id="subject" maxlength="80" data-webtocase-name="subject" data-map-name="subject" name="subject" size="20" type="hidden" value="<?=(isset($subject)?$subject:'');?>" />
    <!-- Description -->
    <input id="description" maxlength="80" data-webtocase-name="description" data-map-name="description" name="description" size="20" type="hidden" value="<?=(isset($description)?$description:'');?>" />
    <!-- Junifer Customer ID -->
    <input id="00N1n00000SB9OK" maxlength="50" data-webtocase-name="junifer_customer_id" data-map-name="junifer_customer_id" name="00N1n00000SB9OK" size="20" type="hidden" value="<?=(isset($junifer_customer_id)?$junifer_customer_id:'');?>" />
    <!-- Dyball Account Number -->
    <input id="00N1n00000SB9NR" maxlength="50" data-webtocase-name="dyball_account_id" data-map-name="dyball_account_id" name="00N1n00000SB9NR" size="20" type="hidden" value="<?=(isset($dyball_account_id)?$dyball_account_id:'');?>" />
    <!-- External System Registration Status -->
    <select id="00N1n00000SB9Pc" data-webtocase-name="external_system_registration_status" data-map-name="status" name="00N1n00000SB9Pc" title="External System Registration Status" style="display:none;">
        <option value="">--None--</option>
        <option <?=(isset($MeterBookingSelected)?$MeterBookingSelected:'')?> value="Meter Booking">Meter Booking</option>
        <option <?=(isset($PendingSelected)?$PendingSelected:'')?>value="Registration Pending">Registration Pending</option>
        <option <?=(isset($SuccessSelected)?$SuccessSelected:'')?>value="Registration Success">Registration Success</option>
        <option <?=(isset($OtherSelected)?$OtherSelected:'')?>value="Other">Other</option>
        <option value="Tariff Switch Success">Tariff Switch Success</option>
        <option value="Tariff Switch Failure">Tariff Switch Failure</option>
        <option value="Tariff Switch Pending">Tariff Switch Pending</option>
    </select>



    <!-- Contact Name - ONLY ACCEPTS SALESFORCE ID -->
    <input id="name" maxlength="80"  name="name" size="20" type="hidden" value=""/>
    <!-- Discounts Electricity -->
    <input id="00N1n00000SakSP" maxlength="255" name="00N1n00000SakSP" size="20" type="hidden" value=""/>
    <!-- Discounts Gas -->
    <input id="00N1n00000SakSU" maxlength="255" name="00N1n00000SakSU" size="20" type="hidden" value=""/>
    <!-- Supply End Date Electricity -->
    <input id="00N1n00000SakSF" maxlength="255" data-webtocase-name="supply_end_date_elec" data-map-name="end_date" name="00N1n00000SakSF" size="20" type="hidden" value="<?=(isset($elec_supply_end_date)?$elec_supply_end_date:'');?>"/>
    <!-- Supply End Date Gas -->
    <input id="00N1n00000SakSK" maxlength="255" data-webtocase-name="supply_end_date_gas" data-map-name="end_date" name="00N1n00000SakSK" size="20" type="hidden" value="<?=(isset($gas_supply_end_date)?$gas_supply_end_date:'');?>"/>
    <!-- Tariff comparison rate Gas -->
    <input id="00N1n00000SajO2" maxlength="255" name="00N1n00000SajO2" size="20" type="hidden" value="<?=(isset($elec_tariff_comparison_rate)?$elec_tariff_comparison_rate:'');?>"/>
    <!-- Tariff comparison rate Electricity -->
    <input id="00N1n00000SajO1" maxlength="255" name="00N1n00000SajO1" size="20" type="hidden" value="<?=(isset($elec_comparison_rate)?$elec_comparison_rate:'');?>"/>
    <!-- Expected supply start date Gas -->
    <input id="00N1n00000SajNw" maxlength="255" data-webtocase-name="expected_supply_start_date_gas" data-map-name="start_date" name="00N1n00000SajNw" size="20" type="hidden" value="<?=(isset($gas_expected_supply_start_date)?$gas_expected_supply_start_date:'');?>"/>
    <!-- Expected supply start date Electricity -->
    <input id="00N1n00000SajNv" maxlength="255" data-webtocase-name="expected_supply_start_date_elec" data-map-name="start_date" name="00N1n00000SajNv" size="20" type="hidden" value="<?=(isset($elec_expected_supply_start_date)?$elec_expected_supply_start_date:'');?>"/>
    <!-- Estimated annual cost Electricity -->
    <input id="00N1n00000SajNt" maxlength="255" data-webtocase-name="estimated_annual_cost_elec" data-map-name="newelec" name="00N1n00000SajNt" size="20" type="hidden" value="<?=(isset($elec_estimated_annual_cost)?$elec_estimated_annual_cost:'');?>"/>
    <!-- Estimated annual cost Gas -->
    <input id="00N1n00000SajNu" maxlength="255" data-webtocase-name="estimated_annual_cost_gas" data-map-name="newgas" name="00N1n00000SajNu" size="20" type="hidden" value="<?=(isset($gas_estimated_annual_cost)?$gas_estimated_annual_cost:'');?>"/>
    <!-- Energy unit rate (p/kwh) Gas -->
    <input id="00N1n00000SajNs" maxlength="255" data-webtocase-name="gas_unit_rate" data-map-name="gas_ur" name="00N1n00000SajNs" size="20" type="hidden" value="<?=(isset($gas_unit_rate)?$gas_unit_rate:'');?>"/>
    <!-- Energy unit rate (p/kwh) Electricity -->
    <input id="00N1n00000SajNr" maxlength="255" data-webtocase-name="elec_unit_rate" data-map-name="elec_ur" name="00N1n00000SajNr" size="20" type="hidden" value="<?=(isset($elec_unit_rate)?$elec_unit_rate:'');?>"/>
    <!-- Daily Standing charge (p/day) Electricity -->
    <input id="00N1n00000SajNp" maxlength="255" data-webtocase-name="daily_standing_charge_elec" data-map-name="elec_sc" name="00N1n00000SajNp" size="20" type="hidden" value="<?=(isset($elec_daily_standing_charge)?$elec_daily_standing_charge:'');?>"/>
    <!-- Daily Standing charge (p/day) Gas -->
    <input id="00N1n00000SajNq" maxlength="255" data-webtocase-name="daily_standing_charge_gas" data-map-name="gas_sc" name="00N1n00000SajNq" size="20" type="hidden" value="<?=(isset($gas_daily_standing_charge)?$gas_daily_standing_charge:'');?>"/>
    <!-- Assumed annual consumption Electricity -->
    <input id="00N1n00000SajNn" maxlength="255" data-webtocase-name="assumed_annual_consumption_elec" data-map-name="eac" name="00N1n00000SajNn" size="20" type="hidden" value="<?=(isset($elec_annual_consumption)?$elec_annual_consumption:'');?>"/>
    <!-- Assumed annual consumption Gas -->
    <input id="00N1n00000SajNo" maxlength="255" data-webtocase-name="assumed_annual_consumption_gas" data-map-name="aq" name="00N1n00000SajNo" size="20" type="hidden" value="<?=(isset($gas_annual_consumption)?$gas_annual_consumption:'');?>"/>
    <!-- Case Record Type -->
    <select id="recordType" data-webtocase-name="record_type" name="recordType" style="display:none;">
        <option value="">--None--</option>
        <option <?=(isset($CustomerIssue)?$CustomerIssue:'')?> value="0120Y000000FfqQ">Customer Issue</option>
        <option <?=(isset($Dyball)?$Dyball:'')?> value="0121n0000007GkE">Dyball</option>
        <option <?=(isset($ECOCase)?$ECOCase:'')?> value="0121n0000007GkF">ECO Case</option>
        <option <?=(isset($InternalCase)?$InternalCase:'')?> value="0121n0000007GkG">Internal Case</option>
        <option <?=(isset($InternalITSupport)?$InternalITSupport:'')?> value="0120Y000000FbCo">Internal IT Support</option>
        <option <?=(isset($NewJuniferAccount)?$NewJuniferAccount:'')?> value="0121n0000007GkH">New Junifer Account</option>
        <option <?=(isset($PrivacyType)?$PrivacyType:'')?> value="0121n0000007GkI">Privacy Type</option>
        <option <?=(isset($UtilityCase)?$UtilityCase:'')?> value="0121n0000007GkJ">Utility Case</option>
    </select>
    <!-- Company - NO COMMERCIAL CLIENTS AT THE MOMENT  -->
    <input id="company" maxlength="80" name="company" size="20" type="hidden" value="<?=(isset($company)?$company:'');?>"/>
    <!-- Case send date & time -->
    <input id="00N1n00000SB9ST" data-webtocase-name="case_send_date" data-map-name="start_date" name="00N1n00000SB9ST" size="20" type="hidden" value="<?=date('d/m/Y');?>"/>




    <!-- Meter Supply Number Electricity -->
    <input id="00N1n00000Saz7d" data-webtocase-name="meter_supply_number_elec" data-map-name="mpancore" name="00N1n00000Saz7d" size="20" type="hidden" value="<?=(isset($meter_supply_number_elec)?$meter_supply_number_elec:'');?>"/>
    <!-- Meter Supply Number Gas -->
    <input id="00N1n00000Saz7e" data-webtocase-name="meter_supply_number_gas" data-map-name="mprns" name="00N1n00000Saz7e" size="20" type="hidden" value="<?=(isset($meter_supply_number_gas)?$meter_supply_number_gas:'');?>"/>

    <!-- Product Name Gas -->
    <input id="00N1n00000Saz7g" data-webtocase-name="product_name_gas" data-map-name="gas_product_code" name="00N1n00000Saz7g" size="20" type="hidden" value="<?=(isset($product_name_gas)?$product_name_gas:'');?>"/>
    <!-- Product Name ELECTRICITY -->
    <input id="00N1n00000Saz7f" data-webtocase-name="product_name_elec" data-map-name="elec_product_code" name="00N1n00000Saz7f" size="20" type="hidden" value="<?=(isset($product_name_elec)?$product_name_elec:'');?>"/>

    <!-- Meter Serial Number Electricity -->
    <input id="00N1n00000Saz7b" data-webtocase-name="meter_serial_number_elec" data-map-name="elecMeterSerial" name="00N1n00000Saz7b" size="20" type="hidden" value="<?=(isset($meter_serial_number_elec)?$meter_serial_number_elec:'');?>"/>
    <!-- Meter Serial Number Gas -->
    <input id="00N1n00000Saz7c" data-webtocase-name="meter_serial_number_gas" data-map-name="gasMeterSerial" name="00N1n00000Saz7c" size="20" type="hidden" value="<?=(isset($meter_serial_number_gas)?$meter_serial_number_gas:'');?>"/>



    <!-- Billing frequency -->
    <input  id="00N1n00000Sb2kW" maxlength="255" data-webtocase-name="billing_frequency" data-map-name="billingMethod" name="00N1n00000Sb2kW" size="20" type="hidden" value="<?=(isset($billing_frequency)?$billing_frequency:'');?>"/>
    <!-- Tariff -->
    <input  id="00N1n00000Sb2k2" maxlength="255" data-webtocase-name="tariff_name" data-map-name="tariff" name="00N1n00000Sb2k2" size="20" type="hidden" value="<?=(isset($tariff_name)?$tariff_name:'');?>"/>
</form>
