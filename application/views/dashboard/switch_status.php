<?php
    // For status output
    function from_camel_case($input) {
        preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $input, $matches);
        $ret = $matches[0];
        foreach ($ret as &$match) {
            $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
        }
        return ucfirst(implode(' ', $ret));
    }
?>
<div class="col-sm-12" >

	<span class="back_step">
        <button  id="back_from" class="material-icons" style="top: 0px;border-width: 0px 1px 1px;border-right-style: solid;border-bottom-style: solid;border-left-style: solid;border-right-color: rgb(220, 228, 230);border-bottom-color: rgb(220, 228, 230);border-left-color: rgb(220, 228, 230);border-image: initial;border-radius: 50%;border-top-style: initial;border-top-color: initial;display: block;padding: 0;cursor: pointer;">
            <img src="<?php echo base_url();?>assets/images/red-back-button.svg">
        </button>
    </span>


    <div class="row">
        <div class="col-md-12 ">
            <div class="gas_box_icon gap_icon_dashboard">
                <span class="gas_card">
                    <h2 class="dash-over-text" style="font-size: 50px;"><strong>Hi <?=$this->session->userdata('login_data')['first_name']; ?>.</strong></h2>

                    <?php if ( $this->session->userdata('login_data')['junifer_account_active'] == 1 ) { ?>

                        <span class="dash-over-text-green" style="text-align:center">Your account is ready - we'll send you to the dashboard</span>

                    <?php } else { ?>

                        <?php if($this->session->userdata('login_data')['elec_status']!='na') { ?>
                            <span class="dash-over-text" style="text-align:center">Your Switch Status is <?=from_camel_case($this->session->userdata('login_data')['elec_status']);?></span>
                        <?php } ?>

                        <span class="dash-over-text-green" style="text-align:center"><?=($this->session->userdata('login_data')['day_left']==1?$this->session->userdata('login_data')['day_left'] . ' Day To Go!':$this->session->userdata('login_data')['day_left'] . ' Days To Go!');?></span>
                    <?php } ?>




                    <?php if( $cooling_off  <= 14 ){ ?>

                        <?php if ( ($this->session->userdata('login_data')['first_reading_elec'] != 'yes') || ($this->session->userdata('login_data')['first_reading_gas'] != 'yes')) { ?>

                            <?php if (($elecMeterPointID !='na') || ($gasMeterPointID !='na') ) {?>
                                <div id="readingsbtn" class="meter-book">
                                        <button id="myelement">Click HERE To Submit Readings</button>
                                    </div>
                            <?php } ?>

                        <?php } ?>

                    <?php } ?>



                    <?php if( $this->session->userdata('login_data')['booked_meter'] <= 1 ) { // Has there been a booking? ?>

                        <div class="meter-book">
    
    
                            <a href="javascript:void(0)" class="book-meter-btn booked_meter">Book a Meter</a>
    
                            <a href="javascript:void(0)" class="book-meter-btn-no booked_meter">Dont want a meter</a>
                        </div>

                    <?php  } ?>



                </span>
            </div>
        </div>
    </div>

    <div class="day_1">

        <div class="col-md-12">
            <div class="gas_card half">

                <div class="row refer-header">
                    <div class="col-12">
                        <h1>Pending rewards</h1>
                        <h2 class="spacetop"><strong><?=$pending;?></strong></h2>
                        <h2 class="spacetop">Refer a friend</h2>
                        <p>Refer Eversmart to your friends. You'll get £50, they'll get £50, each time! Here's your personal link:</p>

                        <div class="referlink" onclick="copylinkss()" onmouseover="document.getElementById('share-tip-s').style.color='#000';" onmouseout="document.getElementById('share-tip-s').style.color='#FFF';">
                            <img src="<?php echo base_url() ?>dashboard/images/refer/link-72.svg" alt=""/>
                            <div class="getthis">
                                <input type="text" class="myshareinput" readonly="readonly" style="font-size: 16px; font-weight:500; width: 80%; text-align: center;" value="<?php echo $esesharelink; ?>" id="myshare">
                                <input type="hidden" class="myshareinput" readonly="readonly" value="<?php echo $sharelink; ?>" id="saas_share">
                            </div>
                            <button onclick="copylinkss()" class="getlinks"><img src="<?php echo base_url() ?>dashboard/images/refer/single-copy-04.svg" alt=""/></button>
                        </div>

                        <div class="share-tip"><span id="share-tip-s" style="display: block; color: #FFF;">Click to copy your personal link!</span>
                        </div>

                        <div class="sharelinkss">
                            <div class="sahretitle">Share your link via:</div>

                            <div class="share emaillink">
                                <img src="<?php echo base_url() ?>dashboard/images/refer/logo-whatsapp-Colored.svg" alt=""/>
                                <a href="<?=$esesharelink ?>"></a>
                            </div>

                            <div class="share facebooklink">
                                <a href="http://www.facebook.com/sharer.php?u=<?=$esesharelink ?>" target="_blank">
                                    <img src="<?php echo base_url() ?>dashboard/images/refer/logo-messenger.svg" alt=""/>
                                </a>
                            </div>

                            <div class="share twitterllink">
                                <a href="https://twitter.com/share?url=<?=$esesharelink ?>" target="_blank">
                                    <img src="<?php echo base_url() ?>dashboard/images/refer/logo-twitter.svg" alt=""/>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="col-md-12">
            <div class="gas_card half right">
                <div class="col-md-10 col-sm-12 mx-auto ">
                    <div class="gas_box_icon gap_icon_dashboard">
                        <span class="progress-bar-step" id="swith-process-bar">
                            <div class="row energy-main-process" id="energy-step-1">


                                <!-- Day 1 -->
                                <div class="mov-step-2-f col-sm-12">
                                    <span class="days-process acc_stat_head acc-day-weight">Day 1</span>
                                    <span class="days-process"><?=date('d/m/Y', strtotime($this->session->userdata('login_data')['create_at']));?></span>
                                        <img class="energy-process-icon" src="<?php echo base_url() ?>dashboard/images/dashboard/done-right.png">
                                    <span class="process-tab-cs acc-status-text-color">Signup</span>
                                </div>

                                <div class="seperator <?=($this->session->userdata('login_data')['current_day'] >= 14 || $this->session->userdata('login_data')['junifer_account_active'] == 1?'green-ln-one':'');?>"></div>


                                <!-- Day 1- 14 -->
                                <div class="text-center col-sm-12">
                                    <span class="days-process acc_stat_head acc-day-weight">Day 1-14</span>
                                    <span class="days-process"><?=date('d/m/Y', strtotime($this->session->userdata('login_data')['create_at']));?><br>- <?=date('d/m/Y', strtotime($this->session->userdata('login_data')['create_at'].'+14 day'));?></span>
                                        <?php if (($this->session->userdata('login_data')['booked_meter'] == 1) || ($this->session->userdata('login_data')['booked_meter'] == 3)) { ?>
                                            <img style="display:none" class="cross energy-process-icon" src="<?php echo base_url() ?>dashboard/images/dashboard/cross.png">
                                            <img style="display:none" class="done_right energy-process-icon" src="<?php echo base_url() ?>dashboard/images/dashboard/done-right.png">
                                            <img class="cross_red energy-process-icon" src="<?php echo base_url() ?>dashboard/images/Group 2612.svg">
                                        <?php } ?>
                                        <?php if ($this->session->userdata('login_data')['booked_meter'] == 2) { ?>
                                            <img style="display:none" class="cross energy-process-icon" src="<?php echo base_url() ?>dashboard/images/dashboard/cross.png">
                                            <img style="display:none" class="cross_red energy-process-icon" src="<?php echo base_url() ?>dashboard/images/Group 2612.svg">
                                            <img class="done_right energy-process-icon" src="<?php echo base_url() ?>dashboard/images/dashboard/done-right.png">
                                        <?php } ?>
                                        <?php if ($this->session->userdata('login_data')['booked_meter'] == 0) { ?>
                                            <?php if ($cooling_off <= 14) { ?>
                                                <img class="cross energy-process-icon" src="<?php echo base_url() ?>dashboard/images/dashboard/Green Tick.svg">
                                            <?php } else { ?>
                                                <img class="cross energy-process-icon" src="<?php echo base_url() ?>dashboard/images/dashboard/cross.png">
                                            <?php } ?>
                                            <img style="display:none" class="done_right energy-process-icon" src="<?php echo base_url() ?>dashboard/images/dashboard/done-right.png">
                                            <img style="display:none" class="cross_red energy-process-icon" src="<?php echo base_url() ?>dashboard/images/Group 2612.svg">
                                        <?php } ?>
                                    <span class="process-tab-cs acc-status-text-color acc-status-text-margin">Book a <br>Smart Meter</span>
                                </div>

                                <div class="seperator  <?=($cooling_off <= 14?'green-ln-two':'');?>"></div>


                                <!-- Day 14 -->
                                <div class="last-home-finish col-sm-12">
                                    <span class="days-process acc_stat_head acc-day-weight">Day 14</span>
                                    <span class="days-process"><?=date('d/m/Y', strtotime($this->session->userdata('login_data')['create_at'].'+14 day'));?></span>
                                        <?php if ($cooling_off <= 14) { ?>
                                            <img class="energy-process-icon" src="<?php echo base_url() ?>dashboard/images/dashboard/done-right.png">
                                        <?php } else { ?>
                                            <img class="energy-process-icon" src="<?php echo base_url() ?>dashboard/images/dashboard/cross.png">
                                        <?php } ?>
                                    <span class="process-tab-cs acc-status-text-color acc-status-text-margin" id="">Cooling off <br>period ends</span>
                                </div>


                                <div class="seperator <?=($this->session->userdata('login_data')['current_day'] >= 16 || $this->session->userdata('login_data')['junifer_account_active'] == 1?'green-ln-three':'');?>"></div>


                                <!-- Day 16 -->
                                <div class="last-home-finish col-sm-12">
                                    <span class="days-process acc_stat_head acc-day-weight">Day 16</span>
                                    <span class="days-process"><?=date('d/m/Y', strtotime($this->session->userdata('login_data')['create_at'].'+16 day'));?></span>
                                    <?php if( ($this->session->userdata('login_data')['first_reading_elec'] === 'yes') && ($this->session->userdata('login_data')['first_reading_gas'] === 'yes')){ ?>
                                        <img  class="energy-process-icon"src="<?php echo base_url() ?>dashboard/images/dashboard/done-right.png">
                                    <?php }else{ ?>
                                        <img class="energy-process-icon hidecompleted" src="<?php echo base_url() ?>dashboard/images/dashboard/cross.png">
                                        <img class="energy-process-icon showcompleted"src="<?php echo base_url() ?>dashboard/images/dashboard/done-right.png">
                                    <?php }  ?>
                                    <span class="process-tab-cs acc-status-text-color acc-status-text-margin" style="margin-top: -27px;" id="">Submit First <br>Meter Readings</span>
                                </div>


                                <div class="seperator <?=($this->session->userdata('login_data')['current_day'] >= 21 || $this->session->userdata('login_data')['junifer_account_active'] == 1?'green-ln-three':'');?>"></div>


                                <!-- Day 21 -->
                                <div class="last-home-finish col-sm-12">
                                    <span class="days-process acc_stat_head acc-day-weight">Day 21</span>
                                    <span class="days-process"><?=date('d/m/Y', strtotime($this->session->userdata('login_data')['create_at'].'+21 day'));?></span>

                                        <?php if ($this->session->userdata('login_data')['current_day'] >= 21 || $this->session->userdata('login_data')['junifer_account_active'] == 1) { ?>
                                            <img class="energy-process-icon" src="<?php echo base_url() ?>dashboard/images/dashboard/done-right.png">
                                        <?php } else { ?>
                                            <img class="energy-process-icon" src="<?php echo base_url() ?>dashboard/images/dashboard/cross.png">
                                        <?php } ?>
                                    <span class="process-tab-cs acc-status-text-color acc-status-text-margin" id="">Account is <br>Live</span>
                                </div>


                            </div>


                            <?php if( $this->session->userdata('login_data')['booked_meter'] <= 1 ) { // Has there been a booking?

                                // Include the form
                                $this->load->view('dashboard/webtocase_form');

                            }  ?>

                        </div>
                    </div>
            </div>
        </div>

    </div>

    <?php if( $cooling_off  <= 14 ){ ?>

        <?php if( $gasMeterPointID != 'na' ) { ?>

            <?php if( $this->session->userdata('login_data')['user_mprn'] != 'na' ) { ?>

                <div class="mobileres" style="display: none; width: 49%; float: left;">

                    <div style="display:block" id="gas_card_selected" class="gas_card white-border <?=(!empty($gas_meter_reading)?'form_opacity':'')?>">

                        <div class="gas_box_icon gap_icon_dashboard">

                            <span class="switch_icon_dashboard"><img id="gas_icon" src="<?php echo base_url() ?>dashboard/images/dashboard/Reading/gas/gas.svg" alt=""></span>
                            <span class="dash-over-text-blue">Gas</span>
                            <span class="dash-over-read">MPRN: <?php if( $this->session->userdata('login_data')['user_mprn'] !=null ) { echo $this->session->userdata('login_data')['user_mprn']; }else{ echo '-'; } ?></span>

                            <form id="submit_reading_meter_gas">

                                <div class="blue-reading-box ">

                                    <div class="blue-reading-box-inner text-center" id="gas_field_wrapper" style="background-color: #CCC;padding-bottom: 35px;">

                                        <span class="w-elec-icon text-center"></span>
                                        <span class="meter-serial-txt text-center">Meter Serial Number:<br> <?php if( $this->session->userdata('login_data')['gas_meter_serial'] !=null ) { echo $this->session->userdata('login_data')['gas_meter_serial']; }else{ echo 'N/A'; } ?></span>

                                        <?php if(empty($gas_meter_reading)){ ?>

                                            <ul class="reading-input-class">

                                                <li style="margin-left:0px;"><input type="text" onkeypress="return isNumberKey(event)" name="gas_meter_submit0" class="form-control " required maxlength="1" placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="Dial_Gas_First"></li>

                                                <li><input type="text" name="gas_meter_submit1" onkeypress="return isNumberKey(event)" class="form-control " maxlength="1" required placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="Dial_Gas_Second"></li>

                                                <li><input type="text" onkeypress="return isNumberKey(event)" name="gas_meter_submit0" class="form-control " maxlength="1" required placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="Dial_Gas_Third"></li>

                                                <li><input type="text" name="gas_meter_submit1" onkeypress="return isNumberKey(event)" class="form-control " maxlength="1" required placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="Dial_Gas_Fourth"></li>

                                                <li><input type="text" name="gas_meter_submit2" onkeypress="return isNumberKey(event)" class="form-control " maxlength="1" required placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="Dial_Gas_Five"></li>

                                                <li><input type="text" name="gas_meter_submit3" onkeypress="return isNumberKey(event)" class="form-control " maxlength="1" required placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="Dial_Gas_Sixth"></li>

                                                <li><input type="text" name="gas_meter_submit4" onkeypress="return isNumberKey(event)" class="form-control redzz" maxlength="1" placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="Dial_Gas_Seventh"></li>

                                                <li style="margin-right:0px;"><input type="text" onkeypress="return isNumberKey(event)" name="gas_meter_submit5" class="form-control redzz" maxlength="1" placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="Dial_Gas_Eight" ></li>

                                            </ul>
                                        <?php } ?>

                                        <input style="display:none"  class="form-control" maxlength="6" placeholder="gas_submit_reds" type="text" name="gas_submit_reds" id="gas_submit_reds">

                                        <input type="hidden" id="gas_signup_type" name="gas_signup_type" value="<?php echo $this->session->userdata('login_data')['signup_type'] ?>">

                                    </div>

                                </div>

                                <div id="gas_button" style="display:inline-block; visibility: hidden">
                                    <?php if(empty($gas_meter_reading)){ ?>
                                        <input class="reading-submit gold_bg" id="submit_reading_meter_gas" type="submit" value="Submit" style="margin-top: 130px;"/>
                                    <?php } ?>
                                </div>


                                <br><br>
                                <div id="error_msg_gas"></div>

                                <a href="#" onclick="meter_reading_info_layer('gas'); return false;">Having trouble reading your gas meter?</a>
                            </form>

                        </div>

                    </div>

                </div>

            <?php } ?>

        <?php } // End if gas meter point not empty ?>

        <?php if( $elecMeterPointID != 'na' ) { ?>

            <?php if( $this->session->userdata('login_data')['mpancore'] != 'na'  && $this->session->userdata('login_data')['meterTypeElec'] != 'E7' ) { ?>

                <div class="mobileres" style="display: none; width: 49%; float: right;">

                    <div id="select_elec" class="gas_card white-blue <?=(!empty($elec_meter_reading)?'form_opacity':'')?>">

                        <div class="gas_box_icon gap_icon_dashboard">

                            <span class="switch_icon_dashboard"><img id="elec_icon" src="<?php echo base_url() ?>dashboard/images/dashboard/Reading/Elec/elec.svg" alt=""></span>
                            <span class="dash-over-text-blue">Electricity</span>
                            <span class="dash-over-read">MPAN: <?php if($this->session->userdata('login_data')['mpancore'] !=null) { echo $this->session->userdata('login_data')['mpancore']; }else{ echo '-'; } ?></span>

                            <form id="submit_reading_meter">

                                <input type="hidden" value="<?php echo $this->session->userdata('login_data')['signup_type'] ?>" name="signupType" id="signupType">

                                <div id="main_electricity_card">

                                    <div class="blue-reading-box ">

                                        <div class="blue-reading-box-inner text-center" id="elec_field_wrapper" style="padding-bottom: 35px;">

                                            <span class="w-elec-icon text-center"></span>
                                            <span class="meter-serial-txt text-center">Meter Serial Number:<br> <?php if( $this->session->userdata('login_data')['elec_meter_serial'] !=null ) { echo $this->session->userdata('login_data')['elec_meter_serial']; }else{ echo 'N/A'; } ?></span>

                                            <?php if(empty($elec_meter_reading)){ ?>
                                                <ul class="reading-input-class">

                                                    <li style="margin-left:0px;"><input type="text" onkeypress="return isNumberKey(event)" name="elec_meter_submit" class="form-control " required maxlength="1" placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="DialFirst" ></li>

                                                    <li><input type="text" name="elec_meter_submit" onkeypress="return isNumberKey(event)" class="form-control "  maxlength="1" required placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="DialSecond" ></li>

                                                    <li><input type="text" onkeypress="return isNumberKey(event)" name="elec_meter_submit" class="form-control " required maxlength="1" placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="DialThird" ></li>

                                                    <li><input type="text" name="elec_meter_submit" onkeypress="return isNumberKey(event)" class="form-control " required maxlength="1" placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="DialFourth" ></li>

                                                    <li><input type="text" name="elec_meter_submit" onkeypress="return isNumberKey(event)" class="form-control " required maxlength="1" placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="DialFive" ></li>

                                                    <li><input type="text" name="elec_meter_submit" onkeypress="return isNumberKey(event)" class="form-control "  required maxlength="1" placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="DialSixth" ></li>

                                                    <li><input type="text" name="elec_meter_submit" onkeypress="return isNumberKey(event)" class="form-control redzz" maxlength="1" placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="DialSeventh" ></li>

                                                    <li style="margin-right:0px;"><input type="text" onkeypress="return isNumberKey(event)" name="elec_meter_submit" class="form-control redzz" maxlength="1" placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="DialEight" ></li>

                                                </ul>
                                            <?php } ?>

                                            <input style="display:none"  class="form-control" maxlength="6" placeholder="elec_submit_reds" type="text" name="elec_submit_reds" id="elec_submit_reds">

                                            <input type="hidden" name="meterIdentifier" value="<?php if( !empty($last_reading['meter']) ) { echo $last_reading['meter']; } ?>">

                                        </div>

                                    </div>

                                    <input type="hidden" name="elec_signup_type" value="<?php echo $this->session->userdata('login_data')['signup_type'] ?>">

                                    <div id="elec_button" style="display:inline-block;">
                                        <?php if(empty($elec_meter_reading)){ ?>
                                            <input class="reading-submit" id="submit_meter" type="submit" value="Submit"/>
                                        <?php } ?>
                                    </div>

                                    <br><br>
                                    <div id="error_msg_elec"></div>

                                    <a href="#" onclick="meter_reading_info_layer('elec'); return false;">Having trouble reading your electricity meter?</a>
                                </div>

                            </form>

                        </div>

                    </div>

                </div>

            <?php } // End if not E7 ?>

            <?php if($this->session->userdata('login_data')['mpancore'] != 'na' && $this->session->userdata('login_data')['meterTypeElec'] === 'E7' ) { ?>

                <div class="mobileres" style="display: none; width: 49%; float: right;">

                    <div id="select_elec" class="gas_card white-blue">

                        <div class="gas_box_icon gap_icon_dashboard">

                            <span class="switch_icon_dashboard"><img id="elec_icon" src="<?php echo base_url() ?>dashboard/images/dashboard/Reading/Elec/elec.svg" alt=""></span>
                            <span class="dash-over-text-blue">Electricity</span>
                            <span class="dash-over-read">MPAN: <?php if($this->session->userdata('login_data')['mpancore'] !=null) { echo $this->session->userdata('login_data')['mpancore']; }else{ echo '-'; } ?></span>

                            <form id="submit_reading_meter">

                                <input type="hidden" value="<?php echo $this->session->userdata('login_data')['signup_type'] ?>" name="signupType" id="signupType">

                                <div id="main_electricity_card">

                                    <div class="blue-reading-box ">

                                        <div class="blue-reading-box-inner text-center" id="elec_field_wrapper" style="padding-bottom:35px;">

                                            <span class="w-elec-icon text-center"></span>
                                            <span class="meter-serial-txt text-center">Meter Serial Number:<br> <?php if( $this->session->userdata('login_data')['elec_meter_serial'] !=null ) { echo $this->session->userdata('login_data')['elec_meter_serial']; }else{ echo 'N/A'; } ?></span>
                                            <?php if(empty($elec_meter_reading)){ ?>
                                                <div class="daynight">Day</div>
                                                <ul class="reading-input-class">

                                                    <li style="margin-left:0px;"><input type="text" onkeypress="return isNumberKey(event)" name="elec_meter_submit" class="form-control " required maxlength="1" placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="DialFirst" ></li>

                                                    <li><input type="text" name="elec_meter_submit" onkeypress="return isNumberKey(event)" class="form-control "  maxlength="1" required placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="DialSecond" ></li>

                                                    <li><input type="text" onkeypress="return isNumberKey(event)" name="elec_meter_submit" class="form-control " required maxlength="1" placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="DialThird" ></li>

                                                    <li><input type="text" name="elec_meter_submit" onkeypress="return isNumberKey(event)" class="form-control " required maxlength="1" placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="DialFourth" ></li>

                                                    <li><input type="text" name="elec_meter_submit" onkeypress="return isNumberKey(event)" class="form-control " required maxlength="1" placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="DialFive" ></li>

                                                    <li><input type="text" name="elec_meter_submit" onkeypress="return isNumberKey(event)" class="form-control "  required maxlength="1" placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="DialSixth" ></li>

                                                    <li><input type="text" name="elec_meter_submit" onkeypress="return isNumberKey(event)" class="form-control redzz" maxlength="1" placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="DialSeventh" ></li>

                                                    <li style="margin-right:0px;"><input type="text" onkeypress="return isNumberKey(event)" name="elec_meter_submit" class="form-control redzz" maxlength="1" placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="DialEight" ></li>

                                                </ul>
                                                <div class="daynight">Night</div>
                                                <ul class="reading-input-class">

                                                    <li style="margin-left:0px;"><input type="text" onkeypress="return isNumberKey(event)" name="elec_meter_submit" class="form-control " required maxlength="1" placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="DialFirst_two" ></li>

                                                    <li><input type="text" name="elec_meter_submit" onkeypress="return isNumberKey(event)" class="form-control "  maxlength="1" required placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="DialSecond_two" ></li>

                                                    <li><input type="text" onkeypress="return isNumberKey(event)" name="elec_meter_submit" class="form-control " required maxlength="1" placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="DialThird_two" ></li>

                                                    <li><input type="text" name="elec_meter_submit" onkeypress="return isNumberKey(event)" class="form-control " required maxlength="1" placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="DialFourth_two" ></li>

                                                    <li><input type="text" name="elec_meter_submit" onkeypress="return isNumberKey(event)" class="form-control " required maxlength="1" placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="DialFive_two" ></li>

                                                    <li><input type="text" name="elec_meter_submit" onkeypress="return isNumberKey(event)" class="form-control "  required maxlength="1" placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="DialSixth_two" ></li>

                                                    <li><input type="text" name="elec_meter_submit" onkeypress="return isNumberKey(event)" class="form-control redzz" maxlength="1" placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="DialSeventh_two" ></li>

                                                    <li style="margin-right:0px;"><input type="text" onkeypress="return isNumberKey(event)" name="elec_meter_submit" class="form-control redzz" maxlength="1" placeholder="0" onclick="this.select();" onkeydown="this.value=''" id="DialEight_two" ></li>

                                                </ul>
                                            <?php } ?>

                                            <input style="display:none"  class="form-control" maxlength="6" placeholder="elec_submit_reds" type="text" name="elec_submit_reds" id="elec_submit_reds">

                                            <input style="display:none"  class="form-control" maxlength="6" placeholder="elec_submit_reds" type="text" name="elec_submit_reds_two" id="elec_submit_reds_two">

                                            <input type="hidden" name="meterIdentifier" value="<?php if( !empty($last_reading['meter']) ) { echo $last_reading['meter']; } ?>">

                                        </div>

                                    </div>

                                    <input type="hidden" name="elec_signup_type" value="<?php echo $this->session->userdata('login_data')['signup_type'] ?>">

                                    <div id="elec_button">
                                        <?php if(empty($elec_meter_reading)){ ?>
                                            <input class="reading-submit" id="submit_meter" type="submit" value="Submit"/>
                                        <?php } ?>
                                    </div>

                                    <br><br>

                                    <div id="error_msg_elec"></div>
                                    <a href="#" onclick="meter_reading_info_layer('elec'); return false;">Having trouble reading your electricity meter?</a>
                                </div>

                            </form>

                        </div>

                    </div>

                </div>

            <?php } // End if E7 ?>

        <?php } // End End if elec meter point not empty ?>

    <?php } // cooling_off ?>

</div>



<div class="booked_smart_meter" style="display: none;">
    <div class="col-md-12">
        <div class="gas_card">
            <div class="col-md-10 col-sm-12 mx-auto ">
                <div class="gas_box_icon gap_icon_dashboard">
                    <span class="progress-bar-step" id="swith-process-bar">
                        <svg class="checkmark" xmlns="" viewBox="0 0 52 52"><circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none"/><path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8"/></svg>
                    </span>
                    <div class="content" style="padding:0px 0px 30px">
                        <div class="col-md-10 mx-auto">
                            <span class="registration-page-heading-energy-H">Thanks ! Your Request <br>to book a meter is received</span>
                            <span class="registration-page-energy-d confirm-pay" style="padding-top:0px!important">We'll contact you within 24 hours on below listed <br> contact number</span>
                            <span class="User-number-eversmart"><?php if (isset($phone1)) { echo $phone1; } ?></span>
                            <span class="dont-have"><a href="#">Don't have access to this number?</a></span>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>



<div class="have_smartmeter" style="display: none; width:100%;">
    <div class="switch_box_main" style="margin-top:0px;">
        <div class="row justify-content-center" id="red-txt-efect">

            <span class="proceed_heading" style="font-size:35px; line-height:42px;">Please let us know why you do not <br>want a smart meter</span>

            <div class="col-md-6 col-sm-12 main_card_box">
                <figure class="effect-sadie">
                    <figcaption>
                        <a href="javascript:void(0)" class="gas_card have_smart_meter booked_meters" data-userid="<?php echo $this->session->userdata('login_data')['id'] ?>"
                           data-book_meter='3'>
                            <div class="gas_box_icon gap_icon">
                                <span class="switch_icon"><img src="<?php echo base_url() ?>dashboard/images/dashboard/yes_status.svg" alt=""></span>
                            </div>
                            <p class="text-center chat-text">I already have a smart meter</p>
                            <span class="click_select_edit">SELECT</span>
                        </a>

                    </figcaption>
                </figure>
            </div><!-------------End NO---------------->

            <div class="col-md-6 col-sm-12 main_card_box">
                <figure class="effect-sadie">
                    <figcaption>
                        <a href="javascript:void(0)" class="gas_card have_smart_meter booked_meters" data-userid="<?php echo $this->session->userdata('login_data')['id'] ?>" data-book_meter='1'>
                            <div class="gas_box_icon gap_icon">
                                <span class="switch_icon"><img src="<?php echo base_url() ?>dashboard/images/dashboard/no_status.svg" alt=""></span>
                            </div>

                            <p class="text-center chat-text">I do not want a smart meter.</p>

                            <span class="click_select_edit">SELECT</span>
                        </a>

                    </figcaption>
                </figure>
            </div><!-------------End yes---------------->

        </div>
    </div>
</div>



<div class="faq" style="display: none;">

    <div class="col-md-12">

        <span class="text-center fre-ques">Frequently Asked Question</span>

        <div id="accordion" class="accordion">
            <div class="mb-0">
                <div class="card-header collapsed" data-toggle="collapse" href="#collapseOne">
                    <a class="card-title">Why should i get smart meter</a>
                </div>
                <div id="collapseOne" class="card-body collapse" data-parent="#accordion">
                    <p>Anim pariatur cliche reprehenderit, enim eiusmod high life
                        accusamus terry richardson ad squid. 3 wolf moon officia
                        aute, non cupidatat skateboard dolor brunch. Food truck
                        quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor,
                        sunt
                        aliqua put a bird on it squid single-origin coffee nulla
                        assumenda shoreditch et. Nihil anim keffiyeh helvetica,
                        craft beer labore wes anderson cred nesciunt sapiente ea
                        proident. Ad vegan excepteur butcher vice lomo. Leggings
                        occaecat
                        craft beer farm-to-table, raw denim aesthetic synth nesciunt
                        you probably haven't heard of them accusamus labore
                        sustainable VHS.
                    </p>
                </div>

            </div>
        </div>


    </div>
</div>



<div class="proccedd" style="display: none">
    <div class="switch_box_main" style="margin-top:0px;">
        <div class="row justify-content-center" id="red-txt-efect">
            <span class="proceed_heading">How do you want to proceed?</span>

            <div class="col-md-6 col-sm-12 main_card_box">
                    <figure class="effect-sadie">
                        <figcaption>
                            <a href="javascript:void(0)" class="gas_card clickmsginter booked_meters closeit" data-userid="<?php echo $this->session->userdata('login_data')['id'] ?>" data-book_meter='2'>
                                <div class="gas_box_icon gap_icon">
                                    <span class="switch_icon"><img src="<?php echo base_url() ?>dashboard/images/dashboard/Chat_status.svg" alt=""></span>
                                </div>
                                <p class="text-center chat-text">Chat with one of our friendly team</p>
                                <span class="click_select_edit">SELECT</span>
                            </a>

                        </figcaption>
                    </figure>
            </div><!-------------End NO---------------->

                <div class="col-md-6 ol-sm-12 main_card_box">
                    <figure class="effect-sadie">
                        <figcaption>
                            <a href="javascript:void(0)" class="gas_card know_current_energy booked_meters call_book_meter" data-userid="<?php echo $this->session->userdata('login_data')['id'] ?>" data-book_meter='2'>
                                <div class="gas_box_icon gap_icon">
                                    <span class="switch_icon"><img src="<?php echo base_url() ?>dashboard/images/dashboard/Call_status.svg" alt=""></span>
                                </div>


                                <p class="text-center chat-text">Request a call back</p>

                                <span class="click_select_edit">SELECT</span>
                            </a>

                        </figcaption>
                    </figure>
            </div><!-------------End yes---------------->

        </div>

    </div>
</div>



<div id="popup-container">
    <div class="popup-background">
        <div class="popup" style="padding:7px 10px; max-width:unset">
        <div class="popup-inner">
            <div class="modal-header" id="contact-pop" style="border-bottom:none">
                <button type="button" class="close" id="close" data-dismiss="slideOut" aria-label="Close" style="left:90%"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="meter-info" style="font-size:28px">Pick the meter that looks most like yours...</h4>
            </div>

            <?php $this->load->view('dashboard/meter-help-elec'); ?>
            <?php $this->load->view('dashboard/meter-help-gas'); ?>

        </div>
        </div>
    </div>
</div>



<style>
.cardss, .hideifboth {display: none;}
button#myelement {background: -moz-linear-gradient(45deg, rgba(252,84,98,1) 0%, rgba(240,70,137,1) 100%);
    background: -webkit-gradient(linear, left bottom, right top, color-stop(0%, rgba(252,84,98,1)), color-stop(100%, rgba(240,70,137,1)));
    background: -webkit-linear-gradient(45deg, rgba(252,84,98,1) 0%, rgba(240,70,137,1) 100%);
    background: -o-linear-gradient(45deg, rgba(252,84,98,1) 0%, rgba(240,70,137,1) 100%);
    background: -ms-linear-gradient(45deg, rgba(252,84,98,1) 0%, rgba(240,70,137,1) 100%);
    background: linear-gradient(45deg, rgba(252,84,98,1) 0%, rgba(240,70,137,1) 100%);
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f04689', endColorstr='#fc5462',GradientType=1 );
    color: #fff;
    font-size: 18px;
    padding: 12px 70px;
    border-radius: 21px;
    margin: auto;
    border: none;
	cursor:pointer;
}
.showcompleted {display:none;}
.energy-process-icon{width:90px; height:auto!important;}
.days-process{color:#1cb81c; font-size:14px; font-weight:bold; width:33%; text-align:right; float:left; margin-bottom:10px; clear: both;}
.dash-over-text-green{color:#1cb81c; font-size:16px; font-weight:bold; width:100%; text-align:right; float:left; }
.energy-main-process{padding:50px 0px; float:left; width:100%;}
.energy-process-icon{ background:#fff; padding:15px;}
.checkmark {width: 56px;height: 56px;border-radius: 50%;display: block;stroke-width: 2;stroke: #fff;stroke-miterlimit: 10;margin: 60px auto; position: relative;}
.User-number-eversmart{font-size:45px; color:#fc5462; font-weight:400;float: left;width: 100%;text-align: center; margin: 30px 0px; }
.dont-have{width:100%; float:left; text-align:center; margin:10px 0px 30px; 0px;}
.dont-have a{color:#3c99e7; font-size:14px; font-weight:400;}
.main_card_box .gas_card{width:100%}
.fre-ques{float:left;font-size:20px; width:100%; margin:23px 0px 10px; color:#666; font-weight:400;}
.accordion{float:left; width:100%;}
.proceed_heading{ font-size:25px; text-align:center; float:left; width:100%; margin:30px 0px; color:#ea495c;}
.chat-text{color:#999!important; font-size:16px!important;}
.acc_stat_head { color: #6e6e6e; margin-top: 20px; font-size:16px; }
.col-12 h1 { text-align: center}
.col-md-12 .right { float: right!important; }
.col-md-12 .right, .col-md-12 .half { height: 714px;width: 49%;float: left;}
.acc-day-weight { font-weight: 900; color: black;}
.acc-status-text-color { font-weight: 550; color: black;}
.acc-status-text-margin { margin-top: -20px;}



</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>

<?php if( $this->session->userdata('login_data')['meterTypeElec'] != 'E7') { ?>

<script type="text/javascript">
    $(document).ready(function () {

    $('#DialFirst').change(function() {
        $('#elec_submit_reds').val($('#DialFirst').val() + $('#DialSecond').val() + $('#DialThird').val() + $('#DialFourth').val() + $('#DialFive').val() + $('#DialSixth').val());
    });

    $('#DialSecond').change(function() {
        $('#elec_submit_reds').val($('#DialFirst').val() + $('#DialSecond').val() + $('#DialThird').val() + $('#DialFourth').val() + $('#DialFive').val() + $('#DialSixth').val());
    });


    $('#DialThird').change(function() {
        $('#elec_submit_reds').val($('#DialFirst').val() + $('#DialSecond').val() + $('#DialThird').val() + $('#DialFourth').val() + $('#DialFive').val() + $('#DialSixth').val());
    });

    $('#DialFourth').change(function() {
        $('#elec_submit_reds').val($('#DialFirst').val() + $('#DialSecond').val() + $('#DialThird').val() + $('#DialFourth').val() + $('#DialFive').val() + $('#DialSixth').val());
    });
    $('#DialFive').change(function() {
        $('#elec_submit_reds').val($('#DialFirst').val() + $('#DialSecond').val() + $('#DialThird').val() + $('#DialFourth').val() + $('#DialFive').val() + $('#DialSixth').val());
    });

    $('#DialSixth').change(function() {
        $('#elec_submit_reds').val($('#DialFirst').val() + $('#DialSecond').val() + $('#DialThird').val() + $('#DialFourth').val() + $('#DialFive').val() + $('#DialSixth').val());
    });

    ///gas

    $('#Dial_Gas_First').change(function() {
        $('#gas_submit_reds').val($('#Dial_Gas_First').val() + $('#Dial_Gas_Second').val() + $('#Dial_Gas_Third').val() + $('#Dial_Gas_Fourth').val() + $('#Dial_Gas_Five').val() + $('#Dial_Gas_Sixth').val());
    });

    $('#Dial_Gas_Second').change(function() {
        $('#gas_submit_reds').val($('#Dial_Gas_First').val() + $('#Dial_Gas_Second').val() + $('#Dial_Gas_Third').val() + $('#Dial_Gas_Fourth').val() + $('#Dial_Gas_Five').val() + $('#Dial_Gas_Sixth').val());
    });


    $('#Dial_Gas_Third').change(function() {
        $('#gas_submit_reds').val($('#Dial_Gas_First').val() + $('#Dial_Gas_Second').val() + $('#Dial_Gas_Third').val() + $('#Dial_Gas_Fourth').val() + $('#Dial_Gas_Five').val() + $('#Dial_Gas_Sixth').val());
    });

    $('#Dial_Gas_Fourth').change(function() {
        $('#gas_submit_reds').val($('#Dial_Gas_First').val() + $('#Dial_Gas_Second').val() + $('#Dial_Gas_Third').val() + $('#Dial_Gas_Fourth').val() + $('#Dial_Gas_Five').val() + $('#Dial_Gas_Sixth').val());
    });
    $('#Dial_Gas_Five').change(function() {
        $('#gas_submit_reds').val($('#Dial_Gas_First').val() + $('#Dial_Gas_Second').val() + $('#Dial_Gas_Third').val() + $('#Dial_Gas_Fourth').val() + $('#Dial_Gas_Five').val() + $('#Dial_Gas_Sixth').val());
    });

    $('#Dial_Gas_Sixth').change(function() {
        $('#gas_submit_reds').val($('#Dial_Gas_First').val() + $('#Dial_Gas_Second').val() + $('#Dial_Gas_Third').val() + $('#Dial_Gas_Fourth').val() + $('#Dial_Gas_Five').val() + $('#Dial_Gas_Sixth').val());
    });

    // close meter_reading_info
    $('#close').click(function(){
        $('#popup-container').addClass('out');
        $('body').removeClass('popup-active');
    });


    });
</script>
	<?php } ?>


    
<?php  if( $this->session->userdata('login_data')['meterTypeElec'] === 'E7') { ?>
<script>
    $(document).ready(function () {

    $('#DialFirst').change(function() {
        $('#elec_submit_reds').val($('#DialFirst').val() + $('#DialSecond').val() + $('#DialThird').val() + $('#DialFourth').val() + $('#DialFive').val() + $('#DialSixth').val());
    });


    $('#DialSecond').change(function() {
        $('#elec_submit_reds').val($('#DialFirst').val() + $('#DialSecond').val() + $('#DialThird').val() + $('#DialFourth').val() + $('#DialFive').val() + $('#DialSixth').val());
    });


    $('#DialThird').change(function() {
        $('#elec_submit_reds').val($('#DialFirst').val() + $('#DialSecond').val() + $('#DialThird').val() + $('#DialFourth').val() + $('#DialFive').val() + $('#DialSixth').val());
    });

    $('#DialFourth').change(function() {
        $('#elec_submit_reds').val($('#DialFirst').val() + $('#DialSecond').val() + $('#DialThird').val() + $('#DialFourth').val() + $('#DialFive').val() + $('#DialSixth').val());
    });
    $('#DialFive').change(function() {
        $('#elec_submit_reds').val($('#DialFirst').val() + $('#DialSecond').val() + $('#DialThird').val() + $('#DialFourth').val() + $('#DialFive').val() + $('#DialSixth').val());
    });

    $('#DialSixth').change(function() {
        $('#elec_submit_reds').val($('#DialFirst').val() + $('#DialSecond').val() + $('#DialThird').val() + $('#DialFourth').val() + $('#DialFive').val() + $('#DialSixth').val());
    });



   $('#DialFirst_two').change(function() {
        $('#elec_submit_reds_two').val($('#DialFirst_two').val() + $('#DialSecond_two').val() + $('#DialThird_two').val() + $('#DialFourth_two').val() + $('#DialFive_two').val() + $('#DialSixth_two').val());
    });

    $('#DialSecond_two').change(function() {
        $('#elec_submit_reds_two').val($('#DialFirst_two').val() + $('#DialSecond_two').val() + $('#DialThird_two').val() + $('#DialFourth_two').val() + $('#DialFive_two').val() + $('#DialSixth_two').val());
    });


    $('#DialThird_two').change(function() {
        $('#elec_submit_reds_two').val($('#DialFirst_two').val() + $('#DialSecond_two').val() + $('#DialThird_two').val() + $('#DialFourth_two').val() + $('#DialFive_two').val() + $('#DialSixth_two').val());
    });

    $('#DialFourth_two').change(function() {
        $('#elec_submit_reds_two').val($('#DialFirst_two').val() + $('#DialSecond_two').val() + $('#DialThird_two').val() + $('#DialFourth_two').val() + $('#DialFive_two').val() + $('#DialSixth_two').val());
    });
    $('#DialFive_two').change(function() {
        $('#elec_submit_reds_two').val($('#DialFirst_two').val() + $('#DialSecond_two').val() + $('#DialThird_two').val() + $('#DialFourth_two').val() + $('#DialFive_two').val() + $('#DialSixth_two').val());
    });

    $('#DialSixth_two').change(function() {
        $('#elec_submit_reds_two').val($('#DialFirst_two').val() + $('#DialSecond_two').val() + $('#DialThird_two').val() + $('#DialFourth_two').val() + $('#DialFive_two').val() + $('#DialSixth_two').val());
    });

    ///gas

    $('#Dial_Gas_First').change(function() {
        $('#gas_submit_reds').val($('#Dial_Gas_First').val() + $('#Dial_Gas_Second').val() + $('#Dial_Gas_Third').val() + $('#Dial_Gas_Fourth').val() + $('#Dial_Gas_Five').val() + $('#Dial_Gas_Sixth').val());
    });

    $('#Dial_Gas_Second').change(function() {
        $('#gas_submit_reds').val($('#Dial_Gas_First').val() + $('#Dial_Gas_Second').val() + $('#Dial_Gas_Third').val() + $('#Dial_Gas_Fourth').val() + $('#Dial_Gas_Five').val() + $('#Dial_Gas_Sixth').val());
    });


    $('#Dial_Gas_Third').change(function() {
        $('#gas_submit_reds').val($('#Dial_Gas_First').val() + $('#Dial_Gas_Second').val() + $('#Dial_Gas_Third').val() + $('#Dial_Gas_Fourth').val() + $('#Dial_Gas_Five').val() + $('#Dial_Gas_Sixth').val());
    });

    $('#Dial_Gas_Fourth').change(function() {
        $('#gas_submit_reds').val($('#Dial_Gas_First').val() + $('#Dial_Gas_Second').val() + $('#Dial_Gas_Third').val() + $('#Dial_Gas_Fourth').val() + $('#Dial_Gas_Five').val() + $('#Dial_Gas_Sixth').val());
    });
    $('#Dial_Gas_Five').change(function() {
        $('#gas_submit_reds').val($('#Dial_Gas_First').val() + $('#Dial_Gas_Second').val() + $('#Dial_Gas_Third').val() + $('#Dial_Gas_Fourth').val() + $('#Dial_Gas_Five').val() + $('#Dial_Gas_Sixth').val());
    });

    $('#Dial_Gas_Sixth').change(function() {
        $('#gas_submit_reds').val($('#Dial_Gas_First').val() + $('#Dial_Gas_Second').val() + $('#Dial_Gas_Third').val() + $('#Dial_Gas_Fourth').val() + $('#Dial_Gas_Five').val() + $('#Dial_Gas_Sixth').val());
    });


    });
</script>
	<?php } ?>

<script>


$('#back_from').css('display', 'none');

$( "#myelement" ).click(function() {     
   $('.mobileres').css('display','block');
   $('.cardss').css('display','block');
   $('.hideifboth').css('display','block');
   $('.day_1').css('display', 'none');
   $('#back_from').css('display', 'block');
   $('html, body').animate({
    scrollTop: ($('.back_step').offset().top - 20)
},500);
});

$( "a.book-meter-btn" ).click(function() {     
   $('.proccedd').css('display','block');
   $('.day_1').css('display', 'none');
   $('#back_from').css('display', 'block');
   $('.have_smartmeter').css('display', 'none');
   $('html, body').animate({
    scrollTop: ($('.back_step').offset().top - 20)
},500);
});

$( "#back_from" ).click(function() {     
   $('.mobileres').css('display','none');
   $('.cardss').css('display','none');
   $('.hideifboth').css('display','none');
   $('.day_1').css('display', 'block');
   $('#back_from').css('display', 'none');
   $('.proccedd').css('display', 'none');
   $('.booked_smart_meter').css('display', 'none');
   $('.have_smartmeter').css('display', 'none');
   $('html, body').animate({
    scrollTop: ($('.back_step').offset().top - 20)
},500);
});

$( ".call_book_meter" ).click(function() {     
   $('.proccedd').css('display','none');
   $('.day_1').css('display', 'none');
   $('#back_from').css('display', 'block');
   $('.booked_smart_meter').css('display', 'block');
   $('.meter-book.row').css('display', 'none');
   $('html, body').animate({
    scrollTop: ($('.back_step').offset().top - 20)
},500);
});

$( ".book-meter-btn-no" ).click(function() {     
   $('.proccedd').css('display','none');
   $('.day_1').css('display', 'none');
   $('#back_from').css('display', 'block');
   $('.booked_smart_meter').css('display', 'none');
   $('.have_smartmeter').css('display', 'block');
   $('.meter-book.row').css('display', 'none');
   $('html, body').animate({
    scrollTop: ($('.back_step').offset().top - 20)
},500);
});

$( ".have_smart_meter" ).click(function() {     
   $('.proccedd').css('display','none');
   $('.day_1').css('display', 'block');
   $('#back_from').css('display', 'block');
   $('.booked_smart_meter').css('display', 'none');
   $('.have_smartmeter').css('display', 'none');
   $('.meter-book.row').css('display', 'none');
   $('html, body').animate({
    scrollTop: ($('.back_step').offset().top - 20)
},500);
});

$( ".closeit" ).click(function() {     
   $('.cardss').css('display','none');
   $('.hideifboth').css('display','none');
   $('.day_1').css('display', 'block');
   $('#back_from').css('display', 'none');
   $('.proccedd').css('display', 'none');
   $('.booked_smart_meter').css('display', 'none');
   $('.have_smartmeter').css('display', 'none');
   $('.meter-book.row').css('display', 'none');
   $('html, body').animate({
    scrollTop: ($('.back_step').offset().top - 20)
},500);
});

  $(document).ready(function () {
$("#back_button").hide();
$("#meter-detail-1").hide();
$("#meter-detail-2").hide();
$("#meter-detail-3").hide();
$("#meter-detail-4").hide();
$("#meter-detail-5").hide();
$("#meter-detail-6").hide();

$('.responsive-side-menu').hide();
});
</script>
<script>
    function copylinkss() {
        var copyText = document.getElementById("myshare");
        copyText.select();
        document.execCommand("copy");

    }
</script>
<script type="text/javascript">
    $('.clickmsginter').on('click', function() {
        Intercom('show');
    });

    // close meter_reading_info
    $('#close').click(function(){
        hide_all_detail();
		$('#popup-container').addClass('out');
		$('body').removeClass('popup-active');
		
		$('#meter-info').text("Pick the meter that looks most like yours...");
		$('#help-index').show('slow');
	});

   // meter_reading_info
   function meter_reading_info_layer(fuel_type)
    {
        if(fuel_type == 'gas')
        {
            $("#help-index").hide();
            $("#meter-detail-gas").show();
            $('#meter-info').text("Information about your meter");

        }
        else
        {
            $("#meter-detail-gas").hide();
        }
            
        $('#popup-container').removeAttr('class').addClass('reveal');
        $('body').addClass('popup-active');
    };
</script>