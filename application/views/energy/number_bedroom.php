<div class="container">
						
							<div class="row">
							<span class="back_step" id="media-mobile-m"><a href="javascript:void(0)" id="backtohousetype" class="material-icons" ><img style="background:none; padding:0; height:auto; width:60px;" src="<?php echo base_url(); ?>assets/images/backnew-mobile.png"/></a>  </span>
								<div class="col-md-10 mx-auto text-center">
									<h4 class="post_box_heading4 mt30 mb30">How many bedrooms do you have?</h4>
									
								</div>
								
							</div>
							<div class="switch_box_main">
							<div class="row">
									<span class="back_step" id="main-bak-dd"><a href="javascript:void(0)" id="backtoprepay" class="material-icons" style="top:-63px;left: 50px;"><img src="<?php echo base_url() ?>assets/images/red-back-button.svg"/></a> </span>
										<div class="col-md-4 main_card_box" id="cc-energy">
											<figure class="effect-sadie">
												<figcaption>
													<a href="javascript:void(0)" class="gas_card bedroom_range" data-numberbedroom="1">
														<div class="gas_box_icon gap_icon">
															<span class="switch_icon"><img src="<?php echo base_url() ?>assets/images/energy/bed-1.png" alt=""/></span>   
														</div> 

														<!-- <div class="content">
															<span class="gas_box_text dark_heading">
															1-2<br> Bedrooms</span>
														</div>
														<p class="text-center">If you have a gas meter. your boiler uses Prepay.</p> -->
														<span class="click_select_edit">SELECT</span>
													</a>

												</figcaption>
											</figure>
										</div><!-------------End Prepay---------------->

									<div class="col-md-4 main_card_box"  id="cc-energy">
											<figure class="effect-sadie">
												<figcaption>
													<a href="javascript:void(0)" class="gas_card bedroom_range" data-numberbedroom="2">
														<div class="gas_box_icon gap_icon">
															<span class="switch_icon"><img src="<?php echo base_url() ?>assets/images/energy/bed-2.png" alt=""/></span>
														</div>

														<!-- <div class="content">
															<span class="gas_box_text dark_heading">
															3-4<br> Bedrooms</span>
														</div>
														<p class="text-center">If you have a gas meter. your boiler uses Direct Debit</p> -->
														<span class="click_select_edit">SELECT</span>

													</a>

												</figcaption>
											</figure>
										</div><!-------------End Debit---------------->

										<div class="col-md-4 main_card_box" id="cc-energy">
											<figure class="effect-sadie">
												<figcaption>
													<a href="javascript:void(0)" class="gas_card bedroom_range" data-numberbedroom="3">
														<div class="gas_box_icon gap_icon">
															<span class="switch_icon"><img src="<?php echo base_url() ?>assets/images/energy/bed-3.png" alt=""/></span>
														</div>

														<!-- <div class="content">
															<span class="gas_box_text dark_heading">
															5+ <br>Bedrooms</span>
														</div>
														<!-- <p class="text-center">If you have a gas meter. your boiler uses Paper Bill.</p> -->
														<span class="click_select_edit">SELECT</span>

													</a>

												</figcaption>
											</figure>
										</div><!-------------End Paper Bill---------------->

									</div><!-------------------End Row------------>
									</div>
							<div class="backend_error" ></div>
						</div>