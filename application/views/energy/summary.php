<div id='summary-page-main-wrap'>
    <div id='summary-body'>
        <span class="back_step" id="main-bak-dd">
            <a href="javascript:go(-1)" id="backfromprepay" class="material-icons backtobedroom">
                <img src="<?php echo base_url(); ?>assets/images/red-back-button.svg"/>
            </a>
        </span>

        <div id='cards-wrap'>
            <!-- Dynamicly Added! -->
        </div>
        
        <?php if( empty($quote_summary['searchResults']) ){ ?>
            <!-- // if no quote info returned -->
            <h1>Sorry there was a problem somewhere with your quote.</h1>
        <?php } ?>
    </div>
</div>

<script type="text/x-handlebars-template" id='quoteSummaryTemplate'>
    <div class='card card-raised2 card-form-horizontal wow fadeInUp summary-card{{#isActive}}{{ type }}{{/isActive}}' data-type='{{ type }}' data-wow-delay='0ms'>
        <div class='content'>
            <div class='card-tariff text-center'>
                {{#if showBadge}}
                <div class="promotion-badge">
                    <div>
                        <div class='spanWrap'>
                            <span>Earn</span>
                            <span>12%</span>
                            <span>interest</span>
                        </div>
                    </div>
                </div>
                {{/if}}
                <div class='container'>
                    <div class='switch-me text-center'>
                        <h1 class='summary-plan-title'>{{ title }}</h1>
                        <div class='price_switch'>
                            <span class='price_switch_value'>£{{ price }}</span>
                            <span class='payment_type_value'>{{#normaliseType}}{{ type }}{{/normaliseType}}</span>
                        </div>

                        <ul class='highlights'>
                            {{#each highlights}}
                            <li>
                                <div class='img-wrap'>
                                    <img src='{{ this.img }}' alt=''/>
                                </div>
                                <span>{{ this.label }}</span>
                            </li>
                            {{/each}}
                        </ul>

                        <form class='quote_summary_form' method='post' action='./user/signup'>
                            <button class='green-btn-summer btn btn-primary btn-round btn-info to_signup_form top' type='submit'>Switch Now</button>

                            <a href='#' class='expand-section' title='expand'>
                                <span>More Info</span>
                            </a>

                            <div class='summary-wrap'>
                                <p class='summary-statement'>
                                    The quote above is a good estimate based on the information you have given us.
                                    Estimated Annual Consumption: <span class='summary-statement-eac'>{{ eac }} kWh</span>.
                                    Annual Quantity: <span class='summary-statement-aq'>{{ aq }} kWh</span>
                                    <!-- If you know your current tariff and your exact energy usage,<br> you can get a more accurate quote  -->
                                    <!-- <span class='back_known'>here</span>. -->
                                    <!-- <div class='back_known comparetocurrent'>Refine your quote</div> -->
                                </p>
                                <div id='exTab1' class='container'>
                                    <ul class='nav nav-pills'>
                                        <li><a href='#{{ tariff.tabID }}' data-toggle='tab' style='border-radius: 30px 0 0 30px;'>Tariff</a></li>
                                        <li><a href='#{{ benefits.tabID }}' data-toggle='tab' class='active show' >Benefits</a></li>
                                        <li><a href='#{{ important.tabID }}' data-toggle='tab' style='border-radius: 0 30px 30px 0;'>Important</a></li>
                                    </ul>
                                    <div class='tab-content clearfix'>
                                        <div class='tab-pane' id='{{ tariff.tabID }}'>
                                            <div class='card'>
                                                <div class='summary-tab-content tariff'></div>
                                            </div>
                                        </div>
                                        <div class='tab-pane active' id='{{ benefits.tabID }}'>
                                            <div class='card'>
                                                <div class='summary-tab-content benefits'></div>
                                            </div>
                                        </div>
                                        <div class='tab-pane' id='{{ important.tabID }}'>
                                            <div class='card'>
                                                <div class='summary-tab-content important'></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <button class='green-btn-summer btn btn-primary btn-round btn-info to_signup_form bottom' type='submit'>Switch Now</button>
                            </div>
                        </form>       
                    </div>
                </div>
            </div>
        </div>
    </div>
</script>

<?php if( !empty($quote_summary['searchResults']) ){ ?>
    <script type='text/javascript' src='/assets/js/handlebars-v4.0.12.js'></script>
    <script type='text/javascript'>
        $(function(){

            function expandSection( evt ){
                evt.preventDefault();
                var $card = $( this ).parents('.summary-card');

                if( !$card.hasClass('expand') ){
                    $card.addClass('expand');
                    $card.find('.summary-wrap').css({ 'max-height' : '3000px' });
                    $(this).find('span').text('Less info');
                }else{
                    $card.removeClass('expand');
                    $card.find('.summary-wrap').css({ 'max-height' : '0px' });
                    $(this).find('span').text('More info');
                };
            };

            function cardActive( evt ){
                if( this.classList.contains('active') ) return ;
                $('.summary-card').each( function( i, item ){ item.classList.contains('active') && item.classList.remove('active') });
                this.classList.add('active');
            }

            function uuid(){
                return 'es'+crypto.getRandomValues(new Uint32Array(4)).join('-');
            }

            function submitPaymentPlan( evt ){
                evt.preventDefault();

                var type = $(this).parents('.card').attr( 'data-type' );
                var paymentType = type === 'yearly' ? 'yearlypay' : type === 'monthly' ? 'monthlypay' : '';

                $('#monthyear').val( paymentType );

                REQUEST( base_url+'index.php/user/monthyear?monthyearinput='+paymentType, 'none' )
                    .then(function(){
                        window.location.href = base_url+'index.php/user/signup';
                    });

            };
            

            Handlebars.registerHelper( 'normaliseType', function( item ) {
                switch( item.data.root.type ){
                    case 'yearly':
                        return 'Per Year';
                    case 'monthly':
                        return '<span class="monthlyPriceFigure">£' + item.data.root.monthlyPrice + '</span> Per Month';
                    default:
                        return 'Per Week';
                };
            });

            Handlebars.registerHelper( 'isActive', function( item ) {
                return item.data.root.paymentType === $('#quotation_form').attr('data-payment-type') ? ' active' : '' ;
            });

            function buildElements( data ){
                return data.filter(function( item ){ return typeof item.condition === 'undefined' || item.condition })
                    .map(function( item ){ return '<'+ item.tag +'>' + item.text + '</'+ item.tag +'>' })
                    .join('');
            };

            function buildSummary( data ){
                data.forEach(function( item, i ){

                    var template = Handlebars.compile( $('#quoteSummaryTemplate').html() );
                    var parsed = template( item );
                    $( '#cards-wrap' ).append( parsed );

                    ['tariff', 'benefits', 'important'].forEach(function( key ){
                        $('[data-type="'+ item.type +'"]').find( '.summary-tab-content.' + key ).append( $.parseHTML( buildElements( item[ key ].statements ) ) );
                    });

                });
                
                $('.expand-section').on( 'click', expandSection );
                $('.summary-card').on( 'click', cardActive );
                $(".to_signup_form").on( 'click', submitPaymentPlan );
            };

            <?php if($desiredPayTypes!=5) { ?>
                $('#summary-body').addClass('single-formation');
                // Not prepay
                buildSummary([
                        type : 'yearly',
                        paymentType : 'pay yearly',
                        title : "<?= ucwords(strtolower($fixed_yearly['productname'])); ?>",
                        price : "<?= $fixed_yearly['estimated_annual_tariff']; ?>".replace(',', ''),
                        eac : "<?= $fixed_yearly['eac'] ?>", // estimated yearly consumption
                        aq : "<?= $fixed_yearly['aq'] ?>", // anual quantity
                        showBadge : true,
                        extraCaption : [],
                        highlights : [
                            {
                                img : '/assets/images/icons/chart.png',
                                label : 'Earn 12% annual interest on your credit balance'
                            },
                            {
                                img : '/assets/images/icons/medal.png',
                                label : 'get on the cheapest fixed-rate tariff on the market'
                            },
                            {
                                img : '/assets/images/icons/wallet.png',
                                label : 'Make 1 simple payment for the whole year'
                            }
                        ],
                        tariff : {
                            tabID : uuid(),
                            statements : [
                                {
                                    tag : "h3",
                                    text : "<?= ucwords(strtolower($fixed_yearly['productname'])); ?>"
                                },
                                {
                                    tag : "p",
                                    text : "Electricity unit rate – <?=number_format($fixed_yearly['electricity_dur']*100,1);?> pence per kWh",
                                    condition : <?= json_encode(!empty($fixed_yearly['electricity_dur'])) ?>
                                },
                                {
                                    tag : "p",
                                    text : "Gas unit rate – <?=number_format($fixed_yearly['gas_ur']*100,1);?> pence per kWh",
                                    condition : <?= json_encode(!empty($fixed_yearly['gas_ur'])) ?>
                                },
                                {
                                    tag : "p",
                                    text : "Electricity standing charge – <?=number_format($fixed_yearly['electricity_sc']*100,1);?> pence per day",
                                    condition : <?= json_encode(!empty($fixed_yearly['electricity_sc'])) ?>
                                },
                                {
                                    tag : "p",
                                    text : "Gas standing charge – <?=number_format($fixed_yearly['gas_sc']*100,1);?> pence per day",
                                    condition : <?= json_encode(!empty($fixed_yearly['gas_sc'])) ?>
                                },
                                {
                                    tag : "p",
                                    text : "Earn 12% interest per annum on your credit balance"
                                },
                                {
                                    tag : "p",
                                    text : "Earn interest on your refer a friend credit balance"
                                }
                            ]
                        },
                        benefits : {
                            tabID : uuid(),
                            statements : [
                                {
                                    tag : "h3",
                                    text : "No hard feelings!"
                                },
                                {
                                    tag : "p",
                                    text : "We're sure that you’ll love being part of the Eversmart family - but if you do decide to leave us, there’s no hard feelings! However, because we've comitted to a years worth of gas and electricity, there will be an exit fee. Please see T&Cs for more info."
                                },
                                {
                                    tag : "h3",
                                    text : "You’ll get a free smart meter"
                                },
                                {
                                    tag : "p",
                                    text : "Don’t have a smart meter yet? You’ll get one for free, fitted by a trained & qualified Eversmart engineer. Smart meters are designed to give you control over your energy and save you money. You don’t need to do anything – we'll contact you to make an appointment."
                                },
                                {
                                    tag : "h3",
                                    text : "Here when you need us"
                                },
                                {
                                    tag : "p",
                                    text : "Our friendly staff are based in the UK and available 7 days a week if you need help or have a problem. You can contact us by phone, email or social media – whatever works for you!"
                                },
                                {
                                    tag : "h3",
                                    text : "Switching is smooth and seamless!"
                                },
                                {
                                    tag : "p",
                                    text : "There’ll be no interruption to your gas & electricity supply during the switch."
                                }
                            ]
                        },
                        important : {
                            tabID : uuid(),
                            statements : [
                                {
                                    tag : "h3",
                                    text : "Don’t worry – we'll do all the hard work for you!"
                                },
                                {
                                    tag : "p",
                                    text : "We’ll let your old supplier know that you want to switch, and we’ll take care of all the boring stuff behind the scenes during the switchover period. All you have to do is sit back and relax!"
                                },
                                {
                                    tag : "h3",
                                    text : "Just so you know.."
                                },
                                {
                                    tag : "p",
                                    text : "The entire switching process takes 21 days. You don’t have to do anything, and we’ll be sure to keep you in the loop every step of the way. Your first payment will be taken on day 21."
                                },
                                {
                                    tag : "p",
                                    text : "<a href='<?= base_url(); ?>index.php/Terms' target='_blank'>Read our Terms &amp; Conditions here</a>"
                                }
                            ]
                        }
                    },*/
                    {
                        type : 'monthly',
                        paymentType : 'pay monthly',
                        title : "<?= $quote_summary['searchResults'][0]['tariffName']; ?>",
                        price : "<?= number_format($quote_summary['searchResults'][0]['newSpend'],2); ?>".replace(',', ''),
                        monthlyPrice : "<?= number_format($quote_summary['searchResults'][0]['newSpend']/12,2); ?>",
                        eac : "<?= $fixed_yearly['eac'] ?>", // estimated yearly consumption
                        aq : "<?= $fixed_yearly['aq'] ?>", // anual quantity
                        showBadge : false,
                        extraCaption : [
                            "* Earn 12% interest per annum on your credit balance!",
                            "*t&amp;c's apply",
                            "* Earn interest on your refer a friend credit balance"
                        ],
                        highlights : [
                            {
                                img : '/assets/images/icons/shield.png',
                                label : 'Get on a great value tariff'
                            },
                            {
                                img : '/assets/images/icons/card.png',
                                label : 'Pay by monthly direct debit'
                            },
                            {
                                img : '/assets/images/icons/hands.png',
                                label : 'No exit fees if you decide to leave'
                            }
                        ],
                        tariff : {
                            tabID : uuid(),
                            statements : [
                                {
                                    tag : "h3",
                                    text : "<?=$quote_summary['searchResults'][0]['tariffName'];?>"
                                },
                                {
                                    tag : "p",
                                    text : "Electricity unit rate – <?=number_format($quote_summary['searchResults'][0]['unitRate1Elec'],1);?> pence per kWh",
                                    condition : <?= json_encode(!empty($quote_summary['searchResults'][0]['unitRate1Elec'])) ?>
                                },
                                {
                                    tag : "p",
                                    text : "Gas unit rate – <?=number_format($quote_summary['searchResults'][0]['unitRate1Gas'],1);?> pence per kWh",
                                    condition : <?= json_encode(!empty($quote_summary['searchResults'][0]['unitRate1Gas'])) ?>
                                },
                                {
                                    tag : "p",
                                    text : "Electricity standing charge – <?=number_format($quote_summary['searchResults'][0]['standingChargeElec'],1);?> pence per day",
                                    condition : <?= json_encode(!empty($quote_summary['searchResults'][0]['standingChargeElec'])) ?>
                                },
                                {
                                    tag : "p",
                                    text : "Gas standing charge – <?=number_format($quote_summary['searchResults'][0]['standingChargeGas'],1);?> pence per day",
                                    condition : <?= json_encode(!empty($quote_summary['searchResults'][0]['standingChargeGas'])) ?>
                                },
                                {
                                    tag : "p",
                                    text : "<?= $quote_summary['searchResults'][0]['featureText1'] ?>",
                                    condition : <?= json_encode(!empty($quote_summary['searchResults'][0]['featureText1'])) ?>
                                },
                                {
                                    tag : "p",
                                    text : "<?= $quote_summary['searchResults'][0]['featureText2'] ?>",
                                    condition : <?= json_encode(!empty($quote_summary['searchResults'][0]['featureText2'])) ?>
                                },
                                {
                                    tag : "p",
                                    text : "<?= $quote_summary['searchResults'][0]['featureText3'] ?>",
                                    condition : <?= json_encode(!empty($quote_summary['searchResults'][0]['featureText3'])) ?>
                                },
                                {
                                    tag : "p",
                                    text : "<?= $quote_summary['searchResults'][0]['featureText4'] ?>",
                                    condition : <?= json_encode(!empty($quote_summary['searchResults'][0]['featureText4'])) ?>
                                }
                            ]
                        },
                        benefits : {
                            tabID : uuid(),
                            statements : [
                                {
                                    tag : "h3",
                                    text : "We don’t charge exit fees!"
                                },
                                {
                                    tag : "p",
                                    text : "We're sure that you’ll love being part of the Eversmart family - but if you do decide to leave us, there’s no hard feelings! Unlike most other suppliers, we won’t charge you an early exit fee."
                                },
                                {
                                    tag : "h3",
                                    text : "You’ll get a free smart meter"
                                },
                                {
                                    tag : "p",
                                    text : "Don’t have a smart meter yet? You’ll get one for free, fitted by a trained & qualified Eversmart engineer. Smart meters are designed to give you control over your energy and save you money. You don’t need to do anything – we'll contact you to make an appointment."
                                },
                                {
                                    tag : "h3",
                                    text : "Here when you need us"
                                },
                                {
                                    tag : "p",
                                    text : "Our friendly staff are based in the UK and available 7 days a week if you need help or have a problem. You can contact us by phone, email or social media – whatever works for you!"
                                },
                                {
                                    tag : "h3",
                                    text : "Switching is smooth and seamless!"
                                },
                                {
                                    tag : "p",
                                    text : "There’ll be no interruption to your gas & electricity supply during the switch."
                                }
                            ]
                        },
                        important : {
                            tabID : uuid(),
                            statements : [
                                {
                                    tag : "h3",
                                    text : "Don’t worry – we'll do all the hard work for you!"
                                },
                                {
                                    tag : "p",
                                    text : "We’ll let your old supplier know that you want to switch, and we’ll take care of all the boring stuff behind the scenes during the switchover period. All you have to do is sit back and relax!"
                                },
                                {
                                    tag : "h3",
                                    text : "Just so you know.."
                                },
                                {
                                    tag : "p",
                                    text : "The entire switching process takes 21 days. You don’t have to do anything, and we’ll be sure to keep you in the loop every step of the way. Your first payment will be taken on day 21."
                                },
                                {
                                    tag : "p",
                                    text : "<a href='<?= base_url(); ?>index.php/Terms' target='_blank'>Read our Terms &amp; Conditions here</a>"
                                }
                            ]
                        }
                    }
                ]);
            <?php }else{ ?>

                $('#summary-body').addClass('single-formation');

                // prepay
                buildSummary([
                    {
                        type : 'weekly',
                        paymentType : 'pay as you go',
                        title : "<?= $quote_summary['searchResults'][0]['tariffName']; ?>",
                        price : "<?= number_format($quote_summary['searchResults'][0]['newSpend']/52,2); ?>".replace(',', ''),
                        eac : "<?= $fixed_yearly['eac'] ?>", // estimated yearly consumption
                        aq : "<?= $fixed_yearly['aq'] ?>", // anual quantity
                        showBadge : false,
                        extraCaption : [],
                        highlights : [
                            {
                                img : '/assets/images/icons/icon3.png',
                                label : 'No monthly bills - you buy credit when it suits you'
                            },
                            {
                                img : '/assets/images/icons/mobile.png',
                                label : 'Top-up your balance via phone, app or SMS (smart meters only)'
                            },
                            {
                                img : '/assets/images/icons/smart-meter.png',
                                label : 'View your remaining credit whenever you like'
                            }
                        ],
                        tariff : {
                            tabID : uuid(),
                            statements : [
                                {
                                    tag : "h3",
                                    text : "<?=$quote_summary['searchResults'][0]['tariffName'];?>"
                                },
                                {
                                    tag : "p",
                                    text : "Electricity unit rate – <?=number_format($quote_summary['searchResults'][0]['unitRate1Elec'],1);?> pence per kWh",
                                    condition : <?= json_encode(!empty($quote_summary['searchResults'][0]['unitRate1Elec'])) ?>
                                },
                                {
                                    tag : "p",
                                    text : "Gas unit rate – <?=number_format($quote_summary['searchResults'][0]['unitRate1Gas'],1);?> pence per kWh",
                                    condition : <?= json_encode(!empty($quote_summary['searchResults'][0]['unitRate1Gas'])) ?>
                                },
                                {
                                    tag : "p",
                                    text : "Electricity standing charge – <?=number_format($quote_summary['searchResults'][0]['standingChargeElec'],1);?> pence per kWh",
                                    condition : <?= json_encode(!empty($quote_summary['searchResults'][0]['standingChargeElec'])) ?>
                                },
                                {
                                    tag : "p",
                                    text : "Gas standing charge – <?=number_format($quote_summary['searchResults'][0]['standingChargeGas'],1);?> pence per kWh",
                                    condition : <?= json_encode(!empty($quote_summary['searchResults'][0]['standingChargeGas'])) ?>
                                },
                                {
                                    tag : "p",
                                    text : "<?= $quote_summary['searchResults'][0]['featureText1'] ?>",
                                    condition : <?= json_encode(!empty($quote_summary['searchResults'][0]['featureText1'])) ?>
                                },
                                {
                                    tag : "p",
                                    text : "<?= $quote_summary['searchResults'][0]['featureText2'] ?>",
                                    condition : <?= json_encode(!empty($quote_summary['searchResults'][0]['featureText2'])) ?>
                                },
                                {
                                    tag : "p",
                                    text : "<?= $quote_summary['searchResults'][0]['featureText3'] ?>",
                                    condition : <?= json_encode(!empty($quote_summary['searchResults'][0]['featureText3'])) ?>
                                },
                                {
                                    tag : "p",
                                    text : "<?= $quote_summary['searchResults'][0]['featureText4'] ?>",
                                    condition : <?= json_encode(!empty($quote_summary['searchResults'][0]['featureText4'])) ?>
                                }
                            ]
                        },
                        benefits : {
                            tabID : uuid(),
                            statements : [
                                {
                                    tag : "h3",
                                    text : "We don’t charge exit fees!"
                                },
                                {
                                    tag : "p",
                                    text : "We're sure that you’ll love being part of the Eversmart family - but if you do decide to leave us, there’s no hard feelings! Unlike most other suppliers, we won’t charge you an early exit fee."
                                },
                                {
                                    tag : "h3",
                                    text : "You’ll get a free smart meter"
                                },
                                {
                                    tag : "p",
                                    text : "Don’t have a smart meter yet? You’ll get one for free, fitted by a trained & qualified Eversmart engineer. Smart meters are designed to give you control over your energy and save you money. You don’t need to do anything – we'll contact you to make an appointment."
                                },
                                {
                                    tag : "h3",
                                    text : "Here when you need us"
                                },
                                {
                                    tag : "p",
                                    text : "Our friendly staff are based in the UK and available 7 days a week if you need help or have a problem. You can contact us by phone, email or social media – whatever works for you!"
                                },
                                {
                                    tag : "h3",
                                    text : "Switching is smooth and seamless!"
                                },
                                {
                                    tag : "p",
                                    text : "There’ll be no interruption to your gas & electricity supply during the switch."
                                }
                            ]
                        },
                        important : {
                            tabID : uuid(),
                            statements : [
                                {
                                    tag : "h3",
                                    text : "Don’t worry – we'll do all the hard work for you!"
                                },
                                {
                                    tag : "p",
                                    text : "We’ll let your old supplier know that you want to switch, and we’ll take care of all the boring stuff behind the scenes during the switchover period. All you have to do is sit back and relax!"
                                },
                                {
                                    tag : "h3",
                                    text : "Just so you know.."
                                },
                                {
                                    tag : "p",
                                    text : "The entire switching process takes 21 days. You don’t have to do anything, and we’ll be sure to keep you in the loop every step of the way. Your first payment will be taken on day 21."
                                },
                                {
                                    tag : "p",
                                    text : "<a href='<?= base_url(); ?>index.php/Terms' target='_blank'>Read our Terms &amp; Conditions here</a>"
                                }
                            ]
                        }
                    }
                ]);
            <?php } ?>
        });
    </script>
<?php } ?>
