<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Eversmart | Registrations</title>       
	 <!-- Font material icon -->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons"rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:500" rel="stylesheet"> 
	  <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="<?php echo base_url() ?>assets/css/mdb.min.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">
	   <!-- Your custom styles (optional) -->
    <link href="<?php echo base_url() ?>assets/css/responsive.css" rel="stylesheet">
	<!--- Font Family Add---------------->
	<link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet"> 
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/aos.css" />

<link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>assets/img/favicon.png" />


	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-79295711-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-79295711-1');
	</script>

	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','<?php echo $this->config->item('gtm_id'); ?>');</script>
    <!-- End Google Tag Manager -->

    <!-- Hotjar Tracking Code for www.eversmartenergy.co.uk -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:1074765,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>
	
</head>

<body class="sky-blue-bg">

	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T44QPZV"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <!-- Start your project here-->
	
	<div class="main-rounded-red-header">
		<div class="red-rounded-wave">
		<div class="topbar" id="red_top">			
			
			<div class="container">
				<div class="row">

				<nav class="mb-1 navbar navbar-expand-lg navbar-dark bg-unique">
					<span class="col-sm-6 col-md-3 px-0">
						<a class="logo" href="<?php echo base_url(); ?>">eversmart.</a>
					</span>

					<div class="col-sm-6 col-md-9 text-right pull-right no-padding mobile_menu">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-3" aria-controls="navbarSupportedContent-3" aria-expanded="false" aria-label="Toggle navigation" style="">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse " id="navbarSupportedContent-3">
                        <ul class="navbar-nav ml-auto mobile_navi">
                            <li class="nav-item">
                                <a class="nav-link waves-effect waves-light" href="<?php echo base_url(); ?>index.php/quotation">energy
                                    <span class="sr-only">(current)</span>
                                </a>
                            </li>
							<li class="nav-item">
                                <a class="nav-link waves-effect waves-light" href="boiler-switch-one.html">Boiler
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link waves-effect waves-light" href="#">smart meters</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link waves-effect waves-light" href="#">home services</a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle waves-effect waves-light" id="navbarDropdownMenuLink-3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">help
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-danger" aria-labelledby="navbarDropdownMenuLink-2">
                                    <a class="dropdown-item waves-effect waves-light" href="#">help & faqs</a>
                                    <a class="dropdown-item waves-effect waves-light" href="#">contact us</a>
                                </div>
                            </li>

							<li><button type="button" class="btn btn-info btn-rounded waves-effect waves-light white-btn">LOGIN</button></li>
							<li><button style="margin-left:25px" type="button" class="btn btn-info btn-rounded waves-effect waves-light white-btn"><a href="https://eversmartpayments.paypoint.com/energy/" target="_blank">TOP UP</a></button></li>

                        </ul>

                    </div>
					</div>
                </nav>
				</div>
			</div>
			<!--- End container-------->
			<!--- End container-------->
			
		</div>
		<!---end top bar bar----------->	


<!----------------start postcode--------------------->


<!----------------End postcode--------------------->	

			<!----------------start postcode-------------------->

		<section class="postcode_top mt0">
				<div class="postcode_box" id="background-none">
					
						<div class="container">
						
						<div class="row posi-rele">
						<span class="back_step" style="position:absolute; top:94px;left:-6px"><a href="boiler-switch-five.html" class="material-icons"><img src="images/red-back-button.svg"></a></span>
								<div class="col-md-6  col-sm-6 ">
																			
									<h3 class="text-center quote-heading">What are you paying?</h3>
									<div class="card card-raised2 card-form-horizontal wow fadeInUp" data-wow-delay="0ms">
										<div class="content">
										<div class="inner-card" style="padding-bottom:74px;">
										<span class="registration-page-heading-energy-q">										
										Your Current Energy <br>Spend
										</span>
										
										<span class="registration-page-energy-q">										
										 <span class="quo-per-month" style="font-size:60px; line-height:72px;">	<strong>&#163; 100.55</strong> </span>
										</span>
										</div>
										</div>
									</div>
								
								</div>
								
									<div class="col-md-6 col-sm-6 ">								
									<h3 class="text-center quote-heading">What are our Offers?</h3>
									<div class="card card-raised2 card-form-horizontal wow fadeInUp" data-wow-delay="0ms">
										<div class="content">
										<div class="inner-card">
										<span class="registration-page-heading-energy-q green-txt-quote">										
										Evermart Plan 1.1 <br> <span style="font-size:16px;">will save</span> &#163; 81/mo.
										</span>
										
										<span class="registration-page-energy-q">										
										 <span class="quo-per-month brown-txt-quote">	<strong>&#163; 100.55</strong><br> <span class="small-price-txt">/mo.</span></span>
										</span>
										<a href="#" class="" data-toggle="modal" data-target="#Triffinfo">
												  View plan details
												</a>
										</div>
										</div>
									</div>
								
								</div>
						</div><!---------------End Row----------->
						
							<div class="row ">
							
						</div><!---------------End Row---------->
						
						
						
						 
						<div class="row" id="bill-plan-table">
							<h3 class="text-center quote-heading">Add Homecare Service <span style="font-size:14px;">(optional):</span></h3>

							<div class="col-sm-3 text-center">
							<div class="card card-raised2 card-form-horizontal wow fadeInUp pt0" data-wow-delay="0ms" id="bill-tab">
																				
											<div class="card-tariff text-center pt0">
												<span class="eversmart-step text-center;">
												
													<span class="card ever_rounded_p text-center">
														
														<span class="a-price"><span class="b-price-a">£3.12</span>
														</span>														
													</span>
												</span>											
												<span class="plan-home-care"><span>Homecare 2.2</span></span>												
												<a href="#" class="" data-toggle="modal" data-target="#Triffinfo">
												  View Now
												</a>
											</div>
										<span class="buy-now-bill">
										<a href="#" class="red-btn">Select Plan</a>
									</span>
									</div>
						
							</div><!---------End Box One---------->
							
							<div class="col-sm-3 text-center">
							<div class="card card-raised2 card-form-horizontal wow fadeInUp pt0" data-wow-delay="0ms" id="bill-tab">
																				
											<div class="card-tariff text-center pt0">
												<span class="eversmart-step text-center;">
												
													<span class="card ever_rounded_p text-center">
														
														<span class="a-price"> <span class="b-price-a">£4.14</span>
														</span>														
													</span>
												</span>
													<span class="plan-home-care"><span>Homecare 1.2</span></span>
																							
												<a href="#" class="" data-toggle="modal" data-target="#Triffinfo">
												  View Now
												</a>
											</div>
										<span class="buy-now-bill">
										<a href="#" class="red-btn">Select Plan</a>
									</span>
									</div>
						
							</div><!---------End Box One---------->
							
							<div class="col-sm-3 text-center">
							<div class="card card-raised2 card-form-horizontal wow fadeInUp pt0" data-wow-delay="0ms" id="bill-tab">
																				
											<div class="card-tariff text-center pt0">
												<span class="eversmart-step text-center;">
												
													<span class="card ever_rounded_p text-center">
														
														<span class="a-price"><span class="b-price-a">£2.18</span>
														</span>														
													</span>
												</span>	
												<span class="plan-home-care"><span>Homecare 2.1</span></span>
																								
												<a href="#" class="" data-toggle="modal" data-target="#Triffinfo">
												  View Now
												</a>
											</div>
										<span class="buy-now-bill">
										<a href="#" class="red-btn">Select Plan</a>
									</span>
									</div>
						
							</div><!---------End Box One---------->
					
						
							<div class="col-sm-3 text-center">
							<div class="card card-raised2 card-form-horizontal wow fadeInUp pt0" data-wow-delay="0ms" id="bill-tab">
																				
											<div class="card-tariff text-center pt0">
												<span class="eversmart-step text-center;">
												
													<span class="card ever_rounded_p text-center">
														
														<span class="a-price"> <span class="b-price-a">£5.21</span>
														</span>														
													</span>
												</span>	

												<span class="plan-home-care"><span>Homecare 2.2</span></span>											
																								
												<a href="#" class="" data-toggle="modal" data-target="#Triffinfo">
												  View Now
												</a>
											</div>
											
									<span class="buy-now-bill">
										<a href="#" class="red-btn">Select Plan</a>
									</span>
										
									</div>
						
							</div><!---------End Box One---------->
						
						
								
				
						
							
						</div>
						
						<div class="row justify-content-center">
									<div class="col-sm-12">
									<span class="input-group-btn btnNew" id="continue_btn">
										<button id="enter_usage_form" style="background:#fff!important; color:#ee4392!important; padding:10px 80px !important; font-size:20px;" class="btn btn-md btn-eversmart btn-round weight-300 text-center wow fadeInUp switchButton waves-effect waves-light " type="button">Continue</button>
										</span>
									</div>

								</div>
						</div>
					</section>
<!----------------End postcode--------------------->	

	
		</div>
		</div>
			
		<!--------------------Start breadcrumbs Care Section------------->
		
		
			
<!--------------------End breadcrumbs Care Section------------->

		
<!----------------Start Footer-------------------------->
	

    	<?php $this->load->view('layout/common_footer'); ?>