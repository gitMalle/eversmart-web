<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Eversmart | Overview</title>
	 <!-- Font material icon -->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:500" rel="stylesheet">
	  <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url() ?>dashboard/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="<?php echo base_url() ?>dashboard/css/mdb.min.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="<?php echo base_url() ?>dashboard/css/style.css" rel="stylesheet">
	   <!-- Your custom styles (optional) -->
    <link href="<?php echo base_url() ?>dashboard/css/responsive.css" rel="stylesheet">

		   <!-- Your custom styles (Deshboard CSS) -->
    <link href="<?php echo base_url() ?>dashboard/css/dashboard.css" rel="stylesheet">
	<!--- Font Family Add---------------->
	<link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">
	<link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>assets/img/favicon.png" />

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-79295711-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-79295711-1');
	</script>

	<!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','<?php echo $this->config->item('gtm_id'); ?>');</script>
    <!-- End Google Tag Manager -->

    <!-- Hotjar Tracking Code for www.eversmartenergy.co.uk -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:1074765,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>

</head>

<body class="sky-blue-bg">

	<!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T44QPZV"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <!-- Start your project here-->
<div class="loading-page">
    <div class="loader"></div>
</div>
	 <div class="main-inner" id="main_content" style="display:none" >
	<div class="main-rounded-red-header">
		<div class="red-rounded-wave">
		<div class="topbar" id="red_top">

      <?php $this->load->view('layout/menu'); ?>

			<!--- End container-------->
			<!--- End container-------->

		</div>
		<!---end top bar bar----------->


<!----------------start postcode-------------------->

			<section class="mt0 ">
				<div class="boiler_header-h">
						<div class="container">
							<div class="row">
								<div class="col-md-10 mx-auto text-center">
									<span class="bolier_home-h-l">Dashboard</span>
									<span class="bolier_home-h-s">Join the Revolution and start Saving</span>
								</div>
							</div>
						</div><!------------------End Container----------------->

				</div><!------------------End Postcode------------->

			</section>
<!----------------End postcode--------------------->
		</div>
		</div>

		<!--------------------Start breadcrumbs Care Section------------>

		<section class="termandconditions">
				<div class="container-fluid">
					<div class="row">
						<div class="col-sm-2">
						    <?php $this->load->view('dashboard/menu.php'); ?>
						</div>

						<div class="col-sm-8">
						<div class="large-screen-fix">
							<div class="row">
								<?php $this->load->view($content); ?>
							</div>
							</div>
						</div>


						<div class="col-sm-2">

							<a href="#" class="text-center pull-right faq-round">FAQ</a>

						</div>

					</div>




				</div>

			</section>

<!--------------------End breadcrumbs Care Section------------->


<!----------------Start Footer-------------------------->


    	<footer class="main_footer">
		<div class="footer-top-sky-rounded"></div>
    		<div class="container">
    			<div class="inner_footer">
    				<div class="row">
    					<div class="col-sm-4 col-md-3">
    						<h4>eversmart.</h4>
    						<p>A smarter, more efficient future for Britain. Eversmart are at the forefront of the smart revolution, providing flexible, low cost energy with smart technology and exceptional customer service.</p>
							<h5>Opening Times: </h5>
							<p>MON - FRI 8am-8pm  <br>SAT 8am-5pm <br>SUN 9am-5pm </p>
    					</div>
    					<div class="col-sm-4 col-md-2">
    						<h4>Services</h4>
    							<ul class="links-vertical">
    								<li><a href="<?php echo base_url(); ?>index.php/quotation">Energy</a></li>
    								<li><a href="#">Smart Meters</a></li>
    								<li><a href="http://13.58.101.121/homeservices/index.php/Landing">Home Services</a></li>
    							</ul>
    					</div>
    					<div class="col-sm-4 col-md-2">
    							<h4>Useful Links</h4>
    							<ul class="links-vertical">
									<li><a href="https://www.eversmartenergy.co.uk/blog/">Eversmart Blog</a></li>
    								<li><a href="<?php echo base_url(); ?>index.php/Terms">Terms &amp; Conditions</a></li>
									<li><a href="<?php echo base_url(); ?>index.php/Policies">Privacy</a></li>
									<li><a target="_blank" href="<?php echo base_url(); ?>assets/pdf/consumercheck_eng.pdf ">Consumer Checklist (UK)</a></li>
									<li><a target="_blank" href="<?php echo base_url(); ?>assets/pdf/consumercheck_wel.pdf ">Consumer Checklist (Welsh)</a></li>
    							</ul>
    					</div>
    					<div class="col-sm-4 col-md-2">
    							<h4>Support</h4>
    							<ul class="links-vertical">
    								<li><a href="<?php echo base_url(); ?>index.php/Helpfaqs">Help & FAQs</a></li>
    								<li><a href="<?php echo base_url(); ?>index.php/contact_us">Contact Us</a></li>
    							</ul>

    					</div>

    					<div class="col-sm-12 col-md-3 col-lg-3">
    						<h4>Contact Us</h4>
    						<ul class="links-vertical" id="small-lower-text">
    								<li><i class="material-icons"><a href="tel:03301027901"><img class="easyimgicon-footer" src="<?php echo base_url(); ?>assets/img/phone-footer.svg" alt=""/></i>   0330 102 7901</a></li>
									<li> <i class="material-icons"><a class="email-f-link" href="mailto:hello@eversmartenergy.co.uk?subject=Website%20Enquiry"><img class="easyimgicon-footer" src="<?php echo base_url(); ?>assets/img/email-footer.svg" alt=""/></i><a class="email-f-link" href="mailto:hello@eversmartenergy.co.uk?subject=Website%20Enquiry"> hello@eversmartenergy.co.uk</a></li>
									<li><i class="material-icons"><a href="http://18.218.252.81"><img class="easyimgicon-footer" src="<?php echo base_url(); ?>assets/img/b-meeting.svg" alt=""/></i>eversmart community</a></li>
    							</ul>

    					</div>

    				</div>
    			</div>

    			<div class="social-footer text-center">
    				<ul class="social-buttons socialButtonHome">
    					<li><a class="btn btn-just-icon btn-simple btn-twitter" href="https://twitter.com/eversmartenergy" target="_blank"><i class="fa fa-twitter"></i></a></li>
    					<li><a class="btn btn-just-icon btn-simple btn-facebook" href="https://www.facebook.com/eversmartenergy" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                        <li><a class="btn btn-just-icon btn-simple btn-twitter" href="https://www.instagram.com/eversmartenergy/" target="_blank"><i class="fa fa-instagram"></i></a></li>

    				</ul>
    			</div>
    			<!----End Social Footer------------->

    			<div class="copyright text-center">
    					&copy; 2018 Eversmart Energy Ltd - 09310427
    				</div>

    		</div>
    		<!---------end Footer container------------>
 
    	</footer>
    	<!----end footer------------>

</div>
      <!-- SCRIPTS -->
      <!-- JQuery -->
      <script type="text/javascript" src="<?php echo base_url() ?>dashboard/js/jquery-3.2.1.min.js"></script>
      <!-- Bootstrap tooltips -->
      <script type="text/javascript" src="<?php echo base_url() ?>dashboard/js/popper.min.js"></script>
      <!-- Bootstrap core JavaScript -->
      <script type="text/javascript" src="<?php echo base_url() ?>dashboard/js/bootstrap.min.js"></script>
      <!-- MDB core JavaScript -->
      <script type="text/javascript" src="<?php echo base_url() ?>dashboard/js/mdb.min.js"></script>
      <!-- common JavaScript -->
      <script type="text/javascript" src="<?php echo base_url() ?>dashboard/js/common.js"></script>
  </body>
  </html>
