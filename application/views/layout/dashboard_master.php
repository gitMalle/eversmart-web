<?php //debug( $this->session->userdata('login_data'),1 ); ?>
<!DOCTYPE html>
<html lang="en">

<head> 
    <meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no" />
    <title>Eversmart | <?php if( !empty($title) ){ echo $title; }else{ echo 'Dashboard'; } ?></title>
	 <!-- Font material icon -->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:500" rel="stylesheet">
	  <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url() ?>dashboard/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="<?php echo base_url() ?>dashboard/css/mdb.min.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">
	   <!-- Your custom styles (optional) --> 
	<link href="<?php echo base_url() ?>assets/css/responsive.css" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/aos.css" />

		   <!-- Your custom styles (Deshboard CSS) -->
    <link href="<?php echo base_url() ?>dashboard/css/dashboard.css" rel="stylesheet">
	<!--- Font Family Add---------------->
	<link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">
 
	<link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>assets/img/favicon.png" />

	<script>
function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>

<script>
        window.intercomSettings = {
        app_id: "qjs70gbf",
        custom_launcher_selector: ".open_intercom"
        <?php if(isset($this->session->userdata('login_data')['intercom_user_id'])){ ?>
            , user_id: "<?=$this->session->userdata('login_data')['intercom_user_id'];?>"
        <?php } ?>
        <?php if(isset($this->session->userdata('login_data')['intercom_id'])){ ?>
            , id: "<?=$this->session->userdata('login_data')['intercom_id'];?>"
        <?php } ?>
        <?php if(isset($this->session->userdata('login_data')['intercom_name'])){ ?>
            , name: "<?=$this->session->userdata('login_data')['intercom_name'];?>"
        <?php } ?>
        <?php if(isset($this->session->userdata('login_data')['intercom_email'])){ ?>
            , email: "<?=$this->session->userdata('login_data')['intercom_email'];?>"
        <?php } ?>
        };
        </script>
<script>(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/qjs70gbf';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})()</script>


<!-- <script type="text/javascript">

function movetoNext(current, nextFieldID) {
if (current.value.length >= current.maxLength) {
document.getElementById(nextFieldID).focus();
}
}
</script> -->
<style>



.loading-page {
margin:20% auto;
width: 100px
 }

 .loader {
  border: 8px solid #fda1b8;
  border-radius: 50%;
  border-top: 8px solid #f64d76;
  width: 90px;
  height: 90px;
  -webkit-animation: spin 2s linear infinite; /* Safari */
  animation: spin 2s linear infinite;
}

/* Safari */
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}


</style>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-79295711-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-79295711-1');
    </script>
    
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','<?php echo $this->config->item('gtm_id'); ?>');</script>
    <!-- End Google Tag Manager -->

    <!-- Hotjar Tracking Code for www.eversmartenergy.co.uk -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:1074765,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>

</head>

<body class="sky-blue-bg">

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T44QPZV"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <!-- Start your project here-->
<div class="loading-page">
    <div class="loader"></div>
</div>


<div class="cd-panel cd-panel--from-left js-cd-panel-left">

    <header class="cd-panel__header">
    <a class="logo" id="logo-dash" href="<?php echo base_url(); ?>"><img class="evrmenu-log" src="<?php echo base_url(); ?>/assets/images/logo-eversmart-new.png"/></a>
      <a href="#0" class="cd-panel__close js-cd-close"></a>
   </header>

   <div class="cd-panel__container">
      <div class="cd-panel__content">

          <nav id="menuslideoutjs" class="menu slideout-menu">
      <section class="menu-section">
        <ul class="menu-section-list">
            <?php if( $this->session->userdata('login_data')['signup_type'] != 3 ) { // Only show these if you are not a Dyball customer ?>
                <li><a href="<?php echo base_url(); ?>index.php/user/dashboard">Overview</a></li>
                <li><a href="<?php echo base_url(); ?>index.php/user/account">My Account</a></li>
                <li><a href="<?php echo base_url(); ?>index.php/user/referral">Refer a friend</a></li>
                <li><a href="<?php echo base_url(); ?>index.php/user/billing">Billing</a></li>
                <li><a href="<?php echo base_url(); ?>index.php/user/payments">Payments</a></li>
                <li><a href="<?php echo base_url(); ?>index.php/user/meter_reading">Meter Reading</a></li>
                <!--<li><a href="<?php// echo base_url(); ?>index.php/user/usage">Usage</a></li>-->
                <li><a href="<?php echo base_url(); ?>index.php/user/referral">Messages</a></li>
                <!--<li><a href="<?php// echo base_url(); ?>index.php/user/moving_home">Moving Home</a></li>-->
            <?php } else { ?>
                <li><a href="<?php echo base_url(); ?>index.php/user/account">My Account</a></li>
            <?php } ?>
        </ul>
      </section>
    </nav>

      </div> <!-- cd-panel__content -->


   </div> <!-- cd-panel__container -->
</div> <!-- cd-panel -->



<div class="cd-panel cd-panel--from-right js-cd-panel-main">
   <header class="cd-panel__header">
   <a class="logo" id="logo-dash" href="<?php echo base_url(); ?>"><img class="evrmenu-log" src="<?php echo base_url(); ?>/assets/images/logo-eversmart-new.png"/></a>
      <a href="#0" class="cd-panel__close js-cd-close"> </a>
   </header>

   <div class="cd-panel__container">
      <div class="cd-panel__content">
      <nav id="userslideoutjs" class="user slideout-menu slideout-menu-right">
      <section class="menu-section">
        <ul class="menu-section-list">
            <?php if($this->session->userdata('login_data')['junifer_account_active']==1||$this->session->userdata('login_data')['signup_type']==3){ // Only allow access to my account if they are Dyball or Junifer active ?>
                <li><a href="<?php echo base_url(); ?>index.php/user/account">My Account</a></li>
            <?php } ?>
            <li><a href="<?php echo base_url(); ?>index.php/Helpfaqs">Help & FAQs</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/contact_us">Contact us</a></li>
            <li><a id="logout_user" onclick="logout_user();" href="#">logout</a></li>
        </ul>
      </section>
    </nav>
      </div> <!-- cd-panel__content -->
   </div> <!-- cd-panel__container -->
</div> <!-- cd-panel -->
</div> <!-- cd-panel -->



	<div class="cd-main-content">
	 <div class="main-inner" id="main_content" style="display:none" >
		<div class="main-rounded-red-header">
		<div class="red-rounded-waves" id="dashboard_main_bg">
		<div class="topbar" id="red_top">

			<div class="container">
				<div class="row">


					<span class="col-sm-12 col-md-4 col-lg-6 px-0">
                        <a  href="<?php echo base_url(); ?>">
                            <img class="logo_dash" src="<?php echo base_url(); ?>/assets/images/logo-eversmart-new.png"/>
                        </a>
					</span>

					<div class="col-sm-12 col-md-4 col-lg-4  pull-right no-padding mobile_menu text-right hideonmobile" id="mobile_menu" style="position: relative; float:left; padding-top:10px">
					<!-- <p class="accnum">Account Number: </p>

					<span class="product-code-no-d">
									<?php
										/*
										if( $this->session->has_userdata('login_data') ){
											 if( $this->session->userdata('login_data')['signup_type'] == 0 || $this->session->userdata('login_data')['signup_type'] == 1 ){
												echo $this->session->userdata('login_data')['account_number'];
												}
												elseif($this->session->userdata('login_data')['signup_type'] == 3)
												{
													echo $this->session->userdata('login_data')['account_number'];
												}
										}else{
											echo '-';
										}
										*/
									?>
					</span> -->
					</div>

					<div class="mobileonly usermobilemenu toggle-button-menu-right cd-btn js-cd-panel-trigger" data-panel="main"><i class="fa fa-user" aria-hidden="true"></i></div>

                    <?php if($this->session->userdata('login_data')['junifer_account_active']==1||$this->session->userdata('login_data')['signup_type']==3){ // Only allow access to my account if they are Dyball or Junifer active ?>
                        <div class="mobileonly dashbaordmobilemenu"><button class="toggle-button-menu-left cd-btn js-cd-panel-trigger" data-panel="left"><img class="responsive_bar_icon" src="<?php echo base_url(); ?>assets/images/menu_bar_responsive.svg" alt=""/> </button></div>
                    <?php } ?>


                    <div class=" col-sm-12 col-md-4 col-lg-2 text-right hideonmobile" id="navbarSupportedContent-3-main" >


						 <a class="nav-link dropdown-toggle waves-effect waves-light" id="navbarDropdownMenuLink-3-main-dashboard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                	<?php
										if( $this->session->has_userdata('login_data') ){
											echo ucfirst($this->session->userdata('login_data')['title']).'. '.ucfirst($this->session->userdata('login_data')['last_name']);
										}else{
											echo '-';
										}
									?>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-danger animated fadeIn" aria-labelledby="navbarDropdownMenuLink-2">

                                    <?php if(($this->session->userdata('login_data')['junifer_account_active']==1||$this->session->userdata('login_data')['signup_type']==3) && $this->session->userdata('login_data')['account_status']==1){ // Only allow access to my account if they are Dyball or Junifer active ?>

                                        <a class="dropdown-item waves-effect waves-light" href="<?php echo base_url() ?>index.php/user/account">
                                            <i class="fa fa-info righ-padd" aria-hidden="true"></i>my account
                                        </a>

                                    <?php } else { ?>

                                        <a class="dropdown-item waves-effect waves-light" href="<?php echo base_url() ?>index.php/user/account_status">
                                            <i class="fa fa-info righ-padd" aria-hidden="true"></i>my account
                                        </a>

                                    <?php } ?>
                                    <a class="dropdown-item waves-effect waves-light" href="<?php echo base_url(); ?>index.php/Helpfaqs"><i class="fa fa-question righ-padd" aria-hidden="true"></i>help & faq</a>
                                    <a class="dropdown-item waves-effect waves-light" href="<?php echo base_url(); ?>index.php/contact_us"><i class="fa fa-address-book righ-padd" aria-hidden="true"></i>contact us</a>
									<a id="logout_user" class="dropdown-item waves-effect waves-light" onclick="logout_user();" href="javascript:void(0)"><i class="fa fa-power-off righ-padd" aria-hidden="true"></i>logout</a>
                                </div>

                    </div>


				</div>
			</div>
			<!--- End container-------->
			<!--- End container-------->

		</div>

		<!---end top bar bar----------->


<!----------------start postcode-------------------->


<!----------------End postcode--------------------->
		</div>
	</div>


		<!--------------------Start breadcrumbs Care Section------------>

		<section class="termandconditions">
				<div class="container-fluid">
					<div class="row amend">
						<div class="col-sm-12 col-md-12 col-lg-2 desk-menu-dashboard">
						    <?php $this->load->view('dashboard/menu.php'); ?>
						</div>


						<div class="col-sm-12 col-md-12 col-lg-8 dashboard-panel-right">
						 
						<div class="large-screen-fix dashboardss">
							<div class="row justify-content-center">
              
								<?php $this->load->view($content); ?>
							</div>
							</div>
						</div>

						</div>


					</div>




				</div>



        <?php $this->load->view('dashboard/faq_slider'); ?>


        </section>

<!--------------------End breadcrumbs Care Section------------->


<!----------------Start Footer-------------------------->


    <?php $this->load->view('dashboard/dashfooter'); ?>
    	<!----end footer------------>

</div>
    <!-- SCRIPTS -->
    <!-- JQuery -->
    <script type="text/javascript" src="<?php echo base_url() ?>dashboard/js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/aos.js"></script>
    <script src="<?php echo base_url() ?>dashboard/js/jquery.easy_number_animate.js"></script>

    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="<?php echo base_url() ?>dashboard/js/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="<?php echo base_url() ?>dashboard/js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="<?php echo base_url() ?>dashboard/js/mdb.min.js"></script>
    <!-- common JavaScript -->
    <script type="text/javascript" src="<?php echo base_url() ?>dashboard/js/common.js"></script>

    <script type="text/javascript" src="<?php echo base_url() ?>dashboard/js/slideout.min.js"></script>

    <script type="text/javascript">
        $().ready(function(){
            $('[rel="tooltip"]').tooltip();

        });

        function rotateCard(btn){
            var $card = $(btn).closest('.card-container');
            console.log($card);
            if($card.hasClass('hover')){
                $card.removeClass('hover');
            } else {
                $card.addClass('hover');
            }
        }
    </script>


    <script type="text/javascript">



    $(document).ready(function(){

    $(".front.first").click(function(){

    $(".manual-flip.first").animate({
    height: "260px",
    margin: "0 0 30px"
    }, 0 ); // how long the animation should be
    });

    $(".back.first").click(function(){
    $(".manual-flip.first").animate({
    height: "260px",
    margin: "0"
    }, 0 ); // how long the animation should be
    });

    $(".front.second").click(function(){

    $(".manual-flip.second").animate({
    height: "260px",
    margin: "0 0 30px"
    }, 0 ); // how long the animation should be
    });

    $(".back.second").click(function(){
    $(".manual-flip.second").animate({
    height: "260px",
    margin: "0"
    }, 0 ); // how long the animation should be
    });

    $(".front.last").click(function(){

    $(".manual-flip.last").animate({
    height: "260px",
    margin: "0 0 30px"
    }, 0 ); // how long the animation should be
    });

    $(".back.last").click(function(){
    $(".manual-flip.last").animate({
    height: "260px",
    margin: "0"
    }, 0 ); // how long the animation should be
    });

    $('.updateaccount').hide();

    $('.editaccount').click(function(){
         $('.editaccount').hide();
         $('.updateaccount').show();
         $('#phonenum').prop('disabled', false);
         $('#phonenum').css('background-color','#ffff66');
         $('#phonenum').focus();

     });

    $('.updateaccount').click(function(){
         $('.editaccount').show();
         $('.updateaccount').hide();
         $('#phonenum').css('background-color','#f1f1f1');
         $('#phonenum').prop('disabled', true);
     });



        // Moving Home Smart Meter

    $(document).on('keypress','#Dial_Moving_Seventh',function () {
        $("#Dial_Moving_Eight").focus();
        $("#Dial_Moving_Eight").select();
    });
    $(document).on('keypress','#Dial_Moving_Sixth',function () {
        $("#Dial_Moving_Seventh").focus();
        $("#Dial_Moving_Seventh").select();
    });
    $(document).on('keypress','#Dial_Moving_Five',function () {
        $("#Dial_Moving_Sixth").focus();
        $("#Dial_Moving_Sixth").select();
    });
    $(document).on('keypress','#Dial_Moving_Fourth',function () {
        $("#Dial_Moving_Five").focus();
        $("#Dial_Moving_Five").select();
    });
    $(document).on('keypress','#Dial_Moving_Third',function () {
        $("#Dial_Moving_Fourth").focus();
        $("#Dial_Moving_Fourth").select();
    });
    $(document).on('keypress','#Dial_Moving_Second',function () {
        $("#Dial_Moving_Third").focus();
        $("#Dial_Moving_Third").select();
    });
    $(document).on('keypress','#Dial_Moving_First',function () {
        $("#Dial_Moving_Second").focus();
        $("#Dial_Moving_Second").select();
    });

    $(document).on('keypress','#Dial_Gas_Moving_Seventh',function () {
        $("#Dial_Gas_Moving_Eight").focus();
        $("#Dial_Gas_Moving_Eight").select();
    });
    $(document).on('keypress','#Dial_Gas_Moving_Sixth',function () {
        $("#Dial_Gas_Moving_Seventh").focus();
        $("#Dial_Gas_Moving_Seventh").select();
    });
    $(document).on('keypress','#Dial_Gas_Moving_Five',function () {
        $("#Dial_Gas_Moving_Sixth").focus();
        $("#Dial_Gas_Moving_Sixth").select();
    });
    $(document).on('keypress','#Dial_Gas_Moving_Fourth',function () {
        $("#Dial_Gas_Moving_Five").focus();
        $("#Dial_Gas_Moving_Five").select();
    });
    $(document).on('keypress','#Dial_Gas_Moving_Third',function () {
        $("#Dial_Gas_Moving_Fourth").focus();
        $("#Dial_Gas_Moving_Fourth").select();
    });
    $(document).on('keypress','#Dial_Gas_Moving_Second',function () {
        $("#Dial_Gas_Moving_Third").focus();
        $("#Dial_Gas_Moving_Third").select();
    });
    $(document).on('keypress','#Dial_Gas_Moving_First',function () {
        $("#Dial_Gas_Moving_Second").focus();
        $("#Dial_Gas_Moving_Second").select();
    });


    function logout_user() {

        $.ajax({
            url: '<?php echo base_url() ?>index.php/user/logout',
            type: 'post',
            dataType: 'json',
            success: function (response) {
                if (response.error == '0') {
                    window.location.href = '<?php echo base_url() ?>index.php/user/login';
                }
            }
        });

    }
            // active to link
            var current = location.href;
            //alert(current);
            //var dashboard = current.match(/overview/g);
            if( current.match(/billing/g) == 'billing' )
            {
                $('.billing').attr('class','billing actiove-link');
            }
            if( current.match(/meter_reading/g) == 'meter_reading' )
            {
                $('.meter_reading').attr('class','meter_reading actiove-link');
            }
            if( current.match(/payments/g) == 'payments' )
            {
                $('.payments').attr('class','payments actiove-link');
            }
            if( current.match(/pay_dashboard/g) == 'pay_dashboard' )
            {
                $('.payments').attr('class','payments actiove-link');
            }
            if( current.match(/usage/g) == 'usage' )
            {
                $('.usage').attr('class','usage actiove-link');
            }
            if( current.match(/moving_home/g) == 'moving_home' )
            {
                $('.moving_home').attr('class','moving_home actiove-link');
            }
            if( current.match(/referral/g) == 'referral' )
            {
                $('.referral').attr('class','referral actiove-link');
            }
            if( current.match(/account/g) == 'account' )
            {
                $('.accounts').attr('class','accounts actiove-link');
            }
            if( current.match(/messages/g) == 'messages' )
            {
                $('.communications').attr('class','communications actiove-link');
            }

            // submit meter reading
            $(document).on('submit', '#submit_reading_meter', function(e){

                e.preventDefault();

                // If values are not set, default them to zero
                var $dials = $( e.target ).find('input.e-dial-input').toArray();
                var isPopulated = $dials.some(function( el ){
                    return $(el).val() !== '';
                });

                if( isPopulated ){
                    $dials.forEach(function( el ) {
                        if ($(el).val() === '') $(el).val(0);
                    })
                }
                else {
                    $('#error_msg_elec').html('<div class="alert alert-danger" role="alert">Error! No readings provided.</div>');
                    setTimeout(function(){ $('#error_msg_elec').empty() }, 10000);
                    return false;
                };


                $.ajax({
                    url: '<?php echo base_url() ?>index.php/user/submit_reading',
                    type: 'post',
                    dataType: 'json',
                    data: $('#submit_reading_meter').serialize(),
                    success:function(response){
                      
                        if( response.error == '0' ){

                            $('#error_msg-elec').html('<div class="alert alert-success" role="alert">Reading Submitted!</div>');

                            setTimeout(function(){ $('#error_msg_elec').empty() }, 1000);

                            var today = new Date();
                            var dd = today.getDate();
                            var mm = today.getMonth()+1; //January is 0!
                            var yyyy = today.getFullYear();
                            if(dd<10){ dd='0'+dd; }
                            if(mm<10){ mm='0'+mm; }
                            var today = dd+'/'+mm+'/'+yyyy;

                            if( $('#gas_submit_reds') && $('#gas_submit_reds').val() ) {

                                $('#elec_button').css('visibility','hidden');
                                $('#elec_field_wrapper').css('background-color','#CCC');
                                $('#select_elec').removeClass('white-blue');
                                $('#select_elec').addClass('white-border');
                                $("#elec_icon").attr("src", base_url+"dashboard/images/dashboard/Reading/gas/elec-grey.svg");

                            }
                            else {
                                $('#gas_card_selected').trigger("click");
                            }
                            $('#select_elec').css('pointer-events','none');
                            $('#select_elec').addClass('form_opacity');
                            $('#first_elec_meter_reading').replaceWith('First Reading: '+$('#elec_submit_reds').val() + ' - ' + today );

                        }
                        else {

                            var errorss = response.errorDescription ;
                            $('#error_msg_elec').html('<div class="alert alert-danger" role="alert">Error! while submitting reading '+errorss+' </div>');
                            setTimeout(function(){ $('#error_msg_elec').empty() }, 10000);
                        }
                    }
                });
            });

            $(document).on('submit', '#submit_reading_meter_gas', function(e){
                e.preventDefault();

                // If values are not set, default them to zero
                var $dials = $( e.target ).find('input.g-dial-input').toArray();
                var isPopulated = $dials.some(function( el ){
                    return $(el).val() !== '';
                });

                if( isPopulated ){
                    $dials.forEach(function( el ) {
                        if ($(el).val() === '') $(el).val(0);
                    })
                }
                else {
                    $('#error_msg_gas').html('<div class="alert alert-danger" role="alert">Error! No readings provided.</div>');
                    setTimeout(function(){ $('#error_msg_gas').empty() }, 10000);
                    return false;
                };


                $.ajax({
                    url: '<?php echo base_url() ?>index.php/user/submit_reading_gas',
                    type: 'post',
                    dataType: 'json',
                    data: $('#submit_reading_meter_gas').serialize(),
                    success:function(response){
                        //console.log(response);
                        if( response.error == '0' ){
                            $('#error_msg_gas').html('<div class="alert alert-success" role="alert">Reading Submitted!</div>');

                            setTimeout(function(){ $('#error_msg_gas').empty() }, 1000);

                            var today = new Date();
                            var dd = today.getDate();
                            var mm = today.getMonth()+1; //January is 0!
                            var yyyy = today.getFullYear();
                            if(dd<10){ dd='0'+dd; }
                            if(mm<10){ mm='0'+mm; }
                            var today = dd+'/'+mm+'/'+yyyy;


                            if( $('#elec_submit_reds') && $('#elec_submit_reds').val()) {

                                $('#gas_button').css('visibility','hidden');
                                $('#gas_field_wrapper').css('background-color','#CCC');
                                $('#gas_card_selected').removeClass('white-gold');
                                $('#gas_card_selected').addClass('white-border');
                                $("#gas_icon").attr("src", base_url+"dashboard/images/dashboard/Reading/gas/gas.svg");

                            }
                            else {
                                $('#select_elec').trigger("click");
                            }
                            $('#gas_card_selected').css('pointer-events','none');
                            $('#gas_card_selected').addClass('form_opacity');
                            $('#first_gas_meter_reading').replaceWith('First Reading: '+$('#gas_submit_reds').val() + ' - ' + today );

                        }
                        else{

                            var errorss = response.errorDescription ;
                            $('#error_msg_gas').html('<div class="alert alert-danger" role="alert">Error! while submitting reading '+errorss+' </div>');
                            setTimeout(function(){ $('#error_msg_gas').empty() }, 10000);
                        }
                    }
                });
            });

            $(document).on('click', '#step_1', function(){
                    $.ajax({
                    url: '<?php echo base_url() ?>index.php/user/move_home_steps',
                    type: 'get',
                    beforeSend:function(){
                    $('.loading-page').fadeIn();
                    $('#main_content').fadeOut();
                    },
                    complete:function(){
                    $('.loading-page').fadeOut();
                    $('#main_content').fadeIn();
                    },
                    success:function(response){

          // var params = { width:1680, height:1050 };
          // var str = jQuery.param( params );
          // var pathname = window.location.pathname.str;
          // console.log(pathname);
                    var date = new Date();
                    var html_append = $('#response_dashboard').html(response);
                        datepicker_function(html_append);
                    }
                    });
                    });

        $(document).on('click', '#save_meter_reading',function(){
            $('#move_step_3').trigger('submit');
        })

            $(document).on('submit', '#move_step_3', function(e){
      e.preventDefault();
      // alert();
      // return false;
                    $.ajax({
                    url: '<?php echo base_url() ?>index.php/user/move_home_3',
                    type: 'get',
                    beforeSend:function(){
                    $('.loading-page').fadeIn();
                    $('#main_content').fadeOut();
                    },
                    complete:function(){
                    $('.loading-page').fadeOut();
                    $('#main_content').fadeIn();
                    },
                    success:function(response){
                    $('#response_dashboard').html(response);
                    }
                    });
                    });

        });

    function datepicker_function(response)
    {
    var dateage = new Date();
    dateage.setDate( dateage.getDate() - 0 );
    //dateage.setFullYear( dateage.getFullYear() - 16 );
    //alert(new Date(dateage));

     response.find('#datepicker').datepicker({
      format: "dd-mm-yyyy",
      //endDate: new Date(dateage),
      minDate:new Date(),
      autoclose: true,
      uiLibrary: 'bootstrap4'
    });
    }
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
          $('.showhidetabs .nav-tabs > li > a').click(function(event) {
            event.preventDefault(); //stop browser to take action for clicked anchor
            //get displaying tab content jQuery selector
            var active_tab_selector = $('.showhidetabs .nav-tabs > li.active > a').attr('href');
            //find actived navigation and remove 'active' css
            var actived_nav = $('.showhidetabs .nav-tabs > li.active');
            actived_nav.removeClass('active');
            //add 'active' css into clicked navigation
            $(this).parents('li').addClass('active');
            //hide displaying tab content
            $(active_tab_selector).removeClass('active');
            $(active_tab_selector).addClass('hide');
            //show target tab content
            var target_tab_selector = $(this).attr('href');
            $(target_tab_selector).removeClass('hide');
            $(target_tab_selector).addClass('active');
          });

        $(".weeklyu").hide();
        $(".monthlyu").show();
        $(".yearlyu").hide();

        $("#weeklyu").click(function(event) {
            event.preventDefault(); //stop brows
        $(".weeklyu").show();
        $(".monthlyu").hide();
        $(".yearlyu").hide();
        });
        $("#monthlyu").click(function(event) {
            event.preventDefault(); //stop brows
        $(".weeklyu").hide();
        $(".monthlyu").show();
        $(".yearlyu").hide();
        });
        $("#yearlyu").click(function(event) {
            event.preventDefault(); //stop brows
        $(".weeklyu").hide();
        $(".monthlyu").hide();
        $(".yearlyu").show();
        });

        $(".weekly-gas").hide();
        $(".weekly-electric").show();
        $(".elec-gas-buttons.weekbtn > li:nth-of-type(1)").addClass("clicked");

        $(".monthly-gas").hide();
        $(".monthly-electric").show();
        $(".elec-gas-buttons.monthlybtn > li:nth-of-type(1)").addClass("clicked");

        $(".yearly-gas").hide();
        $(".yearly-electric").show();
        $(".elec-gas-buttons.yearlybtn > li:nth-of-type(1)").addClass("clicked");

        $("a#weeklyuelectric").click(function(event) {
            event.preventDefault(); //stop brows
        $(".weekly-gas").hide();
        $(".weekly-electric").show();
        $(".elec-gas-buttons.weekbtn > li:nth-of-type(1)").addClass("clicked");
        $(".elec-gas-buttons.weekbtn > li:nth-of-type(2)").removeClass("clicked");
        });
        $("#weeklyugas").click(function(event) {
            event.preventDefault(); //stop brows
        $(".weekly-electric").hide();
        $(".weekly-gas").show();
        $(".elec-gas-buttons.weekbtn > li:nth-of-type(2)").addClass("clicked");
        $(".elec-gas-buttons.weekbtn > li:nth-of-type(1)").removeClass("clicked");
        });

        $("a#monthlyelectric").click(function(event) {
            event.preventDefault(); //stop brows
        $(".monthly-gas").hide();
        $(".monthly-electric").show();
        $(".elec-gas-buttons.monthlybtn > li:nth-of-type(1)").addClass("clicked");
        $(".elec-gas-buttons.monthlybtn > li:nth-of-type(2)").removeClass("clicked");
        });
        $("#monthlygas").click(function(event) {
            event.preventDefault(); //stop brows
        $(".monthly-electric").hide();
        $(".monthly-gas").show();
        $(".elec-gas-buttons.monthlybtn > li:nth-of-type(2)").addClass("clicked");
        $(".elec-gas-buttons.monthlybtn > li:nth-of-type(1)").removeClass("clicked");
        });

         $("a#yearlyelectric").click(function(event) {
            event.preventDefault(); //stop brows
        $(".yearly-gas").hide();
        $(".yearly-electric").show();
        $(".elec-gas-buttons.yearlybtn > li:nth-of-type(1)").addClass("clicked");
        $(".elec-gas-buttons.yearlybtn > li:nth-of-type(2)").removeClass("clicked");
        });
        $("#yearlygas").click(function(event) {
            event.preventDefault(); //stop brows
        $(".yearly-electric").hide();
        $(".yearly-gas").show();
        $(".elec-gas-buttons.yearlybtn > li:nth-of-type(2)").addClass("clicked");
        $(".elec-gas-buttons.yearlybtn > li:nth-of-type(1)").removeClass("clicked");
        });
    });

    </script>

    <script type="text/javascript">
        $(document).ready(function () {

            if ($(window).width() >= 1730)
            {
                $("#slideOut").toggleClass('showSlideOut');
                $('.slideOutTab').toggle("fast");
            }

            var current = location.href;
            if (current.match(/\/dashboard\//g) == 'dashboard') {
                $('.dashboard').attr('class', 'dashboard actiove-link');
            }

            this.$slideOut = $('#slideOut');

            // Slideout show
            this.$slideOut.find('.slideOutTab').on('click', function () {
                $("#slideOut").toggleClass('showSlideOut');
                $('.slideOutTab').toggle("fast");
            });

            this.$slideOut.find('#closy').on('click', function () {
                $("#slideOut").toggleClass('showSlideOut');
                $('.slideOutTab').toggle("fast");
            });
        });
    </script>

    <?php if ( $this->session->userdata('login_data')['account_status'] == 0 && $this->session->userdata('login_data')['junifer_account_active'] == 1 ) { ?>
        <input type="hidden" id="session_userid" value="<?php echo $this->session->userdata('login_data')['id'] ?>" >
        <script type="text/javascript">
            $(document).ready(function(){
                $.ajax({
                    url: '<?php echo base_url() ?>index.php/user/status_active',
                    type: 'post',
                    dataType: 'json',
                    data: { user_id: $('#session_userid').val() },
                    success:function(res){
                        console.log(res);
                        if( res.error == 0 ) {
                            setTimeout( function() { window.location.href = '<?php echo base_url() ?>index.php/user/dashboard'; }, 2000 );
                        }
                    }
                });
            });
        </script>
    <?php } ?>

</div>
</body>
</html>
