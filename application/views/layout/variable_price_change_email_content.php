

<!------------------------------- START MAIN BODY ------------------------------->

<tr>
    <td valign="top" id="templateBody">

        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
            <tbody class="mcnTextBlockOuter">
            <tr>
                <td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
                    <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                        <tr>
                            <td valign="top" width="600" style="width:600px;">
                                <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%; min-width:100%;" width="100%" class="mcnTextContentContainer">
                                    <tbody>
                                    <tr>
                                        <td valign="top" class="mcnTextContent" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;">

                                            <h1 style="margin: 50px 0px; text-align: left;">
                                                <span style="font-family: 'Quicksand', sans-serif; font-weight:normal; font-size:46px; color: #1C3659;">A message from our CEO</span></span>
                                            </h1>

                                           <a href="<?php echo base_url(); ?><?php echo $email_info['encoded_hash']; ?>"> <img alt="" src="https://www.eversmartenergy.co.uk/assets/images/barney.png" style="width:100%;max-width:1200px; padding-bottom: 0; display: inline !important; vertical-align: bottom;" class="mcnImage" width="600" align="middle">
                                           </a>

                                            <font style="font-family: 'Quicksand', sans-serif;">
                                                        <span style="font-size:16px"><br><br>
                                                            <strong>Customer No: <?= $email_info['customer_number']; ?></strong><br><br>
                                                            Hello <?= $email_info['name']; ?>,<br><br>
                                                            Thank you for being a loyal member of Eversmart!<br><br>
                                                             

  <strong>I started Eversmart with a clear Vision:</strong><br>
                                                            <ul style=" color: #EA495C; font-weight: 600;">
                                                                <li>To offer a more price sensitive alternative for gas and electricty consumers.</li>
                                                                <li>To offer a great service to customers.</li>
                                                                <li>To treat others the way I would want to be treated.</li>
                                                                <li>To innovate the gas and electricity industry with ground breaking technology.</li>
                                                            </ul>

                                                            <strong>We have invested millions into our systems and processes over the last twelve months to improve our company and the service that we offer. I want to make a commitment to all of our members!</strong><br><br>
                                                            <ul style=" color: #EA495C; font-weight: 600;">
                                                            <li>We will treat you better than any supplier you have had previously.</li>
                                                                <li>We will continue to invest in our systems and processes to make your experience better and better.</li>
                                                                <li>We will strive to remain as competitive as possible without compromising on quality.</li>
                                                                <li>When you join Eversmart, you will be treated with love and respect.</li>
                                                            </ul>
                                                            
                                                            As mentioned in the email sent out today, I have worked very closely with my team to create a way for all of our members to benefit, despite price increases that have been inevitable accross this industry. 
                                                            <br><br><br>
                                                            <table align="center">
                                                                <tr>
                                                                    <td style="width: 350px; height: 40px; background-color: #EA495C; text-align: center; border-radius: 6px;">
                                                                        <a style="width: 280px; height: 40px; text-decoration: none; color: white; border-radius: 6px; font-size:17px;" href="<?php echo base_url(); ?><?php echo $email_info['encoded_hash']; ?>">Join the Loyal Family Saver Club Now</a>
                                                                    </td>
                                                                </tr>
                                                            </table><br><br>
                                                            Barney Cook<br>
                                                            Founder and CEO 
                                                        </span>
                                            </font>
                                            <br><br><br>
                                        </td>

                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            </tbody>
        </table>
    </td>
</tr>

<!------------------------------- END MAIN BODY ------------------------------->