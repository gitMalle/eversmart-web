<!----------------start postcode-------------------->

			<section class="mt0 ">
				<div class="boiler_header-h">					
						<div class="container">						
							<div class="row">							
								<div class="col-md-10 mx-auto text-center">
									<span class="bolier_home-h-l">Privacy Policy</span>
									<span class="bolier_home-h-s">Join the Revolution and start Saving</span>
								</div>
							</div>							
						</div><!------------------End Container----------------->
							
				</div><!------------------End Postcode------------>
				
			</section>
<!----------------End postcode-------------------->		
		</div>
		</div>
			
		<!--------------------Start breadcrumbs Care Section------------>
		
		<section class="termandconditions cd-section">
		<div class="cd-section-grey">
				<div class="container">
					<div class="row terms-c-box">
						<div class="col-sm-12">
							<span class="term-c-h aos-item" data-aos="fade-in">At Eversmart Energy Ltd, we believe it's important to protect your privacy. 
							During our business relationship we'll obtain certain pieces of personal information about you ("personal data") and we will take every step to protect your personal details. </span>
							<p class="term-c-p aos-item" data-aos="fade-right">By using Eversmart Energy Ltd, you consent to the collection and use of this information by us. If we decide to change our privacy policy, we'll post those changes on this page so that you're always aware of what information we collect, how we use it and under what circumstances we disclose it.</p>
							<p class="term-c-p aos-item" data-aos="fade-right">All personal data held by us is protected under the Data Protection Act 1998. This privacy policy tells you what personal information we collect from you when you purchase products and services from us, use our website or contact us. It also tells you how we store and use your personal data, and how you can access and manage the information we hold. Our Privacy Policy explains how and why we collect information from you, how we store it and use it, and how you can access and manage this information. It also explains the security measures we take to protect your privacy, and outlines things we'll never do to compromise the protection of your personal information.</p>
							<p class="term-c-p aos-item" data-aos="fade-right">Using your personal information, we and our agents may use your information to:</p>
							
							<ul>
							<li><p class="term-c-p aos-item" data-aos="fade-right">Provide you with the services you have requested.</p></li>
							<li><p class="term-c-p aos-item" data-aos="fade-right">Contact you for research about how we can improve the way we operate, or about how we can improve any services and products we provide.</p></li>
							<li><p class="term-c-p aos-item" data-aos="fade-right">Create analytical and statistical reports to analyse customer information, create profiles and create marketing opportunities.</p></li>
							<li><p class="term-c-p aos-item" data-aos="fade-right">Help detect and prevent fraud, debt and loss.</p></li>
							<li><p class="term-c-p aos-item" data-aos="fade-right">Help train our staff. We may use your calls and emails as a part of our quality monitoring and training and development processes as a part of a training and development scheme.</p></li>
							<li><p class="term-c-p aos-item" data-aos="fade-right">Contact you in any way (including email, phone, text or multimedia message or other forms of electronic communications) about products and services we and our selected partners may be offering.</p></li>
							</ul>
							
							<p class="term-c-p aos-item" data-aos="fade-right">If you communicate with us through the internet, we may occasionally contact you by email about our services and products. When you first sign up to Eversmart Energy Ltd, we will give you the opportunity to say whether you would be happy for us to contact you with marketing information. However, you can always contact us (at the address set out below) at any time if you change your mind.</p>
							
							<p class="term-c-p aos-item" data-aos="fade-right">If you have a meter installed at your property that can be connected to the Wide Area Network (a "smart" meter), the gas or electricity meter (or both) will record half-hourly consumption data and this can be remotely transferred in real time or on a daily basis (the equipment will automatically communicate with Eversmart Energy Ltd). We may use this information for the purposes set out above.</p>

							<p class="term-c-p aos-item" data-aos="fade-right">We may have to pass information about you to our agents and service providers. This may involve passing your information outside of the European Economic Area (EEA) to countries that do not have the same data protection standards as we do in the UK. If we, or our agents and service providers, do this, we will ensure we have the relevant legal protection in place and we'll always take reasonable steps to ensure that these organisations will secure the personal data. If we are asked, we may pass your information on to Ofgem for legal or regulatory purposes. We may also monitor and record any communications we have with you, including phone conversations and emails, for staff training and to ensure we are providing an excellent service to our customers.</p>


							<span class="term-c-h aos-item" data-aos="fade-in">Use of Your Personal Data </span>
							<span class="term-c-h aos-item" data-aos="fade-in">The Service </span>
							
							<p class="term-c-p aos-item" data-aos="fade-right">The app is to be used in connection with the Eversmart Pre‐payment Service and You should only use this app if You have an agreement with Us for this service. The terms and conditions applicable to this Service and this app are set out in the Agreement and can be seen at: <a href="<?php echo base_url(); ?>/index.php/Terms">http://eversmartenergy.co.uk/terms.php</a></p>

							
							<span class="term-c-h aos-item" data-aos="fade-in">Scanning Function </span>
							
							<p class="term-c-p aos-item" data-aos="fade-right">This app is to enable You to:</p>
							<p class="term-c-p aos-item" data-aos="fade-right">Scan the barcode on the back of the energy card You have received from Us.</p>
							<p class="term-c-p aos-item" data-aos="fade-right">For You to be able to use this functionality, You need to expressly choose to share certain types of Data with us. This is not automatic.</p>


							<span class="term-c-h aos-item" data-aos="fade-in">Use of the Camera </span>
							
							<p class="term-c-p aos-item" data-aos="fade-right">You can use Your camera function to scan the energy card. To use this functionality You do not need to allow the app to access Your photos library.</p>


							<span class="term-c-h aos-item" data-aos="fade-in">Your images </span>
							
							<p class="term-c-p aos-item" data-aos="fade-right">We will only access images that You specifically choose, and We will never scan or import Your photo library or camera roll. Access to Your images is to allow You to scan the energy card. You can stop sharing scanned images and revoke access at any time by changing the setting option on the app. You should not submit any photos to Us using this app or send photos that are contrary to the terms of Our agreement with You.</p>

							
							<span class="term-c-h aos-item" data-aos="fade-in">Use of the Data </span>
							
							<p class="term-c-p aos-item" data-aos="fade-right">We only use the Data provided to Us via this app for Us and/or our service providers to provide the Eversmart Service to You and do not share it with third parties other than as specified in Our agreement with You.</p>
							
							
							<span class="term-c-h aos-item" data-aos="fade-in">Contact Details </span>
							
							<p class="term-c-p aos-item" data-aos="fade-right">If You have any questions in relation to this privacy policy please contact:</p>
							<p class="term-c-p aos-item" data-aos="fade-right">Unit 10, Empress Business Park, Manchester, M16 9EA</p>

							
							<span class="term-c-h aos-item" data-aos="fade-in">Sources of information </span>
							
							<p class="term-c-p aos-item" data-aos="fade-right">When we contact you, we may use any information we hold about you to do so. We may contact you by email, phone, text or multimedia message (or other forms of electronic communications) or by visiting you.</p>

							
							<span class="term-c-h aos-item" data-aos="fade-in">Disclosing Information </span>
							
							<p class="term-c-p aos-item" data-aos="fade-right">We may allow other organisations and people to use information we hold about you for the following:</p>

							<ul>
								<li><p class="term-c-p aos-item" data-aos="fade-right">To provide any services you have requested.</p></li>
								<li><p class="term-c-p aos-item" data-aos="fade-right">To help manage any reward or loyalty schemes.</p></li>
								<li><p class="term-c-p aos-item" data-aos="fade-right">If we sell one or more of our businesses.</p></li>
							</ul>
							
							<p class="term-c-p aos-item" data-aos="fade-right">To help to prevent and detect debt, fraud, or loss. We may give this information to a credit-reference or a debt collection agency; if you have an outstanding balance, we may transfer your debt to another organisation and give them details about you and that debt; if we have been asked (for example, by Ofgem) to provide information for legal or regulatory purposes</p>
							
							<p class="term-c-p aos-item" data-aos="fade-right">From time to time these other people or organisations may be outside the European Economic Area (EEA), and as a result we may pass your information to countries that do not have the same standards or protection for personal information as the UK. If we, or our agents and service providers, do this, we will ensure we have the relevant legal protection in place and we'll always take reasonable steps to ensure that these organisations will secure the personal data.</p>
							
							<p class="term-c-p aos-item" data-aos="fade-right">You agree that we can ask your previous supplier for necessary information that will allow us to take over your supply, such as information regarding meter readings, meter technical details etc. Also, you agree that we can provide information we hold about you (such as information about meter readings and meter technical details) to your new supplier so they can begin supplying your gas and electricity.</p>

							<p class="term-c-p aos-item" data-aos="fade-right">Priority Service Register/Special Needs Conditions: If we believe that you (or a member of your household) need extra care (for example, because of your age, health, disability or financial circumstances), we may record and share this information. We will use this information so that we do not disrupt your supply.</p>

							<p class="term-c-p head aos-item" data-aos="fade-in">We may share your information with:</p>

							<ul>
								<li><p class="term-c-p aos-item" data-aos="fade-right">The electricity distributor</p></li>
								<li><p class="term-c-p aos-item" data-aos="fade-right">The relevant gas transporter, metering agents or network operator</p></li>
								<li><p class="term-c-p aos-item" data-aos="fade-right">Other energy suppliers if we believe you are considering changing supplier</p></li>
							</ul>

							
							<span class="term-c-h aos-item" data-aos="fade-in">Protecting Information </span>
							
							<p class="term-c-p aos-item" data-aos="fade-right">We follow strict security procedures to protect personal information. This includes following certain guidelines (for example, checking your identity when you phone us).</p>

							<p class="term-c-p aos-item" data-aos="fade-right">We strongly recommend that you do not disclose your login details to anyone. Please always remember to log out of your account when you have finished using it.</p>

							<p class="term-c-p aos-item" data-aos="fade-right">From time to time Eversmart Energy Ltd may include hypertext links to sites which are created by individuals and companies outside of Eversmart Energy Ltd. We do this when there is a particular relevance to the topic you're reading about and the site we take you to. Whilst we endeavour to check that the content of these sites is suitable, we cannot take any responsibility for the practices of the companies who publish the sites that we link to, nor the integrity of the content contained within them.</p>


							<span class="term-c-h aos-item" data-aos="fade-in">The Internet </span>
							
							<p class="term-c-p aos-item" data-aos="fade-right">What are cookies? A cookie is a piece of information stored in a small file which is sent to and from web pages. They can be used to identify that you've visited websites before and some will be stored on your computer by your web browser. They are anonymous, so they don't store any personal information of any kind.</p>
							
							<p class="term-c-p head aos-item" data-aos="fade-in">How does Eversmart Energy Ltd use cookies?</p>

							<p class="term-c-p aos-item" data-aos="fade-right">We use both cookies and / or tracking tags to understand how people use our websites and to help us to make your experience of our websites better. Some of them are essential for our websites to work properly.</p>

							<p class="term-c-p aos-item" data-aos="fade-right">For measuring web traffic we use a program called Google Analytics, which in turn uses cookies to help us find out how many people visit our websites, which pages and parts are most popular, how long people spend in each area and what information people are searching for. All this insight helps us to understand how we can improve our websites.</p>

							<p class="term-c-p aos-item" data-aos="fade-right">For logging into Eversmart Energy Ltd websites our customers and employees can log into all our websites and so we use cookies to track that they are logged in and what areas of the sites they use most.</p>

							<p class="term-c-p aos-item" data-aos="fade-right">For the content on our websites we use short-term cookies to recognise your PC as you move around our site and that any information you have entered, for example into search boxes, is remembered the next time you visit us.</p>

							<p class="term-c-p head aos-item" data-aos="fade-in">How can I manage my cookies?</p>

							<p class="term-c-p aos-item" data-aos="fade-right">If you don't want us to use cookies in your web browser, you can remove them from your computer or change your browser settings so that it either blocks cookies altogether or asks you if you'd like to accept them from some websites</p>

							<p class="term-c-p head aos-item" data-aos="fade-in">Where can I find more information?</p>

							<p class="term-c-p aos-item" data-aos="fade-right">The internet industry body, the Internet Advertising Bureau has set up a website to provide information and advice on cookies, tags and behavioural advertising <a href="http://www.youronlinechoices.com/uk/">http://www.youronlinechoices.com/uk/</a> and another trusted source which gives information about how to delete and control cookies is by the law firm Pinsent Masons <a href="http://www.aboutcookies.org/">http://www.aboutcookies.org/</a>.</p>

							<p class="term-c-p aos-item" data-aos="fade-right">If you have any other questions please visit our <a href="<?php echo base_url(); ?>/index.php/contact_us">Contact&nbsp;Us</a> page and we'll do our best to help you.</p>

							<p class="term-c-p aos-item" data-aos="fade-right">If we change our privacy and cookies policy substantially, we'll tell you by email.</p>

							<p class="term-c-p head ">Updated May 2018</p>
							<p class="term-c-p head aos-item" data-aos="fade-in">Dealing with your personal information<br>Collecting your personal information</p>

							<p class="term-c-p head aos-item" data-aos="fade-in">What will we collect?</p>

							<p class="term-c-p aos-item" data-aos="fade-right">We collect certain personal information from you, which is any information which might identify you. It does not include data where the identity has been removed (anonymous data).</p>

							
							<p class="term-c-p head aos-item" data-aos="fade-in">The information we collect includes:</p>
							
							<p class="term-c-p aos-item" data-aos="fade-right">Contact data (your name, email address, postal address, phone number, date of birth and any other relevant information we need in order to contact or identify you);</p>
							<p class="term-c-p aos-item" data-aos="fade-right">Financial data (your bank account and payment details relating to products and services you receive from us);</p>
							<p class="term-c-p aos-item" data-aos="fade-right">Technical data (internet protocol (IP) address, your login data (including your username and password), browser type and version, operating system, platform and other technology on the devices you use to access this website. We track technical data by using cookies;</p>
							<p class="term-c-p aos-item" data-aos="fade-right">Usage data (information about how you use our website, products and services);</p>
							<p class="term-c-p aos-item" data-aos="fade-right">Marketing and communications data (your preferences in receiving marketing from us and our third parties and your communication preferences); Energy supply data (this is your supply start date, meter technical details, tariff and if you have a smart meter installed at your property we’ll also capture your energy consumption data); and</p>
							<p class="term-c-p aos-item" data-aos="fade-right">Special categories of data (health and financial vulnerability data, if applicable, to include you on the Priority Services Register).</p>
							
							<p class="term-c-p aos-item" data-aos="fade-right">We also use aggregated data, in order to improve our operations and ensure we are providing you with the best service possible. All aggregated data is anonymised and doesn’t reveal your identity. </p>
							<p class="term-c-p aos-item" data-aos="fade-right">It’s really important that the personal data we hold about you is up to date so please let us know if your personal data changes at any time. </p>
							
							<p class="term-c-p head aos-item" data-aos="fade-in">When will we collect it?</p>
							
							<p class="term-c-p aos-item" data-aos="fade-right">We collect certain personal information from you during your sign-up journey and when you get in contact with us or use our website or as part of providing our services to you. </p>
							<p class="term-c-p aos-item" data-aos="fade-right">We may also monitor and record any communications we have with you, including phone calls and emails, to make sure we are providing an excellent service to our customers.</p>

							<p class="term-c-p head aos-item" data-aos="fade-in">Using your personal information</p>
							
							<p class="term-c-p aos-item" data-aos="fade-right">We’ll only use your personal information where we’re allowed to by law. Generally, this will be under one or more of the following circumstances: </p>
							<p class="term-c-p aos-item" data-aos="fade-right">To honour the existing contract that we hold with you;</p>
							<p class="term-c-p aos-item" data-aos="fade-right">it’s necessary for us to run our business and give you the best possible service;</p>
							<p class="term-c-p aos-item" data-aos="fade-right">where we need to comply with a legal or regulatory obligation; and/or</p>
							<p class="term-c-p aos-item" data-aos="fade-right">for marketing purposes where you have agreed to us using your personal data, see the “Marketing Information” section below for more information.</p>
							
							<p class="term-c-p head aos-item" data-aos="fade-in">Contacting you</p>
							
							<p class="term-c-p aos-item" data-aos="fade-right">We’ll contact you by the method we’ve agreed with you in our terms and conditions.</p>

							<p class="term-c-p head aos-item" data-aos="fade-in">Marketing information</p>

							<p class="term-c-p aos-item" data-aos="fade-right">When you first provide your personal information to EVERSMART ENERGY, we’ll give you the chance to choose whether you’d be happy for the EVERSMART ENERGY group to contact you with marketing information. If you choose to be contacted, we may use your identity, contact details , usage and profiled data to tell you about exciting new products and services from EVERSMART ENERGY and our related businesses in the wider EVERSMART ENERGY group (or they may contact you directly) including for up to 12 months after you've left EVERSMART ENERGY.</p>

							<p class="term-c-p aos-item" data-aos="fade-right">The EVERSMART ENERGY group includes EVERSMART ENERGY’s related companies such as EVERSMART LTD and other subsidiaries of EVERSMART ENERGY Group Limited from time to time.</p>

							<p class="term-c-p aos-item" data-aos="fade-right">If you decide you want to opt-out of receiving marketing communications after you have opted-in, or want to change how you receive them, you can always contact us at <a href="mailto:GDPR@eversmartenergy.co.uk">GDPR@eversmartenergy.co.uk</a> at any time. This won’t affect any marketing information we sent to you before you let us know.</p>

							<p class="term-c-p aos-item" data-aos="fade-right">We’ll occasionally remind you that you can update your preferences for receiving marketing information, or if you haven’t told us what your preferences are, we might get in touch to find out.</p>

							<p class="term-c-p head aos-item" data-aos="fade-in">Disclosing your personal information</p>

							<p class="term-c-p aos-item" data-aos="fade-right">We may allow the following types of organisations to use personal information we hold about you, including:</p>

							<p class="term-c-p aos-item" data-aos="fade-right">service providers who provide engineering services, industry data collection and aggregation, IT and system administration services;</p>
							<p class="term-c-p aos-item" data-aos="fade-right">marketing agencies to provide you with information about our products and services or other products and services which may be of interest to you (provided you’ve given your consent) and to help manage any reward or loyalty schemes;</p>
							<p class="term-c-p aos-item" data-aos="fade-right">price comparison sites which might take your details where you decide to request a quote or switch to us through that site; and credit-reference and debt collection agencies. For more information about credit reference agencies and how they use personal information please see the Credit Reference Agency Information Notice.</p>
							<p class="term-c-p aos-item" data-aos="fade-right">professional advisers such as lawyers, bankers, auditors and insurers who provide consultancy, banking, legal, insurance and accounting services;</p>
							<p class="term-c-p aos-item" data-aos="fade-right">regulators and other authorities based in the UK who we need to report to about what we’re doing with your personal data in certain circumstances;</p>
							<p class="term-c-p aos-item" data-aos="fade-right">Our third parties, who only process data on our behalf won’t use your personal data for their own purposes and we only permit them to use it in accordance with our instructions and the law.</p>

							<p class="term-c-p head aos-item" data-aos="fade-in">Disclosing information outside the EU</p>

							<p class="term-c-p aos-item" data-aos="fade-right">Sometimes the organisations listed above may be outside the European Economic Area (EEA), so we may pass your personal information to countries that do not have the same standards or protection for personal information as the UK. If we, our agents or our service providers do this, we’ll always make sure that these organisations adequately secure your personal information.</p>

							<p class="term-c-p head aos-item" data-aos="fade-in">Switching</p>

							<p class="term-c-p aos-item" data-aos="fade-right">We may also ask your previous energy supplier for personal information about you that will allow us to take over your supply (for example, information about your meter readings, meter technical details etc.). Similarly, if you switch away from EVERSMART ENERGY, we may provide similar information to your new supplier, so they can begin supplying your energy.</p>

							<p class="term-c-p head aos-item" data-aos="fade-in">Priority Services Register or special needs</p>

							<p class="term-c-p aos-item" data-aos="fade-right">If we believe that you (or a member of your household) need extra care (for example, because of your age, health or disability) and we have your explicit consent to do so, we may record and share this information with the following people to ensure your supply isn’t interrupted:</p>
							<p class="term-c-p aos-item" data-aos="fade-right">other energy suppliers if we believe you are considering changing supplier;</p>
							<p class="term-c-p aos-item" data-aos="fade-right">an outsourced service company that manages a wide range of energy industry data interactions on our behalf;</p>
							<p class="term-c-p aos-item" data-aos="fade-right">the electricity distributor; and</p>
							<p class="term-c-p aos-item" data-aos="fade-right">the relevant gas transporter, metering agents or network operator.</p>

							<p class="term-c-p head aos-item" data-aos="fade-in">Protecting your personal information</p>

							<p class="term-c-p aos-item" data-aos="fade-right">We follow strict security procedures to protect your personal information. This includes following certain guidelines (for example, checking your identity when you phone us).</p>
							<p class="term-c-p aos-item" data-aos="fade-right">We strongly recommend that you do not disclose any EVERSMART ENERGY login details to anyone. Please always remember to logout of your account when you have finished using any of EVERSMART ENERGY’s websites.</p>
							<p class="term-c-p aos-item" data-aos="fade-right">From time to time, our website may provide hypertext links to sites which are created by individuals and companies outside of EVERSMART ENERGY. We do this if the site is relevant to the topic you’re reading about. Whilst we always try to check that the content of these sites is suitable, we cannot take any responsibility for the practices of the companies who publish the sites that we link to, or the accuracy or relevance of the content on them.</p>

							<p class="term-c-p head aos-item" data-aos="fade-in">How long we'll use your personal information</p>

							<p class="term-c-p aos-item" data-aos="fade-right">We’ll only retain your personal information for as long as necessary for the reason we collected it, including for any legal requirements we have to comply with.</p>
							<p class="term-c-p aos-item" data-aos="fade-right">When deciding on how long to retain your personal information for, we consider the amount, nature, and sensitivity of the personal information, the potential risk of harm from unauthorised use or disclosure of your personal information, the purposes for which we process your personal information and whether we can achieve those purposes through other means, and any applicable legal requirements.</p>
							<p class="term-c-p aos-item" data-aos="fade-right">Details of retention periods for different aspects of your personal information are available in our retention policy which you can request by contacting us.</p>

							<p class="term-c-p head aos-item" data-aos="fade-in">Your legal rights</p>

							<p class="term-c-p aos-item" data-aos="fade-right">You have various rights in relation to your personal information. These rights are as follows:</p>
							<p class="term-c-p aos-item" data-aos="fade-right">request access to your personal information - you can request a copy of the personal information we hold on you;</p>
							<p class="term-c-p aos-item" data-aos="fade-right">request correction of your personal information - if any personal information we hold on you is incorrect, you can request to have it corrected;</p>
							<p class="term-c-p aos-item" data-aos="fade-right">request erasure of your personal information - you can ask us to delete your personal information in certain circumstances;</p>
							<p class="term-c-p aos-item" data-aos="fade-right">object to processing or restrict processing of your personal information - you may object to our processing of your personal data in certain circumstances;</p>
							<p class="term-c-p aos-item" data-aos="fade-right">request the transfer of your personal information - to provide you, or a third party you have chosen, with your personal information; and withdraw consent where we are relying on consent to process your personal information - if you withdraw your consent, we may not be able to provide certain products or services to you. We'll advise you if this is the case at the time you withdraw your consent.</p>

							<p class="term-c-p head aos-item" data-aos="fade-in">To exercise your rights in relation to your personal information, please email <a href="mailto:GDPR@eversmartenergy.co.uk">GDPR@eversmartenergy.co.uk</a></p>
							<p class="term-c-p aos-item" data-aos="fade-right">You are able to exercise your rights free of charge, but if you make unfounded, repetitive or excessive requests, we may charge you to carry these out or refuse to act on such requests.</p>
							<p class="term-c-p aos-item" data-aos="fade-right">We’ll try to respond to all requests within one month. If your request is complex or if you make lots of requests, we may extend our time to respond – if this is the case, we’ll let you know.</p>
															
						</div>
					</div>
					
					
					
				</div>
				</div>			
			</section>
			
<!--------------------End breadcrumbs Care Section------------->
