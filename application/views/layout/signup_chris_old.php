<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Eversmart | Registrations</title>
	 <!-- Font material icon -->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons"rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:500" rel="stylesheet">
	  <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="<?php echo base_url(); ?>assets/css/mdb.min.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
	   <!-- Your custom styles (optional) -->
    <link href="<?php echo base_url(); ?>assets/css/responsive.css" rel="stylesheet">
	<!--- Font Family Add---------------->
	<link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/aos.css" />

<link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>assets/img/favicon.png" />


    <style type="text/css">
      body{font-family: 'Quicksand', sans-serif!important; }
  #redd-btn-pc{padding:18px 29px!important}

.nogo {
    pointer-events: none;
    opacity: 0.6;
}

.registration-page-heading-energy {padding-bottom: 20px!important;}

  .loading-page {
  margin: 24% auto;
  width: 100px
   }
.bank-titles {
    text-align: center;
    font-size: 32px;
    display: block;
    margin-bottom: 25px;
    position: relative;
    margin-top: 40px;
}
 
.mobileonly.dashbaordmobilemenu {
    position: absolute;
    top: 8px!important;
    font-size: 45px;
    left: 0px;
    padding: 5px;
    cursor: pointer;
    z-index: 9999;
    color: white;
}
#account_name {text-align:center; margin-top:20px;}

.account-wrap input
{
max-width:20px;
text-align:center; 
font-size:32px; 
padding:8px 18px; 
border: 1px solid #ced4da!important;
margin:3px;
}
.account-wrap {
    text-align: center;
}
   .loader {
    border: 8px solid #fda1b8;
    border-radius: 50%;
    border-top: 8px solid #f64d76;
    width: 90px;
    height: 90px;
    -webkit-animation: spin 2s linear infinite; /* Safari */
    animation: spin 2s linear infinite;
  }

  /* Safari */
  @-webkit-keyframes spin {
    0% { -webkit-transform: rotate(0deg); }
    100% { -webkit-transform: rotate(360deg); }
  }

  @keyframes spin {
    0% { transform: rotate(0deg); }
    100% { transform: rotate(360deg); }
  }




/**
* The CSS shown here will not be introduced in the Quickstart guide, but shows
* how you can use CSS to style your Element's container.
*/
.StripeElement {background-color: white;padding:20px 12px 9px;border-bottom: 1px solid #ced4da;}
.StripeElement--focus {box-shadow: 0 1px 3px 0 #cfd7df;}

#example1-name--focus {box-shadow: 0 1px 3px 0 #cfd7df;}
.StripeElement--invalid {border-color: #fa755a;}
.StripeElement--webkit-autofill {background-color: #fefde5 !important;}
.card-element{ padding-top:20px; color:#ced4da; font-size:25px; margin-bottom:40px; font-weight:500; }
.gas_card{padding:40px 80px;}
.payment-details {display: none; position: relative; width: 100%; margin-bottom: 30px;border-radius: 10px; color: rgba(0,0,0, 0.87); background: #fff;
    box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 1px 5px 0 rgba(0, 0, 0, 0.12); padding:30px 20px;}
.ElementsApp, .ElementsApp .InputElement{font-size:17px;}
.pay-detail{font-size:18px; line-height:23px; float:left; width:100%; margin:5px 0px; color:#000; padding:20px;}
.pay-price {font-size: 22px;font-weight: 500;width: 100%;float: left;color: #ea495c;line-height: 35px;padding: 0 20px 20px;}
.pay-pay-now{font-size:35px; font-weight:500; width:100%; float:left; color:#000; line-height:40px; padding:20px;}
#example1-name{margin-bottom: 10px; border-bottom: 1px solid #ced4da!important; border:none;}
.StripeElement{float:left; width:100%;}

.process-tab-cs{padding-top:8px;}
.energy-process-icon{width:90px; height:auto!important;}
#active-process{color:#000;}
.days-process{color:#1cb81c; font-size:16px; font-weight:bold; width:33%; text-align:center; float:left; margin-bottom:10px; clear: both;}
.dash-over-text-green{color:#1cb81c; font-size:16px; font-weight:bold; width:100%; text-align:center; float:left; }
.dash-over-text{padding:0px 0px 15px 0px;}
.energy-main-process{padding:50px 0px; float:left; width:100%;}
.energy-process-icon{ background:#fff; padding:15px;}
.checkmark{width: 56px;height: 56px;border-radius: 50%;display: block;stroke-width: 2;stroke: #fff;stroke-miterlimit: 10;margin: 60px auto; position: relative;}
.User-number-eversmart{font-size:45px; color:#fc5462; font-weight:400;float: left;width: 100%;text-align: center; margin: 30px 0px; }
.dont-have{width:100%; float:left; text-align:center; margin:10px 0px 30px; 0px;}
.dont-have a{color:#3c99e7; font-size:14px; font-weight:400;}
.main_card_box .gas_card{width:100%}
.fre-ques{float:left;font-size:20px; width:100%; margin:23px 0px 10px; color:#666; font-weight:400;}
.accordion{float:left; width:100%;}
.pay-form-db-tab{float:left; width:100%;}
.StripeElement {margin-bottom:10px;}
#example1-name::placeholder{opacity: 1;color: #aab7c4; font-weight:400; font-family: 'Quicksand',! sans-serif important;}
#card-element{position:relative;}
#card-type-icon1{position:absolute; right: 9px;top:22px;z-index:1;}
#card-type-icon1.fa-cc-visa{font-size:18px;}
.secure-pay-strip{padding: 170px 0px 10px!important;}
.left-tab-side ul li a{font-size:19px;}
.__PrivateStripeElement-input::placeholder{font-family: 'Quicksand',! sans-serif important;}
.InputElement::placeholder{font-family: 'Quicksand',! sans-serif important;} 
img.img-fluid.stripe {padding-bottom: 25px;}
.card.card-raised2.card-form-horizontal.wow.fadeInUp.pay {margin-bottom: 0; border: none; box-shadow: none;}
button.red-btn.btn.btn-md.btn-eversmart.btn-round.weight-300.text-center.wow.fadeInUp.switchButton.waves-effect.waves-light.pay {font-size: 1.3rem;}
.InputElement{border-top:none;border-left:none;border-right:none;}
	
.amountyearly {
    display: block;
    font-size: 60px;
    color: #ea495c;
    font-weight: 800;
    text-align: center;
    margin: 0px 0 25px;
    padding-top: 100px;
}
section.termandconditions {
    background: #ea495c url(/assets/img/balalaa.png) bottom;
    background-repeat: no-repeat;
}
.back_step .material-icons {
    position: absolute;
    left: -75px;
    top: 50px;
}


#signup-continue-yearlypayments {
    display: none;
}

  </style>



<style>
.password-box { margin:20px auto}
.password-progress {
  height: 12px;
  margin-top: 10px;
  overflow: hidden;
  background-color: #f5f5f5;
  border-radius: 4px;
  -webkit-box-shadow: inset 0 1px 2px rgba(0,0,0,.1);
  box-shadow: inset 0 1px 2px rgba(0,0,0,.1);
}

.progress-bar {
  float: left;
  height: 100%;
  background-color: #337ab7;
  -webkit-box-shadow: inset 0 -1px 0 rgba(0,0,0,.15);
  box-shadow: inset 0 -1px 0 rgba(0,0,0,.15);
  -webkit-transition: width .6s ease;
  -o-transition: width .6s ease;
  transition: width .6s ease;
}

.bg-red {
  background: #E74C3C;
  border: 1px solid #E74C3C;
}

.bg-orange {
  background: #F39C12;
  border: 1px solid #F39C12;
}

.bg-green {
  background: #1ABB9C;
  border: 1px solid #1ABB9C;
}
</style>


    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-79295711-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-79295711-1');
    </script>
    
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','<?php echo $this->config->item('gtm_id'); ?>');</script>
    <!-- End Google Tag Manager -->

    <!-- Hotjar Tracking Code for www.eversmartenergy.co.uk -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:1074765,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>

</head>

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T44QPZV"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->


<?php  //print_r($signup_info);?>

<div class="cd-panel cd-panel--from-left js-cd-panel-main">
   <header class="cd-panel__header">
   <a class="logo" id="logo-dash" href="<?php echo base_url(); ?>"><img class="evrmenu-log" src="<?php echo base_url(); ?>/assets/images/logo-eversmart-new.png"/></a>
      <a href="#0" class="cd-panel__close js-cd-close"> </a>
   </header>

   <div class="cd-panel__container">
      <div class="cd-panel__content">
      <nav id="userslideoutjs" class="user slideout-menu slideout-menu-right">
      <section class="menu-section">
        <ul class="menu-section-list">
            <?php  if(  empty($this->session->userdata('login_data')) ){?>
              <li><a href="<?php echo base_url(); ?>index.php/user/login">Login</a></li> 
           <?php } ?>

            <?php if($this->session->userdata('login_data')['junifer_account_active']==1||$this->session->userdata('login_data')['signup_type']==3){ // Only allow access to my account if they are Dyball or Junifer active ?>
                <li><a href="<?php echo base_url(); ?>index.php/user/account">My Account</a></li>
            <?php } ?>

            <li><a href="<?php echo base_url(); ?>index.php/Helpfaqs">Help & faq</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/contact_us">Contact us</a></li>
            <?php  if(  $this->session->userdata('login_data') ){?>
                <li><a href="#" onclick="logout_user()" id="logout_user">Log out</a></li>
            <?php } ?>
        </ul>
      </section>
    </nav>
    </div> <!-- cd-panel__content -->
   </div> <!-- cd-panel__container -->
</div> <!-- cd-panel -->
<body class="sky-blue-bg">
    <!-- Start your project here-->
 <div class="loading-page">
<div class="loader"></div>
</div>

	<div class="main-rounded-red-header" id="main_content" style="display:none">
		<div class="red-rounded-wave">
    <div class="topbar homemobile">
 
 <div class="container mobileonly">
     <div class="row">


       <span class="col-sm-12 col-md-4 col-lg-6 px-0 ">
       <a class="logo" href="<?php echo base_url(); ?>"><img class="evrmenu" src="<?php echo base_url(); ?>/assets/images/logo-eversmart-new.png"/></a>
       </span>

       <div class="col-sm-12 col-md-4 col-lg-4  pull-right no-padding mobile_menu text-right hideonmobile" id="mobile_menu" style="position: relative; float:left; padding-top:10px">
   

     <!--
           <p class="accnum">Account Number: </p>
         <span class="product-code-no-d">
               <?php
                 /*if( $this->session->has_userdata('login_data') ){
                    if( $this->session->userdata('login_data')['signup_type'] == 0 || $this->session->userdata('login_data')['signup_type'] == 1 ){
                     echo $this->session->userdata('login_data')['account_number'];
                     }
                     elseif($this->session->userdata('login_data')['signup_type'] == 3)
                     {
                       echo $this->session->userdata('login_data')['account_number'];
                     }
                 }else{
                   echo '-';
                 }*/
               ?>
       </span> -->
       </div>
       <div class="mobileonly dashbaordmobilemenu"><button class="toggle-button-menu-left cd-btn js-cd-panel-trigger" data-panel="main"><img class="responsive_bar_icon" src="<?php echo base_url(); ?>assets/images/menu_bar_responsive.svg" alt=""/> </button></div>
     

     </div>
   </div>

 <?php $this->load->view('layout/menu'); ?>
 </div>
		<!---end top bar bar----------->


<!----------------start postcode--------------------->


<!----------------End postcode--------------------->

			<!----------------start postcode-------------------->

		<section class="postcode_top mt0">
				<div class="postcode_box" id="background-none">
        <div class="container">


<?php //var_dump($signup_info['quotation_pay_energy']) ?>

<?php if ($signup_info['quotation_pay_energy'] === 'prepay') {?>
  <form action="JavaScript:void(0)" id="registraion-form-dyball" class="switch-form" name="">
              <div id="step_1">
							<div class="row">
								<div class="col-md-8 mx-auto main-top-margin-60">
								<!-- <span class="back_step"><a href="boiler-switch-five.html" class="material-icons" style="top:-138px;">
									<img src="<?php //echo base_url(); ?>assets/images/back.svg"></a></span> -->
									<div class="card card-raised2 card-form-horizontal wow fadeInUp" data-wow-delay="0ms">
										<div class="content" style="padding:60px 0px">
										<div class="col-md-10 mx-auto">
										<div class="registration-page-heading">
										Enter your Details<br><span>(All fields are required except phone number)</span>
										</div>
											<div class="md-form col-md-3 mb-4" style="float:left">
											<select class="custom-select usage_select" id="red-arrow" name="customer_title" tabindex="0" required>
													<option value="">Title</option>
													<option value="Mr" selected>Mr.</option>
													<option value="Mrs">Mrs.</option>
													<option value="Miss">Miss</option>
													<option value="Ms">Ms</option>
													<option value="Prof">Prof</option>
													<option value="Dr">Dr</option>
												</select>
										</div>

												<div class="col-md-9 mb-4" style="float:left">
													<div class="md-form">
														<input tabindex="1" class="form-control check_first" type="text" required name="fname" placeholder="First Name">
												<div class="valid_first" style="display:none">
												<div class="input-group-addon">
												  <a href="javascript:void(0)" ><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png"/></a>
												 </div>


												</div>

										  <div style="display:none" class="first_error">
										  <div class="input-group-addon">
												<a href="javascript:void(0)" ><img  class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/x-button.png"/></a>
										</div>
										</div>
                        </div>


												</div>

												<div class="col-md-12 mb-4" style=" clear:both">
													<div class="md-form" >
														<input tabindex="2" placeholder="Last Name" id="form1" class="form-control check_last" type="text" name="lname" required>
                            <div class="valid_last" style="display:none">
                            <div class="input-group-addon">
                              <a href="javascript:void(0)" ><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png"/></a>
                             </div>
                          </div>

						   <div style="display:none" class="last_error"><div class="input-group-addon">
                            <a href="javascript:void(0)" ><img  class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/x-button.png"/></a>
                           </div>
                         </div>


													</div>

												</div>


												<div class="col-md-12 mb-4">
													<div class="md-form">
														<input tabindex="3" placeholder="Email Address" id="email" class="form-control" type="email" name="email" required>
                            <div class="valid_email" style="display:none">
                            <div class="input-group-addon">
                              <a href="javascript:void(0)" ><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png"/></a>
                             </div>
                          </div>

						   <div style="display:none" class="email_error"><div class="input-group-addon">
                            <a href="javascript:void(0)" ><img  class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/x-button.png"/></a>
                           </div>
                         </div>
													</div>

												</div>

												<div class="col-md-12 mb-4">
													<div class="md-form show_hide_password">

														<input tabindex="5" data-toggle="tooltippass" title="Passwords must contain the following:<ul style='text-align: left; background: none; border: none;'><li>Uppercase & lowercase letters</li><li>Numbers</li><li>Symbols</li><li>Eight characters or more</li></ul>" data-html="true" placeholder="Enter Password" id="password" class="form-control" type="password" name="customer_password" required>

                                                        <div class="input-group-addon">
															<a href="javascript:void(0)" id="show_password"><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/energy/show-pw-icon.svg"/></a>
														  </div>
													</div>

												</div>

												<div class="col-md-12 mb-4">
													<div class="md-form">
														<input tabindex="6" placeholder="Confirm Password" id="confirm_password" class="form-control" type="password" name="customer_confirm_password" required>
                            <div class="valid_confirm_password" style="display:none">
                            <div class="input-group-addon">
                              <a href="javascript:void(0)" ><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png"/></a>
                             </div>
													</div>
													 <div style="display:none" class="confirm_password_error"><div class="input-group-addon">
                            <a href="javascript:void(0)" ><img  class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/x-button.png"/></a>
                           </div>
                         </div>
													</div>


												</div>

													<div class="col-md-12 mb-4">
													<div class="md-form">
														<input tabindex="7" placeholder="Telephone Number" id="telephone" class="form-control" type="text" name="telephone"required>
                            <div class="valid_telephone" style="display:none">
                            <div class="input-group-addon">
                              <a href="javascript:void(0)" ><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png"/></a>
                             </div>
													</div>
												 <div style="display:none" class="telephone_error"><div class="input-group-addon">
                            <a href="javascript:void(0)" ><img style="right:14px" class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/x-button.png"/></a>
                           </div>
                         </div>

													</div>
                         					</div>

													<div class="col-md-12 mb-4">
													<div class="md-form">
														<input tabindex="8" placeholder="Phone Number  (Optional)" id="phone_number" class="form-control" onkeypress="return isNumberKey(event)" type="text" name="customer_phone">
                            <div class="valid_phone_number" style="display:none">
                            <div class="input-group-addon">
                              <a href="javascript:void(0)" ><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png"/></a>
                             </div>
													</div>

													  <div style="display:none" class="phone_number_error"><div class="input-group-addon">
                            <a href="javascript:void(0)" ><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/x-button.png"/></a>
                           </div>
                         </div>
													</div>

													</div>

													<div class="col-md-12 mb-4">
													<div class="md-form">
														<input tabindex="9" placeholder="Date of Birth in DD-MM-YYYY" id="datepicker" onkeypress="return isNumberKey(event)" maxlength="10" class="form-control date-picker-cal date-o-b" type="text" name="customer_dob"required>
                            <div class="valid_datepicker" style="display:none">
                            <div class="input-group-addon">
                              <a href="javascript:void(0)" ><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png"/></a>
                             </div>
													</div>
													</div>
													</div> 

													<div class="col-md-12 mb-4">
													<div class="md-form">
														
                            <input tabindex="10" placeholder="Supply Address" id="quotation_first_line_address" class="form-control quotation_first_line_address" type="text" name="quotation_first_line_address" value="<?php echo $signup_info['quotation_first_line_address']; ?>" readonly>
                          
                           <?php  if(!empty($signup_info['quotation_dplo_address'])) {?>
                           
                            <input tabindex="10" placeholder="Supply Address" id="quotation_dplo_address" class="form-control quotation_dplo_address" type="text" name="quotation_dplo_address" value="<?php echo $signup_info['quotation_dplo_address']; ?>" readonly>
                            <?php } ?> 

                            <input tabindex="10" placeholder="Supply Address" id="quotation_town_address" class="form-control quotation_town_address" type="text" name="quotation_town_address" value="<?php echo $signup_info['quotation_town_address']; ?>" readonly>

                            <input tabindex="10" placeholder="Supply Address" id="quotation_city_address" class="form-control quotation_city_address" type="text" name="quotation_city_address" value="<?php echo $signup_info['quotation_city_address']; ?>" readonly>

                            <input tabindex="10" placeholder="Supply Address" id="postcode" class="form-control postcode" type="text" name="postcode" value="<?php echo  $signup_info['postcode'] ?>" readonly>
  
													</div>
                        </div>
                        <div class="error_msg"></div>

                        <input type="hidden" name="plan_price" value="<?php if( !empty($summary_from_info['plan_price']) ){ echo $summary_from_info['plan_price']; } ?>">
                        <input type="hidden" name="original_price" value="<?php if( !empty($summary_from_info['plan_price']) ){ echo $summary_from_info['original_price']; } ?>">
                        <input type="hidden" name="complete_price" value="<?php if( !empty($summary_from_info['plan_price']) ){ echo $summary_from_info['complete_price']; } ?>">
                        <input type="hidden" name="plan_id" value="<?php if( !empty($summary_from_info['plan_price']) ){ echo $summary_from_info['plan_id']; } ?>">

<div style="text-align:center">
<form id="complete_form_dyball">
<button style="font-size:18px; padding:10px 84px; width:100%;" class="red-btn btn btn-md btn-eversmart btn-round weight-300 text-center wow fadeInUp switchButton waves-effect waves-light " type="submit">Continue</button>
</form>
</div>

                        </div>


											</div>
										</div>
									</div>

								</div>
                <!-- row  -->
              </form>


              <?php } elseif ($monthyear === 'yearlypay') {?> 

			
<form action="JavaScript:void(0)" id="registraion-form-yearpay" class="switch-form" name="">
<div id="step_1">
<div class="row">
  <div class="col-md-8 mx-auto main-top-margin-60">
  <!-- <span class="back_step"><a href="boiler-switch-five.html" class="material-icons" style="top:-138px;">
    <img src="<?php //echo base_url(); ?>assets/images/back.svg"></a></span> -->
    <div class="card card-raised2 card-form-horizontal wow fadeInUp" data-wow-delay="0ms">
      <div class="content" style="padding:60px 0px">
      <div class="col-md-10 mx-auto">
      <div class="registration-page-heading">
      Enter your Details<br><span>(All fields are required except phone number)</span>
      </div>
        <div class="md-form col-md-3 mb-4" style="float:left">
        <select class="custom-select usage_select" id="red-arrow" name="customer_title" tabindex="0" required>
            <option value="">Title</option>
            <option value="Mr" selected>Mr.</option>
            <option value="Mrs">Mrs.</option>
            <option value="Miss">Miss</option>
            <option value="Ms">Ms</option>
            <option value="Prof">Prof</option>
            <option value="Dr">Dr</option>
          </select>
      </div>

          <div class="col-md-9 mb-4" style="float:left">
            <div class="md-form">
              <input tabindex="1" class="form-control check_first" type="text" required name="fname" placeholder="First Name">
          <div class="valid_first" style="display:none">
          <div class="input-group-addon">
            <a href="javascript:void(0)" ><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png"/></a>
           </div>


          </div>

        <div style="display:none" class="first_error">
        <div class="input-group-addon">
          <a href="javascript:void(0)" ><img  class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/x-button.png"/></a>
      </div>
      </div>
          </div>


          </div>

          <div class="col-md-12 mb-4" style=" clear:both">
            <div class="md-form" >
              <input tabindex="2" placeholder="Last Name" id="form1" class="form-control check_last" type="text" name="lname" required>
              <div class="valid_last" style="display:none">
              <div class="input-group-addon">
                <a href="javascript:void(0)" ><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png"/></a>
               </div>
            </div>

 <div style="display:none" class="last_error"><div class="input-group-addon">
              <a href="javascript:void(0)" ><img  class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/x-button.png"/></a>
             </div>
           </div>


            </div>

          </div>


          <div class="col-md-12 mb-4">
            <div class="md-form">
              <input tabindex="3" placeholder="Email Address" id="email" class="form-control" type="email" name="email" required>
              <div class="valid_email" style="display:none">
              <div class="input-group-addon">
                <a href="javascript:void(0)" ><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png"/></a>
               </div>
            </div>

 <div style="display:none" class="email_error"><div class="input-group-addon">
              <a href="javascript:void(0)" ><img  class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/x-button.png"/></a>
             </div>
           </div>
            </div>

          </div>

          <div class="col-md-12 mb-4">
            <div class="md-form show_hide_password">

              <input tabindex="5" data-toggle="tooltippass" title="Passwords must contain the following:<ul style='text-align: left; background: none; border: none;'><li>Uppercase & lowercase letters</li><li>Numbers</li><li>Symbols</li><li>Eight characters or more</li></ul>" data-html="true" placeholder="Enter Password" id="password" class="form-control" type="password" name="customer_password" required>

                                          <div class="input-group-addon">
                <a href="javascript:void(0)" id="show_password"><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/energy/show-pw-icon.svg"/></a>
                </div>
            </div>

          </div>

          <div class="col-md-12 mb-4">
            <div class="md-form">
              <input tabindex="6" placeholder="Confirm Password" id="confirm_password" class="form-control" type="password" name="customer_confirm_password" required>
              <div class="valid_confirm_password" style="display:none">
              <div class="input-group-addon">
                <a href="javascript:void(0)" ><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png"/></a>
               </div>
            </div>
             <div style="display:none" class="confirm_password_error"><div class="input-group-addon">
              <a href="javascript:void(0)" ><img  class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/x-button.png"/></a>
             </div>
           </div>
            </div>


          </div>

            <div class="col-md-12 mb-4">
            <div class="md-form">
              <input tabindex="7" placeholder="Telephone Number" id="telephone" class="form-control" type="text" name="telephone"required>
              <div class="valid_telephone" style="display:none">
              <div class="input-group-addon">
                <a href="javascript:void(0)" ><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png"/></a>
               </div>
            </div>
           <div style="display:none" class="telephone_error"><div class="input-group-addon">
              <a href="javascript:void(0)" ><img style="right:14px" class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/x-button.png"/></a>
             </div>
           </div>

            </div>
                     </div>

            <div class="col-md-12 mb-4">
            <div class="md-form">
              <input tabindex="8" placeholder="Phone Number  (Optional)" id="phone_number" class="form-control" onkeypress="return isNumberKey(event)" type="text" name="customer_phone">
              <div class="valid_phone_number" style="display:none">
              <div class="input-group-addon">
                <a href="javascript:void(0)" ><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png"/></a>
               </div>
            </div>

              <div style="display:none" class="phone_number_error"><div class="input-group-addon">
              <a href="javascript:void(0)" ><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/x-button.png"/></a>
             </div>
           </div>
            </div>

            </div>

            <div class="col-md-12 mb-4">
            <div class="md-form">
              <input tabindex="9" placeholder="Date of Birth in DD-MM-YYYY" id="datepicker" onkeypress="return isNumberKey(event)" maxlength="10" class="form-control date-picker-cal date-o-b" type="text" name="customer_dob"required>
              <div class="valid_datepicker" style="display:none">
              <div class="input-group-addon">
                <a href="javascript:void(0)" ><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png"/></a>
               </div>
            </div>
            </div>
            </div> 

            <div class="col-md-12 mb-4">
            <div class="md-form">
              
              <input tabindex="10" placeholder="Supply Address" id="quotation_first_line_address" class="form-control quotation_first_line_address" type="text" name="quotation_first_line_address" value="<?php echo $signup_info['quotation_first_line_address']; ?>" readonly>
            
             <?php  if(!empty($signup_info['quotation_dplo_address'])) {?>
             
              <input tabindex="10" placeholder="Supply Address" id="quotation_dplo_address" class="form-control quotation_dplo_address" type="text" name="quotation_dplo_address" value="<?php echo $signup_info['quotation_dplo_address']; ?>" readonly>
              <?php } ?> 

              <input tabindex="10" placeholder="Supply Address" id="quotation_town_address" class="form-control quotation_town_address" type="text" name="quotation_town_address" value="<?php echo $signup_info['quotation_town_address']; ?>" readonly>

              <input tabindex="10" placeholder="Supply Address" id="quotation_city_address" class="form-control quotation_city_address" type="text" name="quotation_city_address" value="<?php echo $signup_info['quotation_city_address']; ?>" readonly>

              <input tabindex="10" placeholder="Supply Address" id="postcode" class="form-control postcode" type="text" name="postcode" value="<?php echo  $signup_info['postcode'] ?>" readonly>

            </div>
          </div>
          <div class="error_msg"></div>
          <div class="signup_msg" style="clear:both"></div>
          <input type="hidden" name="plan_price" value="<?php if( !empty($summary_from_info['plan_price']) ){ echo $summary_from_info['plan_price']; } ?>">
          <input type="hidden" name="original_price" value="<?php if( !empty($summary_from_info['plan_price']) ){ echo $summary_from_info['original_price']; } ?>">
          <input type="hidden" name="complete_price" value="<?php if( !empty($summary_from_info['plan_price']) ){ echo $summary_from_info['complete_price']; } ?>">
          <input type="hidden" name="plan_id" value="<?php if( !empty($summary_from_info['plan_price']) ){ echo $summary_from_info['plan_id']; } ?>">








            <div class="col-sm-12 pd0">
                <div class="tab-content " id="tab-data-box">
                    <span class="pay-pay-now text-center">Yearly Payment</span>
                    <div class="amountyearly">£<?php echo $year_price ?></div>
                </div>
            </div>

<div class="card card-raised2 card-form-horizontal wow fadeInUp pay" data-wow-delay="0ms">
				<div class="content pd0">
					<div class="tabbable tabs-left row">
						<div class="col-sm-12 pd0">
							<div class="tab-content " id="tab-data-box">
								<div id="billing_error_msg"></div>
								<div class="tab-pane active" id="b">
									<span class="pay-pay-now text-center">Payment Details</span>
									<div id="existing_billing_address">
										<?php
										$BillingAddress = $this->user_modal->get_customer_billing_addresses( $this->session->userdata('login_data')['customer_id'] );
										if(count($BillingAddress)>0) { ?>
											<select id="billing_address_id" name="billing_address_id">
												<option value="" selected="selected"><?=$this->session->userdata('login_data')['address']; ?>, <?=$this->session->userdata('login_data')['town_address']; ?>, <?=$this->session->userdata('login_data')['address_postcode']; ?> </option>
												<?php foreach ($BillingAddress as $key => $BillingAddressData) { ?>
													<option value="<?=$BillingAddressData['billing_address_id']?>"><?=$BillingAddressData['address_line_1']?>, <?=$BillingAddressData['address_line_2']?>, <?=$BillingAddressData['postcode']?></option>
												<?php } ?>
											</select>
										<?php } ?>
									</div>
									<input type="checkbox" name="toggle_new_card_address" id="toggle_new_card_address" style="display:none" />
									<span class="pay-form-db-tab">
									
											<div class="row">
												<div class="col-lg-12  col-sm-12 col-md-12">
													<div style="position:relative">
														<div id="card-type-icon1"  style="display: none;">
															<i class="fa fa-cc-visa" aria-hidden="true"></i>
															<i style="display:none" class="fa fa-cc-visa" aria-hidden="true"></i>
															<i style="display:none" class="fa fa-cc-amex" aria-hidden="true"></i>
															<i style="display:none" class="fa fa-cc-diners-club" aria-hidden="true"></i>
															<i style="display:none" class="fa fa-cc-discover" aria-hidden="true"></i>
															<i style="display:none" class="fa fa-cc-jcb" aria-hidden="true"></i>
															<i style="display:none" class="fa fa-cc-mastercard" aria-hidden="true"></i>
															<i style="display:none" class="fa fa-cc-paypal" aria-hidden="true"></i>
															<i style="display:none" class="fa fa-cc-stripe" aria-hidden="true"></i>
														</div>
														<div id="card-element" class="StripeElement"></div>
													</div>
													<input type="hidden" id="amount_pay" value="<?php echo $year_price ; ?>">
												
													<input type="hidden" id="existing_line1" value="">
													<input type="hidden" id="existing_postcode" value="">
													<input type="hidden" id="payment-email" value=">">
												
													<input class="StripeElement ElementsApp InputElement" id="example1-name" data-tid="elements_examples.form.name_placeholder" type="text" placeholder="Card Holder Name" required="" autocomplete="name" novalidate>
													<div id="card-dd_expire"></div>
													<div id="card-dd_ccv"></div>
													<!-- Used to display form errors. -->
													<div id="card-errors_dd" style="display: none; clear:both" role="alert" class="alert alert-danger"></div>
												</div>
												<div class="col-lg-12  col-sm-12 col-md-12 text-center">
													<button type="submit" id="stripebtn" style="margin-top:45px;" class="red-btn btn btn-md btn-eversmart btn-round weight-300 text-center wow fadeInUp switchButton waves-effect waves-light ">Submit Payment</button>
												</div>
											</div>
										
									</span>
								</div>
							</div>
						</div>
						<span class="pay-detail text-center">
							Security is one of the biggest considerations in everything we do. Stripe has been audited by a PCI-certified auditor and is certified to PCI Service Provider Level 1. This is the most stringent level of certification available in the payments industry. 
						</span>
					</div>
				</div>
			</div>









          <button id='signup-continue-yearlypayments' style="font-size:18px;padding:10px 84px;float: none;text-align: center;margin-left: auto;margin-right: auto!important;/* position: absolute; */left: 0;right: 0;margin-left: auto!important;width: 100%;" class="red-btn btn btn-md btn-eversmart btn-round weight-300 text-center wow fadeInUp switchButton waves-effect waves-light passbtn nogo" type="submit">Continue</button>


          </div>


        </div>
      </div>
    </div>

  </div>

    <div class="col-md-12" id="thank_you">
		<div class="payment-details" >
			<div class="col-md-10 col-sm-12 mx-auto ">
				<div class="gas_box_icon gap_icon_dashboard">
					<span class="progress-bar-step" id="swith-process-bar">
						<svg class="checkmark" xmlns="" viewBox="0 0 52 52">
							<circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none"/>
							<path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8"/>
						</svg>
					</span>
					<div class="content" style="padding:0px 0px 30px">
						<div class="col-md-10 mx-auto">
							<span class="registration-page-heading-energy-H">										
								<span id="Transaction_status"></span> 
								<span id="Transaction"></span> 
							</span>
							<span class="registration-page-energy-d confirm-pay" style="padding-top:0px!important">Please check your emails</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

  <!-- row  -->
</form>

              <?php } else {?> 

			
              <form action="JavaScript:void(0)" id="registraion-form" class="switch-form" name="">
              <div id="step_1">
							<div class="row">
								<div class="col-md-8 mx-auto main-top-margin-60">
								<!-- <span class="back_step"><a href="boiler-switch-five.html" class="material-icons" style="top:-138px;">
									<img src="<?php //echo base_url(); ?>assets/images/back.svg"></a></span> -->
									<div class="card card-raised2 card-form-horizontal wow fadeInUp" data-wow-delay="0ms">
										<div class="content" style="padding:60px 0px">
										<div class="col-md-10 mx-auto">
										<div class="registration-page-heading">
										Enter your Details<br><span>(All fields are required except phone number)</span>
										</div>
											<div class="md-form col-md-3 mb-4" style="float:left">
											<select class="custom-select usage_select" id="red-arrow" name="customer_title" tabindex="0" required>
													<option value="">Title</option>
													<option value="Mr" selected>Mr.</option>
													<option value="Mrs">Mrs.</option>
													<option value="Miss">Miss</option>
													<option value="Ms">Ms</option>
													<option value="Prof">Prof</option>
													<option value="Dr">Dr</option>
												</select>
										</div>

												<div class="col-md-9 mb-4" style="float:left">
													<div class="md-form">
														<input tabindex="1" class="form-control check_first" type="text" required name="fname" placeholder="First Name">
												<div class="valid_first" style="display:none">
												<div class="input-group-addon">
												  <a href="javascript:void(0)" ><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png"/></a>
												 </div>


												</div>

										  <div style="display:none" class="first_error">
										  <div class="input-group-addon">
												<a href="javascript:void(0)" ><img  class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/x-button.png"/></a>
										</div>
										</div>
                        </div>


												</div>

												<div class="col-md-12 mb-4" style=" clear:both">
													<div class="md-form" >
														<input tabindex="2" placeholder="Last Name" id="form1" class="form-control check_last" type="text" name="lname" required>
                            <div class="valid_last" style="display:none">
                            <div class="input-group-addon">
                              <a href="javascript:void(0)" ><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png"/></a>
                             </div>
                          </div>

						   <div style="display:none" class="last_error"><div class="input-group-addon">
                            <a href="javascript:void(0)" ><img  class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/x-button.png"/></a>
                           </div>
                         </div>


													</div>

												</div>


												<div class="col-md-12 mb-4">
													<div class="md-form">
														<input tabindex="3" placeholder="Email Address" id="email" class="form-control" type="email" name="email" required>
                            <div class="valid_email" style="display:none">
                            <div class="input-group-addon">
                              <a href="javascript:void(0)" ><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png"/></a>
                             </div>
                          </div>

						   <div style="display:none" class="email_error"><div class="input-group-addon">
                            <a href="javascript:void(0)" ><img  class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/x-button.png"/></a>
                           </div>
                         </div>
													</div>

												</div>

												<div class="col-md-12 mb-4">
													<div class="md-form show_hide_password">

														<input tabindex="5" data-toggle="tooltippass" title="Passwords must contain the following:<ul style='text-align: left; background: none; border: none;'><li>Uppercase & lowercase letters</li><li>Numbers</li><li>Symbols</li><li>Eight characters or more</li></ul>" data-html="true" placeholder="Enter Password" id="password" class="form-control" type="password" name="customer_password" required>

                                                        <div class="input-group-addon">
															<a href="javascript:void(0)" id="show_password"><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/energy/show-pw-icon.svg"/></a>
														  </div>
													</div>

												</div>

												<div class="col-md-12 mb-4">
													<div class="md-form">
														<input tabindex="6" placeholder="Confirm Password" id="confirm_password" class="form-control" type="password" name="customer_confirm_password" required>
                            <div class="valid_confirm_password" style="display:none">
                            <div class="input-group-addon">
                              <a href="javascript:void(0)" ><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png"/></a>
                             </div>
													</div>
													 <div style="display:none" class="confirm_password_error"><div class="input-group-addon">
                            <a href="javascript:void(0)" ><img  class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/x-button.png"/></a>
                           </div>
                         </div>
													</div>


												</div>

													<div class="col-md-12 mb-4">
													<div class="md-form">
														<input tabindex="7" placeholder="Telephone Number" id="telephone" class="form-control" type="text" name="telephone"required>
                            <div class="valid_telephone" style="display:none">
                            <div class="input-group-addon">
                              <a href="javascript:void(0)" ><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png"/></a>
                             </div>
													</div>
												 <div style="display:none" class="telephone_error"><div class="input-group-addon">
                            <a href="javascript:void(0)" ><img style="right:14px" class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/x-button.png"/></a>
                           </div>
                         </div>

													</div>
                         					</div>

													<div class="col-md-12 mb-4">
													<div class="md-form">
														<input tabindex="8" placeholder="Phone Number  (Optional)" id="phone_number" class="form-control" onkeypress="return isNumberKey(event)" type="text" name="customer_phone">
                            <div class="valid_phone_number" style="display:none">
                            <div class="input-group-addon">
                              <a href="javascript:void(0)" ><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png"/></a>
                             </div>
													</div>

													  <div style="display:none" class="phone_number_error"><div class="input-group-addon">
                            <a href="javascript:void(0)" ><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/x-button.png"/></a>
                           </div>
                         </div>
													</div>

													</div>

													<div class="col-md-12 mb-4">
													<div class="md-form">
														<input tabindex="9" placeholder="Date of Birth in DD-MM-YYYY" id="datepicker" onkeypress="return isNumberKey(event)" maxlength="10" class="form-control date-picker-cal date-o-b" type="text" name="customer_dob"required>
                            <div class="valid_datepicker" style="display:none">
                            <div class="input-group-addon">
                              <a href="javascript:void(0)" ><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png"/></a>
                             </div>
													</div>
													</div>
													</div> 

													<div class="col-md-12 mb-4">
													<div class="md-form">
														
                            <input tabindex="10" placeholder="Supply Address" id="quotation_first_line_address" class="form-control quotation_first_line_address" type="text" name="quotation_first_line_address" value="<?php echo $signup_info['quotation_first_line_address']; ?>" readonly>
                          
                           <?php  if(!empty($signup_info['quotation_dplo_address'])) {?>
                           
                            <input tabindex="10" placeholder="Supply Address" id="quotation_dplo_address" class="form-control quotation_dplo_address" type="text" name="quotation_dplo_address" value="<?php echo $signup_info['quotation_dplo_address']; ?>" readonly>
                            <?php } ?> 

                            <input tabindex="10" placeholder="Supply Address" id="quotation_town_address" class="form-control quotation_town_address" type="text" name="quotation_town_address" value="<?php echo $signup_info['quotation_town_address']; ?>" readonly>

                            <input tabindex="10" placeholder="Supply Address" id="quotation_city_address" class="form-control quotation_city_address" type="text" name="quotation_city_address" value="<?php echo $signup_info['quotation_city_address']; ?>" readonly>

                            <input tabindex="10" placeholder="Supply Address" id="postcode" class="form-control postcode" type="text" name="postcode" value="<?php echo  $signup_info['postcode'] ?>" readonly>
  
													</div>
                        </div>
                        <div class="error_msg"></div>
                        <div class="signup_msg" style="clear:both"></div>
                        <input type="hidden" name="plan_price" value="<?php if( !empty($summary_from_info['plan_price']) ){ echo $summary_from_info['plan_price']; } ?>">
                        <input type="hidden" name="original_price" value="<?php if( !empty($summary_from_info['plan_price']) ){ echo $summary_from_info['original_price']; } ?>">
                        <input type="hidden" name="complete_price" value="<?php if( !empty($summary_from_info['plan_price']) ){ echo $summary_from_info['complete_price']; } ?>">
                        <input type="hidden" name="plan_id" value="<?php if( !empty($summary_from_info['plan_price']) ){ echo $summary_from_info['plan_id']; } ?>">

												<span class="input-group-btn btnNew" id="continue_btn" style="padding-bottom:50px; ">
															<button style="font-size:18px; padding:10px 84px;width:100%;" id="next_step" class="red-btn btn btn-md btn-eversmart btn-round weight-300 text-center wow fadeInUp switchButton waves-effect waves-light passbtn nogo" type="submit">Continue</button>
												</span>


                        </div>


											</div>
										</div>
									</div>

								</div>
                <!-- row  -->
              </form>

<?php } ?>

						</div>
            <!-- step 1 -->
            <div id="step_2" style="display:none">
              <div class="row">
								<div class="col-md-8 mx-auto main-top-margin-60">
								<span class="back_step"><a href="javascript:void()" id="back_from" class="material-icons" style="top:-123px;">
									<img src="<?php echo base_url();?>assets/images/red-back-button.svg"></a></span>
									<div class="card card-raised2 card-form-horizontal wow fadeInUp" data-wow-delay="0ms">
										<div class="content" style="padding:20px 0px 60px">
										<div class="col-md-10 mx-auto">
										<span class="registration-page-heading-energy-H">
										Enter your Bank Details
										</span>
										<!---<span class="registration-page-energy-p">
										 	&#163; <?php// if( !empty($summary_from_info['complete_price']) ) { echo $summary_from_info['complete_price']; }else{ echo '-'; } ?>
										</span>--->

										<span class="registration-page-energy-d" style="font-size:15px">
										Your payments will be the same every month, and we'll take them direct from bank
										</span>

											</div>
										</div>
									</div>

								</div>
						</div>
            <div class="row">
								<div class="col-md-8 mx-auto">
									<div class="card card-raised2 card-form-horizontal wow fadeInUp" data-wow-delay="0ms">
										<div class="content" style="padding:60px 0px">
                      <form id="complete_form">
										<div class="col-md-10 mx-auto">
										<span class="registration-page-heading-energy">
										Because you selected Direct Debit as the payment mode <br>
										for Energy Payment, we require you're bank details
										</span>
                    <div id="form_submit" style="display:none"></div>
												<div class="col-md-12 " style="float:left; width:100%">
													<div class="md-form" >				

<div class="bank-titles">Account Number</div>
<div class="account-wrap">
<input type="text" maxlength="1" max="1" id="first-account-number" placeholder="0" onclick="this.select();"/>
<input type="text" maxlength="1" max="1" id="second-account-number" placeholder="0" onclick="this.select();"/>
<input type="text" maxlength="1" max="1" id="thrid-account-number" placeholder="0" onclick="this.select();"/>
<input type="text" maxlength="1" max="1" id="fourth-account-number" placeholder="0" onclick="this.select();"/>
<input type="text" maxlength="1" max="1" id="five-account-number" placeholder="0" onclick="this.select();"/>
<input type="text" maxlength="1" max="1" id="six-account-number" placeholder="0" onclick="this.select();"/>
<input type="text" maxlength="1" max="1" id="seven-account-number" placeholder="0" onclick="this.select();"/>
<input type="text" maxlength="1" max="1" id="eight-account-number" placeholder="0" onclick="this.select();"/>
</div>


														<input style="display:none" class="form-control" type="number" placeholder="Your Bank or society account Number" required name="account_number" id='account_number' maxlength="8">
														<!-- <label for="form1" class="bank-society-acc">Your Bank or society account Number<span class="required-field">*</span></label> -->
                            <div class="valid_account_number" style="display:none">
                            <div class="input-group-addon">
                              <a href="javascript:void(0)" ><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png"/></a>
                             </div>
													</div>
													</div>
                          <div style="display:none" class="account_number_error"><div class="input-group-addon">
                            <a href="javascript:void(0)" ><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/x-button.png"/></a>
                           </div>
                         </div>
												</div>
												<div class="col-md-12 " style=" float:left; width:100%">
													<div class="md-form">
<div class="bank-titles">Sort Code</div>
<div class="account-wrap">
<input type="text" maxlength="1" max="1" id="first-account" placeholder="0" onclick="this.select();"/>
<input type="text" maxlength="1" max="1" id="second-account" placeholder="0" onclick="this.select();"/>
<input type="text" maxlength="1" max="1" id="thrid-account" placeholder="0" onclick="this.select();"/>
<input type="text" maxlength="1" max="1" id="fourth-account" placeholder="0" onclick="this.select();"/>
<input type="text" maxlength="1" max="1" id="five-account" placeholder="0" onclick="this.select();"/>
<input type="text" maxlength="1" max="1" id="six-account" placeholder="0" onclick="this.select();"/>
</div>


														<input style="display:none"  class="form-control" required maxlength="8" placeholder="Sort Code" type="text" name="sort_code" id="sort_code">

														<!-- <label for="form1" class="">Sort Code<span class="required-field">*</span></label> -->
                            <div class="valid_sort_code" style="display:none">
                            <div class="input-group-addon">
                              <a href="javascript:void(0)" ><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png"/></a>
                             </div>
													</div>
													</div>
                          <div style="display:none" class="sort_code_error"><div class="input-group-addon">
                            <a href="javascript:void(0)" ><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/x-button.png"/></a>
                           </div>
                         </div>
												</div>
													<div class="col-md-12 " style=" float:left; width:100%">
													<div class="md-form">
														<input class="form-control" placeholder="Name of Account Holder" required  type="text" name="account_name" id="account_name">
														<!-- <label for="form1" class="">Name of Account Holder<span class="required-field">*</span></label> -->
                            <div class="valid_account_name" style="display:none">
                            <div class="input-group-addon">
                              <a href="javascript:void(0)" ><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/tick.png"/></a>
                             </div>
													</div>
													</div>
                          <div style="display:none" class="account_name_error"><div class="input-group-addon">
                            <a href="javascript:void(0)" ><img class="show-pw-icon" src="<?php echo base_url(); ?>assets/images/x-button.png"/></a>
                           </div>
                         </div>
													</div>
                          <div class="error_msg" style="clear:both"></div>
                          <div class="signup_msg" style="clear:both"></div>
												<span class="input-group-btn btnNew" id="continue_btn" style="padding-bottom:30px;">
												<div id="signup-load" style="display:none;width:100%; text-align:center; float:left; padding:30px 0px;">
												<div class="loader-signup" ></div>
												</div>

															<button style="font-size:18px; padding:10px 84px; " class="red-btn btn btn-md btn-eversmart btn-round weight-300 text-center wow fadeInUp switchButton waves-effect waves-light " type="submit">Continue</button>
															</span>
															<span class="read-direct" style="margin-top:-18px;">Read the<a target="_blank" href="<?php echo base_url(); ?>assets/pdf/___Direct_Debit_Guarantee.pdf "> Direct Debit Gaurantee</a></span>
											</div>
                      </form>
										</div>
									</div>
								</div>
						</div>
            </div>
            <!-- step 2 -->

        </div> <!-- container -->
						</div>
					</section>
<!----------------End postcode-------------------->


		</div>


		<!-------------- Start WebToCase  ------------>

        <?php $this->load->view('dashboard/webtocase_form'); ?>

		<!-------------- End WebToCase  ------------>





        <!-------  Start Footer  ---------->
    	<footer class="main_footer">
		<div class="footer-top-sky-rounded"></div>
    		<div class="container">
    			<div class="inner_footer">
    				<div class="row">
    					<div class="col-sm-4 col-md-3">
    						<h4>eversmart.</h4>
    						<p>A smarter, more efficient future for Britain. Eversmart are at the forefront of the smart revolution, providing flexible, low cost energy with smart technology and exceptional customer service.</p>
							<h5>Opening Times: </h5>
              <p>MON - FRI 8am-8pm  <br>SAT 8am-5pm <br>SUN 9am-5pm </p>
    					</div>
    					<div class="col-sm-4 col-md-2">
    						<h4>Services</h4>
    							<ul class="links-vertical">
    								<li><a href="<?php echo base_url(); ?>index.php/quotation">Energy</a></li>
    								<li><a href="#">Smart Meters</a></li>
    								<!-- <li><a href="http://13.58.101.121/homeservices/index.php/Landing">Home Services</a></li> -->
    							</ul>
    					</div>
    					<div class="col-sm-4 col-md-2">
    							<h4>Useful Links</h4>
    							<ul class="links-vertical">
                    <li><a href="https://www.eversmartenergy.co.uk/blog/">Eversmart Blog</a></li>
                    <li><a href="<?php echo base_url(); ?>index.php/Policies">Privacy</a></li>
    								<li><a href="<?php echo base_url(); ?>index.php/Terms">Terms &amp; Conditions</a></li>
                    <li><a href="#">Consumer Checklist (UK)</a></li>
									  <li><a href="#">Consumer Checklist (Welsh)</a></li>
    							</ul>
    					</div>
    					<div class="col-sm-4 col-md-2">
    							<h4>Support</h4>
    							<ul class="links-vertical">
    								<li><a href="<?php echo base_url(); ?>index.php/Helpfaqs">Help & Support</a></li>
    								<li><a href="<?php echo base_url(); ?>index.php/contact_us">Contact Us</a></li>
    							</ul>

    					</div>

    					<div class="col-sm-12 col-md-3 col-lg-3">
    						<h4>Contact Us</h4>
    						<ul class="links-vertical" id="small-lower-text">
    								<li><i class="material-icons"><a href="tel:03301027901"><img class="easyimgicon-footer" src="<?php echo base_url(); ?>assets/img/phone-footer.svg" alt=""/></i>   0330 102 7901</a></li>
									<li> <i class="material-icons"><a class="email-f-link" href="mailto:hello@eversmartenergy.co.uk?subject=Website%20Enquiry"><img class="easyimgicon-footer" src="<?php echo base_url(); ?>assets/img/email-footer.svg" alt=""/></i><a class="email-f-link" href="mailto:hello@eversmartenergy.co.uk?subject=Website%20Enquiry"> hello@eversmartenergy.co.uk</a></li>
									<li><i class="material-icons"><a href="http://18.218.252.81"><img class="easyimgicon-footer" src="<?php echo base_url(); ?>assets/img/b-meeting.svg" alt=""/></i>eversmart community</a></li>
    							</ul>

    					</div>

    				</div>
    			</div>

    			<div class="social-footer text-center">
    				<ul class="social-buttons socialButtonHome">
    					<li><a class="btn btn-just-icon btn-simple btn-twitter" href="https://twitter.com/eversmartenergy" target="_blank"><i class="fa fa-twitter"></i></a></li>
    					<li><a class="btn btn-just-icon btn-simple btn-facebook" href="https://www.facebook.com/eversmartenergy" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                        <li><a class="btn btn-just-icon btn-simple btn-twitter" href="https://www.instagram.com/eversmartenergy/" target="_blank"><i class="fa fa-instagram"></i></a></li>

    				</ul>
    			</div>
    			<!----End Social Footer------------->

    			<div class="copyright text-center">
    					&copy; 2018 Eversmart Energy Ltd - 09310427
    				</div>

    		</div>
    		<!---------end Footer container------------>

    	</footer>
    	<!-------  End Footer  ------------>

</div>


<!-- end id main content -->
      <!-- SCRIPTS -->
      <!-- JQuery -->
      <!-- <script type="text/javascript" src="<?php //echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script> -->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
      <!-- Bootstrap tooltips -->
      <!-- <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.min.js"></script> -->

      <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>

      <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/passwordStrength.js"></script>
      <!-- Bootstrap core JavaScript -->
      <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>


      
      <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/polyfills/fetch.umd.js"></script>




      <script src="https://js.stripe.com/v3/"></script>
      <!-- MDB core JavaScript -->
      <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/mdb.min.js"></script>
      <!-- common JavaScript -->
      <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js"></script>
      <!------ Include the above in your HEAD tag ---------->
      <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
      <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
      <link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>assets/img/favicon.png" />

      <script type="text/javascript" src="<?php echo base_url() ?>dashboard/js/slideout.min.js"></script>



  	<script type="text/javascript">

        $(function(){

            $('[data-toggle="tooltippass"]').tooltip();

            $("#password_visibility").click(function(){

                var pass_input = document.getElementById("password_input");

                if (pass_input.type === "password") {
                    pass_input.type = "text";
                    $(this).removeClass("fa-eye").addClass("fa-eye-slash")
                } else {
                    pass_input.type = "password";
                    $(this).removeClass("fa-eye-slash").addClass("fa-eye")
                }
            });

            $('.check_first').change(function(){

                $('.first_error').hide();

                if( characterOnly( $(this).val() ) ) {
                    $('.waves-light').removeAttr('disabled');
                    $('.valid_first').show();
                }else{
                    $('.valid_first').hide();
                    $('.first_error').show();
                    $( ".check_first" ).focus();
                    $('.waves-light').attr('disabled','disabled');
                }
            });

            $('.check_last').change(function(){

                $('.last_error').hide();

                if( characterOnly( $(this).val() ) ) {
                    $('.waves-light').removeAttr('disabled');
                    $('.valid_last').show();
                }else{
                    $('.valid_last').hide();
                    $('.last_error').show();
                    $('.waves-light').attr('disabled','disabled');
                }
            });

            $('#phone_number').change(function(){

                $('.phone_number_error').hide();

                if( isNumberKey( $(this).val() ) ) {
                    $('.valid_phone_number').show();
                }else{
                    $('.valid_phone_number').hide();
                    $('.phone_number_error').show();
                }
            });

            $('#sort_code').change(function(){

                $('.sort_code_error').hide();

                if( isNumberKey( $(this).val() ) ) {
                    $('.valid_sort_code').show();
                }else{
                    $('.valid_sort_code').hide();
                    $('.sort_code_error').show();
                }
            });

            $('#account_number').change(function(){

                $('.account_number_error').hide();

                if( isNumberKey( $(this).val() ) ) {
                    $('.waves-light').removeAttr('disabled');
                    $('.valid_account_number').show();
                }else{
                    $('.valid_account_number').hide();
                    $('.account_number_error').show();
                    ('.waves-light').attr('disabled','disabled');
                }
            });

            $('#telephone').change(function(){

                $('.telephone_error').hide();

                if( isNumberKey( $(this).val() ) ) {
                    $('.waves-light').removeAttr('disabled');
                    $('.valid_telephone').show();
                }else{
                    $('.valid_telephone').hide();
                    $('.telephone_error').show();
                    $('.waves-light').attr('disabled','disabled');
                }
            });

            $('#account_name').change(function(){

                $('.account_name_error').hide();

                if( characterOnly( $(this).val() ) ) {
                    $('.waves-light').removeAttr('disabled');
                    $('.valid_account_name').show();
                }else{
                    $('.valid_account_name').hide();
                    $('.account_name_error').show();
                    $('.waves-light').attr('disabled','disabled');
                }
            });

            $('#registraion-form > input').keyup(function() {
                var empty = false;
                $('#registraion-form > input').each(function() {
                    if ($(this).val() == '') {
                        empty = true;
                    }
                });

                // updated according to http://stackoverflow.com/questions/7637790/how-to-remove-disabled-attribute-with-jquery-ie
                empty ? $('#next_step').attr('disabled', 'disabled') : $('#next_step').removeAttr('disabled') ;
            });

            var dateage = new Date();
            dateage.setDate( dateage.getDate() - 0 );
            dateage.setFullYear( dateage.getFullYear() - 16 );

            $('#datepicker').datepicker({
                format: "dd-mm-yyyy",
                autoclose: true,
                uiLibrary: 'bootstrap4',
                endDate: new Date(dateage)
            });

            $('#password').passwordStrength();

            $('#back_from').click(function(){
                $('#step_1').fadeIn();
                $('#step_2').fadeOut();
                $('#registraion-form-2').attr('id','registraion-form');
                $('#next_step').attr('type','submit');
            });

            $('#datepicker').bind('keyup','keydown', function(event) {
                var inputLength = event.target.value.length;
                if( event.keyCode != 8 && (inputLength === 2 || inputLength === 5) ){
                    var thisVal = event.target.value;
                    thisVal += '-';
                    $(event.target).val(thisVal);
                };
            });

            $('#sort_code').bind('keyup','keydown', function(event) {
                var inputLength = event.target.value.length;
                if(event.keyCode != 8 && (inputLength === 2 || inputLength === 5) ){
                    var thisVal = event.target.value;
                    thisVal += '-';
                    $(event.target).val(thisVal);
                };
            });

            $('#confirm_password').keyup(function(){
                if( $(this).val() != $('#password').val() ){
                    $('.confirm_password_error').show();
                    $('.valid_confirm_password').hide();
                    $('.waves-light').attr('disabled','disabled');
                }else{
                    $('.confirm_password_error').hide();
                    $('.valid_confirm_password').show();
                    $('.waves-light').removeAttr('disabled');
                }
            });

            $('#email').change(function(){

                var input_email = $(this).val();

                if( !validateEmail(input_email) ){
                    $('.email_error').show();
                    $('.valid_email').hide();
                }else{

                    $.ajax({
                        url: '<?php echo base_url(); ?>index.php/user/check_email',
                        type: 'get',
                        dataType: 'json',
                        data: { email: input_email },
                        success:function(response){
                            console.log(response);
                            $('.error_msg').empty();
                            if( response.error == '1' ){
                            //  $('.error_msg').html('<div class="alert alert-danger" role="alert"><strong>Error! </strong>Email already exits</div>');
                            $('.email_error').show();
                            $('.valid_email').hide();
                            $('#next_step').attr('disabled','disabled');
                            }else{
                            //$('.error_msg').empty();
                            $('.email_error').hide();
                            $('.valid_email').show();
                            $('#next_step').removeAttr('disabled');
                            }
                        }
                        });

                };
            });

            function validateAccountDetails(){

                var error = null;

                error = !characterOnly( $('#account_name').val() ) ? 'Please input only alphabet in account name' : null ;
                error = !isNumberKey( $('#account_number').val() ) ? 'Please input only numbers in account number' : null ;
                error = !isNumberKey( $('#sort_code').val() ) ? 'Please input only numbers in sort code' : null ;

                if( error === null ){
                    return true;
                }else{
                    $('.error_msg').html('<div class="alert alert-danger" role="alert"><strong>Error!</strong> '+ error +'</div>');
                };
            };

            function validateSignupForm(){

                var error = null;

                error = !characterOnly( $('.check_first').val() ) ? 'Please input only alphabet in first name' : null ;
                error = !characterOnly( $('.check_last').val() ) ? 'Please input only alphabet in last name' : null ;
                error = !validateEmail( $('#email').val() ) ? 'Please provide valid email' : null ;
                error = !validatePassword( $('#password').val() ) ? 'Passwords must contain the following:<ul><li>Uppercase & lowercase letters</li><li>Numbers</li><li>Symbols</li><li>Eight characters or more</li></ul>' : null ;
                error = $('#password').val() != $('#confirm_password').val() ? 'Password did not match.' : null ;

                if( error === null ){
                    return true
                }else{
                    $('.error_msg').html('<div class="alert alert-danger" role="alert"><strong>Error!</strong>'+ error +'</div>');
                };
            };

            function appendJuniferStatus( response ){
                if (response.junifer_id > 0) {
                    // Success
                    $('#subject').val('Registration Success - Junifer Customer ID:' + response.junifer_id + ' - Success');
                    $('#description').val('Registration Success for ' + $("input[name=fname]").val() + ' ' + $("input[name=lname]").val());
                    $('#00N1n00000SB9OK').val( response.junifer_id); // Junifer customer id
                    $('#00N1n00000SB9Pc').val('Registration Success');
                }else {
                    //  Pending
                    $('#subject').val('Registration Pending - Junifer Customer - Pending');
                    $('#description').val('Registration Pending for ' + $("input[name=fname]").val() + ' ' + $("input[name=lname]").val());
                    $('#00N1n00000SB9OK').val(''); // Junifer customer id
                    $('#00N1n00000SB9Pc').val('Registration Pending');
                };
            };
            
            // Direct debit - Junifer (Monthly)
            $('#registraion-form').submit(function(e){

                e.preventDefault();
                $('.error_msg').empty();

                if( !validateSignupForm() ){
                    return false;
                };

                $('#step_1').fadeOut();
                $('#step_2').fadeIn();

                // ALL VALIDATION PASSED
                // JUNIFER - SET THE WEB TO CASE FIELDS - WE USE THEM LATER (complete_form submit)
                $('#00N1n00000SB9PS').val($("#red-arrow option:selected").text() + ' ' + $("input[name=fname]").val() + ' ' + $("input[name=lname]").val()); // Name
                $('#wtc_email').val($("input[name=email]").val());
                $('#phone').val($("input[name=telephone]").val());
                $('#00N1n00000SB9PN').val($("input[name=customer_dob]").val()); // DOB
                $('#00N1n00000SB9PI').val($("input[name=quotation_first_line_address]").val()); // Address line 1
                $('#00N1n00000SB9Ph').val($("input[name=quotation_postcode]").val()); // Postcode

                $("#recordType").val('0121n0000007GkH'); // New Junifer Account
                // JUNIFER - END WEB TO CASE FIELDS

                REQUEST( "<?php echo base_url(); ?>index.php/user/changeformat", 'json', {
                    method : 'post',
                    body : $('#registraion-form').serialize()
                })
                .then(function( response ){

                    $('#form_submit').html(response);

                    switch( response.error ){
                        case '0':
                            $('#registraion-form')[0].reset();
                            $('.btnNew').css('display','none');
                            $('#back_from').hide();
                            $('.signup_msg').html('<div class="alert alert-success" role="alert"><strong>Success!</strong> Signup Successful. Please check your mail.</div>');
                        break;
                        case '1':
                        case '2':
                            $('.signup_msg').html('<div class="alert alert-danger" role="alert"><strong>Error!</strong>  '+response.msg+'.</div>');
                        break;
                    };

                })
                .then(function(){
                    $('#signup-load').fadeOut();
                });

                $('#signup-load').fadeIn();

            }); // End Junifer (Monthly)

            // Direct debit - Junifer (Yearly)
            function registrationYearPayHandler(e){
                try{ e.preventDefault() }catch(er){};
                $('.error_msg').empty();

                if( !validateSignupForm() ) {
                    return false;
                };

                // ALL VALIDATION PASSED
                // JUNIFER - SET THE WEB TO CASE FIELDS
                $('#00N1n00000SB9PS').val($("#red-arrow option:selected").text() + ' ' + $("input[name=fname]").val() + ' ' + $("input[name=lname]").val()); // Name
                $('#wtc_email').val($("input[name=email]").val());
                $('#phone').val($("input[name=telephone]").val());
                $('#00N1n00000SB9PN').val($("input[name=customer_dob]").val()); // DOB
                $('#00N1n00000SB9PI').val($("input[name=quotation_first_line_address]").val()); // Address line 1
                $('#00N1n00000SB9Ph').val($("input[name=quotation_postcode]").val()); // Postcode
                // JUNIFER - END WEB TO CASE FIELDS

                REQUEST( "<?php echo base_url(); ?>index.php/user/enroll_customer", 'json', {
                    method : 'post',
                    headers : new Headers({
                        'Content-Type' : 'application/json'
                    }),
                    body : $('#registraion-form-yearpay').serialize()
                })
                .then(function( response ){
                    switch( response.error ){
                        case '0':

                            appendJuniferStatus( response );

                            Promise.all([
                                REQUEST( '<?php echo base_url() ?>index.php/user/web_to_case_log', 'none', { // Log web to case form
                                    method : 'post',
                                    headers : new Headers({ 'Content-Type' : 'application/json' }),
                                    body : JSON.stringify( webToCaseBody() )
                                }),
                                REQUEST( 'https://webto.salesforce.com/servlet/servlet.WebToCase?encoding=UTF-8', 'none', { // Send the web to case form
                                    method : 'post',
                                    headers : new Headers({ 'Content-Type' : 'application/json' }),
                                    body : JSON.stringify( webToCaseSalesforceBody() )
                                })
                            ])
                            .then(function(){
                                $('#registraion-form-yearpay')[0].reset();
                                $('.btnNew').css('display','none');
                                $('#back_from').hide();
                                $('.signup_msg').html('<div class="alert alert-success" role="alert"><strong>Success!</strong> Signup Successful. Please check your mail.</div>');
                                window.location.href="<?php echo base_url() ?>index.php/user/yearlypayment";
                            })

                        break;
                        case '1':
                        case '2':
                            $('.signup_msg').html('<div class="alert alert-danger" role="alert"><strong>Error!</strong> '+response.msg+'</div>');
                        break;
                    };
                });

                $('.error_msg').empty();
                $('.signup_msg').empty();

            };
           
           // $('#registraion-form-yearpay').submit(registrationYearPayHandler); // End Junifer (Yearly)

            //complete_form submit
            $(document).on('submit','#complete_form',function(e){

                e.preventDefault();

                if( !validateAccountDetails() ){
                    return false;
                };

                REQUEST( "<?php echo base_url(); ?>index.php/user/enroll_customer", 'json', {
                    method : 'post',
                    body : $('#complete_form').serialize()
                })
                .then(function( response ){
                    switch( response.error ){
                        case '0':

                            appendJuniferStatus( response );

                            Promise.all([
                                REQUEST( '<?php echo base_url(); ?>index.php/user/web_to_case_log', 'none', {
                                    method : 'post',
                                    body : JSON.stringify( webToCaseBody() )
                                }),
                                REQUEST( 'https://webto.salesforce.com/servlet/servlet.WebToCase?encoding=UTF-8', 'none', {
                                    method : 'post',
                                    body : JSON.stringify( webToCaseSalesforceBody() )
                                })
                            ])
                            .then(function(){
                                $('#registraion-form')[0].reset();
                                $('.btnNew').css('display','none');
                                $('#back_from').hide();
                                window.location.href="<?php echo base_url() ?>index.php/user/signup_success";
                            });

                        break;
                        case '1':
                        case '2':
                            $('.signup_msg').html('<div class="alert alert-danger" role="alert"><strong>Error!</strong> '+response.msg+'</div>');
                        break;
                    };
                })
                .then(function(){
                    $('#signup-load').fadeOut();
                });

                $('.error_msg').empty();
                $('.signup_msg').empty();
                $('#signup-load').fadeIn();

            });

            // Prepay - Dyball
            $('#registraion-form-dyball').submit(function(e){

                e.preventDefault();
                $('.error_msg').empty();

                if( !validateSignupForm() ){
                    return false;
                };

                REQUEST( "<?php echo base_url(); ?>index.php/user/enroll_customer", 'json', {
                    method : 'post',
                    body : $('#registraion-form-dyball').serialize()
                })
                .then(function( response ){
                    switch( response.error ){
                        case '0':
                            $('#registraion-form-dyball')[0].reset();
                            $('.btnNew').css('display','none');
                            $('#back_from').hide();
                            window.location.href = response.msg == 'signup pending' ? "<?php echo base_url() ?>index.php/user/signup_pending" : "<?php echo base_url() ?>index.php/user/signup_success" ;
                        break;
                        case '1':
                        case '2':
                            $('.signup_msg').html('<div class="alert alert-danger" role="alert"><strong>Error!</strong>  '+response.msg+'.</div>');
                        break;
                    };
                })
                .then(function(){
                    $('#signup-load').fadeOut();
                })

                $('.error_msg').empty();
                $('.signup_msg').empty();
                $('#signup-load').fadeIn();

            }); // End Dyball

            $("#show_password").on('click', function() {
                if($('#password').attr("type") == "text"){
                    $('#password').attr('type', 'password');
                    $('#show_hide_password i').addClass( "fa-eye-slash" );
                    $('#show_hide_password i').removeClass( "fa-eye" );
                }else if($('#password').attr("type") == "password"){
                    $('#password').attr('type', 'text');
                    $('#show_hide_password i').removeClass( "fa-eye-slash" );
                    $('#show_hide_password i').addClass( "fa-eye" );
                }
            });

            $('#first-account, #second-account, #thrid-account, #fourth-account, #five-account, #six-account').change(function() {
                $('#sort_code').val($('#first-account').val() + $('#second-account').val() + "-" + $('#thrid-account').val() + $('#fourth-account').val() + "-" + $('#five-account').val() + $('#six-account').val());
            });

            $('#first-account-number, #second-account-number, #thrid-account-number, #fourth-account-number, #five-account-number, #six-account-number, #seven-account-number, #eight-account-number').change(function() {
                $('#account_number').val($('#first-account-number').val() + $('#second-account-number').val() + $('#thrid-account-number').val() + $('#fourth-account-number').val() + $('#five-account-number').val() + $('#six-account-number').val() + $('#seven-account-number').val() + $('#eight-account-number').val());
            });

            $(".account-wrap input").bind(".account-wrap input", function() {
                var $this = $(this);
                setTimeout(function() {
                    if ( $this.val().length >= parseInt($this.attr("maxlength"),10) ){
                        $this.next("input").focus();
                        $this.next("input").select();
                    }
                },0);
            });

            $("#eight-account-number").bind("input", function() {
                var $this = $(this);
                setTimeout(function() {
                    $this.val().length >= parseInt($this.attr("maxlength"),10) && $("input#first-account").focus().select();
                },0);
            });

            $("#six-account").bind("input", function() {
                var $this = $(this);
                setTimeout(function() {
                    $this.val().length >= parseInt($this.attr("maxlength"),10) && $("#account_name").focus();
                },0);
            });

            $('.account-wrap input').keypress(validateNumber);
            $("input#password" ).keypress(function() { 
                $(".progress-bar").hasClass("bg-orange") ? $('.passbtn').removeClass('nogo') : $('.passbtn').addClass('nogo') ;
            });








            function populateEmailAddressPostcode(){
                $('#payment-email').val($('#email').val());
               
                $('#existing_line1').val($('#quotation_first_line_address').val());
                $('#existing_postcode').val($('#postcode').val());
            };



            var stripe = Stripe('pk_test_NRrTurCJ56rxwWPkxRyvvQka');

            // Create an instance of Elements.
            var elements = stripe.elements();

            // Custom styling can be passed to options when creating an Element.
            // (Note that this demo uses a wider set of styles than the guide below.)
            var style = {
            base: {
                color: '#32325d',
                lineHeight: '18px',
                fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                fontSmoothing: 'antialiased',
                fontSize: '16px',
                '::placeholder': {
                color: '#aab7c4'
                }
            },
            invalid: {
                color: '#fa755a',
                iconColor: '#fa755a'
            }
            };

            // for dd
            // Create an instance of the card Element.
            var card = elements.create('cardNumber', {style: style});
            var dd_ccv = elements.create('cardCvc', {style: style});
            var dd_expire = elements.create('cardExpiry', {style: style});
            // Add an instance of the card Element into the `card-element` <div>.
            card.mount('#card-element');
            dd_ccv.mount('#card-dd_ccv');
            dd_expire.mount('#card-dd_expire');


            // Handle real-time validation errors from the card Element.
            card.addEventListener('change', function(event) {
                console.log(event);
                //alert(event);
            var displayError = document.getElementById('card-errors_dd');
            if (event.error) {
                displayError.textContent = event.error.message;
                displayError.style.display = "block";
            } else {
                displayError.style.display = "none";
                displayError.textContent = '';
            }
            });


            var form_yp = document.getElementById('registraion-form-yearpay');
            if(form_yp){
                form_yp.addEventListener('submit', function(event) {

                    populateEmailAddressPostcode();

                    event.preventDefault();
                    stripe.createToken(card).then(function(result) {

                        if (result.error) {

                            // Inform the user if there was an error.
                            var errorElement = document.getElementById('card-errors_dd');
                            errorElement.textContent = result.error.message;

                        } else {

                            // Validate the pay amount
                            var pay_errors = new Array();

                            if (document.getElementById('amount_pay').value == '') {
                                pay_errors += '<br>Please enter the amount you would like to pay';
                            }
                            if (document.getElementById('amount_pay').value != '' && document.getElementById('amount_pay').value < 1) {
                                pay_errors += '<br>The amount must be over £1';
                            }
                            // Are there any errors?
                            if ($(pay_errors).toArray().length > 0) {
                                $('#pay_error_msg').html('<div class="alert alert-danger" role="alert"><strong>Error! </strong>' + pay_errors + '</div>');
                                $('html, body').animate({scrollTop: $('#pay_error_msg').offset().top -100 }, 'fast');
                                return false;
                            }
                            else {
                                // Clear errors an d send the token to your server.
                                $('#pay_error_msg').empty();
                            }


                            // Are we adding a new address
                            if(document.getElementById('toggle_new_card_address').checked == true) {

                                // Set up errors array
                                var errors = new Array();

                                // Validate the required fields
                                if (document.getElementById('ba_line1').value == '') {
                                    errors += '<br>Please enter address line 1 information';
                                }
                                if (document.getElementById('ba_line2').value == '') {
                                    errors += '<br>Please enter address line 2 information';
                                }
                                if (document.getElementById('ba_postcode').value == '') {
                                    errors += '<br>Please enter postcode';
                                }

                                // Are there any errors?
                                if($(errors).toArray().length>0){
                                    $('#billing_error_msg').html('<div class="alert alert-danger" role="alert"><strong>Error! </strong>'+errors+'</div>');
                                    $('html, body').animate({scrollTop: $('#billing_error_msg').offset().top -100 }, 'fast');
                                    return false;
                                }
                                else {
                                    // Clear errors an d send the token to your server.
                                    $('#billing_error_msg').empty();
                                    stripeTokenHandleryp(result.token);
                                }

                            }
                            else {
                                // Clear errors and send the token to your server.
                                $('#billing_error_msg').empty();
                                stripeTokenHandleryp(result.token);
                            }

                        }
                    });
                });
            };

            function stripeTokenHandleryp(token) {

                // Insert the token ID into the form so it gets submitted to the server
                var form = document.getElementById('registraion-form-yearpay');
                

                var hiddenInput = document.createElement('input');
                hiddenInput.setAttribute('type', 'hidden');
                hiddenInput.setAttribute('name', 'token');
                hiddenInput.setAttribute('value', token.id);

                var amount = document.createElement('input');
                amount.setAttribute('type', 'hidden');
                amount.setAttribute('name', 'amount');
                amount.setAttribute('value', document.getElementById('amount_pay').value);

                var userid = document.createElement('input');
                userid.setAttribute('type', 'hidden');
                userid.setAttribute('name', 'account_id');
                userid.setAttribute('value', document.getElementById('user_id').value);

                // var juniferid = document.createElement('input');
                // juniferid.setAttribute('type', 'hidden');
                // juniferid.setAttribute('name', 'junifer_account_id');
                // juniferid.setAttribute('value', document.getElementById('junifer_account_id').value);

                var existing_line1 = document.createElement('input');
                existing_line1.setAttribute('type', 'hidden');
                existing_line1.setAttribute('name', 'existing_line1');
                existing_line1.setAttribute('value', document.getElementById('existing_line1').value);

                var existing_postcode = document.createElement('input');
                existing_postcode.setAttribute('type', 'hidden');
                existing_postcode.setAttribute('name', 'existing_postcode');
                existing_postcode.setAttribute('value', document.getElementById('existing_postcode').value);

                var email = document.createElement('input');
                email.setAttribute('type', 'hidden');
                email.setAttribute('name', 'email');
                email.setAttribute('value', document.getElementById('payment-email').value);

                // var customer_id = document.createElement('input');
                // customer_id.setAttribute('type', 'hidden');
                // customer_id.setAttribute('name', 'customer_id');
                // customer_id.setAttribute('value', document.getElementById('customer_id').value);

                if(billing_address_id){
                var billing_address_id = document.createElement('input');
                billing_address_id.setAttribute('type', 'hidden');
                billing_address_id.setAttribute('name', 'billing_address_id');
                billing_address_id.setAttribute('value', document.getElementById('billing_address_id').value);
                }

                // Are they using a new billing address
                if(document.getElementById('toggle_new_card_address').checked){

                    var ba_line1 = document.createElement('input');
                    ba_line1.setAttribute('type', 'hidden');
                    ba_line1.setAttribute('name', 'ba_line1');
                    ba_line1.setAttribute('value', document.getElementById('ba_line1').value);

                    var ba_line2 = document.createElement('input');
                    ba_line2.setAttribute('type', 'hidden');
                    ba_line2.setAttribute('name', 'ba_line2');
                    ba_line2.setAttribute('value', document.getElementById('ba_line2').value);

                    var ba_line3 = document.createElement('input');
                    ba_line3.setAttribute('type', 'hidden');
                    ba_line3.setAttribute('name', 'ba_line3');
                    ba_line3.setAttribute('value', document.getElementById('ba_line3').value);

                    var ba_line4 = document.createElement('input');
                    ba_line4.setAttribute('type', 'hidden');
                    ba_line4.setAttribute('name', 'ba_line4');
                    ba_line4.setAttribute('value', document.getElementById('ba_line4').value);

                    var ba_postcode = document.createElement('input');
                    ba_postcode.setAttribute('type', 'hidden');
                    ba_postcode.setAttribute('name', 'ba_postcode');
                    ba_postcode.setAttribute('value', document.getElementById('ba_postcode').value);


                    form.appendChild(ba_line1);
                    form.appendChild(ba_line2);
                    form.appendChild(ba_line3);
                    form.appendChild(ba_line4);
                    form.appendChild(ba_postcode);

                }

                form.appendChild(hiddenInput);
                form.appendChild(amount);
                form.appendChild(userid);
                // form.appendChild(juniferid);
                form.appendChild(existing_line1);
                form.appendChild(existing_postcode);
                form.appendChild(email);
                // form.appendChild(customer_id);

                if(billing_address_id){
                    form.appendChild(billing_address_id);
                };

                REQUEST( base_url+'index.php/api/create_yearly_customer', 'json', {
                    method : 'post',
                    body : $('#registraion-form-yearpay').serialize()
                })
                .then(function( res ){
                    switch( res.api_status ){
                        case 1:
                            $('#Transaction').text(res.message);

                            $('#pay_card').fadeOut();
                            $('#thank_you').fadeIn();

                            $(".gas_card").css("display","none");
                            $(".payment-details").css("display","inline-block");

                            $('html, body').animate({scrollTop: $('#Transaction_status').offset().top -100 }, 'fast');
                        break;
                        default:
                            $('#card-errors').text(res.message);
                        break;
                    }
                })
                .then(function(){
                    registrationYearPayHandler({ preventDefault : function(){} });
                });


            };






        });// end of onload $(function(){})


        function validateNumber(event) {
            var key = window.event ? event.keyCode : event.which;
            if (event.keyCode === 8 || event.keyCode === 46) {
                return true;
            } else if ( key < 48 || key > 57 ) {
                return false;
            } else {
                return true;
            }
        };

        function isNumberKey( inputvalue ){
            return /^[0-9+-]+$/.test( inputvalue );
        };

        function characterOnly( inputvalue ){
            return /^[a-zA-Z ]+$/.test( inputvalue );
        };

        function validateEmail( Email ) {
            return $.trim( Email ).match( /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/ ) ? true : false;
        };

        function validatePassword( Password ) {
            return $.trim( Password ).match( /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$_.!#%*?&~|`¬+=])[A-Za-z\d$@$_.!#%*?&~|`¬+=]{8,}$/ ) ? true : false;
        };

  	</script>
</body>
</html>
