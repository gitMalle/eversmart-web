
        <!------------------------------- START MAIN BODY ------------------------------->

        <tr>
            <td valign="top" id="templateBody">

                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
                    <tbody class="mcnTextBlockOuter">
                    <tr>
                        <td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
                            <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                                <tr>
                                    <td valign="top" width="600" style="width:600px;">
                                        <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%; min-width:100%;" width="100%" class="mcnTextContentContainer">
                                            <tbody>
                                            <tr>
                                                <td valign="top" class="mcnTextContent" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;">

                                                    <h1 style="margin: 50px 0px; text-align: left;">
                                                        <span style="font-family: 'Quicksand', sans-serif; font-weight:normal; font-size:46px; color: #1C3659;">We weren't able to switch you</span></span>
                                                    </h1>

                                                    <font style="font-family: 'Quicksand', sans-serif;">
                                                        <span style="font-size:17px">
                                                            Hello <?= $email_info['name']; ?>,<br><br>
                                                            We're sorry, but your switch to Eversmart was unsuccessful. This is because we believe you have a <strong>pre-payment meter</strong> installed.<br><br>
                                                            If you think this is a mistake, please contact <a href="mailto:hello@eversmartenergy.co.uk" target="_blank">hello@eversmartenergy.co.uk</a> and an adviser will be able to help.<br><br>
                                                            From the Eversmart Team
                                                        </span>
                                                    </font>
                                                    <br><br><br>
                                                </td>

                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>

        <!------------------------------- END MAIN BODY ------------------------------->