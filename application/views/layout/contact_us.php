
                <section class="mt0">
                    <div class="boiler_header-h">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-10 mx-auto text-center">
                                    <span class="bolier_home-h-l">Contact Us</span>
                                    <span class="bolier_home-h-s">Speak to Eversmart</span>
                                </div>
                            </div>
                        </div>

                    </div>
                </section>


            </div>
        </div>

        <section class="contact cd-section">

            <div class="container">

                <div class="row ">

                    <h1 class="text-center aos-item" data-aos="fade-down">Contact Eversmart</h1>
                    <span class="cs_fa_head text-center aos-item" data-aos="fade-down">Our friendly team will be happy to help - If you have any queries then feel free to get in touch:</span>

                    <div class="col-md-6 main_card_box" id="faq">
                        <figure class="effect-sadie">
                            <figcaption>
                                <a href="http://help.eversmartenergy.co.uk" target="_blank" class="gas_card">
                                    <div class="gas_box_icon gap_icon">
                                        <span class="switch_icon"><img width="64px" class="contact-img" src="<?php echo base_url(); ?>assets/images/icons/f-info.svg" alt=""></span>
                                    </div>
                                    <span class="click_select_edit faq-red-color">Knowledgebase</span>
                                </a>
								
                            </figcaption>
                        </figure>
                    </div>

                    <div class="col-md-6 main_card_box" id="faq">
                        <figure class="effect-sadie">
                            <figcaption>
                                <a href="http://help.eversmartenergy.co.uk" target="_blank" class="gas_card open_intercom">
                                    <div class="gas_box_icon gap_icon">
                                        <span class="switch_icon"><img class="contact-img" src="<?php echo base_url(); ?>assets/images/icons/chat.svg" alt=""></span>
                                    </div>
                                    <span class="click_select_edit faq-red-color">Chat</span>
                                </a>

                            </figcaption>
                        </figure>
                    </div>

                    <!--
                    <div class="col-md-4 main_card_box" id="faq">
                        <figure class="effect-sadie">
                            <figcaption>
                                <a href="http://help.eversmartenergy.co.uk" onclick="return false;" class="gas_card" id="cnt_email">
                                    <div class="gas_box_icon gap_icon">
                                        <span class="switch_icon"><img class="contact-img mail-i" src="<?php echo base_url(); ?>assets/images/email-new.png" alt=""></span>
                                    </div>
                                    <span class="click_select_edit faq-red-color">SELECT</span>
                                </a>
                            </figcaption>
                        </figure>
                    </div>

                    <div class="col-md-4 main_card_box" id="faq">
                        <figure class="effect-sadie">
                            <figcaption>
                                <a href="tel:03301027901" class="gas_card">
                                    <div class="gas_box_icon gap_icon">
                                        <span class="switch_icon"><img class="contact-img" src="<?php echo base_url(); ?>assets/images/call-new.png" alt=""></span>
                                    </div>
                                    <span class="click_select_edit faq-red-color">SELECT</span>
                                </a>
                            </figcaption>
                        </figure>
                    </div>
                    -->
                </div>
            </div>

            <div id="popup-container">

                <div class="popup-background">
                    <div class="popup">
                    <div class="popup-inner">
                        <div class="email-modal" style="display:none">

                            <div class="modal-header" id="contact-pop" style="border-bottom: none;margin-bottom: 20px">
                                <button type="button" class="close" id="close-email" aria-label="Close" style="font-size: 29px;"><span aria-hidden="true">×</span></button>
                                <h4 class="modal-title">Please choose the most appropriate<br> email for you enquiry</h4>
                            </div>

                            <div class="container">
                                <div class="row">
                                    <div class="col-md-6 col-sm-12">
                                        <a href="mailto:hello@eversmartenergy.co.uk">
                                            <div class="emails">
                                                <p>hello<span class="email-at">@</span></p>
                                                <p style="font-size:12px">(for general queries)</p>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <a href="mailto:billing@eversmartenergy.co.uk">
                                            <div class="emails">
                                                <p>billing<span class="email-at">@</span></p>
                                                <p style="font-size:12px">(for billing and payment related queries)</p>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <a href="mailto:careers@eversmartenergy.co.uk">
                                            <div class="emails">
                                                <p>careers<span class="email-at">@</span></p>
                                                <p style="font-size:12px">(for employment queries)</p>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <a href="mailto:complaints@eversmartenergy.co.uk">
                                            <div class="emails">
                                                <p>complaints<span class="email-at">@</span></p>
                                                <p style="font-size:12px">(for if you're unsatisfied with our service)</p>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="message-modal" style="display:none">

                            <div class="modal-header" id="contact-pop" style="border-bottom: none;">
                                <button type="button" class="close" id="close-message" aria-label="Close" style="font-size: 29px;"><span aria-hidden="true">×</span></button>
                                <h4 class="modal-title">Contact Us</h4>
                            </div>

                            <form id="contact_us" name="" method="post" >

                                <div class="car_form">

                                    <div class="md-form">
                                        <input type="text" id="form1" class="form-control" required="" name="fname" onkeypress="return">
                                        <label for="form1" class="">Full Name</label>
                                    </div>

                                    <div class="md-form">
                                        <input type="email" id="form1" class="form-control" required="" name="email">
                                        <label for="form1" class="">Email Address</label>
                                    </div>

                                    <div class="md-form">
                                        <input type="text" id="form1" class="form-control" required="" name="phone" onkeypress="return isNumber(event)">
                                        <label for="form1" class="">Phone</label>
                                    </div>

                                    <div class="md-form">
                                        <textarea type="text" id="form7" class="form-control md-textarea" name="message" required="" rows="3"></textarea>
                                        <label for="form7" class="">Your Message</label>
                                    </div>

                                    <div class="col-sm-12 text-center" style="padding-top:30px">
                                        <input value="Submit" style="border-radius:30px" class="btn btn-info btn-round btn-lg waves-effect waves-light " id="cs_form_sub_btn" type="submit">
                                        <div id="message"></div>
                                    </div>

                                </div>
                                
                            </form>

                        </div>
                    </div>    
                    </div>
                </div>
            </div>

        </section>

