<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Eversmart | Thank You</title>
    <!-- Font material icon -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:500" rel="stylesheet">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="<?php echo base_url() ?>assets/css/mdb.min.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="<?php echo base_url() ?>assets/css/responsive.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/css/font-awesome-animation.min.css" rel="stylesheet">

    <!--- Font Family Add---------------->
    <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">

    <style>
        .email-amimation {
            float: left;
            width: 100%;
            text-align: center;
            margin: 30px 0px;
        }

        .email-amimation img {
            height: 80px;
            margin-top: 30px;
            width: auto;
        }


    </style>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-79295711-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-79295711-1');
    </script>
    
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','<?php echo $this->config->item('gtm_id'); ?>');</script>
    <!-- End Google Tag Manager -->

    <!-- Hotjar Tracking Code for www.eversmartenergy.co.uk -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:1074765,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>

</head>

<body class="sky-blue-bg">

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T44QPZV"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <div class="main-rounded-red-header">

        <div class="red-rounded-wave">

            <div class="topbar" id="red_top">
                <?php $this->load->view('layout/menu'); ?>
            </div>

            <section class="postcode_top mt0">

                <div class="postcode_box" id="background-none">

                    <div class="container">

                        <div class="row">

                            <div class="col-md-8 mx-auto mtop60 posi-rele">

                                <div class="card card-raised2 card-form-horizontal wow fadeInUp" data-wow-delay="0ms" style="display:block; float:left" >

                                    <span class="email-amimation">
                                        <img src="<?php echo base_url() ?>assets/images/email-sent-icon.svg">
                                    </span>

                                    <div class="content" style="padding:20px 0px 60px">

                                        <div class="col-md-10 mx-auto">

                                            <span class="registration-page-heading-energy-H" style="color:#ea495c">Signup Successful!</span>

                                            <span class="registration-page-energy-d confirm-pay">
                                                We'll be in touch within the next 14 days to complete your signup process.
                                            </span>

                                            <span class="registration-page-energy-d confirm-pay text-center" style="color:#ee4392; padding-top:5px!important; padding-bottom:25px;">
                                                Thanks, <br>Eversmart Team
                                            </span>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </section>

        </div>

    </div>


    <!----------------Start Footer-------------------------->
    <footer class="main_footer">
        <div class="footer-top-sky-rounded"></div>
        <div class="container">
            <div class="inner_footer">
                <div class="row">
                    <div class="col-sm-4 col-md-3 aos-item" data-aos="fade-right">
                        <h4>eversmart.</h4>
                        <p>A smarter, more efficient future for Britain. Eversmart are at the forefront of the smart
                            revolution, providing flexible, low cost energy with smart technology and exceptional customer
                            service.</p>
                        <h5>Opening Times: </h5>
                        <p>MON - FRI 8am-8pm <br>SAT 8am-5pm <br>SUN 9am-5pm </p>
                    </div>
                    <div class="col-sm-4 col-md-2 aos-item" data-aos="fade-up">
                        <h4>Services</h4>
                        <ul class="links-vertical">
                            <li><a href="<?php echo base_url(); ?>index.php/quotation">Energy</a></li>
                            <li><a href="#">Smart Meters</a></li>
                            <li><a href="http://13.58.101.121/homeservices/index.php/Landing">Home Services</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-4 col-md-2 aos-item" data-aos="fade-down">
                        <h4>Useful Links</h4>
                        <ul class="links-vertical">
                            <li><a href="https://www.eversmartenergy.co.uk/blog/">Eversmart Blog</a></li>
                            <li><a href="<?php echo base_url(); ?>index.php/Terms">Terms &amp; Conditions</a></li>
                            <li><a href="<?php echo base_url(); ?>index.php/Policies">Privacy</a></li>
                            <li><a target="_blank" href="<?php echo base_url(); ?>assets/pdf/consumercheck_eng.pdf ">Consumer
                                    Checklist (UK)</a></li>
                            <li><a target="_blank" href="<?php echo base_url(); ?>assets/pdf/consumercheck_wel.pdf ">Consumer
                                    Checklist (Welsh)</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-4 col-md-2 aos-item" data-aos="fade-up">
                        <h4>Support</h4>
                        <ul class="links-vertical">
                            <li><a href="<?php echo base_url(); ?>index.php/Helpfaqs">Help & FAQs</a></li>
                            <li><a href="<?php echo base_url(); ?>index.php/contact_us">Contact Us</a></li>
                        </ul>

                    </div>

                    <div class="col-sm-12 col-md-3 col-lg-3 3 aos-item" data-aos="fade-left">
                        <h4>Contact Us</h4>
                        <ul class="links-vertical" id="small-lower-text">
                            <li><i class="material-icons"><a href="tel:03301027901"><img class="easyimgicon-footer"
                                                                                         src="<?php echo base_url(); ?>assets/img/phone-footer.svg"
                                                                                         alt=""/></i> 0330 102 7901</a></li>
                            <li><i class="material-icons"><a class="email-f-link"
                                                             href="mailto:hello@eversmartenergy.co.uk?subject=Website%20Enquiry"><img
                                                class="easyimgicon-footer"
                                                src="<?php echo base_url(); ?>assets/img/email-footer.svg" alt=""/></i><a
                                        class="email-f-link"
                                        href="mailto:hello@eversmartenergy.co.uk?subject=Website%20Enquiry">
                                    hello@eversmartenergy.co.uk</a></li>
                            <li><i class="material-icons"><a href="http://18.218.252.81"><img class="easyimgicon-footer"
                                                                                              src="<?php echo base_url(); ?>assets/img/b-meeting.svg"
                                                                                              alt=""/></i>eversmart
                                community</a></li>
                        </ul>

                    </div>

                </div>
            </div>

            <div class="social-footer text-center">
                <ul class="social-buttons socialButtonHome">
                    <li><a class="btn btn-just-icon btn-simple btn-twitter" href="https://twitter.com/eversmartenergy"
                           target="_blank"><i class="fa fa-twitter"></i></a></li>
                    <li><a class="btn btn-just-icon btn-simple btn-facebook" href="https://www.facebook.com/eversmartenergy"
                           target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                    <li><a class="btn btn-just-icon btn-simple btn-twitter"
                           href="https://www.instagram.com/eversmartenergy/" target="_blank"><i class="fa fa-instagram"></i></a>
                    </li>

                </ul>
            </div>
            <!----End Social Footer------------->

            <div class="copyright text-center">
                &copy; 2018 Eversmart Energy Ltd - 09310427
            </div>

        </div>
        <!---------end Footer container------------>

    </footer>
    <!----------------End Footer-------------------------->


    <!-- SCRIPTS -->
    <!-- JQuery -->
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/prefixfree/1.0.7/prefixfree.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/js/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/js/mdb.min.js"></script>
    <!-- common JavaScript -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/js/common.js"></script>

    <script>
        $(document).ready(function () {
            $('.salesforcesubmit').trigger('click');
            console.log('click click');
        });

    </script>

    <script type="text/javascript">

        $(function () {
            $("#password_visibility").click(function () {
                var pass_input = document.getElementById("password_input");
                if (pass_input.type === "password") {
                    pass_input.type = "text";
                    $(this).removeClass("fa-eye").addClass("fa-eye-slash")
                } else {
                    pass_input.type = "password";
                    $(this).removeClass("fa-eye-slash").addClass("fa-eye")
                }
            });

            $('#continue_login').click(function () {
                window.location.href = '<?php echo base_url() ?>index.php/user/login';
            });
        });
    </script>

    <!-- START of Rakuten Marketing Conversion Tag -->
    <script type="text/javascript">

        var rm_trans = {

            displayConfig: {rdMID: '7733'},
            orderid: Date.now() + "_" + Math.floor(Math.random() * 999999999),

            currency: 'GBP',

            conversionType: 'Registration',

            lineitems: [{

                quantity: 1,

                unitPrice: 1,

                unitPriceLessTax: 1,

                SKU: '1234',

            }]

        };

        /*Do not edit any information beneath this line*/
        if (!window.DataLayer) {
            window.DataLayer = {Sale: {Basket: rm_trans}}
        } else {
            DataLayer.Sale = DataLayer.Sale || {Basket: rm_trans};
            DataLayer.Sale.Basket = DataLayer.Sale.Basket || rm_trans
        }
        DataLayer.Sale.Basket.Ready = true;

        function __readRMCookie(e) {
            for (var a = e + "=", r = document.cookie.split(";"), t = 0; t < r.length; t++) {
                for (var n = r[t]; " " == n.charAt(0);) n = n.substring(1, n.length);
                if (0 == n.indexOf(a)) return n.substring(a.length, n.length)
            }
            return ""
        }

        function __readRMCookiev2(e, a) {
            for (var r = __readRMCookie(a = a || "rmStore"); r !== decodeURIComponent(r);) r = decodeURIComponent(r);
            for (var t = r.split("|"), n = 0; n < t.length; n++) {
                var i = t[n].split(":")[0], o = t[n].split(":")[1];
                if (i === e) return o
            }
            return ""
        }

        function __readParam(e, a, r, t) {
            var n = e || "", i = a || "", o = r || "", s = t || {}, d = __readRMCookiev2(n), u = s[i],
                m = (d = s.ignoreCookie || !1 ? 0 : d) || u || o;
            return m = ("string" != typeof m || "false" !== m.toLowerCase()) && m
        }

        function sRAN() {
            var e = DataLayer && DataLayer.Sale && DataLayer.Sale.Basket ? DataLayer.Sale.Basket : {},
                a = e.affiliateConfig || {}, r = __readParam("atm", "tagType", "pixel", a),
                t = __readParam("adr", "discountType", "order", a), n = __readParam("acs", "includeStatus", "false", a),
                i = __readParam("arto", "removeOrderTax", "false", a),
                o = __readParam("artp", "removeTaxFromProducts", "false", a),
                s = __readParam("artd", "removeTaxFromDiscount", "false", a), d = __readParam("atr", "taxRate", 0, a);
            d = Number(d);
            var u = __readParam("ald", "land", !1, {}) || (a.land && !0 === a.land ? __readRMCookie("ranLandDateTime") : a.land) || !1,
                m = __readParam("atrv", "tr", !1, {}) || (a.tr && !0 === a.tr ? __readRMCookie("ranSiteID") : a.tr) || !1,
                l = !1, c = __readParam("amid", "ranMID", "", a) || e.ranMID;
            if (!c) return !1;
            if (!(void 0 === a.allowCommission || "false" !== a.allowCommission)) return !1;
            var p = e.orderid || "OrderNumberNotAvailable", f = "", y = "", _ = "", v = "", N = e.currency || "";
            N = N.toUpperCase();
            var h = e.taxAmount ? Math.abs(Math.round(100 * Number(e.taxAmount))) : 0,
                g = e.discountAmount ? Math.abs(Math.round(100 * Number(e.discountAmount))) : 0;
            if (s && d) var C = (100 + Number(d)) / 100, g = Math.round(g / C);
            var b = "pixel" === r ? "ep" : "mop" === r ? "eventnvppixel" : "ep", S = e.customerStatus || "",
                D = document.location.protocol + "//track.linksynergy.com/" + b + "?", w = "";
            null != S && "" != S && (n && "EXISTING" == S.toUpperCase() || n && "RETURNING" == S.toUpperCase()) && (w = "R_");
            for (var P = [], x = 0, T = 0; T < (e.lineitems ? e.lineitems.length : 0); T++) {
                for (var R = !1, k = window.JSON ? JSON.parse(JSON.stringify(e.lineitems[T])) : e.lineitems[T], L = 0; L < P.length; L++) P[L].SKU === k.SKU && (R = !0, P[L].quantity = Number(P[L].quantity) + Number(k.quantity));
                R || P.push(k), x += Number(k.quantity) * Number(k.unitPriceLessTax || k.unitPrice) * 100
            }
            for (T = 0; T < P.length; T++) {
                var k = P[T], I = encodeURIComponent(k.SKU), M = k.unitPriceLessTax || k.unitPrice, U = k.quantity,
                    A = encodeURIComponent(k.productName) || "", O = Math.round(Number(M) * Number(U) * 100);
                !o || !d || k.unitPriceLessTax && k.unitPriceLessTax !== k.unitPrice || (O /= C = (100 + d) / 100), "item" === t.toLowerCase() && g && (O -= g * O / x), f += w + I + "|", y += U + "|", _ += Math.round(O) + "|", v += w + A + "|"
            }
            f = f.slice(0, -1), y = y.slice(0, -1), _ = _.slice(0, -1), v = v.slice(0, -1), g && "order" === t.toLowerCase() ? (f += "|" + w + "DISCOUNT", v += "|" + w + "DISCOUNT", y += "|0", _ += "|-" + g) : g && "item" === t.toLowerCase() && (l = !0), i && h && (f += "|" + w + "ORDERTAX", y += "|0", _ += "|-" + h, v += "|" + w + "ORDERTAX"), D += "mid=" + c + "&ord=" + p + "&skulist=" + f + "&qlist=" + y + "&amtlist=" + _ + "&cur=" + N + "&namelist=" + v + "&img=1&", u && (D += "land=" + u + "&"), m && (D += "tr=" + m + "&"), l && (D += "discount=" + g + "&"), "&" === D[D.length - 1] && (D = D.slice(0, -1));
            var E, B = document.createElement("img");
            B.setAttribute("src", D), B.setAttribute("height", "1px"), B.setAttribute("width", "1px"), (E = document.getElementsByTagName("script")[0]).parentNode.insertBefore(B, E)
        }

        function sDisplay() {
            var e = null, a = null, r = null, t = null,
                n = window.DataLayer && window.DataLayer.Sale && window.DataLayer.Sale.Basket ? window.DataLayer.Sale.Basket : {},
                i = n.displayConfig || {}, o = n.customerStatus || "",
                s = n.discountAmount ? Math.abs(Number(n.discountAmount)) : 0, d = null,
                u = __readParam("dmid", "rdMID", "", i);
            if (!u) return !1;
            var m = __readParam("dtm", "tagType", "js", i),
                l = "if" === (m = "js" === m || "if" === m || "img" === m ? m : "js") ? "iframe" : "img" === m ? m : "script",
                c = "//" + __readParam("ddn", "domain", "tags.rd.linksynergy.com", i) + "/" + m + "/" + u,
                p = __readParam("dis", "includeStatus", "false", i), f = "";
            if (null != o && "" != o && (p && "EXISTING" == o.toUpperCase() || p && "RETURNING" == o.toUpperCase()) && (f = "R_"), !n.orderid || !n.conversionType) return !1;
            r = 0, a = f + n.orderid, e = "", t = "conv", d = n.currency;
            for (var y = 0; y < (n.lineitems ? n.lineitems.length : 0); y++) r += Number(n.lineitems[y].unitPriceLessTax) * Number(n.lineitems[y].quantity) || Number(n.lineitems[y].unitPrice) * Number(n.lineitems[y].quantity), e += encodeURIComponent(n.lineitems[y].SKU) + ",";
            r = Math.round(100 * (r - s)) / 100, (e = e.slice(0, -1)) && (c = c.indexOf("?") > -1 ? c + "&prodID=" + e : c + "/?prodID=" + e), a && (c = c.indexOf("?") > -1 ? c + "&orderNumber=" + a : c + "/?orderNumber=" + a), r && (c = c.indexOf("?") > -1 ? c + "&price=" + r : c + "/?price=" + r), d && (c = c.indexOf("?") > -1 ? c + "&cur=" + d : c + "/?cur=" + d), t && (c = c.indexOf("?") > -1 ? c + "&pt=" + t : c + "/?pt=" + t);
            var _ = document.createElement(l);
            _.src = c, "script" === l ? _.type = "text/javascript" : "iframe" === l && _.setAttribute("style", "display: none;"), document.getElementsByTagName("body")[0].appendChild(_)
        }

        function sSearch() {
            var e = window.DataLayer && window.DataLayer.Sale && window.DataLayer.Sale.Basket ? window.DataLayer.Sale.Basket : {},
                a = e.searchConfig || {}, r = __readParam("smid", "rsMID", "", a);
            if (!r) return !1;
            var t = function () {
                var t = e.discountAmount ? Math.abs(Number(e.discountAmount)) : 0,
                    n = __readParam("sct", "conversionType", "conv", a), i = 0, o = "";
                if (!e.orderid) return !1;
                i = 0, o = e.orderid;
                for (var s = 0; s < (e.lineitems ? e.lineitems.length : 0); s++) i += Number(e.lineitems[s].unitPrice) * Number(e.lineitems[s].quantity);
                i = Math.round(100 * (i - t)) / 100;
                window.DataLayer.Sale.Basket;
                var d = [];
                d[0] = "id=" + r, d[1] = "type=" + n, d[2] = "val=" + i, d[3] = "orderId=" + o, d[4] = "promoCode=" + e.discountCode || "", d[5] = "valueCurrency=" + e.currency || "USD", d[6] = "GCID=", d[7] = "kw=", d[8] = "product=", k_trackevent(d, "113")
            }, n = document.location.protocol.indexOf("s") > -1 ? "https://" : "http://";
            n += "113.xg4ken.com/media/getpx.php?cid=" + r;
            var i = document.createElement("script");
            i.type = "text/javascript", i.src = n, i.onload = t, i.onreadystatechange = function () {
                "complete" != this.readyState && "loaded" != this.readyState || t()
            }, document.getElementsByTagName("head")[0].appendChild(i)
        }

        sRAN(), sDisplay(), sSearch();
    </script>
    <!-- END of Rakuten Marketing Conversion Tag -->

</body>
</html>

