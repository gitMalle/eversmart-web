<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Switching Energy Suppliers Made Easy - Eversmart</title>
   <!-- Font material icon -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons"rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:500" rel="stylesheet">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="<?php echo base_url(); ?>assets/css/mdb.min.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
     <!-- Your custom styles (optional) -->
    <link href="<?php echo base_url(); ?>assets/css/responsive.css" rel="stylesheet">

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/aos.css" />

  <link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>assets/img/favicon.png" />
  <!--- Font Family Add----------------->
  <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/aos.css" />

<link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>assets/img/favicon.png" />


  

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-79295711-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-79295711-1');
	</script>

	<!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','<?php echo $this->config->item('gtm_id'); ?>');</script>
    <!-- End Google Tag Manager -->

    <!-- Hotjar Tracking Code for www.eversmartenergy.co.uk -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:1074765,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>

</head>


<body class="sky-blue-bg">

	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T44QPZV"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <!-- Start your project here-->
<div class="main-rounded-red-header">
		<div class="red-rounded-wave">
		<div class="topbar" id="red_top">			
			
		<?php $this->load->view('layout/menu'); ?>
			<!--- End container-------->
			
		</div>
		<!---end top bar bar----------->	


<!----------------start postcode---------------------->

			<section class="mt0" >
				<div class="boiler_header-h" id="padding-down">					
						<div class="container">						
							<div class="row">							
								<div class="col-md-10 mx-auto text-center">
									<span class="bolier_home-h-l">Our Tariff</span>
									<span class="bolier_home-h-s">Join the Revolution and start Saving</span>
								
								</div>
							</div>							
						</div><!------------------End Container----------------->
							
				</div><!------------------End Postcode-------------->
				
			</section>
<!----------------End postcode---------------------->		
		</div>
		</div>
<!----------------End postcode--------------------->	
<!--------------------End breadcrumbs Care Section------------->				
		<!--------------------Start breadcrumbs Care Section-------------->
			<section class="" id="our-tariff-top">
				<div class="container">
					<div class="row">
							<div class=" col-sm-10 mx-auto aos-item" data-aos="fade-down">					
								<h1 class=" text-center" style="color:#000;">Enter Postcode</h1>
								
							</div>
					</div>
				
				<div class="row">
					<div class="col-sm-6 mx-auto aos-item" data-aos="fade-down">
						<span class="grey-txt-our-tariff" >Enter your postcode to find out tariff details for your area:</span>
						
						 <div class="card card-raised2 card-form-horizontal wow fadeInUp" data-wow-delay="0ms">
							<div class="content">
							  <form id="form-postcode-simple" class="switch-form" name="">
								<div class="row wow fadeInUp" data-wow-delay="400ms" style="padding:12px 0px">
								  <div class="form-group postCodeBox" id="rounder-b-energy">

									<div class="input-group">
									  <div class="md-form" id="p-s-code">
										  <input id="form-postcode" class="form-control" placeholder="Postcode" maxlength="10" required="" type="text">
										</div>
									  <span class="input-group-btn btnNew">
									  <button  class="tariff-btn-sbt btn btn-default btn-round blue-landing-btn waves-effect waves-light  " type="submit" style="">Get Quote</button>
									  </span>
									</div>
								  </div>
								</div>
								<div class="error_msg" style="display:none">
								  <div class="alert alert-danger" role="alert" id="error_message"></div>
								</div>
							  </form>
							</div>
						  </div>
				
					</div>
				</div>
					
				</div>
									
			</section>
<!--------------------End breadcrumbs Care Section-------------->
	<section class="relavent-tariff">
			<div class="container">
					<div class="row text-center">
						<div class="col-sm-8 mx-auto aos-item" data-aos="fade-down">
						<h2>List of Relevant Tariffs</h2>
						<p class="col-sm-8 mx-auto aos-item" data-aos="fade-right">How do we compare to other providers?</p>
						<p class="col-sm-8 mx-auto aos-item" data-aos="fade-left">How can we save you money and help you into a smarter future?</p>
						</div>
						
						<span class="tariff-chart text-center aos-item" data-aos="fade-right">
						<div id="chartContainer" style="height: 300px; width: 100%;"></div>
						<!---<img class="" src="<?php echo base_url(); ?>assets/img/chart-tariff.png">-->
						
	
						
						</span>
					</div>
					
			</div>
		
	
	
	</section>

		
	
<!----------------Start Footer---------------------------->


	<script>
								window.onload = function () {

								//Better to construct options first and then pass it as a parameter
								var options = {
									animationEnabled: true,
									exportEnabled: true,
									data: [
									{
										type: "spline", //change it to line, area, column, pie, etc
										dataPoints: [
											{ x: 10, y: 10 },
											{ x: 20, y: 12 },
											{ x: 30, y: 8 },
											{ x: 40, y: 14 },
											{ x: 50, y: 6 },
											{ x: 60, y: 24 },
											{ x: 70, y: -4 },
											{ x: 80, y: 10 }
										]
									}
									]
								};
								$("#chartContainer").CanvasJSChart(options);

								}
						</script>
						
						<script src="https://canvasjs.com/assets/script/jquery-1.11.1.min.js"></script>
						<script src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>
	<?php $this->load->view('layout/common_footer'); ?>
