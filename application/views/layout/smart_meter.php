<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Eversmart | Smart Meters</title>
    <!-- Font material icon -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:500" rel="stylesheet">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="<?php echo base_url(); ?>assets/css/mdb.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/homecare.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="<?php echo base_url(); ?>assets/css/responsive.css" rel="stylesheet">
    <!--- Font Family Add----------------->
    <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/aos.css" />

  <link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>assets/img/favicon.png" />

    <style>
        body{font-family: 'Quicksand', sans-serif!important; }


        .loading-page {
            margin:20% auto;
            width: 100px
        }

        .loader {
            border: 8px solid #12CFFF;
            border-radius: 50%;
            border-top: 8px solid #B3FFAB;
            width: 90px;
            height: 90px;
            -webkit-animation: spin 2s linear infinite; /* Safari */
            animation: spin 2s linear infinite;
        }

        /* Safari */
        @-webkit-keyframes spin {
            0% { -webkit-transform: rotate(0deg); }
            100% { -webkit-transform: rotate(360deg); }
        }

        @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }
    </style>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-79295711-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-79295711-1');
    </script>
    
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','<?php echo $this->config->item('gtm_id'); ?>');</script>
    <!-- End Google Tag Manager -->

    <!-- Hotjar Tracking Code for www.eversmartenergy.co.uk -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:1074765,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>

</head>

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T44QPZV"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

<div class="cd-panel cd-panel--from-left js-cd-panel-main">
   <header class="cd-panel__header">
   <a class="logo" id="logo-dash" href="<?php echo base_url(); ?>"><img class="evrmenu-log" src="<?php echo base_url(); ?>/assets/images/logo-eversmart-new.png"/></a>
      <a href="#0" class="cd-panel__close js-cd-close"> </a>
   </header>

   <div class="cd-panel__container">
      <div class="cd-panel__content">
      <nav id="userslideoutjs" class="user slideout-menu slideout-menu-right">
      <section class="menu-section">
        <ul class="menu-section-list">

        <?php  if(  empty($this->session->userdata('login_data')) ){?>
      <li><a href="<?php echo base_url(); ?>index.php/user/login">Login</a></li>
   <?php } ?>

     <?php  if(  $this->session->userdata('login_data') ){?>
      <li><a href="<?php echo base_url(); ?>index.php/user/account">My Account</a></li>
      <?php } ?>
        <li><a href="<?php echo base_url(); ?>index.php/quotation">Energy</a></li>
        <!--<li><a href="<?php echo base_url(); ?>index.php/FamilySaver">Family Saver</a></li>-->
        <li><a href="<?php echo base_url(); ?>index.php/Vision">Our Vision</a></li>
        <li><a href="<?php echo base_url(); ?>index.php/Career">Careers</a></li>
        <li><a href="<?php echo base_url(); ?>index.php/Helpfaqs">Help & Faqs</a></li>
        <li><a href="<?php echo base_url(); ?>index.php/contact_us">Contact us</a></li>
        <li><a href="<?php echo base_url(); ?>index.php/Policies">Privacy</a></li>
        <li><a href="<?php echo base_url(); ?>index.php/Terms">T&Cs</a></li>
     <?php  if(  $this->session->userdata('login_data') ){?>
      <li><a href="#" id="logout_user">Log out</a></li>
      <?php } ?>
</ul>
      </section>
    </nav>
    </div> <!-- cd-panel__content -->
   </div> <!-- cd-panel__container -->
</div> <!-- cd-panel -->

<body class="index-page main-page">

  <div class="loading">
  <div class="finger finger-1">
    <div class="finger-item">
      <span></span><i></i>
    </div>
  </div>
  			<div class="finger finger-2">
    <div class="finger-item">
      <span></span><i></i>
    </div>
  </div>
  			<div class="finger finger-3">
    <div class="finger-item">
      <span></span><i></i>
    </div>
  </div>
  			<div class="finger finger-4">
    <div class="finger-item">
      <span></span><i></i>
    </div>
  </div>
  			<div class="last-finger">
    <div class="last-finger-item"><i></i></div>
  </div>
		</div>

  <!-- Start your project here-->
  <div class="cd-main-content" >
        <div class="red-rounded-wave-smart">
        <div class="topbar-smart homemobile">
 
 <div class="container mobileonly">
             <div class="row">


                 <span class="col-sm-12 col-md-4 col-lg-6 px-0 ">
                 <a class="logo" href="<?php echo base_url(); ?>"><img class="evrmenu" src="<?php echo base_url(); ?>/assets/images/logo-eversmart-new.png"/></a>
                 </span>

                 <div class="col-sm-12 col-md-4 col-lg-4  pull-right no-padding mobile_menu text-right hideonmobile" id="mobile_menu" style="position: relative; float:left; padding-top:10px">
         

             <!--
               <p class="accnum">Account Number: </p>
           <span class="product-code-no-d">
                                 <?php
                                     /*if( $this->session->has_userdata('login_data') ){
                                          if( $this->session->userdata('login_data')['signup_type'] == 0 || $this->session->userdata('login_data')['signup_type'] == 1 ){
                                             echo $this->session->userdata('login_data')['account_number'];
                                             }
                                             elseif($this->session->userdata('login_data')['signup_type'] == 3)
                                             {
                                                 echo $this->session->userdata('login_data')['account_number'];
                                             }
                                     }else{
                                         echo '-';
                                     }*/
                                 ?>
                 </span> -->
                 </div>
       <div class="mobileonly dashbaordmobilemenu"><button class="toggle-button-menu-left cd-btn js-cd-panel-trigger" data-panel="main"><img class="responsive_bar_icon" src="<?php echo base_url(); ?>assets/images/menu_bar_responsive.svg" alt=""/> </button></div>
             

             </div>
         </div>
                <?php $this->load->view('layout/menu');  ?>
            </div>
            <!---end top bar bar----------->

            <!----------------start postcode--------------------->
            <section class="postcode_top mt0">
                <div class="boiler_header-h">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-10 mx-auto text-center">
                                <span class="bolier_home-h-l">Smart Meters</span>
                                <span class="bolier_home-h-s">Join the Revolution and start Saving</span>
                            </div>
                        </div>
                    </div><!------------------End Container----------------->

                </div><!------------------End Postcode------------->
            </section>

        </div>
    </div>
    <!----------------End postcode--------------------->

    <section class="what-smart-meter">
        <div class="container">
            <div class="row justify-content-center">
                <span class="what-m-heading">What are Smart Meters?</span>



                <div class="col-lg-5 co-md-6 co-sm-12 pull-right text-right" id="first-meter-mobile">
                    <img class="sm-icon-meter" src="<?php echo base_url() ?>assets/img/sm-icon-metern.png" alt=""/>
                </div>

                <div class="col-lg-5 co-md-6 co-sm-12 pull-left text-left">
                    <span class="detail-smart-meter">Smart Meters are Gas and<br> Electricity meters that help<br> customers with smart energy </span>

                    <span class="get-m-s"><a href="#" class="get-meter-btn">Get Smart Meter</a></span>

                </div>

                <div class="col-lg-5 co-md-6 co-sm-12 pull-right text-right" id="first-meter-desktop">
                    <img class="sm-icon-meter" src="<?php echo base_url() ?>assets/img/sm-icon-metern.png" alt=""/>
                </div>

            </div>
        </div>
    </section>

    <!--------------------start ever Repair---->
    <section class="ever_reapir" id="ever_reapir_gap">

        <div class="annaul_bg_box2" id="background-none" >
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 col-md-6 col-lg-7"></div>
                        <div class="col-sm-12 col-md-6 col-lg-5 pull-right">


                            <h3 style="font-size:35px; line-height:45px;">What are the benefits of a Smart Meter?</h3>

                            <span style="font-size: 25px; line-height: 35px; color: #FFF">A Smart Meter does everything your normal energy meter does, but it also sends automatic gas and electricity readings to your supplier and displays your energy consumption.</span>

                            <a href="<?php echo base_url(); ?>index.php/contact_us"  class="get-care btn btn-md  btn-round weight-300 wow fadeInUp switchButton waves-effect waves-light pull-left ">BOOK AN APPOINTMENT </a>


                        </div>
                    </div>
                </div>
        </div>

    </section>

    <!--------------------Start breadcrumbs Care Section------------->
    <section class="three-care-b home-ser-col-full" id="sm-meter-three-col">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <h1 class="b-replace-h">Why upgrade to a Smart Meter?</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-4 col-lg-4 text-center">
						<span class="boiler-care b-replace-box">
							<a href="#"><img src="<?php echo base_url() ?>assets/img/b1.jpg" alt=""/></a>

							<h4>Keep in Touch</h4>

                                <span class="b-replace-detail">We can now send messages to your IHD. We can notify you of any loyalty rewards added to your account. </span>
							<span class="sm-learn-more"><a href="#">Learn More?</a></span>

						</span>
                </div><!---------End Box One----------->
                <div class="col-sm-12 col-md-4 col-lg-4 text-center">
						<span class="boiler-care b-replace-box">
							<a href="#"><img src="<?php echo base_url() ?>assets/img/b2.png" alt=""/></a>

							<h4>Visibility of Usage</h4>

							<span class="b-replace-detail">With the in Home Device we will install for you, keeping up to date with your usage has never been easier.  </span>
							<span class="sm-learn-more"><a href="#">Learn More?</a></span>

						</span>
                </div><!---------End Box One----------->

                <div class="col-sm-12 col-md-4 col-lg-4 text-center">
						<span class="boiler-care b-replace-box">
							<a href="#"><img src="<?php echo base_url() ?>assets/img/b3.png" alt=""/></a>

							<h4>View Live Usage</h4>

							<span class="b-replace-detail">Are you actually aware of how much electric your appliances use? Your IHD can help you save. </span>

							<span class="sm-learn-more"><a href="#">Learn More?</a></span>


						</span>
                </div><!---------End Box One----------->

            </div>

        </div><!------------------End Container----------------->
    </section>
    <!--------------------End top Section------------->

    <!--------------------start annual Section---->
    <section class="sm-last-section" >
        <div class="annaul_bg_box2">
            <div class="bottom-rounded-wave" id="repair-box-pd">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <h3>How do I get my free smart meter?</h3>


                        </div>

                    </div>
                    <div class="row mtop50">
                        <div class="col-sm-12 col-md-4 col-lg-4 text-center">
									<span class="boiler-care">
										<img class="meter-three-icon" src="<?php echo base_url() ?>assets/img/Switching.svg" alt="">
										<span class="r-step-h">Step 01</span>
										<span class="r-step-m-h">Make the Switch</span>
										<p class="r-step-m-triff">To qualify for your free smart meter firstly choose one of our cost saving tarrifs for your property.</p>
									</span>
                        </div><!---------End Box One---------->
                        <div class="col-sm-12 col-md-4 col-lg-4 text-center">
									<span class="boiler-care">
										<img class="meter-three-icon" src="<?php echo base_url() ?>assets/img/Settings.svg" alt="">
										<span class="r-step-h">Step 02</span>
										<span class="r-step-m-h">We do the Work</span>
										<p class="r-step-m-triff">Already a Customer? Book an appointment and we will take care of all the preparations for your new smart meter.</p>
									</span>
                        </div><!---------End Box One---------->

                        <div class="col-sm-12 col-md-4 col-lg-4 text-center">
									<span class="boiler-care">
										<img class="meter-three-icon" src="<?php echo base_url() ?>assets/img/Energy-usage-yes.svg" alt="">
										<span class="r-step-h">Step 03</span>
										<span class="r-step-m-h">Get your Meter</span>
										<p class="r-step-m-triff">Get your smart meter installed by one of our certified engineers and start to see the benefits</p>
									</span>
                        </div><!---------End Box One---------->

                    </div>

                    <div class="row justify-content-center">
                        <div class="col-sm-12 col-md-4 col-lg-4 text-right" id="btn-center">
                            <a id="sm-meter-btn" style="float: right;padding: 10px 64px !important;" href="<?php echo base_url(); ?>index.php/quotation"  class="get-care btn btn-md  btn-round weight-300 wow fadeInUp switchButton waves-effect waves-light pull-left ">SWITCH NOW </a>

                        </div>
                        <div class="col-sm-12 col-md-4 col-lg-4 text-left" id="btn-center">
                            <a href="<?php echo base_url(); ?>index.php/contact_us"  class="get-care btn btn-md  btn-round weight-300 wow fadeInUp switchButton waves-effect waves-light pull-left ">BOOK AN APPOINTMENT </a>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </section>
    <!---------------end Annual Section------------------->

    <!----------------Start Footer--------------------------->
    <?php
        $this->load->view('layout/common_footer');
    ?>