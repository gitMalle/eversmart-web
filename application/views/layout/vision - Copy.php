
<!----------------start postcode-------------------->

			<section class="mt0 ">
				<div class="boiler_header-h">					
						<div class="container">						
							<div class="row">							
								<div class="col-md-10 mx-auto text-center">
									<span class="bolier_home-h-l">Our Vision</span>
									<span class="bolier_home-h-s">Join the Revolution and start Saving</span>
								</div>
							</div>							
						</div><!------------------End Container---------------->
							
				</div><!------------------End Postcode------------>
				
			</section>
<!----------------End postcode-------------------->		
		</div>
		</div>
			
		<!--------------------Start breadcrumbs Care Section------------>

		<!--------------- Careers Download------------------------>
		
			<section class="our-vision-first cd-section">
			<div class="cd-section-grey">
				<div class="container">
					<div class="row">
						<div class="col-md-10 mx-auto text-center">
							<span class="our-vision-hd aos-item" data-aos="fade-down">Life is complicated enough – at least your energy can be simple</span>							
						
						</div>
					</div>
					
							<div class="row justify-content-center vision-gap-f ">
					
								<div class="col-sm-12 col-md-6 col-lg-5 pull-left aos-item" data-aos="fade-right">
								<img class="responsive" src="<?php echo base_url(); ?>assets/img/vision.png" alt="">
							
							</div>						
							<div class="col-sm-12 col-md-6 col-lg-5 aos-item" data-aos="fade-left">								
								<p>At Eversmart we offer affordable energy with great customer service and no hassle.</p>
								<p>Don’t have time to call us on the phone? That’s fine – send us a text, send us a tweet or message us on Facebook!</p>	
								<p>The way people communicate is changing, so why should your energy company keep doing things the old way?</p>						
							</div>							
						</div>				
				</div>
				</div>	
			</section>
		
		
			<!--------------- End Careers Download----------------------->


<!--------------------start annual Section---->
			<section class="our-mision-first ">
				<div class="container">									
							<div class="row inner-mission">
								<div class="col-sm-12 col-md-5 col-lg-5 aos-item" data-aos="fade-right">
								<h1>How do we do it?</h1>									
									<p>We offer some of the cheapest energy tariffs in the UK.</p> 
									<p>Unlike the ‘big 6’ energy firms, we’re smaller, smarter and more flexible – and we’re not tied down by some of the government obligations that apply to the bigger companies.</p>
									<p>By running our company efficiently, we can pass our savings on to you!</p>							
								</div>
																	
						</div>				
					
					</div><!------------------End Container----------------->
					
			</section>
<!---------------end Annual Section------------------->



<!-----------------------use for Mobile responsive---------------->
	   <section class="mobile-responsive-shape">
			<div class="mobile-responsive-shape-top">
				<div class="mobile-responsive-shape-bottom">
				<div class="container">
					<div class="row mob-top-padding">
					<div class="col-sm-12 col-md-6 col-lg-6 float-right">
						<span class="text-center">
						<img class="rounded-circle img-fluid" src="<?php echo base_url() ?>assets/images/conatct_support.jpg" alt=""/>
												
						</div>

						<div class="col-sm-12 col-md-6 col-lg-6 aos-item" data-aos="fade-left">
						<span class="text-center">
							<h2>How do we do it?</h2>
							<p>We offer some of the cheapest energy tariffs in the UK.</p> 
									<p>Unlike the ‘big 6’ energy firms, we’re smaller, smarter and more flexible – and we’re not tied down by some of the government obligations that apply to the bigger companies.</p>
									<p>By running our company efficiently, we can pass our savings on to you!</p>
							
							</span>
						</div>
					</div>
				</div>		
				</div>
			</div>
		</section>
<!-----------------------use for Mobile responsive---------------->


		<!--------------- Careers Download------------------------>
		
			<section class="our-vision-last">
				
				<div class="container">
					<div class="row">
						<div class="col-md-10 mx-auto text-center" >
							<span class="our-vision-hd aos-item" data-aos="fade-down">What does the future hold?</span>						
						
						</div>
					</div>
					
							<div class="row justify-content-center vision-gap-f">
					
								<div class="col-sm-12 col-md-6 col-lg-5 aos-item" data-aos="fade-right">
								<img class="responsive" src="<?php echo base_url(); ?>assets/img/vision.png" alt="">
							
							</div>						
							<div class="col-sm-12 col-md-6 col-lg-5 pull-right text-center aos-item" data-aos="fade-left">								
								<p>Affordable energy is just the beginning.</p>
								<p>We’re already offering free energy-efficient boilers to qualifying households, and we will keep on looking for new ways to make your home easier and more affordable to run. </p>
								<p style="font-weight:500">Ready to enjoy cheaper, smarter energy? Join Eversmart today!</p>							
							</div>							
						</div>				
					
				</div>	
			</section>
		
		
			<!--------------- End Careers Download----------------------->