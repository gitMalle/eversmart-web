<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Eversmart | Can't find your property?</title>       
	 <!-- Font material icon -->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons"rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:500" rel="stylesheet"> 
	  <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="<?php echo base_url(); ?>assets/css/mdb.min.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
	   <!-- Your custom styles (optional) -->
    <link href="<?php echo base_url(); ?>assets/css/responsive.css" rel="stylesheet">
	<!--- Font Family Add----------------->
	<link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet"> 
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/aos.css" />

  <link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>assets/img/favicon.png" />


<style>
#clickmsginter {
    color: #ea495c;
    text-align: center;
    line-height: 28px;
    width: 100%;
    margin-bottom: 0;
    font-weight: 400;
    margin-top: 0;
    font-size: 33px;
    font-weight: 400;
}
</style>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-79295711-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-79295711-1');
	</script>

	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','<?php echo $this->config->item('gtm_id'); ?>');</script>
    <!-- End Google Tag Manager -->

    <!-- Hotjar Tracking Code for www.eversmartenergy.co.uk -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:1074765,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>

</head>

<body class="sky-blue-bg">

	<!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T44QPZV"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <!-- Start your project here-->
	
	<div class="main-rounded-red-header">
		<div class="red-rounded-wave">
		<div class="topbar" id="red_top">
			<?php $this->load->view('layout/menu'); ?>
	
			
		</div>


			<section class="postcode_top mt0">
				<div class="postcode_box" style="background:none; padding-bottom:290px;">
					
						<div class="container">
						<span class="back_step" id="media-mobile-m">
										<a href="javascript:void(0)" id="back_postcode" class="material-icons">
									<img style="background:none; padding:0; height:auto; width:60px;" src="<?php echo base_url(); ?>assets/images/backnew-mobile.png"></a> </span>

									<span class="back_step" id="main-bak-dd">
										<a href="<?php echo base_url(); ?>index.php/quotation" id="back_postcode" class="material-icons" >
											<!-- <a href="javascript:void(0)" id="back_postcode" class="material-icons" style="top:-61px;"> -->

											<img src="<?php echo base_url(); ?>assets/images/red-back-button.svg">
										</a> </span>
						
							<div class="row">
								<div class="col-md-10 mx-auto text-center">
									<h2>Can't find your <strong>property?</strong></h2>
									
								</div>
							</div>
							<div class="row">
								<div class="switch_box_main">
								<div class="row justify-content-center">
								
										<div class="col-md-8 text-center">
											<div class="gas_card ">
											<div class="row">
											<span class="col-sm-10 mx-auto">
												<span class="notification_txt" id="ever_busi-h">Please get in touch</span>
												</span>
											</div>
									<div class="row">
										<span class="col-6">
										<span class="ring-box">
												<span class="col-phone-icon">
											     <i class="material-icons"> <a href="tel:03301027901"><img class="red-call-icon" src="<?php echo base_url(); ?>assets/img/phone-footers.svg" alt=""/></a></i>
												</span>
												<span class="col-icon-detail">
												 <a href="tel:03301027901"> 0330 102 7901</a><br><br>
												 <a href="tel:03301027901" style="color:#aaa;font-size:17px;line-height 21px;padding-top:18px;text-align:center;">Lines are open 8am–8pm,<br> 7 days a week</a>
											   </span>
											   </span>
										</span>	

										<span class="col-6" style="left: -20px;">
										<span class="ring-box" id="clickmsginter">
												<span class="col-phone-icon">
											     <i class="material-icons"><img  class="red-call-icon" src="<?php echo base_url(); ?>assets/img/msg-footers.svg" alt=""/></i>
												</span>
												<span class="col-icon-detail">
												<div>Chat to an agent</div>
												<p>Our agents are online 8am–8pm,<br> 7 days a week</p> 
											   </span>
											   </span>
										</span>	

									
										</div>
											
											
											</div>
										</div>
										
		
													
								</div>
								
								
								</div>
							
							</div>
						</div>
						</div>
					</section>
					
					</div>
					</div>

	<?php $this->load->view('layout/common_footer'); ?>