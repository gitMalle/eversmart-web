
        <!------------------------------- START MAIN BODY ------------------------------->

        <tr>
            <td valign="top" id="templateBody">

                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
                    <tbody class="mcnTextBlockOuter">
                    <tr>
                        <td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
                            <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                                <tr>
                                    <td valign="top" width="600" style="width:600px;">
                                        <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%; min-width:100%;" width="100%" class="mcnTextContentContainer">
                                            <tbody>
                                            <tr>
                                                <td valign="top" class="mcnTextContent" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;">

                                                    <h1 style="margin: 50px 0px; text-align: left;">
                                                        <span style="font-family: 'Quicksand', sans-serif; font-weight:normal; font-size:46px; color: #1C3659;">We have objected to your switch</span></span>
                                                    </h1>

                                                    <font style="font-family: 'Quicksand', sans-serif;">
                                                        <span style="font-size:17px">
                                                            <strong>Customer No: <?= $email_info['customer_number']; ?></strong><br>
                                                            <strong>Bill No: <?= $email_info['bill_number']; ?></strong><br><br>
                                                            Hello <?= $email_info['name']; ?>,<br><br>
                                                            We understand you are trying to switch to another energy supplier. Eversmart Energy have <strong>objected to this switch taking place</strong>. We have attached a letter explaining the reasons for the objection.<br><br>
                                                            <strong>Why has this happened?</strong><br>
                                                            Energy suppliers can object to a customer leaving if there is a problem with their account – for example an unpaid debt. You cannot switch to another supplier until the problem has been resolved.<br><br>
                                                            If you have any further questions, please contact us at <a href="mailto:hello@eversmartenergy.co.uk" target="_blank">hello@eversmartenergy.co.uk</a>.<br><br>
                                                            From the Eversmart Team
                                                        </span>
                                                    </font>
                                                    <br><br><br>
                                                </td>

                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>

        <!------------------------------- END MAIN BODY ------------------------------->