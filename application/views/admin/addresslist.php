	<select name="address_full" id="quotation_address" style="font-size:1rem;background-color:white" class="custom-select  form-control paddingtb">
        <option value="0">(Please Select Address)</option>
        <?php
            if (!empty($address_list)) {

                for ($j=0; $j<count($address_list); $j++) {

                    $stripped = preg_replace('/\s+/', ' ', $address_list[$j]['address']); ?>

                    <option value="<?php echo $stripped ?>"
                            data-selectedpostcode="<?php echo $address_list[$j]['meteringPointPostcode']; ?>"
                            data-mpan-serial="<?php echo $address_list[$j]['meterIdSerialNumber']; ?>">
                        <?php echo $stripped; ?>
                    </option>
                    <?php
                }
            }
        ?>
    </select>

<div class="gasdropdownlist" style="display: none">
    <select name="quotation_address_gas" id="quotation_address_gas" style="font-size:1rem;background-color:white" class="custom-select  form-control paddingtb">
        <option value="0">(Select Gas Address)</option>
        <?php
            if (!empty($address_list_gas)) {

                for ($j=0; $j<count($address_list_gas); $j++ ) {

                    $stripped_gas = preg_replace('/\s+/', ' ', $address_list_gas[$j]['address']); ?>

                    <option data-selectedpostcode_gas="<?php echo $address_list_gas[$j]['meteringPointPostcode']; ?>" value="<?php echo $stripped_gas ?>">
                        <?php echo $stripped_gas; ?>
                    </option>
                    <?php
                }
            }
        ?>
        <option value="noaddress">My address is not listed</option>
    </select>
</div>

<div class="missingmeters" style="display: none;">
    <input class="form-control input-md" type="text" value="" id="checkinput" name="checkinput" placeholder="Enter meter serial number" maxlength="13">
    <button id="check_msn_btn" class="btn btn-primary" type="submit">Check</button>
    <div id="msn_msg"></div>
</div>

<div class="multiplemeters" style="display: none;">
    <input class="form-control input-md" type="text" value="" id="checkinput2" name="checkinput2" placeholder="Enter meter serial number" maxlength="13">
    <button id="check_msn_btn2" class="btn btn-primary" type="submit">Check</button>
    <div id="msn2_msg"></div>
</div>
