<?php
// Redirect if not logged in
$AdminUser = $this->session->userdata('login_data');
if(empty($AdminUser) || $AdminUser['admin_role_type_id'] !='1'){
    $this->load->helper('url');
    redirect('/', 'refresh');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Admin - Email Templates</title>
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>assets/img/favicon.png" />
</head>
<body>
<div class="topbar-admin col-lg-12">
    <a href="<?php echo base_url(); ?>">
        <img src="<?php echo base_url(); ?>assets/images/logo-eversmart-new.png">
    </a>

    <a class="admin-links" href="<?php echo base_url() ?>Admin/admin_customer_signup">Customer Sign-up</a>
    <a class="admin-links" href="<?php echo base_url() ?>Admin/admin_activation_email">Resend Emails</a>
    <a class="admin-links" href="<?php echo base_url() ?>Admin/admin_webtocase">Web to Case</a>
    <a class="admin-links admin-here" href="<?php echo base_url() ?>Admin/admin_email_templates">Email Templates</a>
    <a class="admin-links" href="<?php echo base_url() ?>Admin/admin_datacapture">Capture Form</a>
    <a class="admin-links" href="<?php echo base_url() ?>Admin/admin_leadupdate">Lead Update</a>
    <a class="admin-links" href="<?php echo base_url() ?>Admin/admin_sassquatch_update">Friend Referrals</a>
    <a class="admin-links" href="<?php echo base_url() ?>Admin/admin_show_pending_energy_registration">Pending Registrations</a>


    <select class="mob-admin-links" onchange="location=this.value">
        <option value="<?php echo base_url() ?>Admin/admin_customer_signup">Customer Sign-up</option>
        <option value="<?php echo base_url() ?>Admin/admin_webtocase">Web to Case</option>
        <option value="<?php echo base_url() ?>Admin/admin_activation_email">Resend Emails</option>
        <option value="<?php echo base_url() ?>Admin/admin_email_templates" selected>Email Templates</option>
        <option value="<?php echo base_url() ?>Admin/admin_datacapture">Capture Form</option>
        <option value="<?php echo base_url() ?>Admin/admin_leadupdate">Lead Update</option>
        <option value="<?php echo base_url() ?>Admin/admin_sassquatch_update">Friend Referrals</option>
        <option value="<?php echo base_url() ?>Admin/admin_show_pending_energy_registration">Pending Registrations</option>
    </select>

    <a class="admin-logout" href="#" onclick="logout_user()">Log out</a>
</div>

<div class="container custform col-lg-12">

    <form id="search-params">
        <fieldset>
            <div class="row contact_row">
                <div class="contact_col first_col col-md-6">

                    <legend>Search Parameters</legend>
                    <div class="line-sep"></div>

                    <div class="form-group">
                        <label class="control-label form-style-label" for="id_param">Customer ID</label>
                        <div class="form-style-field-lg">
                            <input id="id_param" name="id_param" type="text" onkeypress="return isNumberKey(event)" placeholder="" maxlength="20" spellcheck="false" value="000299" class="form-control input-md" required>
                        </div>
                    </div>

                </div>

                <div class="contact_col second_col col-md-6">

                    <legend>Select Email Type</legend>
                    <div class="line-sep"></div>

                    <select id="email_type" name="email_type" class="form-control input-md" style="margin-bottom: 20px">
                        <option value="">(Please Select)</option>
                        <option value="preview_activation_email">Activation</option>
                        <option value="preview_balance_overdue_1">Balance Overdue 1</option>
                        <option value="preview_balance_overdue_2">Balance Overdue 2</option>
                        <option value="preview_balance_overdue_3">Balance Overdue 3</option>
                        <option value="preview_book_meter">Book Meter</option>
                        <option value="preview_direct_debit_changes">Direct Debit Changes</option>
                        <option value="preview_direct_debit_confirmation">Direct Debit Confirmation</option>
                        <option value="preview_direct_debit_problem">Direct Debit Problem</option>
                        <option value="preview_first_meter_reading">First Meter Reading</option>
                        <option value="preview_latest_energy_bill">Latest Energy Bill</option>
                        <option value="preview_leaving">Leaving</option>
                        <option value="preview_moving_home_email">Moving Home</option>
                        <option value="preview_new_payment_plan">New Payment Plan</option>
                        <option value="preview_new_payment_schedule">New Payment Schedule</option>
                        <option value="preview_objection">Objection</option>
                        <option value="preview_payment_plan_cancelled">Payment Plan Cancelled</option>
                        <option value="preview_previous_supplier_objection">Previous Supplier Objection</option>
                        <option value="preview_provide_meter_reading">Provide Meter Reading</option>
                        <option value="preview_registration_withdrawal">Registration Withdrawal</option>
                        <option value="preview_reset_password">Reset Password</option>
                    </select>
                </div>
            </div>

            <div class="row search_row">
                <button id="id-lookup" name="id-lookup" class="btn btn-primary">Look Up</button>
            </div>

            <div class="row submit_row">
                <div class="contact_col first_col col-md-6">

                    <legend>Customer Details</legend>
                    <div class="line-sep"></div>

                    <div class="form-group">
                        <label class="control-label form-style-label" for="name_param">Forename</label>
                        <div class="form-style-field-lg">
                            <input id="name_param" name="name_param" type="text" onkeypress="return !isNumberKey(event)" placeholder="" maxlength="25" spellcheck="false" value="Barney" class="form-control input-md" required>
                        </div>
                    </div>

                    <div id="post_code" class="form-group">
                        <label class="control-label form-style-label" for="pcod_param">Post Code</label>
                        <div class="form-style-field-lg">
                            <input id="pcod_param" name="pcod_param" type="text" placeholder="" maxlength="8" spellcheck="false" class="form-control input-md" required>
                        </div>
                    </div>

                    <div id="phone_num" class="form-group">
                        <label class="control-label form-style-label" for="phone_param">Phone Number</label>
                        <div class="form-style-field-lg">
                            <input id="phone_param" name="phone_param" type="text" onkeypress="return isNumberKey(event)" placeholder="" maxlength="11" spellcheck="false" class="form-control input-md">
                        </div>
                    </div>

                    <div id="bill_num" class="form-group">
                        <label class="control-label form-style-label" for="bill_param">Bill Number</label>
                        <div class="form-style-field-lg">
                            <input id="bill_param" name="bill_param" type="text" onkeypress="return isNumberKey(event)" placeholder="" maxlength="4" spellcheck="false" class="form-control input-md">
                        </div>
                    </div>

                    <div id="new_add" class="form-group">
                        <label class="control-label form-style-label" for="address_param">New Address</label>
                        <div class="form-style-field-lg">
                            <input id="address_param" name="address_param" type="text" placeholder="" maxlength="50" spellcheck="false" class="form-control input-md">
                        </div>
                    </div>

                    <div id="old_dd" class="form-group">
                        <label class="control-label form-style-label" for="old_dd_param">Old DD Amount</label>
                        <div class="form-style-field-lg">
                            <input id="old_dd_param" name="old_dd_param" type="text" onkeypress="return numberDecimals(event)" placeholder="" maxlength="50" spellcheck="false" class="form-control input-md">
                        </div>
                    </div>

                    <div id="new_dd" class="form-group">
                        <label class="control-label form-style-label" for="new_dd_param">New DD Amount</label>
                        <div class="form-style-field-lg">
                            <input id="new_dd_param" name="new_dd_param" type="text" onkeypress="return numberDecimals(event)" placeholder="" maxlength="50" spellcheck="false" class="form-control input-md">
                        </div>
                    </div>

                </div>

                <div class="contact_col second_col col-md-6">

                    <legend>Preview Email</legend>
                    <div class="line-sep"></div>

                    <div class="form-group">
                        <button id="send-email" name="send-email" class="btn btn-primary">Preview</button>
                    </div>
                </div>
            </div>
        </fieldset>
    </form>
</div>

<div id="email_outcome"></div>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/admin_email_templates.js"></script>

</body>
