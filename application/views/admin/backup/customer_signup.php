<?php
// Redirect if not logged in
$AdminUser = $this->session->userdata('login_data');
if(empty($AdminUser) || $AdminUser['admin_role_type_id'] !='1'){
    $this->load->helper('url');
    redirect('/', 'refresh');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Admin - Customer Sign-Up</title>

    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">

    <link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>assets/img/favicon.png" />

    <style>
        .StripeElement {background-color: white;padding:20px 12px 9px;border-bottom: 1px solid #ced4da;}
        .StripeElement--focus {box-shadow: 0 1px 3px 0 #cfd7df;}
        .StripeElement--invalid {border-color: #fa755a;}
        .StripeElement--webkit-autofill {background-color: #fefde5 !important;}
        .card-element{ padding-top:20px; color:#ced4da; font-size:25px; margin-bottom:40px; font-weight:500; }
        .gas_card{padding:40px 80px;}
        .payment-details {display: none; position: relative; width: 100%; margin-bottom: 30px;border-radius: 10px; color: rgba(0,0,0, 0.87); background: #fff;
            box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 1px 5px 0 rgba(0, 0, 0, 0.12); padding:30px 20px;}
        .ElementsApp, .ElementsApp .InputElement{font-size:17px;}
        .pay-detail{font-size:18px; line-height:23px; float:left; width:100%; margin:5px 0px; color:#000; padding:20px;}
        .pay-price {font-size: 22px;font-weight: 500;width: 100%;float: left;color: #ea495c;line-height: 35px;padding: 0 20px 20px;}
        .pay-pay-now{font-size:35px; font-weight:500; width:100%; float:left; color:#000; line-height:40px; padding:20px;}
        #example1-name{margin-bottom: 10px; border-bottom: 1px solid #ced4da!important; border:none;}
        .StripeElement{float:left; width:100%;}

        .process-tab-cs{padding-top:8px;}
        .energy-process-icon{width:90px; height:auto!important;}
        #active-process{color:#000;}
        .days-process{color:#1cb81c; font-size:16px; font-weight:bold; width:33%; text-align:center; float:left; margin-bottom:10px; clear: both;}
        .dash-over-text-green{color:#1cb81c; font-size:16px; font-weight:bold; width:100%; text-align:center; float:left; }
        .dash-over-text{padding:0px 0px 15px 0px;}
        .energy-main-process{padding:50px 0px; float:left; width:100%;}
        .energy-process-icon{ background:#fff; padding:15px;}
        .checkmark{width: 56px;height: 56px;border-radius: 50%;display: block;stroke-width: 2;stroke: #fff;stroke-miterlimit: 10;margin: 60px auto; position: relative;}
        .User-number-eversmart{font-size:45px; color:#fc5462; font-weight:400;float: left;width: 100%;text-align: center; margin: 30px 0px; }
        .dont-have{width:100%; float:left; text-align:center; margin:10px 0px 30px; 0px;}
        .dont-have a{color:#3c99e7; font-size:14px; font-weight:400;}
        .main_card_box .gas_card{width:100%}
        .fre-ques{float:left;font-size:20px; width:100%; margin:23px 0px 10px; color:#666; font-weight:400;}
        .accordion{float:left; width:100%;}
        .pay-form-db-tab{float:left; width:100%;}
        .StripeElement {margin-bottom:10px;}
        #example1-name::placeholder{opacity: 1;color: #aab7c4; font-weight:400; font-family: 'Quicksand',! sans-serif important;}
        #card-element{position:relative;}
        #card-type-icon1{position:absolute; right: 9px;top:22px;z-index:1;}
        #card-type-icon1.fa-cc-visa{font-size:18px;}
        .secure-pay-strip{padding: 170px 0px 10px!important;}
        .left-tab-side ul li a{font-size:19px;}
        .__PrivateStripeElement-input::placeholder{font-family: 'Quicksand',! sans-serif important;}
        .InputElement::placeholder{font-family: 'Quicksand',! sans-serif important;}
        img.img-fluid.stripe {padding-bottom: 25px;}
        .card.card-raised2.card-form-horizontal.wow.fadeInUp.pay {margin-bottom: 0; border: none; box-shadow: none;}
        button.red-btn.btn.btn-md.btn-eversmart.btn-round.weight-300.text-center.wow.fadeInUp.switchButton.waves-effect.waves-light.pay {font-size: 1.3rem;}
        .InputElement{border-top:none;border-left:none;border-right:none;}
    </style>

</head>

<body>

<div class="topbar-admin col-lg-12">
    <a href="<?php echo base_url(); ?>">
        <img src="<?php echo base_url(); ?>assets/images/logo-eversmart-new.png">
    </a>

    <a class="admin-links" href="<?php echo base_url() ?>index.php/Admin/customer_signup">Customer Sign-up</a>
    <a class="admin-links" href="<?php echo base_url() ?>index.php/Admin/admin_activation_email">Resend Emails</a>

    <ul class="menu-section-list">
        <?php  if(  $AdminUser ){?>
            <li><p><?php echo $AdminUser['email']; ?></p></li>
        <?php } ?>

        <?php  if(  $AdminUser ){?>
            <li><a href="#" onclick="logout_user()">Log out</a></li>
        <?php } ?>
    </ul>
</div>

<div class="container custform col-lg-12">

    <div class="row quote_row" id="form-quote">

        <div class="quote_col first_col col-md-6">

            <fieldset>

                <legend>Contact Address</legend>
                <div class="line-sep"></div>

                <div class="form-group">

                    <label class="control-label form-style-label" for="contact-postcode">Postcode</label>

                    <div class="form-style-field-lg">
                        <input id="contact-postcode" name="contact-postcode" type="search" placeholder="" maxlength="10" spellcheck="false" class="form-control input-md">

                    </div>
                </div>

                <div class="form-group">

                    <label class="control-label form-style-label" for="contact-lookup"></label>

                    <div class="form-style-field-lg">
                        <button id="contact-lookup" name="contact-lookup" class="btn btn-primary">Look Up</button>
                    </div>
                </div>

                <div class="form-group">

                    <label class="control-label form-style-label" for="address_select" id="pcod-address"></label>

                    <div class="form-style-field-lg">
                        <div id="address_select"></div>
                    </div>
                </div>

            </fieldset>

        </div>

        <div class="quote_col second_col col-md-6">
            <fieldset>

                <legend>Supply Address</legend>
                <div class="line-sep"></div>

                <div class="form-group">

                    <label class="control-label form-style-label" for="supply-postcode">Postcode</label>

                    <div class="form-style-field-lg">
                        <input id="supply-postcode" name="supply-postcode" type="search" placeholder="" maxlength="10" spellcheck="false" class="form-control input-md">
                    </div>
                </div>

                <div class="form-group">

                    <label class="control-label form-style-label" for="detail-lookupsupply"></label>

                    <div class="form-style-field-lg">
                        <button id="detail-lookupsupply" name="detail-lookupsupply" class="btn btn-primary">Look Up</button>
                    </div>
                </div>

                <div class="form-group">

                    <label class="control-label form-style-label" for="supply-lookup" id="pcod-supp-address"></label>

                    <div class="form-style-field-lg">
                        <div id="supply_select"></div>
                    </div>
                </div>

            </fieldset>

        </div>

        <div class="quote_col third_col col-md-6">

            <fieldset>

                <div class="form-group">
                    <div id="multiple_mpan_select"></div>
                </div>

                <div id="address_na">

                    <div class="form-group">

                        <label class="control-label form-style-label" for="na-add1">Address Line 1</label>

                        <div class="form-style-field-lg">
                            <input id="na-add1" name="na-add1" type="text" placeholder="" maxlength="35" spellcheck="false" class="form-control input-md">
                        </div>
                    </div>

                    <div class="form-group">

                        <label class="control-label form-style-label" for="na-add2">Address Line 2</label>

                        <div class="form-style-field-lg">
                            <input id="na-add2" name="na-add2" type="text" placeholder="" maxlength="35" spellcheck="false" class="form-control input-md">
                        </div>
                    </div>

                    <div class="form-group">

                        <label class="control-label form-style-label" for="na-addtown">Town</label>

                        <div class="form-style-field-lg">
                            <input id="na-addtown" name="na-addtown" type="text" placeholder="" maxlength="20" spellcheck="false" class="form-control input-md">
                        </div>
                    </div>

                    <div class="form-group">

                        <label class="control-label form-style-label" for="na-addcounty">County</label>

                        <div class="form-style-field-lg">
                            <input id="na-addcounty" name="na-addcounty" type="text" placeholder="" maxlength="20" spellcheck="false" class="form-control input-md">
                        </div>
                    </div>

                    <div class="form-group">

                        <label class="control-label form-style-label" for="na-pcod">Postcode</label>

                        <div class="form-style-field-lg">
                            <input id="na-pcod" name="na-pcod" type="text" placeholder="" maxlength="10" spellcheck="false" class="form-control input-md">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label form-style-label" for="na-addcheck"></label>
                        <div class="form-style-field-lg">
                            <div class="checkbox">
                                <input class="na-add-check" type="checkbox" name="na-addcheck" id="na-addcheck-0" value="1">
                                <label class="emailcheck" for="na-addcheck-0" id="na-add-check">Use same address for supply?</label>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="form-group">
                    <label class="control-label form-style-label" for="contact-addcheck"></label>
                    <div class="form-style-field-lg">
                        <div class="checkbox">
                            <input class="add-check" type="checkbox" name="contact-addcheck" id="contact-addcheck-0" value="1">
                            <label class="emailcheck" for="contact-addcheck-0" id="add-check">Use same address for supply?</label>
                        </div>
                    </div>
                </div>

            </fieldset>

        </div>

        <div class="quote_col fourth_col col-md-6">

            <fieldset>

                <div id="supply_na">

                    <div class="form-group">

                        <label class="control-label form-style-label" for="na-supp1">Address Line 1</label>

                        <div class="form-style-field-lg">
                            <input id="na-supp1" name="na-supp1" type="text" placeholder="" maxlength="35" spellcheck="false" class="form-control input-md">
                        </div>
                    </div>

                    <div class="form-group">

                        <label class="control-label form-style-label" for="na-supp2">Address Line 2</label>

                        <div class="form-style-field-lg">
                            <input id="na-supp2" name="na-supp2" type="text" placeholder="" maxlength="35" spellcheck="false" class="form-control input-md">
                        </div>
                    </div>

                    <div class="form-group">

                        <label class="control-label form-style-label" for="na-supptown">Town</label>

                        <div class="form-style-field-lg">
                            <input id="na-supptown" name="na-supptown" type="text" placeholder="" maxlength="20" spellcheck="false" class="form-control input-md">
                        </div>
                    </div>

                    <div class="form-group">

                        <label class="control-label form-style-label" for="na-suppcounty">County</label>

                        <div class="form-style-field-lg">
                            <input id="na-suppcounty" name="na-suppcounty" type="text" placeholder="" maxlength="20" spellcheck="false" class="form-control input-md">
                        </div>
                    </div>

                    <div class="form-group">

                        <label class="control-label form-style-label" for="na-supppcod">Postcode</label>

                        <div class="form-style-field-lg">
                            <input id="na-supppcod" name="na-supppcod" type="text" placeholder="" maxlength="10" spellcheck="false" class="form-control input-md">
                        </div>
                    </div>

                </div>

            </fieldset>

        </div>

        <div class="quote_col fifth_col col-md-6">

            <fieldset>

                <legend>Tariff & Direct Debit</legend>
                <div class="line-sep"></div>

                <div class="form-group">

                    <label class="control-label form-style-label" for="tariff-tariff">Tariff</label>

                    <div class="form-style-field-lg">
                        <div id="tariffselect" style="width:100%;"></div>
                    </div>
                </div>

                <div class="form-group" id="pay-type">

                    <label style="margin-top: 0px" class="control-label form-style-label" for="tariff-pay">Payment Type</label>

                    <div class="form-style-field-lg">
                        <select id="tariff-pay" name="tariff-pay" class="form-control">
                            <option value="default">(Please Select) </option>
                            <option value="2">Standard</option>
                            <option value="3">Bank Transfer</option>
                            <option value="4">Cheque</option>
                            <option value="5">One-off Direct Debit</option>
                            <option value="6">Stripe</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">

                    <label class="control-label form-style-label" for="sel_prev_supplier">Prev Supplier</label>

                    <div class="form-style-field-lg">
                        <div id="sel_prev_supplier" style="width:100%;"></div>
                    </div>
                </div>

                <div class="form-group">

                    <label class="control-label form-style-label" for="sel_prev_tariff">Prev Tariff</label>

                    <div class="form-style-field-lg">
                        <select id="sel_prev_tariff_empty" name="sel_prev_tariff_empty" class="form-control" style="width:100%;">
                            <option value="1">(Please Select Tariff)</option>
                        </select>
                        <div id="sel_prev_tariff" style="width:100%;"></div>
                    </div>
                </div>

            </fieldset>

        </div>

        <div class="quote_col sixth_col col-md-6">

            <fieldset>
                <legend>Additional Details</legend>
                <div class="line-sep"></div>

                <div class="form-group">

                    <label class="control-label form-style-label" for="mtrpoint-mpan-eac">Home Type (Elec)</label>

                    <div class="form-style-field-lg">
                        <select id="mtrpoint-mpan-eac" name="mtrpoint-mpan-eac" class="form-control">
                            <option value="default">(Please Select) </option>
                            <option value="2">1-2 bed Flat</option>
                            <option value="3">1-2 bed Terrace</option>
                            <option value="4">1-2 bed Semi</option>
                            <option value="5">1-2 bed Detached</option>
                            <option value="6">3-4 bed Flat</option>
                            <option value="7">3-4 bed Terrace</option>
                            <option value="8">3-4 bed Semi</option>
                            <option value="9">3-4 bed Detached</option>
                            <option value="10">5+ bed Flat</option>
                            <option value="11">5+ bed Terrace</option>
                            <option value="12">5+ bed Semi</option>
                            <option value="13">5+ bed Detached</option>
                        </select>
                    </div>
                </div>

                <div class="form-group" id="eac-or">

                    <label class="control-label form-style-label" for="eac-override">Or Enter EAC</label>

                    <div class="form-style-field-lg">
                        <input id="eac-override" name="eac-override" type="number" spellcheck="false" class="form-control input-md">
                    </div>
                </div>

                <div class="form-group">

                    <label class="control-label form-style-label" for="mtrpoint-mprn-aq">Home Type (Gas)</label>

                    <div class="form-style-field-lg">
                        <select id="mtrpoint-mprn-aq" name="mtrpoint-mprn-aq" class="form-control">
                            <option value="default">(Please Select) </option>
                            <option value="2">1-2 bed Flat</option>
                            <option value="3">1-2 bed Terrace</option>
                            <option value="4">1-2 bed Semi</option>
                            <option value="5">1-2 bed Detached</option>
                            <option value="6">3-4 bed Flat</option>
                            <option value="7">3-4 bed Terrace</option>
                            <option value="8">3-4 bed Semi</option>
                            <option value="9">3-4 bed Detached</option>
                            <option value="10">5+ bed Flat</option>
                            <option value="11">5+ bed Terrace</option>
                            <option value="12">5+ bed Semi</option>
                            <option value="13">5+ bed Detached</option>
                        </select>
                    </div>
                </div>

                <div class="form-group" id="aq-or">

                    <label class="control-label form-style-label" for="aq-override">Or Enter AQ</label>

                    <div class="form-style-field-lg">
                        <input id="aq-override" name="aq-override" type="number" spellcheck="false" class="form-control input-md">
                    </div>
                </div>

            </fieldset>

        </div>

        <div class="quote_col seventh_col col-md-6">

            <fieldset>

                <legend>Electricity</legend>

                <div id="main-mpan">
                    <div class="line-sep lsfix"></div>

                    <div class="form-group">

                        <label class="control-label form-style-label" for="mtrpoint-mpancheck">Original MPAN</label>

                        <div class="form-style-field-lg">
                            <div class="checkbox">
                                <input class="mpan-check" type="checkbox" name="mtrpoint-mpancheck" id="mtrpoint-mpancheck-main" value="mpan">
                                <label class="mpan-check-label" id="mpan-found-main" for="mtrpoint-mpancheck-main"></label>
                            </div>

                        </div>
                    </div>

                </div>

                <div id="mpan-existing">
                    <div class="line-sep"></div>

                    <div class="form-group">

                        <label class="control-label form-style-label" for="mtrpoint-mpancheck">MPAN</label>

                        <div class="form-style-field-lg">
                            <div class="checkbox">
                                <input class="mpan-check" type="checkbox" name="mtrpoint-mpancheck" id="mtrpoint-mpancheck-0" value="mpan">
                                <label class="mpan-check-label" id="mpan-found" for="mtrpoint-mpancheck-0"></label>
                            </div>

                        </div>
                    </div>

                </div>

                <div class="line-sep"></div>
                <div class="form-group">

                    <label class="control-label form-style-label" for="mtrpoint-mpan">New MPAN</label>

                    <div class="form-style-field-lg">
                        <input id="mtrpoint-mpan" name="mtrpoint-mpan" type="text" placeholder="" onkeypress="return isNumberKey(event)" maxlength="13" spellcheck="false" class="form-control input-md">
                    </div>
                </div>

                <div class="form-group">

                    <label class="control-label form-style-label" for="mtrpoint-mpanadd"></label>

                    <div class="form-style-field-lg">
                        <button id="mtrpoint-mpanadd" name="mtrpoint-mpanadd"  class="btn btn-primary">Add</button>
                    </div>
                </div>

            </fieldset>

        </div>

        <div class="quote_col eigth_col col-md-6">

            <fieldset>

                <legend>Gas</legend>

                <div id="main-mprn">
                    <div class="line-sep lsfix"></div>

                    <div class="form-group">

                        <label class="control-label form-style-label" for="mtrpoint-mprncheck">Original MPRN</label>

                        <div class="form-style-field-lg">
                            <div class="checkbox">
                                <input class="mprn-check" type="checkbox" name="mtrpoint-mprncheck" id="mtrpoint-mprncheck-main" value="mprn">
                                <label class="mprn-check-label" id="mprn-found-main" for="mtrpoint-mprncheck-main"></label>
                            </div>

                        </div>
                    </div>

                </div>

                <div id="mprn-existing">
                    <div class="line-sep lsfix"></div>

                    <div class="form-group">

                        <label class="control-label form-style-label" for="mtrpoint-mprncheck">MPRN</label>

                        <div class="form-style-field-lg">
                            <div class="checkbox">
                                <input class="mprn-check" type="checkbox" name="mtrpoint-mprncheck" id="mtrpoint-mprncheck-0" value="mpan">
                                <label class="mprn-check-label" id="mprn-found" for="mtrpoint-mprncheck-0"></label>
                            </div>

                        </div>
                    </div>

                </div>

                <div class="line-sep"></div>

                <div class="form-group">

                    <label class="control-label form-style-label" for="mtrpoint-mprn">New MPRN</label>

                    <div class="form-style-field-lg">
                        <input id="mtrpoint-mprn" name="mtrpoint-mprn" type="text" placeholder="" onkeypress="return isNumberKey(event)" maxlength="11" spellcheck="false" class="form-control input-md">
                    </div>
                </div>

                <div class="form-group">

                    <label class="control-label form-style-label" for="mtrpoint-mprnadd"></label>

                    <div class="form-style-field-lg">
                        <button id="mtrpoint-mprnadd" name="mtrpoint-mprnadd" class="btn btn-primary">Add</button>
                    </div>
                </div>

            </fieldset>

        </div>

        <div class="container" style="margin-top:40px">
            <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-10">
                    <fieldset>

                        <div class="line-sep"></div>

                        <div class="form-group">

                            <button id="get-quote" name="get-quote" class="btn btn-primary">GET QUOTE</button>

                        </div>

                        <div class="form-group">

                            <div id="admin_quote"></div>

                        </div>

                        <div class="form-group" id="quote-or">

                            <label class="control-label form-style-label" for="quote-override">Quote Override</label>

                            <div class="form-style-field-lg">
                                <input id="quote-override" name="quote-override" type="text" pattern="^[+-]?\d+(\.\d+)?$" placeholder="£" spellcheck="false" class="form-control input-md">
                            </div>
                        </div>

                    </fieldset>
                </div>
                <div class="col-sm-1"></div>
            </div>
        </div>
    </div>

    <div id="submit-panel">
        <div class="row contact_row">

            <div class="contact_col first_col col-md-6">

                <fieldset>

                    <legend>Contact Details</legend>
                    <div class="line-sep"></div>

                    <div class="form-group">

                        <label class="control-label form-style-label" for="contact-title">Name</label>

                        <div class="form-style-field-sm">
                            <select class="form-control input-md" id="contact-title" tabindex="0" required>
                                <option selected value="0">Title</option>
                                <option value="Mr">Mr.</option>
                                <option value="Mrs">Mrs.</option>
                                <option value="Miss">Miss</option>
                                <option value="Ms">Ms</option>
                                <option value="Prof">Prof</option>
                                <option value="Dr">Dr</option>
                            </select>
                        </div>

                        <div class="form-style-field-md">
                            <input id="contact-fname" onkeypress="return !isNumberKey(event)" name="contact-fname" type="text" placeholder="Forename" class="form-control input-md" maxlength="25" spellcheck="false">
                        </div>

                        <div class="form-style-field-md">
                            <input id="contact-lname" onkeypress="return !isNumberKey(event)" name="contact-lname" type="text" placeholder="Surname" class="form-control input-md" maxlength="25" spellcheck="false">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label form-style-label" for="contact-dob">Date of Birth</label>

                        <div class="form-style-field-lg">
                            <input id="contact-dob" name="contact-dob" type="date" placeholder="" class="form-control input-md">
                        </div>
                    </div>

                    <div class="form-group">

                        <label class="control-label form-style-label" for="contact-email">Email</label>

                        <div class="form-style-field-lg">
                            <input id="contact-email" name="contact-email" type="email" placeholder="" spellcheck="false" class="form-control input-md">
                        </div>
                    </div>

                    <div class="form-group">

                        <label class="control-label form-style-label" for="contact-tel">Phone 1</label>

                        <div class="form-style-field-lg">
                            <input id="contact-tel" name="contact-tel" onkeypress="return isNumberKey(event)" type="text" placeholder="" maxlength="14" class="form-control input-md">
                        </div>
                    </div>

                    <div class="form-group">

                        <label class="control-label form-style-label" for="contact-mobile">Phone 2</label>

                        <div class="form-style-field-lg">
                            <input id="contact-mobile" name="contact-mobile" onkeypress="return isNumberKey(event)" type="text" placeholder="" maxlength="14" class="form-control input-md">
                        </div>
                    </div>

                </fieldset>
            </div>

            <div class="contact_col second_col col-md-6">
                <div id="ddbank">
                    <fieldset>

                        <legend>Bank Details</legend>
                        <div class="line-sep"></div>

                        <div class="form-group">

                            <label class="control-label form-style-label" for="bank-bname">Account Name</label>

                            <div class="form-style-field-lg">
                                <input id="bank-bname" name="bank-bname" onkeypress="return !isNumberKey(event)" type="text" maxlength="25" spellcheck="false" placeholder="" class="form-control input-md">
                            </div>
                        </div>

                        <div class="form-group">

                            <label class="control-label form-style-label" for="bank-bnum">Account Number</label>

                            <div class="form-style-field-lg">
                                <input id="bank-bnum" name="bank-bnum" onkeypress="return isNumberKey(event)" maxlength="8" type="text" placeholder="" class="form-control input-md">
                            </div>
                        </div>

                        <div class="form-group">

                            <label class="control-label form-style-label" for="bank-sort">Sort Code</label>

                            <div class="form-style-field-lg">
                                <input id="bank-sort" name="bank-sort" onkeypress="return isNumberKey(event)" maxlength="6" type="text" placeholder="" class="form-control input-md">
                            </div>
                        </div>

                    </fieldset>
                </div>

                <div id="stripe">
                    <form id="newCustForm" class="hide">
                        <fieldset>
                            <legend>Card Details</legend>
                            <div class="line-sep"></div>

                            <div style="position:relative">
                                <div id="card-type-icon1"  style="display: none;">
                                    <i class="fa fa-cc-visa" aria-hidden="true"></i>
                                    <i style="display:none" class="fa fa-cc-visa" aria-hidden="true"></i>
                                    <i style="display:none" class="fa fa-cc-amex" aria-hidden="true"></i>
                                    <i style="display:none" class="fa fa-cc-diners-club" aria-hidden="true"></i>
                                    <i style="display:none" class="fa fa-cc-discover" aria-hidden="true"></i>
                                    <i style="display:none" class="fa fa-cc-jcb" aria-hidden="true"></i>
                                    <i style="display:none" class="fa fa-cc-mastercard" aria-hidden="true"></i>
                                    <i style="display:none" class="fa fa-cc-paypal" aria-hidden="true"></i>
                                    <i style="display:none" class="fa fa-cc-stripe" aria-hidden="true"></i>
                                </div>
                                <div id="card-element" class="StripeElement"></div>
                            </div>
                            <input type="hidden" name="amount_pay" id="amount_pay" value=""/>

                            <input type="hidden" name="existing_line1" id="existing_line1" value=""/>
                            <input type="hidden" name="existing_postcode" id="existing_postcode" value=""/>
                            <input type="hidden" name="payment-email" id="payment-email" value=""/>
                            <input type="hidden" name="token" id="token" value=""/>


                            <input class="StripeElement ElementsApp InputElement" name="card_full_name" id="card_full_name" data-tid="elements_examples.form.name_placeholder" type="text" placeholder="Card Holder Name" novalidate/>
                            <div id="card-dd_expire"></div>
                            <div id="card-dd_ccv"></div>
                            <!-- Used to display form errors. -->
                            <div id="card-errors_dd" style="display: none; clear:both" role="alert" class="alert alert-danger"></div>

                            <button class="btn btn-primary" id="token-auth" type="submit">Submit</button>

                        </fieldset>
                    </form>
                </div>

            </div>

        </div>

        <div class="row submit_row" id="form-end">
            <div class="submit_col first_col col-md-6">

                <fieldset>

                    <div class="form-group">

                        <button id="submit-form" name="submit-form" class="btn btn-primary">SUBMIT FORM</button>

                    </div>

                </fieldset>

            </div>

            <div class="submit_col second_col col-md-6">

                <fieldset>

                    <div class="form-group">

                        <button id="clear-form" name="clear-form" class="btn btn-primary">CLEAR FORM</button>

                    </div>

                </fieldset>

            </div>
        </div>
    </div>
</div>

<div id="admin_signup_msg"></div>

<section class="postcode_top mt0">
    <div class="postcode_box ie-extra-padding" id="background-none">
        <form id="newcust_form" style="display:none">

            <input type="text" name="senderReference" id="senderReference">
            <input type="text" name="reference" id="reference">
            <input type="text" name="billingEntityCode" id="senderReference">
            <input type="text" name="marketingOptOutFl" id="marketingOptOutFl">
            <input type="text" name="submittedSource" id="submittedSource">
            <input type="text" name="changeOfTenancyFl" id="changeOfTenancyFl">
            <input type="text" name="tariff" id="tariff">
            <input type="text" name="elec_ur" id="elec_ur">
            <input type="text" name="elec_sc" id="elec_sc">
            <input type="text" name="gas_ur" id="gas_ur">
            <input type="text" name="gas_sc" id="gas_sc">

            <input type="text" name="title" id="title">
            <input type="text" name="forename" id="forename">
            <input type="text" name="surname" id="surname">
            <input type="text" name="email" id="email">
            <input type="text" name="phone1" id="phone1">
            <input type="text" name="phone2" id="phone2">
            <input type="text" name="dateOfBirth" id="dateOfBirth">
            <input type="text" name="billingMethod" id="billingMethod">
            <input type="text" name="primaryContact" id="primaryContact">

            <input type="text" name="address_careOf" id="address_careOf">
            <input type="text" name="address_address1" id="address_address1">
            <input type="text" name="address_address2" id="address_address2">
            <input type="text" name="address_town" id="address_town">
            <input type="text" name="address_county" id="address_county">
            <input type="text" name="address_postcode" id="address_postcode">
            <input type="text" name="address_countryCode" id="address_countryCode">

            <input type="text" name="supply_careOf" id="supply_careOf">
            <input type="text" name="supply_address1" id="supply_address1">
            <input type="text" name="supply_address2" id="supply_address2">
            <input type="text" name="supply_town" id="supply_town">
            <input type="text" name="supply_county" id="supply_county">
            <input type="text" name="supply_postcode" id="supply_postcode">
            <input type="text" name="supply_countryCode" id="supply_countryCode">

            <input type="text" name="newspend" id="newspend">
            <input type="text" name="newelec" id="newelec">
            <input type="text" name="newgas" id="newgas">
            <input type="text" name="bankAccountName" id="bankAccountName">
            <input type="text" name="bankAccountNumber" id="bankAccountNumber">
            <input type="text" name="bankAccountSortCode" id="bankAccountSortCode">

            <input type="text" name="quotefor" id="quotefor">

            <input type="text" name="elecReference" id="elecReference">
            <input type="text" name="elecPrevSupplier" id="elecPrevSupplier">
            <input type="text" name="supplierId" id="supplierId">

            <input type="text" name="elecPrevTariff" id="elecPrevTariff">
            <input type="text" name="mpancore" id="mpancore">
            <input type="text" name="mpans" id="mpans">
            <input type="text" name="eac" id="eac">
            <input type="text" name="meterTypeElec" id="meterTypeElec">
            <input type="text" name="elecMeterSerial" id="elecMeterSerial">

            <input type="text" name="gasReference" id="gasReference">
            <input type="text" name="mprns" id="mprns">
            <input type="text" name="aq" id="aq">
            <input type="text" name="gasMeterSerial" id="gasMeterSerial">

            <input type="text" name="monthyear" id="monthyear">
            <input type="text" name="firstpaymentid" id="firstpaymentid"/>
            <input type="text" name="card_token" id="stripe_token"/>

            <input type="text" name="created_by" id="created_by" value="<?php if($AdminUser){ echo $AdminUser['id']; } ?>"/>

        </form>
    </div>
</section>

<?php $this->load->view('dashboard/webtocase_form'); ?>

<script src="https://js.stripe.com/v3/"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/admin_signup.js"></script>

</body>
