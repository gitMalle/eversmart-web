<?php

if( !empty($email_type_list) )
{

    ?>

    <select name="email_type" id="email_type" style="font-size:1rem;" class="custom-select form-control">
        <option value="default">(Please Select Email)</option>
        <?php
        foreach($email_type_list as $key => $EmailType )
        {
            ?>
            <option value="<?php echo $EmailType['email_type_id']?>">
                <?php echo $EmailType['name']; ?>
            </option>
            <?php
        }

        ?>

    </select>
<?php }
else
{
    ?>
    <p>No emails found.</p>
    <?php
}
?>
