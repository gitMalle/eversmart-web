<?php if( !empty($mprn) ) { ?>
    
    <label class="control-label form-style-label" for="multiple_mprn_select" id="multi_mprn">Select MPRN:</label>

    <div class="form-style-field-lg">
        <select name="selected_mprn" id="selected_mprn" style="font-size:1rem;" class="custom-select form-control">
            <option value="default">(Please Select MPRN)</option>
            <?php foreach($mprn as $key => $MPRNCode ) { ?>
                <option value="<?php echo $MPRNCode?>"><?php echo $MPRNCode; ?></option>
            <?php } ?>
        </select>
    </div>

<?php } else { ?>
    
    <p>No mprns for selected address.</p>
    
<?php } ?>
