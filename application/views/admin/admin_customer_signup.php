<?php
// Redirect if not logged in
$AdminUser = $this->session->userdata('login_data');
if (empty($AdminUser) || $AdminUser['admin_role_type_id'] != '1') {
    $this->load->helper('url');
    redirect('/', 'refresh');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Admin - Customer Sign-Up</title>
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/admin/critical.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>assets/img/favicon.png"/>
    <style type="text/css">
        .rm-pad {
            padding: 0;
        }
    </style>
</head>
<body>
<div class="topbar-admin col-lg-12">
    <a href="<?php echo base_url(); ?>">
        <img src="<?php echo base_url(); ?>assets/images/logo-eversmart-new.png">
    </a>

    <a class="admin-links admin-here" href="<?php echo base_url() ?>Admin/admin_customer_signup">Customer Sign-up</a>
    <a class="admin-links" href="<?php echo base_url() ?>Admin/admin_activation_email">Resend Emails</a>
    <a class="admin-links" href="<?php echo base_url() ?>Admin/admin_webtocase">Web to Case</a>
    <a class="admin-links" href="<?php echo base_url() ?>Admin/admin_email_templates">Email Templates</a>
    <a class="admin-links" href="<?php echo base_url() ?>Admin/admin_datacapture">Capture Form</a>
    <a class="admin-links" href="<?php echo base_url() ?>Admin/admin_leadupdate">Lead Update</a>
    <?php if ($AdminUser['admin_role_type_id'] == 1) { ?>
        <a class="admin-links" href="<?php echo base_url() ?>Admin/admin_sassquatch_update">Friend Referrals</a>
        <a class="admin-links" href="<?php echo base_url() ?>Admin/admin_show_pending_energy_registration">Pending
            Registrations</a>

    <?php } ?>

    <select class="mob-admin-links" onchange="location=this.value">
        <option value="<?php echo base_url() ?>Admin/admin_customer_signup" selected>Customer Sign-up</option>
        <option value="<?php echo base_url() ?>Admin/admin_webtocase">Web to Case</option>
        <option value="<?php echo base_url() ?>Admin/admin_activation_email">Resend Emails</option>
        <option value="<?php echo base_url() ?>Admin/admin_email_templates">Email Templates</option>
        <option value="<?php echo base_url() ?>Admin/admin_datacapture">Capture Form</option>
        <option value="<?php echo base_url() ?>Admin/admin_leadupdate">Lead Update</option>
        <?php if ($AdminUser['admin_role_type_id'] == 1) { ?>
            <option value="<?php echo base_url() ?>Admin/admin_sassquatch_update" selected>Friend Referrals</option>
            <option value="<?php echo base_url() ?>Admin/admin_show_pending_energy_registration">Pending Registrations
            </option>
        <?php } ?>
    </select>

    <a class="admin-logout" href="#" onclick="logout_user()">Log out</a>
</div>
<div class="container custform col-lg-12">

    <div>
        <form id="lead_details">
            <?php if (!empty($lead_data)) { ?>
                <input id="lead_flag" type="hidden" value="<?= (!empty($lead_data) ? '1' : '0'); ?>">
                <input id="lead_id" name="lead_id" type="hidden" value="<?php echo $lead_data['lead_id'] ?>">
                <input id="lead_name" name="lead_name" type="hidden" value="<?php echo $lead_data['lead_name'] ?>">
                <input id="lead_title" name="lead_title" type="hidden" value="<?php echo $lead_data['lead_title'] ?>">
                <input id="lead_forename" name="lead_forename" type="hidden"
                       value="<?php echo $lead_data['lead_forename'] ?>">
                <input id="lead_surname" name="lead_surname" type="hidden"
                       value="<?php echo $lead_data['lead_surname'] ?>">
                <input id="lead_dob" name="lead_dob" type="hidden" value="<?php echo $lead_data['lead_dob'] ?>">
                <input id="lead_phone" name="lead_phone" type="hidden" value="<?php echo $lead_data['lead_phone'] ?>">
                <input id="lead_email" name="lead_email" type="hidden" value="<?php echo $lead_data['lead_email'] ?>">
                <input id="lead_address" name="lead_address" type="hidden"
                       value="<?php echo $lead_data['lead_address'] ?>">
                <input id="lead_postcode" name="lead_postcode" type="hidden"
                       value="<?php echo $lead_data['lead_postcode'] ?>">
                <input id="lead_tariff" name="lead_tariff" type="hidden"
                       value="<?php echo $lead_data['lead_tariff'] ?>">
                <input id="lead_eac" name="lead_eac" type="hidden" value="<?php echo $lead_data['lead_eac'] ?>">
                <input id="lead_aq" name="lead_aq" type="hidden"
                       value="<?= (!empty($lead_data['lead_aq']) ? $lead_data['lead_aq'] : ''); ?>">
                <input id="lead_quote" name="lead_quote" type="hidden" value="<?php echo $lead_data['lead_quote'] ?>">
                <?php
            } ?>
        </form>
    </div>

    <div class="row quote_row" id="form-quote">
        <div class="quote_col first_col col-md-6">
            <fieldset>
                <legend>Contact Address</legend>
                <div class="line-sep"></div>
                <div class="form-group">
                    <label class="control-label form-style-label" for="contact-postcode">Postcode</label>
                    <div class="form-style-field-lg">
                        <input id="contact-postcode" name="contact-postcode" type="search" placeholder="" maxlength="10"
                               spellcheck="false" class="form-control input-md">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label form-style-label" for="contact-lookup"></label>
                    <div class="form-style-field-lg">
                        <button id="contact-lookup" name="contact-lookup" class="btn btn-primary">Look Up</button>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label form-style-label" for="address_select" id="pcod-address"></label>
                    <div class="form-style-field-lg">
                        <div id="address_response"></div>
                    </div>
                </div>
            </fieldset>
        </div>
        <div class="quote_col second_col col-md-6">
        </div>
        <div class="quote_col third_col col-md-6">
            <fieldset>
                <div class="form-group">
                    <div id="multiple_mpan_select"></div>
                </div>
                <div class="form-group">
                    <div id="multiple_mprn_select"></div>
                </div>
                <div id="address_na">
                    <div class="form-group">
                        <label class="control-label form-style-label" for="na-add1">Address Line 1</label>
                        <div class="form-style-field-lg">
                            <input id="na-add1" name="na-add1" type="text" placeholder="" maxlength="35"
                                   spellcheck="false" class="form-control input-md">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label form-style-label" for="na-add2">Address Line 2</label>
                        <div class="form-style-field-lg">
                            <input id="na-add2" name="na-add2" type="text" placeholder="" maxlength="35"
                                   spellcheck="false" class="form-control input-md">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label form-style-label" for="na-addtown">Town</label>
                        <div class="form-style-field-lg">
                            <input id="na-addtown" name="na-addtown" type="text" placeholder="" maxlength="20"
                                   spellcheck="false" class="form-control input-md">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label form-style-label" for="na-addcounty">County</label>
                        <div class="form-style-field-lg">
                            <input id="na-addcounty" name="na-addcounty" type="text" placeholder="" maxlength="20"
                                   spellcheck="false" class="form-control input-md">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label form-style-label" for="na-pcod">Postcode</label>
                        <div class="form-style-field-lg">
                            <input id="na-pcod" name="na-pcod" type="text" placeholder="" maxlength="10"
                                   spellcheck="false" class="form-control input-md">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label form-style-label" for="tariff-tariff">Tariff</label>
                    <div class="form-style-field-lg">
                        <div id="tariffselect" style="width:100%;"></div>
                    </div>
                </div>
                <div class="form-group" id="pay-type">
                    <label style="margin-top: 0px" class="control-label form-style-label" for="tariff-pay">Payment
                        Type</label>
                    <div class="form-style-field-lg">
                        <select id="tariff-pay" name="tariff-pay" class="form-control">
                            <option value="default">(Please Select)</option>
                            <option value="2">Standard</option>
                            <option value="3">Bank Transfer</option>
                            <option value="4">Cheque</option>
                            <option value="5">One-off Direct Debit</option>
                            <option value="6">Stripe</option>
                        </select>
                    </div>
                </div>
                <div class="form-group" id="sales_agent">
                    <label class="control-label form-style-label" for="sel_sales_agent">Sales Rep</label>
                    <div class="form-style-field-lg">
                        <select id="sel_sales_agent" name="sel_sales_agent" class="form-control">
                            <option value="default">No Rep</option>
                            <?php
                            if ($sales_reps) {
                                for ($i = 0; $i < count($sales_reps); $i++) { ?>
                                    <option value="<?= $sales_reps[$i]['rep_code'] ?>"><?= $sales_reps[$i]['name'] . ' (' . $sales_reps[$i]['rep_code'] . ')' ?></option> <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label form-style-label" for="mtrpoint-mpan-eac">Home Type</label>
                    <div class="form-style-field-lg">
                        <select id="mtrpoint-mpan-eac" name="mtrpoint-mpan-eac" class="form-control">
                            <option value="default">(Please Select)</option>
                            <option value="2">Flat: 1-2 bed</option>
                            <option value="6">Flat: 3-4 bed</option>
                            <option value="10">Flat: 5+ bed</option>
                            <option value="3">Terrace: 1-2 bed</option>
                            <option value="7">Terrace: 3-4 bed</option>
                            <option value="11">Terrace: 5+ bed</option>
                            <option value="4">Semi: 1-2 bed</option>
                            <option value="8">Semi: 3-4 bed</option>
                            <option value="12">Semi: 5+ bed</option>
                            <option value="5">Detached: 1-2 bed</option>
                            <option value="9">Detached: 3-4 bed</option>
                            <option value="13">Detached: 5+ bed</option>
                        </select>
                    </div>
                </div>
                <div class="form-group" style="display: none;">
                    <label class="control-label form-style-label" for="mtrpoint-mprn-aq">Home Type (Gas)</label>
                    <div class="form-style-field-lg">
                        <select id="mtrpoint-mprn-aq" name="mtrpoint-mprn-aq" class="form-control">
                            <option value="default">(Please Select)</option>
                            <option value="2">1-2 bed Flat</option>
                            <option value="3">1-2 bed Terrace</option>
                            <option value="4">1-2 bed Semi</option>
                            <option value="5">1-2 bed Detached</option>
                            <option value="6">3-4 bed Flat</option>
                            <option value="7">3-4 bed Terrace</option>
                            <option value="8">3-4 bed Semi</option>
                            <option value="9">3-4 bed Detached</option>
                            <option value="10">5+ bed Flat</option>
                            <option value="11">5+ bed Terrace</option>
                            <option value="12">5+ bed Semi</option>
                            <option value="13">5+ bed Detached</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <h5>OR</h5>
                </div>
                <div class="form-group" id="eac-or">
                    <label class="control-label form-style-label" for="eac-override">EAC</label>
                    <div class="form-style-field-lg">
                        <input id="eac-override" name="eac-override" type="number" spellcheck="false"
                               class="form-control input-md">
                    </div>
                </div>
                <div class="form-group" id="aq-or">
                    <label class="control-label form-style-label" for="aq-override">AQ</label>
                    <div class="form-style-field-lg">
                        <input id="aq-override" name="aq-override" type="number" spellcheck="false"
                               class="form-control input-md">
                    </div>
                </div>

                <div class="line-sep"></div>
                <div class="form-group">
                    <button id="get-quote" name="get-quote" class="btn btn-primary">GET QUOTE</button>
                </div>
                <div class="form-group" id="quote-or">
                    <label class="control-label form-style-label" for="quote-override">Quote Override</label>
                    <div class="form-style-field-lg">
                        <input id="quote-override" name="quote-override" type="text" pattern="^[+-]?\d+(\.\d+)?$"
                               placeholder="£" spellcheck="false" class="form-control input-md">
                    </div>
                </div>

            </fieldset>
        </div>
        <div class="quote_col third_col col-md-12">
            <div class="form-group">
                <div id="admin_quote"></div>
            </div>
        </div>
    </div>

    <div id="submit-panel">

        <div class="row contact_row">

            <div class="contact_col first_col col-md-6">
                <fieldset>
                    <legend>Contact Details</legend>
                    <div class="line-sep"></div>
                    <div class="form-group">
                        <label class="control-label form-style-label" for="contact-title">Name</label>
                        <div class="form-style-field-sm">
                            <select class="form-control input-md rm-pad" id="contact-title" tabindex="0" required>
                                <option value="Mr">Mr</option>
                                <option value="Mrs">Mrs</option>
                                <option value="Miss">Miss</option>
                                <option value="Ms">Ms</option>
                                <option value="Prof">Prof</option>
                                <option value="Dr">Dr</option>
                            </select>
                        </div>
                        <div class="form-style-field-md">
                            <input id="contact-fname" onkeypress="return !isNumberKey(event)" name="contact-fname"
                                   type="text" placeholder="Forename" class="form-control input-md" maxlength="25"
                                   spellcheck="false">
                        </div>
                        <div class="form-style-field-md">
                            <input id="contact-lname" onkeypress="return !isNumberKey(event)" name="contact-lname"
                                   type="text" placeholder="Surname" class="form-control input-md" maxlength="25"
                                   spellcheck="false">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label form-style-label" for="contact-dob">Date of Birth</label>
                        <div class="form-style-field-lg">
                            <input id="contact-dob" name="contact-dob" type="date" placeholder=""
                                   class="form-control input-md">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label form-style-label" for="contact-email">Email</label>
                        <div class="form-style-field-lg">
                            <input id="contact-email" name="contact-email" type="email" placeholder=""
                                   spellcheck="false" class="form-control input-md">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label form-style-label" for="contact-tel">Mobile</label>
                        <div class="form-style-field-lg">
                            <input id="contact-mobile" name="contact-mobile" onkeypress="return isNumberKey(event)"
                                   type="text" placeholder="" maxlength="14" class="form-control input-md">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label form-style-label" for="contact-mobile">Landline (optional)</label>
                        <div class="form-style-field-lg">
                            <input id="contact-tel" name="contact-tel" onkeypress="return isNumberKey(event)"
                                   type="text" placeholder="" maxlength="14" class="form-control input-md">
                        </div>
                    </div>
                </fieldset>
            </div>

            <div class="contact_col second_col col-md-6">
                <fieldset>
                    <legend>Address Details</legend>
                    <div class="line-sep"></div>
                    <div class="form-group">
                        <label class="control-label form-style-label" for="quotation_address">Address</label>
                        <div class="form-style-field-lg">
                            <div id="address_select">
                                <select id="quotation_address" class="form-control"></select>
                            </div>
                        </div>
                    </div>

                    <div id="gas_address_select" style="display: none">
                        <div class="form-group">
                            <label class="control-label form-style-label" for="gas_quotation_address">Gas Address</label>
                            <div class="form-style-field-lg">
                                <select id="gas_quotation_address" class="form-control"></select>
                            </div>
                        </div>
                    </div>

                    <div id="meter_details" style="display: none">
                        <div class="form-group">
                            <h3>Elec Meter Type: <span id="meter_type"></span></h3>
                        </div>
                        <div class="form-group">
                            <h3>Elec Meter Serial: <span id="meter_serial"></span></h3>
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>

        <div class="row contact_row">

            <div class="contact_col first_col col-md-6">
                <fieldset>
                    <legend>Boiler Cover Opt-in</legend>
                    <div class="line-sep"></div>
                    <div class="form-group">
                        <label class="control-label form-style-label" for="boiler_cover">Opt-in</label>
                        <div class="form-style-field-lg">
                            <div class="checkbox">
                                <input class="mpan-check" type="checkbox" name="boiler_cover" id="boiler_cover"
                                       value="0">
                            </div>
                        </div>
                    </div>
                    <legend>Eligible for PSR?</legend>
                    <div class="line-sep"></div>
                    <div class="form-group">
                        <label class="control-label form-style-label" for="psr_flag">Eligible</label>
                        <div class="form-style-field-lg">
                            <div class="checkbox">
                                <input class="mpan-check" type="checkbox" name="psr_flag" id="psr_flag" value="0">
                            </div>
                        </div>
                    </div>
                </fieldset>
            </div>

            <div class="contact_col second_col col-md-6">
                <div id="ddbank">
                    <fieldset>
                        <legend>Bank Details</legend>
                        <div class="line-sep"></div>
                        <div class="form-group">
                            <label class="control-label form-style-label" for="bank-bname">Account Name</label>
                            <div class="form-style-field-lg">
                                <input id="bank-bname" name="bank-bname" onkeypress="return !isNumberKey(event)"
                                       type="text" maxlength="25" spellcheck="false" placeholder=""
                                       class="form-control input-md">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label form-style-label" for="bank-bnum">Account Number</label>
                            <div class="form-style-field-lg">
                                <input id="bank-bnum" name="bank-bnum" onkeypress="return isNumberKey(event)"
                                       maxlength="8" type="text" placeholder="" class="form-control input-md">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label form-style-label" for="bank-sort">Sort Code</label>
                            <div class="form-style-field-lg">
                                <input id="bank-sort" name="bank-sort" onkeypress="return isNumberKey(event)"
                                       maxlength="6" type="text" placeholder="" class="form-control input-md">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label form-style-label" for="bank-validate"></label>
                            <div class="form-style-field-lg">
                                <button id="bank-validate" name="bank-validate" class="btn btn-primary">VALIDATE
                                </button>
                            </div>
                        </div>
                    </fieldset>
                    <div id="validate-msg"></div>
                </div>
                <div id="stripe">
                    <form id="newCustForm" class="hide">
                        <fieldset>
                            <legend>Card Details</legend>
                            <div class="line-sep"></div>
                            <div style="position:relative">
                                <div id="card-type-icon1" style="display: none;">
                                    <i class="fa fa-cc-visa" aria-hidden="true"></i>
                                    <i style="display:none" class="fa fa-cc-visa" aria-hidden="true"></i>
                                    <i style="display:none" class="fa fa-cc-amex" aria-hidden="true"></i>
                                    <i style="display:none" class="fa fa-cc-diners-club" aria-hidden="true"></i>
                                    <i style="display:none" class="fa fa-cc-discover" aria-hidden="true"></i>
                                    <i style="display:none" class="fa fa-cc-jcb" aria-hidden="true"></i>
                                    <i style="display:none" class="fa fa-cc-mastercard" aria-hidden="true"></i>
                                    <i style="display:none" class="fa fa-cc-paypal" aria-hidden="true"></i>
                                    <i style="display:none" class="fa fa-cc-stripe" aria-hidden="true"></i>
                                </div>
                                <div id="card-element" class="StripeElement"></div>
                            </div>
                            <input type="hidden" name="amount_pay" id="amount_pay" value=""/>
                            <input type="hidden" name="existing_line1" id="existing_line1" value=""/>
                            <input type="hidden" name="existing_postcode" id="existing_postcode" value=""/>
                            <input type="hidden" name="payment-email" id="payment-email" value=""/>
                            <input type="hidden" name="token" id="token" value=""/>
                            <input class="StripeElement ElementsApp InputElement" name="card_full_name"
                                   id="card_full_name" data-tid="elements_examples.form.name_placeholder" type="text"
                                   placeholder="Card Holder Name" novalidate/>
                            <div id="card-dd_expire"></div>
                            <div id="card-dd_ccv"></div>
                            <!-- Used to display form errors. -->
                            <div id="card-errors_dd" style="display: none; clear:both" role="alert"
                                 class="alert alert-danger"></div>
                            <button class="btn btn-primary" id="token-auth" type="submit">Submit</button>
                        </fieldset>
                    </form>
                </div>
                <div id="validate-msg"></div>
            </div>
        </div>

        <div class="row submit_row" id="form-end">
            <div class="submit_col first_col col-md-6">
                <fieldset>
                    <div class="form-group">
                        <button id="submit-form" name="submit-form" class="btn btn-primary">SUBMIT FORM</button>
                    </div>
                </fieldset>
            </div>

            <div class="submit_col second_col col-md-6">
                <fieldset>
                    <div class="form-group">
                        <button id="clear-form" name="clear-form" class="btn btn-primary">CLEAR FORM</button>
                    </div>
                </fieldset>
            </div>
        </div>

    </div>
</div>
<div id="admin_signup_msg"></div>
<div id="update_result"></div>
<section class="postcode_top mt0">
    <div class="postcode_box ie-extra-padding" id="background-none">
        <form id="newcust_form" style="display:none">
            <input type="text" name="senderReference" id="senderReference">
            <input type="text" name="reference" id="reference">
            <input type="text" name="billingEntityCode" id="senderReference">
            <input type="text" name="marketingOptOutFl" id="marketingOptOutFl">
            <input type="text" name="submittedSource" id="submittedSource">
            <input type="text" name="changeOfTenancyFl" id="changeOfTenancyFl">
            <input type="text" name="tariff" id="tariff">
            <input type="text" name="elec_ur" id="elec_ur">
            <input type="text" name="elec_sc" id="elec_sc">
            <input type="text" name="gas_ur" id="gas_ur">
            <input type="text" name="gas_sc" id="gas_sc">
            <input type="text" name="title" id="title">
            <input type="text" name="forename" id="forename">
            <input type="text" name="surname" id="surname">
            <input type="text" name="email" id="email">
            <input type="text" name="phone1" id="phone1">
            <input type="text" name="phone2" id="phone2">
            <input type="text" name="dateOfBirth" id="dateOfBirth">
            <input type="text" name="billingMethod" id="billingMethod">
            <input type="text" name="psr_flag" id="psr_flag">
            <input type="text" name="primaryContact" id="primaryContact">
            <input type="text" name="address_careOf" id="address_careOf">
            <input type="text" name="address_address1" id="address_address1">
            <input type="text" name="address_address2" id="address_address2">
            <input type="text" name="address_town" id="address_town">
            <input type="text" name="address_county" id="address_county">
            <input type="text" name="address_postcode" id="address_postcode">
            <input type="text" name="address_countryCode" id="address_countryCode">
            <input type="text" name="newspend" id="newspend">
            <input type="text" name="newelec" id="newelec">
            <input type="text" name="newgas" id="newgas">
            <input type="text" name="bankAccountName" id="bankAccountName">
            <input type="text" name="bankAccountNumber" id="bankAccountNumber">
            <input type="text" name="bankAccountSortCode" id="bankAccountSortCode">
            <input type="text" name="quotefor" id="quotefor">
            <input type="text" name="elecPrevSupplier" id="elecPrevSupplier">
            <input type="text" name="supplierId" id="supplierId">
            <input type="text" name="elecPrevTariff" id="elecPrevTariff">
            <input type="text" name="mpancore" id="mpancore">
            <input type="text" name="mpans" id="mpans">
            <input type="text" name="eac" id="eac">
            <input type="text" name="meterTypeElec" id="meterTypeElec">
            <input type="text" name="elecMeterSerial" id="elecMeterSerial">
            <input type="text" name="mprns" id="mprns">
            <input type="text" name="aq" id="aq">
            <input type="text" name="gasMeterSerial" id="gasMeterSerial">
            <input type="text" name="monthyear" id="monthyear">
            <input type="text" name="firstpaymentid" id="firstpaymentid"/>
            <input type="text" name="card_token" id="stripe_token"/>
            <input type="text" name="boiler_cover" id="boiler_cover"/>
            <input type="text" name="created_by" id="created_by" value="<?php if ($AdminUser) {
                echo $AdminUser['id'];
            } ?>"/>
        </form>
    </div>
</section>
<?php $this->load->view('dashboard/webtocase_form'); ?>
<script src="https://js.stripe.com/v3/"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/admin_signup.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/new-world/everywhere/plugins.js"></script>

</body>
