<?php
if( !empty($get_tariffs) )
{
    $TariffArray = json_decode($get_tariffs, true);
    $TariffArray = $TariffArray['data']['tariffs'];
    ?>

    <select name="tariffselect" id="tariffselect" style="font-size:1rem;" class="custom-select form-control">
        <option value="default">(Please Select Tariff)</option>
        <option data-paytype="9" data-fueltype="elecgas" value="Family Saver Club">Family Saver Club (elecgas)</option>
        <option data-paytype="9" data-fueltype="elec" value="Family Saver Club">Family Saver Club (elec)</option>
        <?php
        foreach($TariffArray as $key => $TariffData )
        {
            ?>
            <option data-paytype="<?php echo $TariffData['payTypes'][0]?>"
                    data-fueltype="<?php echo $TariffData['fuelType']?>" value="<?php echo $TariffData['tariffName']?>">
                <?php echo $TariffData['tariffName']; ?> (<?php echo $TariffData['fuelType']; ?>)
            </option>
            <?php
        }

        ?>

    </select>
<?php }