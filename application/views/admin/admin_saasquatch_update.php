<?php
// Redirect if not logged in
$AdminUser = $this->session->userdata('login_data');
if (empty($AdminUser) || $AdminUser['admin_role_type_id'] != '1') {
    $this->load->helper('url');
    redirect('/', 'refresh');
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Admin - Lead Update</title>
    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>assets/img/favicon.png" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
</head>

<body>

    <div class="topbar-admin col-lg-12">
        <a href="<?php echo base_url(); ?>">
            <img src="<?php echo base_url(); ?>assets/images/logo-eversmart-new.png">
        </a>


        <a class="admin-links" href="<?php echo base_url() ?>Admin/admin_customer_signup">Customer Sign-up</a>
        <a class="admin-links" href="<?php echo base_url() ?>Admin/admin_activation_email">Resend Emails</a>
        <a class="admin-links" href="<?php echo base_url() ?>Admin/admin_webtocase">Web to Case</a>
        <a class="admin-links" href="<?php echo base_url() ?>Admin/admin_email_templates">Email Templates</a>
        <a class="admin-links" href="<?php echo base_url() ?>Admin/admin_datacapture">Capture Form</a>
        <a class="admin-links" href="<?php echo base_url() ?>Admin/admin_leadupdate">Lead Update</a>
        <a class="admin-links admin-here" href="<?php echo base_url() ?>Admin/admin_sassquatch_update">Friend Referrals</a>
        <a class="admin-links" href="<?php echo base_url() ?>Admin/admin_show_pending_energy_registration">Pending Registrations</a>

        <select class="mob-admin-links" onchange="location=this.value">
            <option value="<?php echo base_url() ?>Admin/admin_customer_signup">Customer Sign-up</option>
            <option value="<?php echo base_url() ?>Admin/admin_webtocase">Web to Case</option>
            <option value="<?php echo base_url() ?>Admin/admin_activation_email">Resend Emails</option>
            <option value="<?php echo base_url() ?>Admin/admin_email_templates">Email Templates</option>
            <option value="<?php echo base_url() ?>Admin/admin_datacapture">Capture Form</option>
            <option value="<?php echo base_url() ?>Admin/admin_leadupdate">Lead Update</option>
            <option value="<?php echo base_url() ?>Admin/admin_sassquatch_update">Friend Referrals</option>
            <option value="<?php echo base_url() ?>Admin/admin_show_pending_energy_registration">Pending Registrations</option>
        </select>
        <a class="admin-logout" href="#" onclick="logout_user()">Log out</a>

    </div>
    <div class="container custform col-lg-12">

        <div class="row submit_row">
            <div class="contact_col first_col col-md-12">
                <legend>New Referral Rewards</legend>
                <div class="line-sep"></div>
            </div>

            <!-- Bootstrap CSS -->
            <!-- jQuery first, then Bootstrap JS. -->
            <!-- Nav tabs -->
            <div class="row col-12">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" href="#referrer" role="tab" data-toggle="tab"><span
                                class="badge badge-secondary"><?php echo count($referral)?></span> Referrer</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#referee" role="tab" data-toggle="tab"><span
                                class="badge badge-secondary"><?php echo count($referred)?></span> Referred</a>
                    </li>
                </ul>
            </div>

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="referrer">
                    <div class='col-12'>
                        <table id="admin_lead_table" class="table table-hover">

                            <thead>
                                <tr class="admin_lead_table_head">
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone Number</th>
                                    <th>Amount(£)</th>
                                    <th>Action</th>
                                    <th></th>
                                </tr>
                            </thead>

                            <?php

                                if (empty($referral)) {
                                    echo "<p style='padding:1%'><div class='alert alert-info'  role='alert'> No Record Found! </div></p>";
                                } else {

                                    foreach($referral as $sq_referral){

                                        if (isset($sq_referral->customer_id)) {
                                            echo "<tr>";
                                            echo "<td><b>" . $sq_referral->customer_id . "</b></td>";
                                            echo "<td>" . $sq_referral->forename . " " . $sq_referral->surname . "</td>";
                                            echo "<td>" . $sq_referral->email . "</td>";
                                            echo "<td><div class='input-group'><input type='text' class='form-control form-control-sm' name='mobile' value='" . $sq_referral->phone1 . "' /></div></td>";
                                            $hidden = array('customer_id' => $sq_referral->customer_id, 'mobile' => $sq_referral->phone1, 'account_id' => $sq_referral->account_id, 'saas_type' => 'referrer');
                                            echo form_open('admin/admin_saasquatch_confirm_payment', '', $hidden);
                                            echo "<td><div class='input-group'>
                                                </div><input type='text' class='form-control form-control-sm' name='amount' value='40' readonly/></div> </td>";
                                            echo "<td/> <div class=''>
                                                    <button onclick='return confirm_click()' type='submit' class='btn btn-outline-danger btn-sm'>Confirm Payment</button>
                                                </td>";
                                            echo "</form>";

                                            if (isset($error_msg) && $sq_referral->customer_id == $customer_id) {
                                                echo "<td><div class='alert alert-danger'>{$error_msg}</danger></td>";
                                            }

                                            if (isset($sms_sent) && $sq_referral->customer_id == $customer_id) {
                                                echo "<td><div class='alert alert-success'>{$sms_sent}!</div></td>";
                                            }

                                            echo "</tr>";
                                        }
                                    }
                                }

                            ?>
                        </table>
                    </div>
                </div>

                <div role="tabpanel" class="tab-pane fade" id="referee">
                    <div class='col-12'>
                        <table id="admin_lead_table" class="table table-hover">
                            <thead>
                                <tr class="admin_lead_table_head">
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone Number</th>
                                    <th>Amount(£)</th>
                                    <th>Action</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <?php

                                        if (empty($referred)) {
                                            echo "<p style='padding:1%'><div class='alert alert-info'  role='alert'> No Record Found! </div></p>";
                                        } else {

                                            foreach($referred as $sq_referred){

                                                if (isset($sq_referred->customer_id)) {
                                                    echo "<tr>";
                                                    echo "<td><b>" . $sq_referred->customer_id . "</b></td>";
                                                    echo "<td>" . $sq_referred->forename . " " . $sq_referred->surname . "</td>";
                                                    echo "<td>" . $sq_referred->email . "</td>";
                                                    echo "<td><div class='input-group'><input type='text' class='form-control form-control-sm' name='mobile' value='" . $sq_referred->phone1 . "' /></div></td>";
                                                    $hidden = array('customer_id' => $sq_referred->customer_id, 'mobile' => $sq_referred->phone1, 'account_id' => $sq_referred->account_id, 'saas_type' => 'referred');
                                                    echo form_open('admin/admin_saasquatch_confirm_payment', '', $hidden);
                                                    echo "<td>
                                                            <input type='text' class='form-control form-control-sm' name='amount' value='10' readonly/></div> 
                                                        </td>";
                                                    echo "<td/> <div class=''><button type='submit' onclick='return confirm_click()' class='btn btn-outline-danger btn-sm'>Confirm Payment</button> </td>";
                                                    echo "</form>";

                                                    if (isset($error_msg) && $sq_referred->customer_id == $customer_id) {
                                                        echo "<td><div class='alert alert-danger'>{$error_msg}</danger></td>";
                                                    }

                                                    if (isset($sms_sent) && $sq_referred->customer_id == $customer_id) {
                                                        echo "<td><div class='alert alert-success'>{$sms_sent}!</div></td>";
                                                    }

                                                    echo "</tr>";
                                                }
                                            }
                                        }    
                            ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
    <script>
    function confirm_click() {
        return confirm('The action you are about to perform will trigger an SMS message!');
    }
    </script>
    <!-- <script type="text/javascript" src="<?php //echo base_url(); ?>assets/js/admin_leadupdate.js"></script> -->

</body>
