
CREATE TABLE `email_type` (
  `email_type_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `email_type` (`email_type_id`, `name`, `created_date`) VALUES
(1, 'Activation', '2018-08-24 07:11:17'),
(2, 'Payment plan cancelled', '2018-08-24 07:11:17'),
(3, 'Latest energy bill', '2018-08-24 07:11:17'),
(4, 'First meter reading', '2018-08-24 07:11:17'),
(5, 'Provide meter reading', '2018-08-24 07:11:17'),
(6, 'Sorry to see you go', '2018-08-24 07:11:17'),
(7, 'Balance overdue 1', '2018-08-24 07:11:17'),
(8, 'Balance overdue 2', '2018-08-24 07:11:17'),
(9, 'Balance overdue 3', '2018-08-24 07:11:17'),
(10, 'Direct debit confirmation', '2018-08-24 07:11:17'),
(11, 'Direct debit changes', '2018-08-24 07:11:17'),
(12, 'Direct debit problem', '2018-08-24 07:11:17'),
(13, 'New payment plan', '2018-08-24 07:11:17'),
(14, 'New payment schedule', '2018-08-24 07:11:17'),
(15, 'Objection', '2018-08-24 07:11:17'),
(16, 'Previous supplier objection', '2018-08-24 07:11:36'),
(17, 'Registration withdrawal', '2018-08-24 07:11:36'),
(18, 'Reset password', '2018-08-28 09:00:07'),
(19, 'Book meter', '2018-08-28 09:00:07'),
(20, 'Moving home email', '2018-08-28 09:00:22');

ALTER TABLE `email_type` ADD PRIMARY KEY (`email_type_id`);

ALTER TABLE `email_type`  MODIFY `email_type_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21; COMMIT;




CREATE TABLE `email_log` (
  `email_log_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `email_type_id` int(11) NOT NULL,
  `sent_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `email_log` ADD PRIMARY KEY (`email_log_id`);

ALTER TABLE `email_log` MODIFY `email_log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;COMMIT;
