
CREATE TABLE `referral_link` (
  `referral_link_id` int(11) NOT NULL,
  `ese_generated_link` varchar(255) NOT NULL,
  `saas_generated_link` varchar(255) NOT NULL,
  `deleted_date` timestamp NULL DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `referral_link` ADD PRIMARY KEY (`referral_link_id`);
ALTER TABLE `referral_link` MODIFY `referral_link_id` int(11) NOT NULL AUTO_INCREMENT;COMMIT;

ALTER TABLE `referral_link` CHANGE `deleted_date` `redeemed_date` TIMESTAMP NULL DEFAULT NULL;
ALTER TABLE `referral_link` ADD `redeemed_by` INT(11) NULL COMMENT 'person_id' AFTER `redeemed_date`;
ALTER TABLE `referral_link` ADD `customer_id` INT(11) NOT NULL AFTER `referral_link_id`;

ALTER TABLE `referral_link`
  DROP `redeemed_date`,
  DROP `redeemed_by`;