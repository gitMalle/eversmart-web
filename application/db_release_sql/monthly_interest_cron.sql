CREATE TABLE `first_payment_status_type` ( `first_payment_status_type_id` INT NOT NULL AUTO_INCREMENT , `payment_status_type` VARCHAR(255) NOT NULL , `created_date` TIMESTAMP NOT NULL , PRIMARY KEY (`first_payment_status_type_id`)) ENGINE = InnoDB;

ALTER TABLE `customer_info` ADD `first_payment_status_type_id` INT(11) NULL AFTER `monthly_interest_customer_signup_date`;

INSERT INTO `first_payment_status_type` (`first_payment_status_type_id`, `payment_status_type`, `created_date`) VALUES (NULL, 'Successful', CURRENT_TIMESTAMP), (NULL, 'Failed', CURRENT_TIMESTAMP);