-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 18, 2018 at 10:47 AM
-- Server version: 5.6.40-84.0-log
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `crateman_ever`
--

-- --------------------------------------------------------

--
-- Table structure for table `tariff_change_notification`
--

CREATE TABLE `tariff_change_notification` (
  `account_number` int(11) NOT NULL,
  `customer_name` varchar(255) NOT NULL,
  `mpan` int(11) DEFAULT NULL,
  `elec_status` varchar(255) DEFAULT NULL,
  `elec_start_date` varchar(255) DEFAULT NULL,
  `elec_end_date` varchar(255) DEFAULT NULL,
  `meterid` varchar(255) DEFAULT NULL,
  `meter_type` varchar(255) DEFAULT NULL,
  `mprn` int(11) DEFAULT NULL,
  `gas_status` varchar(255) DEFAULT NULL,
  `gas_start_date` varchar(255) DEFAULT NULL,
  `gas_end_date` varchar(255) DEFAULT NULL,
  `meter_serial_number` varchar(255) DEFAULT NULL,
  `meter_mechanism` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `telephone` varchar(255) DEFAULT NULL,
  `contact_name` varchar(255) DEFAULT NULL,
  `contact_telephone` varchar(255) DEFAULT NULL,
  `contact_mobile` varchar(255) DEFAULT NULL,
  `contact_address_1` varchar(255) DEFAULT NULL,
  `contact_address_2` varchar(255) DEFAULT NULL,
  `contact_address_3` varchar(255) DEFAULT NULL,
  `contact_address_4` varchar(255) DEFAULT NULL,
  `contact_address_5` varchar(255) DEFAULT NULL,
  `contact_post_code` varchar(255) DEFAULT NULL,
  `e_address_line1` varchar(255) DEFAULT NULL,
  `e_address_line2` varchar(255) DEFAULT NULL,
  `e_address_line3` varchar(255) DEFAULT NULL,
  `e_address_line4` varchar(255) DEFAULT NULL,
  `e_address_line5` varchar(255) DEFAULT NULL,
  `e_address_line6` varchar(255) DEFAULT NULL,
  `e_address_line7` varchar(255) DEFAULT NULL,
  `e_address_line8` varchar(255) DEFAULT NULL,
  `e_address_line9` varchar(255) DEFAULT NULL,
  `building_number` varchar(255) DEFAULT NULL,
  `sub_building` varchar(255) DEFAULT NULL,
  `building_name` varchar(255) DEFAULT NULL,
  `principle_street` varchar(255) DEFAULT NULL,
  `dep_locality` varchar(255) DEFAULT NULL,
  `posttown` varchar(255) DEFAULT NULL,
  `postcode` varchar(255) DEFAULT NULL,
  `gas_tariff_name` varchar(255) DEFAULT NULL,
  `electricity_tariff_name` varchar(255) DEFAULT NULL,
  `letter` varchar(255) DEFAULT NULL,
  `mpan_gsp` varchar(255) DEFAULT NULL,
  `mpan_duos` int(11) DEFAULT NULL,
  `mpan_pc` int(11) DEFAULT NULL,
  `rate_type` varchar(255) DEFAULT NULL,
  `gsp_from_postcode` varchar(255) DEFAULT NULL,
  `duos_from_postcode` int(11) DEFAULT NULL,
  `duostouse` int(11) DEFAULT NULL,
  `has_elec` varchar(255) DEFAULT NULL,
  `has_gas` varchar(255) DEFAULT NULL,
  `current_elec_tariff_lookup` varchar(255) DEFAULT NULL,
  `current_gas_tariff_lookup` varchar(255) NOT NULL,
  `current_elec_sc` float DEFAULT NULL,
  `current_elec_dur` float DEFAULT NULL,
  `current_elec_nur` float DEFAULT NULL,
  `current_gas_sc` float DEFAULT NULL,
  `current_gas_ur` float DEFAULT NULL,
  `new_elec_tariff` varchar(255) DEFAULT NULL,
  `new_gas_tariff` varchar(255) DEFAULT NULL,
  `new_electricity_tariff_lookup` varchar(255) DEFAULT NULL,
  `new_gas_tariff_lookup` varchar(255) DEFAULT NULL,
  `new_elec_sc` float DEFAULT NULL,
  `new_elec_dur` float DEFAULT NULL,
  `new_elec_nr` float DEFAULT NULL,
  `new_gas_sc` float DEFAULT NULL,
  `new_gas_ur` float DEFAULT NULL,
  `elec_on_supply` varchar(255) DEFAULT NULL,
  `gas_on_supply` varchar(255) DEFAULT NULL,
  `on_supply` varchar(255) DEFAULT NULL,
  `elec_product_type` varchar(255) DEFAULT NULL,
  `gas_product_type` varchar(255) DEFAULT NULL,
  `elec_payment_type` varchar(255) DEFAULT NULL,
  `gas_payment_type` varchar(255) DEFAULT NULL,
  `elec_fix_end` varchar(255) DEFAULT NULL,
  `gas_fix_end` varchar(255) DEFAULT NULL,
  `elec_fix_ended` varchar(255) DEFAULT NULL,
  `gas_fix_ended` varchar(255) DEFAULT NULL,
  `elec_to_include` varchar(255) DEFAULT NULL,
  `gas_to_include` varchar(255) DEFAULT NULL,
  `price_rise` varchar(255) DEFAULT NULL,
  `has_email` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tariff_change_notification`
--

INSERT INTO `tariff_change_notification` (`account_number`, `customer_name`, `mpan`, `elec_status`, `elec_start_date`, `elec_end_date`, `meterid`, `meter_type`, `mprn`, `gas_status`, `gas_start_date`, `gas_end_date`, `meter_serial_number`, `meter_mechanism`, `email`, `telephone`, `contact_name`, `contact_telephone`, `contact_mobile`, `contact_address_1`, `contact_address_2`, `contact_address_3`, `contact_address_4`, `contact_address_5`, `contact_post_code`, `e_address_line1`, `e_address_line2`, `e_address_line3`, `e_address_line4`, `e_address_line5`, `e_address_line6`, `e_address_line7`, `e_address_line8`, `e_address_line9`, `building_number`, `sub_building`, `building_name`, `principle_street`, `dep_locality`, `posttown`, `postcode`, `gas_tariff_name`, `electricity_tariff_name`, `letter`, `mpan_gsp`, `mpan_duos`, `mpan_pc`, `rate_type`, `gsp_from_postcode`, `duos_from_postcode`, `duostouse`, `has_elec`, `has_gas`, `current_elec_tariff_lookup`, `current_gas_tariff_lookup`, `current_elec_sc`, `current_elec_dur`, `current_elec_nur`, `current_gas_sc`, `current_gas_ur`, `new_elec_tariff`, `new_gas_tariff`, `new_electricity_tariff_lookup`, `new_gas_tariff_lookup`, `new_elec_sc`, `new_elec_dur`, `new_elec_nr`, `new_gas_sc`, `new_gas_ur`, `elec_on_supply`, `gas_on_supply`, `on_supply`, `elec_product_type`, `gas_product_type`, `elec_payment_type`, `gas_payment_type`, `elec_fix_end`, `gas_fix_end`, `elec_fix_ended`, `gas_fix_ended`, `elec_to_include`, `gas_to_include`, `price_rise`, `has_email`) VALUES
(25300, 'Miss Carol Fields', 2147483647, 'Objection Upheld', '17/12/2017', '', '', '', 0, 'Registration Rejected', '17/12/2017', '', '', '', 'instantonlinesuccess@gmail.com', '7488339817', 'Miss Carol Fields', '7488339817', '', '2 Marnwood Walk', 'Liverpool', 'Merseyside', '', '', 'L32 5TS', '', '', '2', '', 'MARNWOOD WALK', '', '', 'LIVERPOOL', 'MERSEYSIDE', '', '', '', '', '', '', 'L32 5TS', 'goodbye standing charge', 'goodbye standing charge', 'FALSE', '_D', 13, 1, '1R', '#N/A', 0, 13, 'TRUE', 'TRUE', 'GOODBYE STANDING CHARGE|1R|13', 'GOODBYE STANDING CHARGE|1R|13', 0, 0.154455, 0, 0, 0.0398002, 'Simply Smart', 'Simply Smart', 'Simply Smart|1R|13', 'Simply Smart|1R|13', 0.226419, 0.161949, 0, 0.265586, 0.0343253, 'FALSE', 'FALSE', 'FALSE', 'Variable', 'Variable', 'Prepayment', 'Prepayment', '17/12/2018', '17/12/2018', 'FALSE', 'FALSE', 'FALSE', 'FALSE', 'FALSE', 'TRUE'),
(36626, 'Mrs Susanne Penlington', 2147483647, 'Registered', '13/04/2018', '', '17P6301722', 'S1', 160120, 'Registration Rejected', '13/04/2018', '', '', '', 'jack.akrigg@eversmartenergy.co.uk', '7913188775', 'Mrs Susanne Penlington', '7913188775', '', 'Flat 1', '24 Moss Grove', 'Birkenhead', 'Merseyside', '', 'CH42 9LD', '', 'FLAT 1', '24', '', 'MOSS GROVE', '', '', 'BIRKENHEAD', 'MERSEYSIDE', '', '', '', '', '', '', 'CH42 9LD', 'goodbye standing charge', 'Safeguard PAYG', 'FALSE', '_D', 13, 1, '1R', '_D', 13, 13, 'TRUE', 'TRUE', 'SAFEGUARD PAYG|1R|13', 'GOODBYE STANDING CHARGE|1R|13', 0.30418, 0.15664, 0.15664, 0, 0.0398002, 'Simply Smart', 'Simply Smart', 'Simply Smart|1R|13', 'Simply Smart|1R|13', 0.226419, 0.161949, 0, 0.265586, 0.0343253, 'TRUE', 'FALSE', 'TRUE', 'Variable', 'Variable', 'Prepayment', 'Prepayment', '13/04/2019', '13/04/2019', 'FALSE', 'FALSE', 'TRUE', 'FALSE', 'TRUE', 'TRUE'),
(34726, 'Mrs Edna Edmonds', 2147483647, 'Withdrawal Accepted', '31/03/2018', '', '', '', 850335, 'Registration Rejected', '31/03/2018', '', '', '', 'jack.akrigg@eversmartenergy.co.uk', '1248361364', 'Mrs Edna Edmonds', '1248361364', '', '2 Llys Dylan', 'Penrhyn Avenue', 'Bangor', 'Gwynedd', '', 'LL57 1LT', '', '', '2 LLYS DYLAN', '', 'PENRHYN AVENUE', '', '', 'BANGOR', 'GWYNEDD', '', '', '', '', '', '', 'LL57 1LT', 'goodbye standing charge', 'goodbye standing charge', 'FALSE', '_D', 13, 1, '1R', '_D', 13, 13, 'TRUE', 'TRUE', 'GOODBYE STANDING CHARGE|1R|13', 'GOODBYE STANDING CHARGE|1R|13', 0, 0.154455, 0, 0, 0.0398002, 'Simply Smart', 'Simply Smart', 'Simply Smart|1R|13', 'Simply Smart|1R|13', 0.226419, 0.161949, 0, 0.265586, 0.0343253, 'FALSE', 'FALSE', 'FALSE', 'Variable', 'Variable', 'Prepayment', 'Prepayment', '31/03/2019', '31/03/2019', 'FALSE', 'FALSE', 'FALSE', 'FALSE', 'FALSE', 'TRUE'),
(36614, 'Miss Catherine Delaney', 2147483647, 'Lost', '13/04/2018', '23/05/2018', '16P0530535', 'S1', 1200891, 'Registration Rejected', '13/04/2018', '', '', '', '', '7758116836', 'Miss Catherine Delaney', '7758116836', '', 'Flat 3', '24 Moss Grove', 'Birkenhead', 'Merseyside', '', 'CH42 9LD', '', 'FLAT 3', '24', '', 'MOSS GROVE', '', '', 'BIRKENHEAD', 'MERSEYSIDE', '', '', '', '', '', '', 'CH42 9LD', 'goodbye standing charge', 'goodbye standing charge', 'FALSE', '_D', 13, 1, '1R', '_D', 13, 13, 'TRUE', 'TRUE', 'GOODBYE STANDING CHARGE|1R|13', 'GOODBYE STANDING CHARGE|1R|13', 0, 0.154455, 0, 0, 0.0398002, 'Simply Smart', 'Simply Smart', 'Simply Smart|1R|13', 'Simply Smart|1R|13', 0.226419, 0.161949, 0, 0.265586, 0.0343253, 'FALSE', 'FALSE', 'FALSE', 'Variable', 'Variable', 'Prepayment', 'Prepayment', '13/04/2019', '13/04/2019', 'FALSE', 'FALSE', 'FALSE', 'FALSE', 'FALSE', 'TRUE'),
(39302, 'miss julie byrne', 2147483647, 'Registered', '30/04/2018', '', '17P0442800', 'S1', 1716509, 'Registered', '30/04/2018', '', 'NOT INSTALLED', '', 'instantonlinesuccess@gmail.com', '7444518158', 'miss julie byrne', '7444518158', '', '11 Whincraig Littlemoss Hey', 'Liverpool', 'Merseyside', '', '', 'L28 5RP', '', '', '11', '', 'WHINCRAIG LITTLEMOSS HEY', '', '', 'LIVERPOOL', 'MERSEYSIDE', '1', '', '', 'WHINCRAIG', '', 'LIVERPOOL', 'L28 5RP', 'Safeguard PAYG', 'Safeguard PAYG', 'FALSE', '_D', 13, 1, '1R', '#N/A', 0, 13, 'TRUE', 'TRUE', 'SAFEGUARD PAYG|1R|13', 'SAFEGUARD PAYG|1R|13', 0.30418, 0.15664, 0.15664, 0.28551, 0.03756, 'Simply Smart', 'Simply Smart', 'Simply Smart|1R|13', 'Simply Smart|1R|13', 0.226419, 0.161949, 0, 0.265586, 0.0343253, 'TRUE', 'TRUE', 'TRUE', 'Variable', 'Variable', 'Prepayment', 'Prepayment', '30/04/2019', '30/04/2019', 'FALSE', 'FALSE', 'TRUE', 'TRUE', 'TRUE', 'TRUE');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

CREATE TABLE `outcome_type` ( `outcome_type_id` INT NOT NULL AUTO_INCREMENT , `outcome` VARCHAR(255) NOT NULL , `created_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP , PRIMARY KEY (`outcome_type_id`)) ENGINE = InnoDB;
INSERT INTO `outcome_type` (`outcome_type_id`, `outcome`, `created_date`) VALUES (NULL, 'Signed Up', CURRENT_TIMESTAMP), (NULL, 'No Further Action', CURRENT_TIMESTAMP);
ALTER TABLE `tariff_change_notification` ADD `outcome_type_id` INT NULL DEFAULT NULL AFTER `eac_difference`;