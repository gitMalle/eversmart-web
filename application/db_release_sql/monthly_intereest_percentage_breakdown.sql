

/* INSERT QUERY NO: 1 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/12/2018', 1, '01/12/2018', 0.104774701, 0.141339108
);

/* INSERT QUERY NO: 2 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/12/2018', 2, '01/01/2019', 0.103057051, 0.151897708
);

/* INSERT QUERY NO: 3 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/12/2018', 3, '01/02/2019', 0.090130794, 0.134399837
);

/* INSERT QUERY NO: 4 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/12/2018', 4, '01/03/2019', 0.091260669, 0.127771856
);

/* INSERT QUERY NO: 5 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/12/2018', 5, '01/04/2019', 0.079244909, 0.091453799
);

/* INSERT QUERY NO: 6 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/12/2018', 6, '01/05/2019', 0.073480894, 0.0581554
);

/* INSERT QUERY NO: 7 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/12/2018', 7, '01/06/2019', 0.067489405, 0.030584728
);

/* INSERT QUERY NO: 8 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/12/2018', 8, '01/07/2019', 0.069179871, 0.024799066
);

/* INSERT QUERY NO: 9 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/12/2018', 9, '01/08/2019', 0.070797477, 0.024322973
);

/* INSERT QUERY NO: 10 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/12/2018', 10, '01/09/2019', 0.071546963, 0.032405749
);

/* INSERT QUERY NO: 11 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/12/2018', 11, '01/10/2019', 0.084232853, 0.071941087
);

/* INSERT QUERY NO: 12 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/12/2018', 12, '01/11/2019', 0.094804411, 0.110928688
);

/* INSERT QUERY NO: 13 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/01/2019', 1, '01/01/2019', 0.103061809, 0.151804215
);

/* INSERT QUERY NO: 14 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/01/2019', 2, '01/02/2019', 0.090134956, 0.134317113
);

/* INSERT QUERY NO: 15 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/01/2019', 3, '01/03/2019', 0.091264883, 0.127693213
);

/* INSERT QUERY NO: 16 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/01/2019', 4, '01/04/2019', 0.079248569, 0.091397509
);

/* INSERT QUERY NO: 17 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/01/2019', 5, '01/05/2019', 0.073484288, 0.058119606
);

/* INSERT QUERY NO: 18 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/01/2019', 6, '01/06/2019', 0.067492522, 0.030565903
);

/* INSERT QUERY NO: 19 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/01/2019', 7, '01/07/2019', 0.069183066, 0.024783802
);

/* INSERT QUERY NO: 20 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/01/2019', 8, '01/08/2019', 0.070800747, 0.024308002
);

/* INSERT QUERY NO: 21 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/01/2019', 9, '01/09/2019', 0.071550267, 0.032385803
);

/* INSERT QUERY NO: 22 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/01/2019', 10, '01/10/2019', 0.084236743, 0.071896807
);

/* INSERT QUERY NO: 23 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/01/2019', 11, '01/11/2019', 0.094808789, 0.110860411
);

/* INSERT QUERY NO: 24 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/01/2019', 12, '01/12/2019', 0.104733363, 0.141867614
);

/* INSERT QUERY NO: 25 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/02/2019', 1, '01/02/2019', 0.090156492, 0.134337815
);

/* INSERT QUERY NO: 26 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/02/2019', 2, '01/03/2019', 0.09128669, 0.127712893
);

/* INSERT QUERY NO: 27 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/02/2019', 3, '01/04/2019', 0.079267504, 0.091411596
);

/* INSERT QUERY NO: 28 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/02/2019', 4, '01/05/2019', 0.073501846, 0.058128563
);

/* INSERT QUERY NO: 29 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/02/2019', 5, '01/06/2019', 0.067508648, 0.030570614
);

/* INSERT QUERY NO: 30 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/02/2019', 6, '01/07/2019', 0.069199596, 0.024787622
);

/* INSERT QUERY NO: 31 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/02/2019', 7, '01/08/2019', 0.070817663, 0.024311749
);

/* INSERT QUERY NO: 32 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/02/2019', 8, '01/09/2019', 0.071567363, 0.032390795
);

/* INSERT QUERY NO: 33 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/02/2019', 9, '01/10/2019', 0.08425687, 0.071907888
);

/* INSERT QUERY NO: 34 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/02/2019', 10, '01/11/2019', 0.094831442, 0.110877497
);

/* INSERT QUERY NO: 35 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/02/2019', 11, '01/12/2019', 0.104758388, 0.141889479
);

/* INSERT QUERY NO: 36 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/02/2019', 12, '01/01/2020', 0.102847499, 0.151673488
);

/* INSERT QUERY NO: 37 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/03/2019', 1, '01/03/2019', 0.091020372, 0.127205672
);

/* INSERT QUERY NO: 38 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/03/2019', 2, '01/04/2019', 0.07903625, 0.091048547
);

/* INSERT QUERY NO: 39 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/03/2019', 3, '01/05/2019', 0.073287413, 0.057897701
);

/* INSERT QUERY NO: 40 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/03/2019', 4, '01/06/2019', 0.0673117, 0.0304492
);

/* INSERT QUERY NO: 41 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/03/2019', 5, '01/07/2019', 0.068997714, 0.024689176
);

/* INSERT QUERY NO: 42 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/03/2019', 6, '01/08/2019', 0.070611061, 0.024215193
);

/* INSERT QUERY NO: 43 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/03/2019', 7, '01/09/2019', 0.071358574, 0.032262152
);

/* INSERT QUERY NO: 44 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/03/2019', 8, '01/10/2019', 0.08401106, 0.0716223
);

/* INSERT QUERY NO: 45 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/03/2019', 9, '01/11/2019', 0.094554783, 0.110437139
);

/* INSERT QUERY NO: 46 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/03/2019', 10, '01/12/2019', 0.104452767, 0.141325954
);

/* INSERT QUERY NO: 47 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/03/2019', 11, '01/01/2020', 0.102547453, 0.151071105
);

/* INSERT QUERY NO: 48 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/03/2019', 12, '01/02/2020', 0.092810853, 0.13777586
);

/* INSERT QUERY NO: 49 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/04/2019', 1, '01/04/2019', 0.079074535, 0.091174223
);

/* INSERT QUERY NO: 50 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/04/2019', 2, '01/05/2019', 0.073322912, 0.057977619
);

/* INSERT QUERY NO: 51 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/04/2019', 3, '01/06/2019', 0.067344305, 0.03049123
);

/* INSERT QUERY NO: 52 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/04/2019', 4, '01/07/2019', 0.069031136, 0.024723255
);

/* INSERT QUERY NO: 53 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/04/2019', 5, '01/08/2019', 0.070645264, 0.024248618
);

/* INSERT QUERY NO: 54 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/04/2019', 6, '01/09/2019', 0.071393139, 0.032306684
);

/* INSERT QUERY NO: 55 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/04/2019', 7, '01/10/2019', 0.084051754, 0.071721162
);

/* INSERT QUERY NO: 56 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/04/2019', 8, '01/11/2019', 0.094600584, 0.110589577
);

/* INSERT QUERY NO: 57 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/04/2019', 9, '01/12/2019', 0.104503363, 0.141521029
);

/* INSERT QUERY NO: 58 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/04/2019', 10, '01/01/2020', 0.102597126, 0.151279631
);

/* INSERT QUERY NO: 59 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/04/2019', 11, '01/02/2020', 0.092855809, 0.137966035
);

/* INSERT QUERY NO: 60 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/04/2019', 12, '01/03/2020', 0.090580073, 0.126000936
);

/* INSERT QUERY NO: 61 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/05/2019', 1, '01/05/2019', 0.07337043, 0.05805277
);

/* INSERT QUERY NO: 62 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/05/2019', 2, '01/06/2019', 0.067387948, 0.030530753
);

/* INSERT QUERY NO: 63 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/05/2019', 3, '01/07/2019', 0.069075873, 0.024755302
);

/* INSERT QUERY NO: 64 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/05/2019', 4, '01/08/2019', 0.070691047, 0.024280049
);

/* INSERT QUERY NO: 65 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/05/2019', 5, '01/09/2019', 0.071439406, 0.03234856
);

/* INSERT QUERY NO: 66 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/05/2019', 6, '01/10/2019', 0.084106225, 0.071814128
);

/* INSERT QUERY NO: 67 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/05/2019', 7, '01/11/2019', 0.094661891, 0.110732924
);

/* INSERT QUERY NO: 68 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/05/2019', 8, '01/12/2019', 0.104571088, 0.14170447
);

/* INSERT QUERY NO: 69 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/05/2019', 9, '01/01/2020', 0.102663616, 0.151475721
);

/* INSERT QUERY NO: 70 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/05/2019', 10, '01/02/2020', 0.092915986, 0.138144868
);

/* INSERT QUERY NO: 71 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/05/2019', 11, '01/03/2020', 0.090638775, 0.12616426
);

/* INSERT QUERY NO: 72 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/05/2019', 12, '01/04/2020', 0.078477714, 0.089996196
);

/* INSERT QUERY NO: 73 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/06/2019', 1, '01/06/2019', 0.067387551, 0.030567404
);

/* INSERT QUERY NO: 74 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/06/2019', 2, '01/07/2019', 0.069075466, 0.024785019
);

/* INSERT QUERY NO: 75 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/06/2019', 3, '01/08/2019', 0.070690631, 0.024309196
);

/* INSERT QUERY NO: 76 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/06/2019', 4, '01/09/2019', 0.071438986, 0.032387393
);

/* INSERT QUERY NO: 77 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/06/2019', 5, '01/10/2019', 0.08410573, 0.071900337
);

/* INSERT QUERY NO: 78 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/06/2019', 6, '01/11/2019', 0.094661334, 0.110865855
);

/* INSERT QUERY NO: 79 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/06/2019', 7, '01/12/2019', 0.104570472, 0.14187458
);

/* INSERT QUERY NO: 80 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/06/2019', 8, '01/01/2020', 0.102663011, 0.151657562
);

/* INSERT QUERY NO: 81 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/06/2019', 9, '01/02/2020', 0.092915439, 0.138310705
);

/* INSERT QUERY NO: 82 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/06/2019', 10, '01/03/2020', 0.090638241, 0.126315715
);

/* INSERT QUERY NO: 83 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/06/2019', 11, '01/04/2020', 0.078477252, 0.090104233
);

/* INSERT QUERY NO: 84 */
INSERT INTO monthly_interest_percentage_lookup(start_date, month_number, date_iteration, elec_percentage, gas_percentage)
VALUES
(
  '01/06/2019', 12, '01/05/2020', 0.073375886, 0.056922001
);

