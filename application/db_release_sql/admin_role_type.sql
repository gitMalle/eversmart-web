CREATE TABLE `admin_role_type` ( `admin_role_type_id` INT NOT NULL AUTO_INCREMENT , `role_type` VARCHAR(255) NOT NULL , PRIMARY KEY (`admin_role_type_id`)) ENGINE = InnoDB;
INSERT INTO `admin_role_type` (`admin_role_type_id`, `role_type`) VALUES (NULL, 'Super Admin'), (NULL, 'Customer Services');
ALTER TABLE `customer_info` ADD `admin_role_type_id` INT NULL AFTER `dyball_account_id`;
INSERT INTO `admin_role_type` (`admin_role_type_id`, `role_type`) VALUES (NULL, 'Tariff Management');


CREATE TABLE `tariff` ( `tariff_id` INT NOT NULL AUTO_INCREMENT , `tariff_name` VARCHAR(255) NOT NULL , `tariff_code` VARCHAR(255) NOT NULL , `created_at` TIMESTAMP NOT NULL , PRIMARY KEY (`tariff_id`)) ENGINE = InnoDB;
INSERT INTO `tariff` (`tariff_id`, `tariff_name`, `tariff_code`, `created_at`) VALUES (NULL, 'Goodbye Standing Charge', 'GSC-VAR', CURRENT_TIMESTAMP), (NULL, 'Goodbye Standing Charge', 'GSC-FIX', CURRENT_TIMESTAMP), (NULL, 'VariSmart', 'VS-VAR', CURRENT_TIMESTAMP), (NULL, 'Winter is Coming', 'WIC-VAR', CURRENT_TIMESTAMP), (NULL, 'Winter is Coming', 'WIC-FIX', CURRENT_TIMESTAMP);