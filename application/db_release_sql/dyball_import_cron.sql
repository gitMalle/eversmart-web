ALTER TABLE `customer_info` ADD `dyball_import_success` TINYINT NULL AFTER `booked_meter`;

CREATE TABLE `generated_file_type` ( `generated_file_type_id` INT NOT NULL AUTO_INCREMENT , `name` VARCHAR(255) NOT NULL , `created_date` TIMESTAMP NOT NULL , PRIMARY KEY (`generated_file_type_id`)) ENGINE = InnoDB;
INSERT INTO `generated_file_type` (`generated_file_type_id`, `name`, `created_date`) VALUES (NULL, 'New Dyball Customer Import', CURRENT_TIMESTAMP);
CREATE TABLE `generated_file` ( `generated_file_id` INT NOT NULL AUTO_INCREMENT , `generated_file_type_id` INT(11) NOT NULL , `filename` VARCHAR(255) NOT NULL , `created_at` TIMESTAMP NOT NULL , PRIMARY KEY (`generated_file_id`)) ENGINE = InnoDB;


ALTER TABLE `generated_file` ADD `affected_rows` INT NOT NULL DEFAULT '0' AFTER `filename`;