<?php

use Twilio\Rest\Client;

/**
 * Helper function to check if a customer has
 * been converted
 *
 * @param int $referred_customer_id
 * @return boolean
 */

function get_base_instance()
{
    $ci =& get_instance();
    return $ci;
}

/**
 *
 * Update referral log when a customer redeems their reward
 *
 * @param int customer id
 * @return boolean
 */
function update_referral_log($customer_id)
{
    $response = get_base_instance()->saas_model->get_referral_log_id($customer_id);

    if (!empty($response)) {
        $db_param = [
            'type' => ($customer_id == $response[0]['referrer_customer_id']) ? 'referrer' : 'referred',
            'referral_log_id' => $response[0]['referral_log_id']
        ];

        return get_base_instance()->saas_model->saas_customer_redeemed($db_param);
    }
}

/**
 * Function to be triggered when a customers reward has been
 * transfered
 * @param array $pay_record mixed array of required table fields
 * @return boolean
 */
function insert_payment_transaction($pay_record = [])
{
    if (empty($pay_record)) {
        die("No record provided!");
    }

    foreach ($pay_record as $pay_value) {
        if ($pay_value == "") {
            throw new Exception("You have either supplied an empty record or the customers id cannot be zero or null");
            return 0;
        }
    }

    get_base_instance()->load->model('saas_model');
    return get_base_instance()->saas_model->insert_payment_transaction($pay_record);
}


/**
 * Function to call Saasquatch API to redeem rewards
 *
 * @param array $sass_data mixed array containing customers id, amount and units
 * @return void
 */
function redeem_reward($customer_id, $amount)
{
    return get_base_instance()->curl_requests_saasquatch->saasquatch_debit_referral_user($customer_id, $amount);

}

/**
 * Admin redeem update
 *
 * @param string $type
 * @param string $todays_date
 * @param string $customer_id
 * @return void
 */
function admin_reward_confirmation($type, $todays_date, $customer_id)
{
    get_base_instance()->saas_model->admin_reward_confirmation($type, $todays_date, $customer_id);
}


/**
 * Function to send out SMS to customers when the get cash
 * for referrals
 *
 * @param string $phone_number Customers phone number
 * @return string unique sms send id
 */
function payment_sms_alert($phone_number, $amount)
{

    if (!preg_match('/^07/si', $phone_number, $output_array)) {
        return false;
    } else {
        if (strpos($phone_number, '0') == 0) {
            $phone_number = substr($phone_number, 1);
            $phone_number = "+44" . $phone_number;
        } else if (strpos($phone_number, '7') == 0) {
            $phone_number = "+44" . $phone_number;
        } else {
            return false;
        }
    }

    $sid = "ACf1ec1c905729e2657f15962b514775ce";
    $token = "49b1ee59b9be369341e85f8c7450186a";
    $client = new Twilio\Rest\Client($sid, $token);
    $message = $client->messages->create($phone_number, array('from' => 'Eversmart', 'body' => "THANK YOU for using Eversmart, £{$amount} has been sent to your bank account 😊 Continue to sign friends up & remember to use your code to receive £40, there is NO LIMIT $ "));
    if (strlen($message->sid) > 0) {
        return true;
    } else {
        return false;
    }
}

/**
 * Function to send SMS alert to customers upon registration
 *
 * @params
 * string $phone_number customers phone number
 * string $referrer_link customers referral
 * @returns string unique sms send id
 */
function registration_sms_alert($phone_number, $referrer_link = null)
{

    if (!preg_match('/^07/si', $phone_number, $output_array)) {
        return false;
    } else {
        if (strpos($phone_number, '0') == 0) {
            $phone_number = substr($phone_number, 1);
            $phone_number = "+44" . $phone_number;
        } else if (strpos($phone_number, '7') == 0) {
            $phone_number = "+44" . $phone_number;
        } else {
            return false;
        }
    }

    $sid = "ACf1ec1c905729e2657f15962b514775ce";
    $token = "49b1ee59b9be369341e85f8c7450186a";
    $client = new Twilio\Rest\Client($sid, $token);
    // Content - We've emailed you your welcome pack.
    $body = "Welcome to Eversmart Energy. We will email you your welcome pack within the next 24 hours. Make sure you check your junk folder and save Eversmart into your email contacts to ensure you receive all of our correspondence.";
    $body .= $referrer_link !='' ? " Refer your friends and receive £40 cash using this {$referrer_link}" : "" ;

    $message = $client->messages->create($phone_number, array('from' => 'Eversmart', 'body' => $body ));
    return $message->sid;
}

