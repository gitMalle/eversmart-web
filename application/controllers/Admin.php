<?php // if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use \vendor\stripe;

require_once('./vendor/autoload.php');

class Admin extends ESE_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('user_modal');
        $this->load->model('saas_model');
		$this->load->model('Admin_model');
        $this->load->model('error_modal');
        $this->load->model('curl/curl_requests_saasquatch');
        $this->load->helper('form');
        $this->load->helper('eversmart_helper');
        $this->mysql = $this->load->database('mysql', true);
    }

    function login()
    {

    }

    function get_random_junifer_customer()
    {
        return $this->mysql->select('*')->from('customer_info')->where('billingMethod', 'directdebit')->order_by('id', 'RANDOM')->limit(1)->get()->first_row('array');
    }

    function get_random_dyball_customer()
    {
        return $this->mysql->select('*')->from('customer_info')->where('billingMethod', 'prepay')->order_by('id', 'RANDOM')->limit(1)->get()->first_row('array');
    }

    function admin_customer_signup()
    {
        if ($this->input->post()) {
            $lead_data = $this->input->post();
            $admin_data['lead_data'] = $lead_data;
        }

        $sales_reps = $this->admin_get_sales_reps();
        $admin_data['sales_reps'] = $sales_reps;

        $admin_data['page_ref'] = 'adminsignup';
        $admin_data['page_title'] = 'Customer Admin';
        $this->load->view('admin/admin_customer_signup', $admin_data);
    }

    function admin_activation_email()
    {
        $admin_data['page_ref'] = 'adminemail';
        $admin_data['page_title'] = 'Customer Admin';
        $this->load->view('admin/admin_activation_email', $admin_data);
    }

    function admin_webtocase()
    {
        $admin_data['page_ref'] = 'webtocase';
        $admin_data['page_title'] = 'Web To Case';
        $this->load->view('admin/admin_webtocase', $admin_data);
    }

    function admin_email_templates()
    {
        $admin_data['page_ref'] = 'emailtemplates';
        $admin_data['page_title'] = 'Email Templates';
        $this->load->view('admin/admin_email_templates', $admin_data);
    }

    function admin_datacapture()
    {
        $admin_data['page_ref'] = 'datacapture';
        $admin_data['page_title'] = 'Capture Form';
        $this->load->view('admin/admin_datacapture', $admin_data);
    }

    function admin_leadupdate()
    {
        $admin_data['leads'] = $this->admin_get_leads(null, 'array');

        $lead_status = $this->mysql->select('*')->from('lead_status_type')->get()->result_array();
        $admin_data['lead_status'] = $lead_status;

        $admin_data['page_ref'] = 'leadupdate';
        $admin_data['page_title'] = 'Lead Update';
        $this->load->view('admin/admin_leadupdate', $admin_data);

    }

    /**
     * Class method to redirect users to a friend
     * referral log page
     *
     * @return void
     */
    function admin_sassquatch_update()
    {
        $sassquatch_referral_data = $this->saas_model->get_referral_rewards();
        $sassquatch_referred_data = $this->saas_model->get_referred_rewards();
        $admin_data['page_ref'] = 'saasquatch_update';
        $admin_data['page_title'] = 'Friend Referrals';
        $admin_data['referral'] = $sassquatch_referral_data;
        $admin_data['referred'] = $sassquatch_referred_data;
        $this->load->view('admin/admin_saasquatch_update', $admin_data);
    }

    /**
     * Method to complete to update the referral log and payment log tables
     * A customers payment has been verified
     *
     * @return void
     */
    function admin_saasquatch_confirm_payment()
    {
        if (!$this->input->post('customer_id')) {
            redirect('admin/admin_sassquatch_update');
        }

        $customer_id = $this->input->post('customer_id');
        $amount = $this->input->post('amount');
        $mobile = $this->input->post('mobile');
        $account_id = $this->input->post('account_id');
        $todays_date = strftime('%Y-%m-%d %H:%M:%S', time());
        $saas_amount = $amount . "00";
        $admin_data['sms_status'] = payment_sms_alert('07957068668', $amount);

        if ($admin_data['sms_status'] == 0) {
            $sassquatch_referral_data = $this->saas_model->get_referral_rewards();
            $sassquatch_referred_data = $this->saas_model->get_referred_rewards();
            $admin_data['page_ref'] = 'saasquatch_update';
            $admin_data['page_title'] = 'Friend Referrals';
            $admin_data['referral'] = $sassquatch_referral_data;
            $admin_data['referred'] = $sassquatch_referred_data;
            $admin_data['customer_id'] = $customer_id;
            $admin_data['error_msg'] = " Sorry, we are unable to send out an SMS message to the customer.<br/> Please contact technical support.";
            return $this->load->view('admin/admin_saasquatch_update', $admin_data);
        } else if (!redeem_reward($account_id, $saas_amount)) {
            $sassquatch_referral_data = $this->saas_model->get_referral_rewards();
            $sassquatch_referred_data = $this->saas_model->get_referred_rewards();
            $admin_data['page_ref'] = 'saasquatch_update';
            $admin_data['page_title'] = 'Friend Referrals';
            $admin_data['referral'] = $sassquatch_referral_data;
            $admin_data['referred'] = $sassquatch_referred_data;
            $admin_data['customer_id'] = $customer_id;
            $admin_data['error_msg'] = " Sorry, we are unable to redeem the customers balance in saasquatch. Please contact technical support";
            return $this->load->view('admin/admin_saasquatch_update', $admin_data);
        } else {
            $this->mysql->set('referrer_redeemed_date', $todays_date);
            $this->mysql->where('referrer_customer_id', $customer_id);
            $this->mysql->update('referral_log');
            $update_db = insert_payment_transaction(['account_id' => $account_id, 'amount' => $amount, 'payment_type' => 'Friend Referral', 'insert_date' => $todays_date]);
        }

        if ($this->mysql->affected_rows() && $update_db) {
            redirect('admin/admin_sassquatch_update');
        } else {
            $sassquatch_referral_data = $this->saas_model->get_referral_rewards();
            $sassquatch_referred_data = $this->saas_model->get_referred_rewards();
            $admin_data['page_ref'] = 'saasquatch_update';
            $admin_data['page_title'] = 'Friend Referrals';
            $admin_data['customer_id'] = $customer_id;
            $admin_data['referral'] = $sassquatch_referral_data;
            $admin_data['referred'] = $sassquatch_referred_data;
            $admin_data['error_msg'] = " Sorry, there is a problem updating this customers record. Please contact support.";
            $admin_data['sms_sent'] = "<b>SMS notification sent</b>";
            return $this->load->view('admin/admin_saasquatch_update', $admin_data);
        }
    }

	function admin_show_pending_energy_registration()
    {

        $admin_data['msg'] = "";
        $admin_data['error_msg'] = "";

        if ($this->input->post('database_id')) {

            if ($this->input->post('type') == 'junifer') {
                if (empty($this->input->post('mpan')) || empty($this->input->post('mprn')) || empty($this->input->post('elec_serial')) || empty($this->input->post('gas_serial'))) {
                    echo "called inside junifer";
                }

                $message = $this->Admin_model->enroll_junifer_customer($this->input->post('database_id'), $this->input->post('mpan'), $this->input->post('mprn'),
                    $this->input->post('type'), $this->input->post('elec_serial'), $this->input->post('gas_serial'));

                if ($message['error'] == true) {
                    $admin_data['error_msg'] = $message['description'];
                } else if ($message['error'] == false) {
                    $admin_data['msg'] = "Registration Successful";
                }

 
            } else if ($this->input->post('type') == 'dyball') {

                if (empty($this->input->post('dy_mpan')) || empty($this->input->post('dy_mprn'))) {
                }
                $message = $this->Admin_model->enroll_dyball_customer($this->input->post('database_id'), $this->input->post('dy_mpan'), $this->input->post('dy_mprn'), $this->input->post('type'));


                if ($message['error'] == true) {

                    $admin_data['error_msg'] = $message['description'];

                } else if ($message['error'] == false) {
                    $admin_data['msg'] = "Registration Successful";
                }

            }

        }

        $admin_data['junifer_registration'] = $this->Admin_model->get_pending_junifer_registration();
        $admin_data['dyball_registration'] = $this->Admin_model->get_pending_dyball_registration();
        $admin_data['page_title'] = 'CUSTOMER REGISTRATION';
        $this->load->view('admin/admin_show_pending_energy_registration', $admin_data);
    }

    function admin_get_leads($lead_status_type_id = null, $type = 'json')
    {
        if ($this->input->post()) {
            $lead_status_type_id = $this->input->post('search_status');
        }

        $sql = "SELECT l.*, lst.name AS `status`
                    FROM lead l
                    INNER JOIN lead_status ls ON l.lead_id = ls.lead_id AND ls.end_date IS NULL
                    INNER JOIN lead_status_type lst ON ls.lead_status_type_id = lst.lead_status_type_id\n";
        $sql .= "WHERE ls.end_date IS NULL\n";
        $sql .= "AND ls.lead_status_type_id NOT IN(5, 6)\n";

        if ($lead_status_type_id > 0) {
            $sql .= "AND lst.lead_status_type_id = " . $lead_status_type_id;
        }


        $query = $this->mysql->query($sql);
        $leads = $query->result_array();

        if ($type == 'json') {
            echo json_encode(['error' => '0', 'leads' => $leads]);
        } else {
            return $leads;
        }
    }

    function admin_get_sales_reps()
    {
        $sql = 'SELECT * FROM sales_rep ORDER BY name';
        $query = $this->mysql->query($sql);
        $reps = $query->result_array();

        return $reps;
    }

    public static function logMessage($errLevel, $msg, $depth = 1)
    {
        $btStack = debug_backtrace($depth);
        $callerFrame = $btStack[$depth - 1];

        $sid = substr(session_id(), 0, 8);
        $method = $callerFrame['class'] . '.' . $callerFrame['function'];
        $lineNo = $callerFrame['line'];
        $fullMessage = 'SID:' . $sid . ' ' . $method . '(' . $lineNo . '): ' . $msg;

        log_message($errLevel, $fullMessage);
    }

    function parseHttpResponse($response)
    {
        // Parse the headers
        $headers = array();

        $body = null;

        // Split up the httpResponse
        $responseParts = explode("\r\n\r\n", $response);
        foreach ($responseParts as $i => $responsePart) {
            $lines = explode("\r\n", $responsePart);

            // If the part has 200 response Code, parse the rest of the lines as headers.
            //if($lines[0] == "HTTP/1.1 " )
            if (strpos($lines[0], 'HTTP/1.1') !== false) {
                if (preg_match('/^HTTP\/1.1 [4,5]/', $lines[0])) {
                    // 4xx or 5xx error
                    // so log the error
                    $this->logMessage('error', 'Line 0=' . $lines[0], 2);
                }

                foreach ($lines as $j => $line) {
                    if ($j === 0) {
                        $headers['http_code'] = $line;
                    } else {
                        list ($key, $value) = explode(': ', $line);

                        $headers[$key] = $value;
                    }
                }
            } // If last element, probably body
            else if ($i === sizeof($responseParts) - 1) {
                $body = json_decode($responsePart, true);
            }
        }

        return array(
            'headers' => $headers,
            'body' => $body
        );
    }

    function insert_email_log($data)
    {
        $this->mysql->insert('email_log', $data);
        return $this->mysql->insert_id();
    }

    function reset_password($customer_info_id, $token, $forename, $recipient_email)
    {

        $this->load->library('email');
        $customer_number = $customer_info_id;

        $email_data['email_info'] = ['token' => $token, 'name' => $forename, 'customer_number' => $customer_number];
        $email_data['email_header_img'] = 'account_header.jpg';
        $email_data['content'] = 'layout/reset_password_email_content';

        $template = $this->load->view('layout/email_template', $email_data, true);

        $this->email->from('no-reply@eversmartenergy.co.uk', 'Eversmart');
        $this->email->to($recipient_email);
        $this->email->subject('Reset password');
        $this->email->message($template);
        $this->email->send();

        // Log email
        $email_log_db_parameter['customer_id'] = $customer_info_id;
        $email_log_db_parameter['email_type_id'] = 18;

        // Do the insert
        $this->insert_email_log($email_log_db_parameter);

        echo json_encode(['success' => 'Reset password email sent to: ' . $recipient_email]);
    }

    function activation_email($customer_info_id, $token, $forename, $recipient_email)
    {

        $this->load->library('email');

        $email_data['email_info'] = ['token' => $token, 'name' => $forename];
        $email_data['email_header_img'] = 'thumb_header.jpg';
        $email_data['content'] = 'layout/activation_email_content';

        $template = $this->load->view('layout/email_template', $email_data, true);

        $this->email->from('no-reply@eversmartenergy.co.uk', 'Eversmart');
        $this->email->to($recipient_email);
        $this->email->subject('Signup Confirmation');
        $this->email->message($template);
        $this->email->send();

        // Log email
        $email_log_db_parameter['customer_id'] = $customer_info_id;
        $email_log_db_parameter['email_type_id'] = 1;

        // Do the insert
        $this->insert_email_log($email_log_db_parameter);


        //echo json_encode(['success' => 'Activation email sent to: '.$recipient_email]);
    }

    function create_password_email($customer_info_id, $token, $forename, $recipient_email)
    {
        $this->load->library('email');

        $email_data['email_info'] = ['token' => $token, 'name' => $forename];
        $email_data['email_header_img'] = 'thumb_header.jpg';
        $email_data['content'] = 'layout/create_password_email_content';

        $template = $this->load->view('layout/email_template', $email_data, true);

        $this->email->from('no-reply@eversmartenergy.co.uk', 'Eversmart');
        $this->email->to($recipient_email);
        $this->email->subject('Signup Confirmation');
        $this->email->message($template);
        $this->email->send();

        // Log email
        $email_log_db_parameter['customer_id'] = $customer_info_id;
        $email_log_db_parameter['email_type_id'] = 24;
        $this->insert_email_log($email_log_db_parameter);
    }

    function admin_id_lookup()
    {
        $id = $this->input->post('id_param');
        $details = $this->user_modal->get_email_template_details($id);

        echo json_encode(['error' => '0', 'phone' => $details['phone1'], 'postcode' => $details['address_postcode']]);
    }

    function preview_email()
    {
        $send_data = $this->input->post();
        $customer_number = $send_data['customer_number'];
        $forename = $send_data['forename'];
        $selected = $send_data['selected'];
        $email_data = array();
        $token = md5(uniqid(rand(0, 9999), true));
        $email_data['email_info'] = ['token' => $token, 'name' => $forename, 'customer_number' => $customer_number];

        switch ($selected) {
            case 'preview_activation_email':
                $email_data['email_header_img'] = 'thumb_header.jpg';
                $email_data['content'] = 'layout/activation_email_content';
                break;
            case 'preview_balance_overdue_1':
                $email_data['email_info']['bill_number'] = $send_data['bill_number'];
                $email_data['email_header_img'] = 'wallet_header.jpg';
                $email_data['content'] = 'layout/balance_overdue_1_email_content';
                break;
            case 'preview_balance_overdue_2':
                $email_data['email_info']['bill_number'] = $send_data['bill_number'];
                $email_data['email_header_img'] = 'wallet_header.jpg';
                $email_data['content'] = 'layout/balance_overdue_2_email_content';
                break;
            case 'preview_balance_overdue_3':
                $email_data['email_info']['bill_number'] = $send_data['bill_number'];
                $email_data['email_header_img'] = 'wallet_header.jpg';
                $email_data['content'] = 'layout/balance_overdue_3_email_content';
                break;
            case 'preview_book_meter':
                $email_data['email_info']['user_phone'] = $send_data['user_phone'];
                $email_data['email_info']['post_code'] = $send_data['post_code'];
                $email_data['email_header_img'] = 'meter_header.jpg';
                $email_data['content'] = 'layout/book_meter_email_content';
                break;
            case 'preview_direct_debit_changes':
                $email_data['email_info']['bill_number'] = $send_data['bill_number'];
                $email_data['email_header_img'] = 'wallet_header.jpg';
                $email_data['content'] = 'layout/direct_debit_change_email_content';
                break;
            case 'preview_direct_debit_confirmation':
                $email_data['email_info']['old_dd'] = $send_data['old_dd'];
                $email_data['email_info']['new_dd'] = $send_data['new_dd'];
                $email_data['email_header_img'] = 'wallet_header.jpg';
                $email_data['content'] = 'layout/direct_debit_confirmation_email_content';
                break;
            case 'preview_direct_debit_problem':
                $email_data['email_header_img'] = 'wallet_header.jpg';
                $email_data['content'] = 'layout/direct_debit_problem_email_content';
                break;
            case 'preview_first_meter_reading':
                $email_data['email_header_img'] = 'meter_header.jpg';
                $email_data['content'] = 'layout/first_meter_reading_email_content';
                break;
            case 'preview_latest_energy_bill':
                $email_data['email_header_img'] = 'account_header.jpg';
                $email_data['content'] = 'layout/latest_energy_bill_email_content';
                break;
            case 'preview_leaving':
                $email_data['email_info']['bill_number'] = $send_data['bill_number'];
                $email_data['email_header_img'] = 'account_header.jpg';
                $email_data['content'] = 'layout/leaving_email_content';
                break;
            case 'preview_moving_home_email':
                $email_data['email_info']['new_address'] = $send_data['new_address'];
                $email_data['email_header_img'] = 'switch_header.jpg';
                $email_data['content'] = 'layout/moving_home_email_content';
                break;
            case 'preview_new_payment_plan':
                $email_data['email_info']['bill_number'] = $send_data['bill_number'];
                $email_data['email_header_img'] = 'wallet_header.jpg';
                $email_data['content'] = 'layout/new_payment_plan_email_content';
                break;
            case 'preview_new_payment_schedule':
                $email_data['email_info']['bill_number'] = $send_data['bill_number'];
                $email_data['email_header_img'] = 'wallet_header.jpg';
                $email_data['content'] = 'layout/new_payment_schedule_email_content';
                break;
            case 'preview_objection':
                $email_data['email_info']['bill_number'] = $send_data['bill_number'];
                $email_data['email_header_img'] = 'account_header.jpg';
                $email_data['content'] = 'layout/objection_email_content';
                break;
            case 'preview_payment_plan_cancelled':
                $email_data['email_header_img'] = 'wallet_header.jpg';
                $email_data['content'] = 'layout/payment_plan_cancelled_email_content';
                break;
            case 'preview_previous_supplier_objection':
                $email_data['email_info']['bill_number'] = $send_data['bill_number'];
                $email_data['email_header_img'] = 'account_header.jpg';
                $email_data['content'] = 'layout/previous_supplier_objection_email_content';
                break;
            case 'preview_provide_meter_reading':
                $email_data['email_header_img'] = 'meter_header.jpg';
                $email_data['content'] = 'layout/provide_meter_reading_email_content';
                break;
            case 'preview_registration_withdrawal':
                $email_data['email_header_img'] = 'meter_header.jpg';
                $email_data['content'] = 'layout/registration_withdrawal_email_content';
                break;
            case 'preview_reset_password':
                $email_data['email_header_img'] = 'account_header.jpg';
                $email_data['content'] = 'layout/reset_password_email_content';
                break;
        }

        $template = $this->load->view('layout/email_template', $email_data, true);
        echo json_encode(['success' => $template]);
    }

    function admin_customer_lookup()
    {
        unset($_SESSION['account_list']);

        if ($this->input->post('search_surname')) {
            $surname = $this->input->post('search_surname');
        }

        if ($this->input->post('search_email')) {
            $email = $this->input->post('search_email');
        }

        if (isset($surname) && isset($email)) {
            $account_list = $this->mysql->select('id, forename, surname, email')
                ->from('customer_info')
                ->where('surname', $surname)
                ->where('email', $email)
                ->get()
                ->first_row('array');
        }

        if (isset($surname) && !isset($email)) {
            $account_list = $this->mysql->select('id, forename, surname, email')
                ->from('customer_info')
                ->where('surname', $surname)
                ->get()->result_array();
        }

        if (!isset($surname) && isset($email)) {
            $account_list = $this->mysql->select('id, forename, surname, email')
                ->from('customer_info')
                ->where('email', $email)
                ->get()->result_array();
        }

        if ($account_list != null) {

            $this->session->set_userdata('account_list', $account_list);

            $this->account_list();
        } else {
            header('Content-Type: application/json');
            http_response_code(204);
        }
    }

    function account_list()
    {
        $data = array();
        $account_list = $this->session->userdata('account_list');

        if (!empty($account_list)) {
            for ($i = 0; $i < count($account_list); $i++) {
                ($account_list[$i]['id']) ? $id = $account_list[$i]['id'] : $id = '';
                ($account_list[$i]['forename']) ? $forename = $account_list[$i]['forename'] : $forename = '';
                ($account_list[$i]['surname']) ? $surname = $account_list[$i]['surname'] : $surname = '';
                ($account_list[$i]['email']) ? $email = $account_list[$i]['email'] : $email = '';

                $account_list[$i]['account'] = $forename . " " . $surname . " - " . $email;
            }
            $data['account_list'] = $account_list;
        }
        $this->load->view('admin/accountlist', $data);
    }

    function email_type_lookup()
    {
        unset($_SESSION['email_type_list']);

        $email_type_list = $this->mysql->select('*')->from('email_type')->where('dropdown_flag', 1)->get()->result_array();

        if ($email_type_list != null) {
            $this->session->set_userdata('email_type_list', $email_type_list);

            $this->email_type_list();
        }
    }

    function email_type_list()
    {
        $data = array();
        $email_type_list = $this->session->userdata('email_type_list');

        if (!empty($email_type_list)) {
            for ($i = 0; $i < count($email_type_list); $i++) {
                ($email_type_list[$i]['email_type_id']) ? $id = $email_type_list[$i]['email_type_id'] : $id = '';
                ($email_type_list[$i]['name']) ? $name = $email_type_list[$i]['name'] : $name = '';
            }
            $data['email_type_list'] = $email_type_list;
        }
        $this->load->view('admin/email_typelist', $data);
    }

    function admin_lead_lookup()
    {
        if ($this->input->post('search_surname')) {
            $surname = $this->input->post('search_surname');
        }

        if ($this->input->post('search_email')) {
            $email = $this->input->post('search_email');
        }

        if (isset($surname) && isset($email)) {
            $sql = "SELECT l.*, lst.name AS `status`
                    FROM lead l
                    INNER JOIN lead_status ls ON l.lead_id = ls.lead_id AND ls.end_date IS NULL
                    INNER JOIN lead_status_type lst ON ls.lead_status_type_id = lst.lead_status_type_id
                    WHERE l.surname = '" . $surname . "'
                    AND l.email = '" . $email . "'";
            $query = $this->mysql->query($sql);
            $result = $query->first_row('array');
        }

        if (isset($surname) && !isset($email)) {
            $sql = "SELECT l.*, lst.name AS `status`
                    FROM lead l
                    INNER JOIN lead_status ls ON l.lead_id = ls.lead_id AND ls.end_date IS NULL
                    INNER JOIN lead_status_type lst ON ls.lead_status_type_id = lst.lead_status_type_id
                    WHERE l.surname = '" . $surname . "'";
            $query = $this->mysql->query($sql);
            $result = $query->first_row('array');
        }

        if (!isset($surname) && isset($email)) {
            $sql = "SELECT l.*, lst.name AS `status`
                    FROM lead l
                    INNER JOIN lead_status ls ON l.lead_id = ls.lead_id AND ls.end_date IS NULL
                    INNER JOIN lead_status_type lst ON ls.lead_status_type_id = lst.lead_status_type_id
                    WHERE l.email = '" . $email . "'";
            $query = $this->mysql->query($sql);
            $result = $query->first_row('array');
        }

        if (!empty($result)) {
            echo json_encode(['error' => '0', 'lead_id' => $result['lead_id'], 'forename' => $result['forename'], 'surname' => $result['surname'], 'status' => $result['status']]);
        } else {
            echo json_encode(['error' => '1', 'msg' => 'No results found']);
        }
    }

    function admin_update_lead()
    {
        if ($this->input->post('search_surname')) {
            $lead_id = $this->input->post('lead_id');
        }

        $sql = "SELECT * FROM lead_status ORDER BY lead_id WHERE lead_id = '" . $lead_id . "' DESC LIMIT 1;";
        $query = $this->mysql->query($sql);
        $result = $query->first_row('array');

        if (!empty($result)) {
            echo json_encode(['error' => '0', 'lead' => $result]);
        } else {
            echo json_encode(['error' => '1', 'msg' => 'Update failed']);
        }
    }

    function admin_resend_email()
    {
        $email_data = $this->input->post();
        $customer_info_id = $email_data['id'];
        $forename = $email_data['forename'];
        $recipient_email = $email_data['email'];
        $email_type = $email_data['email_type'];

        if ($email_type === 'Activation') {

            /* NEEDS TESTING WHEN JUNIFER VERSION HAS BEEN UPDATED

            1. get webUser
            $getWebUser = $this->curl->junifer_request('/rest/v1/webUsers?username='.$email.'billingEntity=Eversmart');
            $WebUser = $this->parseHttpResponse($getWebUser);

            2. save webUser id to variable
            $webUserID = $WebUser['results']['id'];

            3. set post variable & request webUser token regen
            $data = [
                'expiryDttm' => date('Y-m-d H:i:s', strtotime('+3 day', strtotime( date('Y-m-d H:i:s') )))
            ];
            $regen = $this->curl->junifer_request('/rest/v1/webUsers/'.$webUserID.'/regenerateActivationToken');

            4. set token to variable
            $regenResponse = $this->parseHttpResponse($regen);
            $token = $regenResponse['token]';

            5. save to db
            $this->user_modal->token_in_database($token, $customer_info_id);

            6. send email
            $this->activation_email($customer_info_id, $token, $forename, $recipient_email);

             */

            $token = bin2hex(openssl_random_pseudo_bytes(16));

            $this->user_modal->token_in_database($token, $customer_info_id);
            $this->activation_email($customer_info_id, $token, $forename, $recipient_email);
        } else if ($email_type === 'Reset password') {
            $token = bin2hex(openssl_random_pseudo_bytes(16));

            $this->user_modal->token_in_database($token, $customer_info_id, '1');
            $this->reset_password($customer_info_id, $token, $forename, $recipient_email);
        }
    }

    function address_lookup()
    {
        unset($_SESSION['address_data']);
        unset($_SESSION['address_data_gas']);

        $this->load->model('curl/curl_requests_junifer');

        $pc = trim(str_replace(' ', '', $this->input->post('postcode')));
        $pc = wordwrap($pc, strlen($pc)-3,' ', true);

        $pc_arr = explode(' ', $pc);
        count($pc_arr)===3 ? $pc = $pc_arr[0] . ' ' . $pc_arr[1].$pc_arr[2] : null;

        $e = $this->curl_requests_junifer->junifer_elec_addresses_pcod($pc);
        $this->session->set_userdata('address_data', $e);

        $g = $this->curl_requests_junifer->junifer_gas_addresses_pcod($pc);
        $this->session->set_userdata('address_data_gas', $g);

        if (!empty($e)) {
            echo json_encode([
                'success' => true,
                'msg' => 'Address found.<br/>Proceed with quote process and select address after quote.',
                'postcode' => $this->input->post('postcode')
            ]);
            exit;
        }
        else {
            echo json_encode(['success' => false, 'msg' => 'No addresses found.']);
            exit;
        }
    }

    function admin_getmprn_energylinx()
    {
        $selectedpostcode = strtoupper($this->input->post('contact-postcode'));
        $gasserial = $this->input->post('checkinput');

        if ($gasserial) {
            $gasserial_para = ['meterSerialGas' => $gasserial];
            $response = $this->curl->curl_request('/v3.1/partner-resources/' . $this->affiliate_id . '/mpas/addresses-by-meter-serial-gas/', $gasserial_para, 'post');
            $response = json_decode($response, true);

            if (!empty($response['data']['addresses'])) {
                for ($r = 0; $r < count($response['data']['addresses']); $r++) {
                    if ($response['data']['addresses'][$r]['pcod'] == $selectedpostcode) {
                        if ($response['data']['addresses'][$r]['metersGas'][0]['meterSerialGas'] == $gasserial) {
                            $checkedserial = $response['data']['addresses'][$r]['metersGas'][0]['meterSerialGas'];
                            $mprn = $response['data']['addresses'][$r]['metersGas'][0]['mprn'];
                            echo json_encode(['error' => '0', 'gasserial' => $checkedserial, 'mprn' => $mprn, 'msg' => 'MPRN found!']);
                            break;
                        } else {
                            echo json_encode(['error' => '1', 'msg' => 'Could not find MPRN!']);
                        }
                    } else {
                        echo json_encode(['error' => '1', 'msg' => 'Serial number does not relate to postcode']);
                    }
                }
            } else {
                echo json_encode(['error' => '1', 'msg' => 'Serial number could not be found']);
            }
        }
    }

    function admin_getmprn_junifer()
    {
        $selectedpostcode = strtoupper($this->input->post('contact-postcode'));
        $postcode = rawurlencode($selectedpostcode);
        $gasserial = $this->input->post('checkinput2');

        if ($selectedpostcode && $gasserial) {
            $response = $this->curl->junifer_request('/rest/v1/des/mprns?postcode=' . $postcode);
            $response = $this->parseHttpResponse($response);

            if (!empty($response['body']['results'])) {
                foreach ($response['body']['results'] as $key => $value) {
                    if ($value['meterSerialNumber'] == $gasserial) {
                        $Mprn = $key;
                        break;
                    } else {
                        $Mprn = '';
                    }
                }
            } else {
                echo json_encode(['error' => '1', 'msg' => 'Could not find MPRN!']);
            }

            if ($Mprn != '') {
                $mprn = $response['body']['results'][$Mprn]['mprn'];
                $msn = $response['body']['results'][$Mprn]['meterSerialNumber'];
                echo json_encode(['error' => '0', 'gasserial' => $msn, 'mprn' => $mprn, 'msg' => 'MPRN found!']);
            } else {
                echo json_encode(['error' => '1', 'msg' => 'Could not find MPRN!']);
            }
        }
    }

    function our_tariffs()
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://apis2.awsprod.energylinx.com/v2/partner-resources/EVERSMARTENERGYSUPP/tariffs-onsale-by-supplier-name/",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{\n    \"supplierName\": \"Eversmart Energy\"\n}",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "cache-control: no-cache"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);
        $data['get_tariffs'] = $response;
        $this->load->view('admin/tariff_dropdown', $data);

    }

    function multiple_mpan()
    {
        $data['mpan'] = explode(',', $this->input->post('mpan'));
        $this->load->view('admin/multiple_mpan', $data);
    }

    function multiple_mprn()
    {
        $data['mprn'] = explode(',', $this->input->post('mprn'));
        $this->load->view('admin/multiple_mprn', $data);
    }

    function lookup_yearly_tariff_data($dist_id, $rate_type)
    {

        // Look up
        if ($rate_type == 'STD') {
            $rate_type = '1R';
        }
        return $this->mysql->select('*')->from('new_yearly_tariff')->where('ratetype', $rate_type)->where('duosid', $dist_id)->get()->first_row('array');

    }

    /**
     * Take the entered information e.g. tariff, address etc. and calculate the estimated price
     *
     * @params null - posted information
     * @return string - json array
     */
    function calculate_newspend()
    {
        $form_detail = $this->input->post();
        $monthyear = $form_detail['monthyear'];

        if ($form_detail['billingMethod'] == 'prepay') {
            $_POST['existingPayTypesElec'] = '5';
            $_POST['desiredPayTypes'] = '5';
        }

        if ($form_detail['billingMethod'] == 'directdebit') {
            switch ($form_detail['tariff']) {
                case 'Simply Smart':
                    $_POST['existingPayTypesElec'] = '9';
                    $_POST['desiredPayTypes'] = '9';
                break;
                default:
                    $_POST['existingPayTypesElec'] = '1';
                    $_POST['desiredPayTypes'] = '1';
                break;

            }
        }

        // Get dist id
        $dist_parameter['postcode'] = $form_detail['address_postcode'];
        $dist_response = json_decode($this->curl->curl_request('/v2/partner-resources/' . $this->affiliate_id . '/dist-ids/', $dist_parameter, 'post'), true);

        // If not found default to 17
        if (!empty($dist_response['data']['entriesFromMpanFields'])) {
            $ElecDistId = $dist_response['data']['entriesFromMpanFields'][0]['distId'];
            $ElecDistId = $ElecDistId >= 24 ? 17 : $ElecDistId;
        } else {
            $ElecDistId = 17;
        }

        // Get yearly tariff info
        $YearlyTariff = $this->lookup_yearly_tariff_data($ElecDistId, '1R');

        $eac = $form_detail['eac'];
        $aq = $form_detail['aq'];

        if ($monthyear === 'yearlypay') {
            if ($form_detail['quotefor'] == 'both') {
                $EstGas = (($YearlyTariff['gas_sc'] * 365) + ($form_detail['aq'] * $YearlyTariff['gas_ur']));
                $EstElec = (($YearlyTariff['electricity_sc'] * 365) + ($form_detail['eac'] * $YearlyTariff['electricity_dur']));

                $newspend = $EstGas + $EstElec;
                $newelec = $EstElec;
                $newgas = $EstGas;
                $elec_ur = $YearlyTariff['electricity_dur'];
                $elec_sc = $YearlyTariff['electricity_sc'];
                $gas_ur = $YearlyTariff['gas_ur'];
                $gas_sc = $YearlyTariff['gas_sc'];
            }
            if ($form_detail['quotefor'] == 'electricity') {
                $newspend = (($YearlyTariff['electricity_sc'] * 365) + ($form_detail['eac'] * $YearlyTariff['electricity_dur']));
                $newelec = (($YearlyTariff['electricity_sc'] * 365) + ($form_detail['eac'] * $YearlyTariff['electricity_dur']));
                $elec_ur = $YearlyTariff['electricity_dur'];
                $elec_sc = $YearlyTariff['electricity_sc'];
            }
        } else {
            if ($form_detail['quotefor'] == 'both') {

                $linx_param = [
                    'singleOrSeparate' => 'separate',
                    'hasExistingSupply' => false,
                    'desiredPayTypes' => $_POST['desiredPayTypes'],
                    'existingkWhsElec' => $eac,
                    'existingkWhsGas' => $aq,
                    'existingPayTypesElec' => $_POST['existingPayTypesElec'],
                    'existingPayTypesGas' => $_POST['existingPayTypesElec'],
                    "existingSupplierIdGas" => 5,
                    "existingSupplierIdElec" => 5,
                    'existingTariffNameGas' => 'Standard',
                    'existingTariffNameElec' => 'Standard',
                    'existingkWhsIntervalGas' => 'year',
                    'existingkWhsIntervalElec' => 'year',
                    'postcode' => $form_detail['address_postcode'],
                    'nightUsePercentElec' => '10',
                    'meterTypeElec' => 'STD',
                    'distId' => $ElecDistId
                ];

                $linx_response_dual = $this->curl->curl_request('/v2.2/partner-resources/' . $this->affiliate_id . '/quotes/elecgas/', $linx_param, 'post');
                $linx_response_dual = json_decode($linx_response_dual, true);

                if ($linx_response_dual['statusCode'] == "3042") {
                    echo json_encode(['error' => '1', 'msg' => 'No results']);
                    exit;
                } else {

                    foreach ($linx_response_dual['data']['searchResults'] as $key => $value) {
                        if ($value['tariffName'] == $form_detail['tariff']) {
                            $TariffKey = $key;
                            break;
                        }
                    }

                    $newspend = $linx_response_dual['data']['searchResults'][$TariffKey]['newSpend'];
                    $newelec = $linx_response_dual['data']['searchResults'][$TariffKey]['newSpendElec'];
                    $newgas = $linx_response_dual['data']['searchResults'][$TariffKey]['newSpendGas'];
                    $elec_ur = $linx_response_dual['data']['searchResults'][$TariffKey]['unitRate1Elec'];
                    $elec_sc = $linx_response_dual['data']['searchResults'][$TariffKey]['standingChargeElec'];
                    $gas_ur = $linx_response_dual['data']['searchResults'][$TariffKey]['unitRate1Gas'];
                    $gas_sc = $linx_response_dual['data']['searchResults'][$TariffKey]['standingChargeGas'];
                }

            }

            if ($form_detail['quotefor'] == 'electricity') {

                $linx_param = [
                    "postcode" => $form_detail['address_postcode'],
                    "hasExistingSupply" => true,
                    "existingPayTypesElec" => $_POST['existingPayTypesElec'],
                    "desiredPayTypes" => $_POST['desiredPayTypes'],
                    "meterTypeElec" => "STD",
                    "existingSupplierIdElec" => 5,
                    "nightUsePercentElec" => "10",
                    "existingTariffNameElec" => 'Standard',
                    "existingkWhsElec" => $eac,
                    "existingkWhsIntervalElec" => "year"
                ];

                $linx_response_elec = $this->curl->curl_request('/v2.2/partner-resources/' . $this->affiliate_id . '/quotes/elec/', $linx_param, 'post');
                $linx_response_elec = json_decode($linx_response_elec, true);

                if ($linx_response_elec['statusCode'] == "3042") {
                    echo json_encode(['error' => '1', 'msg' => 'No results']);
                    exit;
                } else {

                    foreach ($linx_response_elec['data']['searchResults'] as $key => $value) {
                        if ($value['tariffName'] == $form_detail['tariff']) {
                            $TariffKey = $key;
                            break;
                        }
                    }

                    $newspend = $linx_response_elec['data']['searchResults'][$TariffKey]['newSpend'];
                    $newelec = $linx_response_elec['data']['searchResults'][$TariffKey]['newSpendElec'];
                    $elec_ur = $linx_response_elec['data']['searchResults'][$TariffKey]['unitRate1Elec'];
                    $elec_sc = $linx_response_elec['data']['searchResults'][$TariffKey]['standingChargeElec'];
                }
            }
        }

        if (empty($form_detail['quotefor'])) {
            echo json_encode(['error' => true, 'msg' => 'Quote for cannot be empty']);
            exit;
        }

        // Price adjustments
        switch ($form_detail['tariff']) {
            case 'SmartMeter 12':
                $reduction = $form_detail['quotefor']=='both'?100:50;
                $newspend = $newspend - $reduction;
                break;
            case 'Spring Smart Special':
                $newspend = $newspend - 50;
                break;
        }


        if ($form_detail['billingMethod'] == 'prepay') {

            if ($form_detail['quotefor'] == 'both') {
                echo json_encode(['error' => false,
                    'newspend' => number_format($newspend, 2, '.', ''),
                    'newelec' => $newelec,
                    'newgas' => $newgas,
                    'elec_ur' => round(($elec_ur / 100), 4),
                    'elec_sc' => round(($elec_sc / 100), 4),
                    'gas_ur' => round(($gas_ur / 100), 4),
                    'gas_sc' => round(($gas_sc / 100), 4),
                    'quote' => '£' . number_format(($newspend) / 52, 2) . ' per week',
                    'quote_year' => '£' . number_format(($newspend), 2) . ' per year']);
            }

            if ($form_detail['quotefor'] == 'electricity') {
                echo json_encode(['error' => false,
                    'newspend' => number_format($newspend, 2, '.', ''),
                    'newelec' => $newelec,
                    'elec_ur' => round(($elec_ur / 100), 4),
                    'elec_sc' => round(($elec_sc / 100), 4),
                    'quote' => '£' . number_format(($newspend) / 52, 2) . ' per week',
                    'quote_year' => '£' . number_format(($newspend), 2) . ' per year']);
            }

        } else if ($monthyear === 'yearlypay') {

            if ($form_detail['quotefor'] == 'both') {
                echo json_encode(['error' => false,
                    'newspend' => number_format($newspend, 2, '.', ''),
                    'newelec' => $newelec,
                    'newgas' => $newgas,
                    'elec_ur' => $elec_ur,
                    'elec_sc' => $elec_sc,
                    'gas_ur' => $gas_ur,
                    'gas_sc' => $gas_sc,
                    'quote' => '£' . number_format($newspend / 12, 2) . ' per month',
                    'quote_year' => '£' . number_format(($newspend), 2) . ' per year']);
            }

            if ($form_detail['quotefor'] == 'electricity') {
                echo json_encode(['error' => false,
                    'newspend' => number_format($newspend, 2, '.', ''),
                    'newelec' => $newelec,
                    'elec_ur' => $elec_ur,
                    'elec_sc' => $elec_sc,
                    'quote' => '£' . number_format($newspend / 12, 2) . ' per month',
                    'quote_year' => '£' . number_format(($newspend), 2) . ' per year']);
            }

            if (empty($form_detail['quotefor'])) {
                echo json_encode(['error' => true, 'msg' => 'Quote for is empty, please select tariff type.']);
            }

        } else {

            if ($form_detail['quotefor'] == 'both') {
                echo json_encode(['error' => false,
                    'newspend' => number_format($newspend, 2, '.', ''),
                    'newelec' => $newelec,
                    'newgas' => $newgas,
                    'elec_ur' => round(($elec_ur / 100), 4),
                    'elec_sc' => round(($elec_sc / 100), 4),
                    'gas_ur' => round(($gas_ur / 100), 4),
                    'gas_sc' => round(($gas_sc / 100), 4),
                    'quote' => '£' . number_format(($newspend) / 12, 2) . ' per month',
                    'quote_year' => '£' . number_format(($newspend), 2) . ' per year']);
            }

            if ($form_detail['quotefor'] == 'electricity') {
                echo json_encode(['error' => false,
                    'newspend' => number_format($newspend, 2, '.', ''),
                    'newelec' => $newelec,
                    'elec_ur' => round(($elec_ur / 100), 4),
                    'elec_sc' => round(($elec_sc / 100), 4),
                    'quote' => '£' . number_format(($newspend) / 12, 2) . ' per month',
                    'quote_year' => '£' . number_format(($newspend), 2) . ' per year']);
            }
        }
    }

    /**
     * Take the form data e.g. customer details, tariff, address etc. and enrol customer in Junifer / Dyball & DB
     *
     * @params null - posted information
     * @return string - json array
     */
    function admin_enroll_customer()
    {
        $form_detail = $this->input->post();

        $monthyear = $form_detail['monthyear'];
        $fname = $form_detail['forename'];
        $lname = $form_detail['surname'];
        $email = $form_detail['email'];

        $form_detail['dateOfBirth'] = str_replace('/', '-', $form_detail['dateOfBirth']);
        $dob = date('Y-m-d', strtotime($form_detail['dateOfBirth']));

        $dplo = isset($form_detail['address_address2']) ? $form_detail['address_address2'] : 'Area';


        if ($form_detail['billingMethod'] == 'prepay') {
            $_POST['existingPayTypesElec'] = '5';
            $_POST['desiredPayTypes'] = '5';
        }

        if ($form_detail['billingMethod'] == 'directdebit') {
            $_POST['existingPayTypesElec'] = '9';
            $_POST['desiredPayTypes'] = '9';
        }

        // Get dist id
        $dist_parameter['postcode'] = $form_detail['address_postcode'];
        $dist_response = json_decode($this->curl->curl_request('/v2/partner-resources/' . $this->affiliate_id . '/dist-ids/', $dist_parameter, 'post'), true);

        // If not found default to 17
        if (!empty($dist_response['data']['entriesFromMpanFields'])) {
            $ElecDistId = $dist_response['data']['entriesFromMpanFields'][0]['distId'];
            $ElecDistId = $ElecDistId >= 24 ? 17 : $ElecDistId;
        } else {
            $ElecDistId = 17;
        }

        // Find the tariff code matching the name
        if ($form_detail['tariff']) {

            $TariffData = $this->mysql->select('*')->from('tariff')->like('tariff_name', $form_detail['tariff'])->get()->first_row('array');
            $TariffCode = $TariffData['tariff_code'];
        }

        $parameter = [
            'senderReference' => $form_detail['senderReference'],
            'reference' => $form_detail['reference'],
            'billingEntityCode' => $form_detail['billingEntityCode'],
            'marketingOptOutFl' => $form_detail['marketingOptOutFl'],
            'submittedSource' => $form_detail['submittedSource'],
            'changeOfTenancyFl' => $form_detail['changeOfTenancyFl'],
            'contacts' => [
                [
                    'title' => $form_detail['title'],
                    'forename' => $form_detail['forename'],
                    'surname' => $form_detail['surname'],
                    'email' => $form_detail['email'],
                    'phone1' => $form_detail['phone1'],
                    'phone2' => $form_detail['phone2'],
                    'dateOfBirth' => $dob,
                    'billingMethod' => "Both",
                    'primaryContact' => true,
                    'address' => [
                        'careOf' => $form_detail['address_careOf'],
                        'address1' => $form_detail['address_address1'],
                        'address2' => $form_detail['address_address2'],
                        'town' => $form_detail['address_town'],
                        'county' => $form_detail['address_county'],
                        'postcode' => $form_detail['address_postcode'],
                        'countryCode' => $form_detail['address_countryCode']
                    ],
                ]
            ],
            'supplyAddress' => [
                'careOf' => $form_detail['address_careOf'],
                'address1' => $form_detail['address_address1'],
                'address2' => $form_detail['address_address2'],
                'town' => $form_detail['address_town'],
                'county' => $form_detail['address_county'],
                'postcode' => $form_detail['address_postcode'],
                'countryCode' => $form_detail['address_countryCode']
            ],
            'electricityProduct' => [
                'reference' => $this->user_modal->latest_product_ref('elec_reference_count'),
                'previousSupplier' => $form_detail['elecPrevSupplier'],
                'previousTariff' => $form_detail['elecPrevTariff'],
                'productCode' => $TariffCode . '-E',
                'mpans' => array(['mpanCore' => $form_detail['mpancore']]),
            ],
            'electricityEstimate' => [
                'consumption' => $form_detail['eac'],
                'period' => 'Year'
            ]
        ];


        if ($form_detail['bankAccountSortCode'] != '' && $form_detail['bankAccountSortCode'] != '' && $form_detail['bankAccountSortCode'] != '') {

            $parameter['directDebitInfo'] = [
                'bankAccountSortCode' => $form_detail['bankAccountSortCode'],
                'bankAccountNumber' => $form_detail['bankAccountNumber'],
                'bankAccountName' => $form_detail['bankAccountName'],
                'regularPaymentAmount' => number_format(($form_detail['newspend'] / 12), 2),
                'regularPaymentDayOfMonth' => 15
            ];
        }



        // Dual
        if ($form_detail['quotefor'] == 'both') {

            $parameter['gasProduct'] = [
                'reference' => $this->user_modal->latest_product_ref('gas_reference_count'),
                'productCode' => $TariffCode . '-G',
                'mprns' => array(['mprn' => $form_detail['mprns']]),
            ];
            $parameter['electricityEstimate'] = [
                'consumption' => $form_detail['aq'],
                'period' => 'Year'
            ];

        }

        // Sign up to Junifer
        if ($form_detail['billingMethod'] == 'directdebit') {

            $response = $this->curl->junifer_request('/rest/v1/customers/enrolCustomer', $parameter, 'post');
            $junifer_response = $this->parseHttpResponse($response);

            // Verify address is not already registered
            if (isset($junifer_response['body']['errorDescription']) && (
                    strstr($junifer_response['body']['errorDescription'], 'is currently billed to a customer') ||
                    strstr($junifer_response['body']['errorDescription'], 'This enrolment has previously been successful')
                )
            ) {
                $this->error_modal->log('signup error', $junifer_response['body']['errorDescription']);
                echo json_encode(['error' => true, 'token' => null, 'msg' => $junifer_response['body']['errorDescription']]);
                exit;
            } else {

                if (!isset($junifer_response['body']['customerId']) || $junifer_response['body']['customerId'] == '') {
                    $this->error_modal->log('admin signup error', $junifer_response['body']['errorDescription']);
                }

                $db_parameter = [
                    'customer_id' => isset($junifer_response['body']['customerId']) ? $junifer_response['body']['customerId'] : 0,
                    'title' => $parameter['contacts'][0]['title'],
                    'forename' => $parameter['contacts'][0]['forename'],
                    'surname' => $parameter['contacts'][0]['surname'],
                    'billingMethod' => $form_detail['billingMethod'],
                    'email' => $parameter['contacts'][0]['email'],
                    'phone1' => $parameter['contacts'][0]['phone1'],
                    'phone2' => $parameter['contacts'][0]['phone2'],
                    'dateOfBirth' => $dob,
                    'address_careOf' => $parameter['contacts'][0]['address']['careOf'],
                    'first_line_address' => $parameter['contacts'][0]['address']['address1'],
                    'dplo_address' => $dplo,
                    'town_address' => $parameter['contacts'][0]['address']['town'],
                    'city_address' => $parameter['contacts'][0]['address']['county'],
                    'address_postcode' => $parameter['contacts'][0]['address']['postcode'],
                    'address_countryCode' => $parameter['contacts'][0]['address']['countryCode'],
                    'supplyAddress_careOf' => $parameter['supplyAddress']['careOf'],
                    'supplyAddress_address1' => $parameter['supplyAddress']['address1'],
                    'supplyAddress_town_address' => $parameter['supplyAddress']['town'],
                    'supplyAddress_city_address' => $parameter['supplyAddress']['county'],
                    'supplyAddress_postcode' => $parameter['supplyAddress']['postcode'],
                    'supplyAddress_countryCode' => $parameter['supplyAddress']['countryCode'],
                    "signup_type" => '0',
                    "psr_flag" => $form_detail['psr_flag'],
                    'quotation_eac' => $form_detail['eac'],
                    'quotation_aq' => $form_detail['aq'],
                    'exitFeeElec' => '0',
                    'elec_mpan_core' => $form_detail['mpancore'],
                    'electricityProduct_mpans' => $form_detail['mpancore'],
                    'duosid' => $ElecDistId,
                    'bankAccountSortCode' => $form_detail['bankAccountSortCode'],
                    'bankAccountNumber' => $form_detail['bankAccountNumber'],
                    'bankAccountName' => $form_detail['bankAccountName'],
                    'added_by_admin' => $form_detail['created_by'],
                    'first_payment_status_type_id' => $form_detail['firstpaymentid'] > 0 ? $form_detail['firstpaymentid'] : null,
                    'meterTypeElec' => $form_detail['meterTypeElec'] == '' ? 'STD' : $form_detail['meterTypeElec']
                ];

                // Elec & dual params
                $db_parameter['tariff_newspend'] = $form_detail['newspend'];
                $db_parameter['tariff_tariffName'] = $form_detail['tariff'];
                $db_parameter['tariff_unitRate1Elec'] = $form_detail['elec_ur'];
                $db_parameter['tariff_standingCharge1Elec'] = $form_detail['elec_sc'];
                $db_parameter['elecProduct_reference'] = $this->user_modal->latest_product_ref('elec_reference_count');
                $db_parameter['electricityProduct_previousSupplier'] = isset($form_detail['elecPrevSupplier']) && $form_detail['elecPrevSupplier'] !== '' ? $form_detail['elecPrevSupplier'] : 'British Gas';
                $db_parameter['electricityProduct_previousTariff'] = $form_detail['elecPrevTariff'];
                $db_parameter['electricityProduct_productCode'] = $TariffCode . '-E';
                $db_parameter['elec_meter_serial'] = $form_detail['elecMeterSerial'];

                // Gas params for dual
                if ($form_detail['quotefor'] == 'both') {

                    $db_parameter['tariff_unitRate1Gas'] = $form_detail['gas_ur'];
                    $db_parameter['tariff_standingCharge1Gas'] = $form_detail['gas_sc'];
                    $db_parameter['gasProduct_reference'] = $this->user_modal->latest_product_ref('gas_reference_count');
                    $db_parameter['gasProduct_productCode'] = $TariffCode . '-G';
                }

                //insert cust into our db
                $insert_customer_info = $this->user_modal->insert_customer_info($db_parameter);

                // Only insert into opt-in DB if tariff is eligible
                if ($form_detail['tariff'] !== 'She\'s Electric' && $form_detail['boiler_cover'] == "1") {
                    $this->user_modal->boiler_cover_optin($insert_customer_info);
                }

                //Generate a random string.
                $token = openssl_random_pseudo_bytes(16);
                $token = bin2hex($token);
                $this->create_password_email($insert_customer_info, $token, $fname, $email);
                $this->user_modal->token_in_database($token, $insert_customer_info);

                // intercom
                $para_com = ["user_id" => $insert_customer_info, "email" => $email, "name" => $fname . ' ' . $lname];
                $this->curl->curlIntercom('users', $para_com, 'post');

                //insert intercom id into our db
                $get_com_user = $this->curl->curlIntercom('users?user_id=' . $insert_customer_info . '');
                $get_com_user = json_decode($get_com_user, true);
                $db_parameter['intercom_id'] = $get_com_user['id'];
                $this->user_modal->update_customer_info($db_parameter, $insert_customer_info);

                // saasquatch
                $random = rand(0, 99);
                $random = ($random <= 9 ? "0" . $random : $random);

                $para_saas = [
                    "id" => $insert_customer_info,
                    "accountId" => $insert_customer_info,
                    "firstName" => $fname,
                    "lastName" => $lname,
                    "email" => $email,
                    "referable" => "true",
                    "referralCode" => $fname . $lname . $random,    // forename . surname . random number between 00 and 99
                    "locale" => "en_UK"
                ];

                $saas_response = $this->curl->curlSaas('open/account/' . $insert_customer_info . '/user/' . $insert_customer_info . '', $para_saas, 'post');
                $new_saas_data = $this->parseHttpResponse($saas_response);

                if (isset($new_saas_data['body']['shareLinks']['shareLink'])) {

                    $ese_share_link = 'http://eversmart.family/' . trim($fname . $lname) . ($random <= 9 ? "0" . $random : $random);

                    // Tie nasty saas link to nice ESE link
                    $saas_parameter = [
                        'customer_id' => $insert_customer_info,
                        'ese_generated_link' => $ese_share_link,
                        'saas_generated_link' => $new_saas_data['body']['shareLinks']['shareLink']
                    ];

                    $this->saas_model->insert_ese_share_link($saas_parameter);
                }

                registration_sms_alert($form_detail['phone1'], isset($ese_share_link) ? $ese_share_link : '');

                // If the customer was successfully created in junifer - get the account id and tie it to the customer record
                $get_account_id = $this->curl->junifer_request('/rest/v1/accounts/?email=' . $email);
                $get_account = $this->parseHttpResponse($get_account_id);

                if (strtolower($get_account['headers']['http_code']) == 'http/1.1 200 ok') {

                    $first_account = count($get_account['body']['results']) > 0 ? $get_account['body']['results'][0]['id'] : '';

                    if ($first_account > 0) {
                        $this->user_modal->update_account_id($insert_customer_info, $first_account);
                    }
                } else {
                    $this->error_modal->log('admin signup error', $get_account['body']['errorDescription']);
                }

                echo json_encode(['error' => '0', 'token' => null,
                    'junifer_id' => isset($junifer_response['body']['customerId']) ? $junifer_response['body']['customerId'] : 0,
                    'elec_product_code' => (isset($db_parameter['electricityProduct_productCode']) ? $db_parameter['electricityProduct_productCode'] : ''),
                    'gas_product_code' => (isset($db_parameter['gasProduct_productCode']) ? $db_parameter['gasProduct_productCode'] : ''),
                    'frequency' => ($monthyear === 'yearlypay' ? 'Yearly' : 'Monthly'),
                    'msg' => 'Sign-up successful',
                    'type' => 'junifer',
                    'ese_share_link' => (isset($ese_share_link) ? $ese_share_link : '')
                ]);
                exit;
            }
        }

        if ($form_detail['billingMethod'] == 'prepay') {
            $customer_data = ['forename' => $fname, 'signup_type' => '3'];

            $insert_customer_info = $this->user_modal->insert_customer_info($customer_data);

            if (empty($insert_customer_info) || $insert_customer_info == 0) {
                echo json_encode(['error' => '1', 'msg' => 'Server Error', 'type' => 'dyball']);
                exit;
            }

            $dplo = $form_detail['address_address2'] == NULL ? 'Area' : $form_detail['address_address2'];

            $db_parameter = [
                'title' => $parameter['contacts'][0]['title'],
                'forename' => $parameter['contacts'][0]['forename'],
                'surname' => $parameter['contacts'][0]['surname'],
                'billingMethod' => $form_detail['billingMethod'],
                'email' => $parameter['contacts'][0]['email'],
                'phone1' => $parameter['contacts'][0]['phone1'],
                'phone2' => $parameter['contacts'][0]['phone2'],
                'dateOfBirth' => $dob,
                'address_careOf' => $parameter['contacts'][0]['address']['careOf'],
                'first_line_address' => $parameter['contacts'][0]['address']['address1'],
                'dplo_address' => $dplo,
                'town_address' => $parameter['contacts'][0]['address']['town'],
                'city_address' => $parameter['contacts'][0]['address']['county'],
                'address_postcode' => $parameter['contacts'][0]['address']['postcode'],
                'address_countryCode' => $parameter['contacts'][0]['address']['countryCode'],
                'supplyAddress_careOf' => $parameter['supplyAddress']['careOf'],
                'supplyAddress_address1' => $parameter['supplyAddress']['address1'],
                'supplyAddress_town_address' => $parameter['supplyAddress']['town'],
                'supplyAddress_city_address' => $parameter['supplyAddress']['county'],
                'supplyAddress_postcode' => $parameter['supplyAddress']['postcode'],
                'supplyAddress_countryCode' => $parameter['supplyAddress']['countryCode'],
                "signup_type" => '3',
                'prefix' => 'DYB_DUAL_',
                'tariff_newspend' => $form_detail['newspend'],
                'tariff_tariffName' => $form_detail['tariff'],
                'added_by_admin' => $form_detail['created_by'],
                'elec_mpan_core' => $form_detail['mpancore'],
                'duosid' => $ElecDistId,
                'exitFeeElec' => '0',
                'quotation_eac' => $form_detail['eac']
            ];

            // intercom
            $para_com = [
                "user_id" => $insert_customer_info,
                "email" => $email,
                "name" => $fname . ' ' . $lname
            ];

            //create intercom user
            $this->curl->curlIntercom('users', $para_com, 'post');

            //get user
            $get_com_user = $this->curl->curlIntercom('users?user_id=' . $insert_customer_info . '');
            $get_com_user = json_decode($get_com_user, true);
            $db_parameter["intercom_id"] = $get_com_user['id'];

            if ($form_detail['quotefor'] == 'both') {
                $db_parameter['tariff_unitRate1Elec'] = $form_detail['elec_ur'];
                $db_parameter['tariff_standingCharge1Elec'] = $form_detail['elec_sc'];
                $db_parameter['electricityProduct_previousSupplier'] = isset($form_detail['elecPrevSupplier']) && $form_detail['elecPrevSupplier'] !== '' ? $form_detail['elecPrevSupplier'] : 'British Gas';
                $db_parameter['electricityProduct_previousTariff'] = $form_detail['elecPrevTariff'];
                $db_parameter['electricityProduct_productCode'] = $TariffCode . '-E';

                $db_parameter['tariff_unitRate1Gas'] = $form_detail['gas_ur'];
                $db_parameter['tariff_standingCharge1Gas'] = $form_detail['gas_sc'];
                $db_parameter['gasProduct_productCode'] = $TariffCode . '-G';
                $db_parameter['elecProduct_reference'] = $this->user_modal->latest_product_ref('elec_reference_count');
                $db_parameter['gasProduct_reference'] = $this->user_modal->latest_product_ref('gas_reference_count');
                $db_parameter['gasProduct_mprns'] = $form_detail['mprns'];
                $db_parameter['quotation_aq'] = $form_detail['aq'];
                $db_parameter['elec_meter_serial'] = $form_detail['elecMeterSerial'];
                $db_parameter['gas_meter_serial'] = $form_detail['gasMeterSerial'];

                $dyb_parameter = [
                    'AuthKey' => $this->authkey_dyball_salemade,
                    'CustomerReference' => "DYB_" . $insert_customer_info,
                    'SaleMadeBy' => "Eversmart Website",
                    'SaleMadeOn' => date('Y-m-d', time()),
                    'SaleType' => "CoS", // only CoS currently supported
                    'CustomerTitle' => $form_detail['title'],
                    'CustomerFirstName' => $parameter['contacts'][0]['forename'],
                    'CustomerLastName' => $parameter['contacts'][0]['surname'],
                    'CustomerDateOfBirth' => $dob,
                    'CustomerAddressLine1' => $parameter['contacts'][0]['address']['address1'],
                    'CustomerPostcode' => $parameter['contacts'][0]['address']['postcode'],
                    'CustomerPhoneNumber' => $parameter['contacts'][0]['phone1'],
                    'PaymentMethod' => "PrePay",
                    'ElectricityTariffName' => $form_detail['tariff'],
                    'GasTariffName' => $form_detail['tariff'],
                    'ElectricEAC' => $form_detail['eac'],
                    'GasEAC' => $form_detail['aq'],
                    'MPANs' => [[ // Leave this - API expects double array
                        "MPAN" => $form_detail['mpancore'],
                        "UseIndustryDataLookup" => true
                    ]],
                    'MPRNs' => [[ // Leave this - API expects double array
                        "MPRN" => $form_detail['mprns'],
                        "UseIndustryDataLookup" => true
                    ]]
                ];

                $createDyball = $this->curl->dyball_curl($dyb_parameter, 'salemade');
                $dual_response = $this->parseHttpResponse($createDyball);

                if (isset($dual_response['body']['Value']['Success']) && $dual_response['body']['Value']['Success'] == 1) {
                    //insert token in database
                    $token = md5(uniqid(rand(0, 9999), true));
                    $this->create_password_email($insert_customer_info, $token, $fname, $email);
                    $this->user_modal->token_in_database($token, $insert_customer_info);

                    //update data in our database
                    $update_customer_info = $this->user_modal->update_customer_info($db_parameter, $insert_customer_info);

                    registration_sms_alert($form_detail['phone1']);

                    if ($update_customer_info) {
                        echo json_encode(['error' => '0',
                            'dyball_id' => $insert_customer_info,
                            'elec_product_code' => $db_parameter['electricityProduct_productCode'],
                            'gas_product_code' => $db_parameter['gasProduct_productCode'],
                            'frequency' => 'Monthly',
                            'msg' => 'Success! Dyball enrollment is successful',
                            'type' => 'dyball']);
                        exit;
                    }
                } else {
                    $this->error_modal->log('Dyball register fail', $dual_response['body']['Value']['ErrorMessage']);

                    // delete redundant db record
                    $this->user_modal->delete_customer_info($insert_customer_info);

                    echo json_encode(['error' => '1', 'token' => null,
                        'msg' => $dual_response['body']['Value']['ErrorMessage'],
                        'type' => 'dyball'
                    ]);
                    exit;
                }
            }

            if ($form_detail['quotefor'] == 'electricity') {

                $db_parameter['tariff_unitRate1Elec'] = $form_detail['elec_ur'];
                $db_parameter['tariff_standingCharge1Elec'] = $form_detail['elec_sc'];
                $db_parameter['electricityProduct_previousSupplier'] = isset($form_detail['elecPrevSupplier']) && $form_detail['elecPrevSupplier'] !== '' ? $form_detail['elecPrevSupplier'] : 'British Gas';
                $db_parameter['electricityProduct_previousTariff'] = $form_detail['elecPrevTariff'];
                $db_parameter['elecProduct_reference'] = $this->user_modal->latest_product_ref('elec_reference_count');
                $db_parameter['electricityProduct_productCode'] = $TariffCode . '-E';

                $dyb_parameter = [
                    'AuthKey' => $this->authkey_dyball_salemade,
                    'CustomerReference' => "DYB_" . $insert_customer_info,
                    'SaleMadeBy' => "Eversmart Website",
                    'SaleMadeOn' => date('Y-m-d', time()),
                    'SaleType' => "CoS", // only CoS currently supported
                    'CustomerTitle' => $parameter['contacts'][0]['title'],
                    'CustomerFirstName' => $parameter['contacts'][0]['forename'],
                    'CustomerLastName' => $parameter['contacts'][0]['surname'],
                    'CustomerDateOfBirth' => $dob,
                    'CustomerAddressLine1' => $parameter['contacts'][0]['address']['address1'],
                    'CustomerPostcode' => $parameter['contacts'][0]['address']['postcode'],
                    'CustomerPhoneNumber' => $parameter['contacts'][0]['phone1'],
                    'PaymentMethod' => "PrePay",
                    'ElectricityTariffName' => $form_detail['tariff'],
                    'ElectricEAC' => $form_detail['eac'],
                    'MPANs' => [[ // Leave this - API expects double array
                        "MPAN" => $form_detail['mpancore'],
                        "UseIndustryDataLookup" => true
                    ]]
                ];

                $createDyball = $this->curl->dyball_curl($dyb_parameter, 'salemade');
                $elec_response = $this->parseHttpResponse($createDyball);

                if (isset($elec_response['body']['Value']['Success']) && $elec_response['body']['Value']['Success'] == 1) {
                    //insert token in database
                    $token = md5(uniqid(rand(0, 9999), true));
                    $this->create_password_email($insert_customer_info, $token, $fname, $email);
                    $this->user_modal->token_in_database($token, $insert_customer_info);

                    //update data in our database
                    $update_customer_info = $this->user_modal->update_customer_info($db_parameter, $insert_customer_info);

                    registration_sms_alert($form_detail['phone1']);

                    if ($update_customer_info) {
                        echo json_encode(['error' => '0',
                            'dyball_id' => $insert_customer_info,
                            'elec_product_code' => $db_parameter['electricityProduct_productCode'],
                            'frequency' => 'Monthly',
                            'msg' => 'Success! Dyball enrollment is successful',
                            'type' => 'dyball']);
                        exit;
                    }
                } else {
                    $this->error_modal->log('Dyball register fail', $elec_response['body']['Value']['ErrorMessage']);

                    // delete redundant db record
                    $this->user_modal->delete_customer_info($insert_customer_info);

                    echo json_encode(['error' => '1', 'token' => null,
                        'msg' => $elec_response['body']['Value']['ErrorMessage'],
                        'type' => 'dyball'
                    ]);
                    exit;
                }
            }
        }
    } // end enroll customer

    function newspendCalc($elec_standingcharge, $elec_unitrate, $gas_standingcharge, $gas_unitrate, $eac, $aq)
    {
        $EstimatedElec = ($elec_standingcharge * 365) + ($eac * $elec_unitrate);
        $EstimatedGas = ($gas_standingcharge * 365) + ($aq * $gas_unitrate);

        $newspend = number_format(($EstimatedElec + $EstimatedGas), 2);
        return $newspend;
    }

    function newspendResult()
    {
        $test = $this->newspendCalc(0.1771, 0.1412, 0.1399, 0.035, 1285, 7733);
        print_r($test);
    }

    function validate_bank_details()
    {
        $accountNumber = $this->input->post('bank-bnum');
        $sortCode = $this->input->post('bank-sort');

        $para = ['accountNumber' => $accountNumber, 'sortCode' => $sortCode];
        $validate = $this->curl->junifer_request('/rest/v1/directDebits/validateBankAccount', $para, 'post');
        $validateResponse = $this->parseHttpResponse($validate);

        $valid = isset($validateResponse['body']['isValid'])?$validateResponse['body']['isValid']:false;

        echo json_encode(['error' => '0', 'msg' => 'Account details verified']); exit;
        if ($valid == false) {
            echo json_encode(['error' => '1', 'msg' => 'Account details not valid']);

        } else {
            echo json_encode(['error' => '0', 'msg' => 'Account details verified']);
        }
    }

    function lead_log()
    {
        $data = $this->input->post();
        $this->mysql->insert('lead', $data);
        $id = $this->mysql->insert_id();

        if ($id > 0) {
            $status = [
                'lead_id' => $id,
                'lead_status_type_id' => '1'
            ];
            $this->update_lead($status);
        } else {
            echo json_encode(['error' => '1', 'msg' => 'Something went wrong']);
        }
    }

    function update_lead($status = null)
    {
        if ($status == null) {
            $status = json_decode(file_get_contents('php://input'), true);
        }

        // end date existing log if there is one
        $this->end_existing_lead_status($status['lead_id']);

        // add new status
        $this->mysql->insert('lead_status', $status);
        $id = $this->mysql->insert_id();

        if ($id > 0) {
            echo json_encode(['error' => '0', 'msg' => 'Lead status updated']);
        } else {
            echo json_encode(['error' => '1', 'msg' => 'Something went wrong']);
        }
    }

    function end_existing_lead_status($lead_id)
    {
        $data = ['end_date' => date('Y-m-d H:i:S', time())];
        $this->user_modal->update_lead_status($lead_id, $data);
    }

    function sales_report()
    {
        $sql = 'SELECT IFNULL(added_by_admin, \'WEB\') AS "Sales Rep", UPPER(billingMethod) AS "Pay Type", UPPER(tariff_tariffName) AS Tariff, tariff_newspend AS "Quote (yearly)", round(tariff_newspend/12,2) AS "Quote (monthly)", 
                UPPER(title) AS Title, UPPER(forename) AS Forname, UPPER(surname) AS Surname, LOWER(email) AS Email, phone1 AS Phone, first_line_address AS "Address 1", town_address AS "Address 2", 
                city_address AS "Address 3",  UPPER(address_postcode) AS Postcode, bankAccountSortCode AS "Sort Code", bankAccountNumber AS "Account Number",  create_at AS "Date Created" 
                FROM eversmartlivedbn.customer_info
                WHERE create_at >= current_date()-1 and create_at <= current_date()
                ORDER BY added_by_admin';
        $query = $this->mysql->query($sql);
        $result = $query->result_array();

        $recipients = array(
            "josh@eversmartenergy.co.uk",
            "bjc@eversmartenergy.co.uk",
            "steven.warren@eversmartenergy.co.uk",
            "dan.jones@eversmartenergy.co.uk",
            "david.lovejoy@eversmartenergy.co.uk"
        );

        $this->load->library('email');
        $token = md5(uniqid(rand(0, 9999), true));
        $email_data['email_info'] = ['token' => $token];
        $this->email->from('no-reply@eversmartenergy.co.uk', 'Eversmart');
        $this->email->to(implode(',', $recipients));
        $this->email->subject('Sales Report - ' . date('d-m-Y'));

        if ($result) {
            foreach ($result[0] as $key => $value) {
                $headings[] = $key;         // sets xls headers
            }

            $this->load->library('PHPExcel');
            $path = 'assets/temp_csv/';
            $filename = "Sales Report " . date('d-m-Y') . ".xls";
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getProperties()->setTitle("export")->setDescription("none");
            $objPHPExcel->setActiveSheetIndex(0);
            $objPHPExcel->getActiveSheet()->fromArray($headings, null, "A1");       // puts xls headers on top row
            $objPHPExcel->getActiveSheet()->fromArray($result, null, "A2");         // puts rest of results beneath
            $objPHPExcel->setActiveSheetIndex(0);
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

            // Sending headers to force the user to download the file
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename=' . $filename);
            header('Cache-Control: max-age=0');

            $objWriter->save($path . $filename);                                              // saves file in temp_csv folder

            $this->email->message('Please find attached the sales report from yesterday.' . "<br><br>" . 'This data is quite sensitive, please be mindful of what you do with it.');
            $this->email->attach($path . $filename);
            if ($this->email->send()) {
                if (file_exists($path . $filename)) {
                    if (unlink($path . $filename)) {
                        echo json_encode(['error' => '0', 'msg' => 'Email sent. File deleted.']);      // sends email and deletes csv from temp_csv folder
                    } else {
                        echo json_encode(['error' => '1', 'msg' => 'Could not delete file.']);
                    }
                } else {
                    echo json_encode(['error' => '1', 'msg' => 'File not found.']);
                }
            } else {
                echo json_encode(['error' => '1', 'msg' => 'Email not sent. Something went wrong.']);
            }
        } else {
            $this->email->message('No results in daily sales report.');
            if ($this->email->send()) {
                echo json_encode(['error' => '0', 'msg' => 'Email sent.']);
            } else {
                echo json_encode(['error' => '1', 'msg' => 'Email not sent. Something went wrong.']);
            }
        }
    }
}
