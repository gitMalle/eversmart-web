<?php

/**
 * Class Messages
 */
class Messages extends ESE_Controller
{
    /**
     * Messages constructor.
     */
    function __construct()
    {
        parent::__construct();
        $this->load->model('curl/curl_requests_junifer');
    }


    /**
     * Get information required to populate Messages screen
     *
     * @return string
     */
    function index()
    {
        // Response headers needs
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: X-Customer-Token");

        $data = $this->curl_requests_junifer->junifer_get_messages($this->account_id->junifer);

        $message_data = array();


        // loop through and unset redundant fields
        $i = 0;
        foreach($data['results'] as $key => $value) {

            $message_data[$i]['download_id'] = $value['files'][0]['id'];

            unset($value['files']);
            unset($value['links']);
            unset($value['testFl']);

            $message_data[$i] = array_merge($message_data[$i], $value);

            $i++;
        }

        if (!empty($message_data)) {
            echo json_encode(['success' => true, 'error' => false, 'data' => array_reverse($message_data), 'token' => $this->get_latest_token() ]);
        } else {
            echo json_encode(['success' => false, 'error' => 'No customer information found']);
        }
    }


    /**
     * Download specified message
     *
     * @param $msgid
     * @return mixed
     */
    function download_message($msgid)
    {
        // Response headers needs
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: X-Customer-Token ");
        header('Content-type: application/pdf');    // ensures that image of bill is displayed as a PDF

        // First we need to check that the message id is actually for this customer
        if($this->verify_message_download($msgid) == false) {
            echo json_encode(['success' => false, 'error' => 'No message found.']);
        }

        return $this->curl_requests_junifer->junifer_view_message($msgid);
    }


    /**
     * Check whether message belongs to this customer
     *
     * @param $msgid
     * @return boolean
     */
    function verify_message_download($msgid){

        $junifer_msgs = $this->curl_requests_junifer->junifer_get_messages($this->account_id->junifer);

        // If we cant find any messages for this customer send them to a 404
        if (empty($junifer_msgs['results'])) {
            return false;
        }

        // Set up an array of message ids found
        $CheckID = array();

        foreach($junifer_msgs['results'] as $key => $data) {

            foreach($data['files'] as $m_key => $m_data) {

                $CheckID[] = $m_data['id'];
            }
        }

        $CheckID = array_unique($CheckID);

        // If the message id is not in the customers messages, kick them out
        if(!in_array($msgid, $CheckID)){
            return false;
        }
        else {
            return true;
        }
    }

}
