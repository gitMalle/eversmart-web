<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Northwest extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->database();
		$this->load->helper('url');

		$this->load->library('grocery_CRUD');
		$this->load->library('session');
	}

	public function _example_output($output = null)
	{
		$this->load->view('meter_bookings_nw.php',(array)$output);
	} 

	public function offices() 
	{ 
		$output = $this->grocery_crud->render();

		$this->_example_output($output);
	}

	public function index()
	{
		$this->_example_output((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
	}

	public function sessionss()
	{
		$didgit = $this->input->get('pagemunber'); 
		$this->session->set_userdata('pagemunber', $didgit);
		echo json_encode(['sessions' => $didgit]);
	}

	public function meter_bookings_nw()
	{


			$crud = new grocery_CRUD();

			$crud->set_theme('twitter-bootstrap');

			$crud->set_table('addressbooknw'); 

			//$crud->where('booked_meter','2');
			//$crud->set_relation('booked_meter','booked_meter','booked_meter_words');
			$crud->columns('ACCOUNT_NUMBER','MPAN', 'MPRN', 'ELEC_START_DATE', 'CUSTOMER_NAME', 'CONTACT_ADDRESS_1','CONTACT_POST_CODE', 'CONTACT_TELEPHONE','EMAIL', 'Outcome','Agent', 'Notes', 'Date');
			$crud->display_as('ACCOUNT_NUMBER','ACCOUNT NUMBER')
				 ->display_as('ELEC_START_DATEr','ELEC START DATE')
				 ->display_as('CUSTOMER_NAME', 'CUSTOMER NAME')
				 ->display_as('CONTACT_ADDRESS_1','CONTACT ADDRESS 1')
				 ->display_as('CONTACT_POST_CODE', 'CONTACT POST CODE')
				 ->display_as('CONTACT_TELEPHONE', 'CONTACT TELEPHONE');
			$crud->set_subject('Meter Booking');
			$crud->unset_delete();
			$crud->unset_add();
			$crud->unset_print();

			$crud->order_by('ELEC_START_DATE','desc');
			$output = $crud->render();


			if ( empty($this->session->userdata('login_data'))) {

				redirect('/index.php/user/login/');
		
			} else {
			
				if (isset($savedsearch)) {

				$this->_example_output($output, $savedsearch);
				}

				else {$this->_example_output($output);}
			}
		
	}

}
