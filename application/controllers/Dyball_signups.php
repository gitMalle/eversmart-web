<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dyball_signups extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->database();
        $this->load->helper('url');

        $this->load->library('Grocery_CRUD');
        $this->load->library('session');
    }

    public function _example_output($output = null)
    {
        $admin_data['page_ref'] = 'dyball_pending';
        $admin_data['page_title'] = 'Dyball Pending Sign Ups';
        $admin_data['content'] = (array)$output;
        $this->load->view('layout/admin_template',$admin_data);
    }

    public function _csv_output($output = null)
    {
        $admin_data['page_ref'] = 'dyball_csv';
        $admin_data['page_title'] = 'Dyball CSV Import';
        $admin_data['content'] = (array)$output;
        $this->load->view('layout/admin_template',$admin_data);
    }


    public function offices()
    {
        $output = $this->grocery_crud->render();

        $this->_example_output($output);
    }

    public function index()
    {
        $this->_example_output((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
    }

    public function pending()
    {


        $crud = new grocery_CRUD();

        $crud->set_theme('twitter-bootstrap');

        $crud->set_table('customer_info');

        $crud->where('billingMethod','prepay');
        //	$crud->where('prefix','DYB_ELEC_');
        //	$crud->where('prefix','DYB_GAS_');
        //$crud->set_relation('booked_meter','booked_meter','booked_meter_words');
        $crud->columns('title','forename', 'surname', 'billingMethod', 'phone1','first_line_address', 'address_postcode','tariff_tariffName','elec_meter_serial', 'gas_meter_serial','Error_msg');
        //$crud->display_as('ACCOUNT_NUMBER','ACCOUNT NUMBER')
        // ->display_as('ELEC_START_DATEr','ELEC START DATE')


        $crud->set_subject('Pending ');
        $crud->unset_delete();
        $crud->unset_add();
        $crud->unset_print();

        //$crud->order_by('ELEC_START_DATE','desc');
        $output = $crud->render();


        if ( empty($this->session->userdata('login_data')) ) {

            redirect('/index.php/user/login/');

        }
        elseif( in_array($this->session->userdata('login_data')['admin_role_type_id'], array(1,2))  ){

            if (isset($savedsearch)) {
                $this->_example_output($output, $savedsearch);
            }
            else {
                $this->_example_output($output);
            }


        }
        else {
            redirect('/index.php/user/');
        }

    }

    public function csv_files()
    {

        $crud = new grocery_CRUD();
        $crud->set_theme('twitter-bootstrap');
        $crud->set_table('generated_file');
        $crud->columns('created_at', 'affected_rows' , 'filename' );
        $crud->where('generated_file_type_id','1'); // New Dyball Customer Import
        $crud->display_as('affected_rows','new accounts');
        $crud->set_subject('CSV');
        $crud->unset_delete();
        $crud->unset_add();
        $crud->unset_edit();
        $crud->unset_print();
        $crud->callback_column('filename',array($this,'_callback_webpage_url'));
        $output = $crud->render();
        


        if ( empty($this->session->userdata('login_data')) ) {

            redirect('/index.php/user/login/');

        }
        elseif( in_array($this->session->userdata('login_data')['admin_role_type_id'], array(1,2))  ){

            if (isset($savedsearch)) {
                $this->_csv_output($output, $savedsearch);
            }
            else {
                $this->_csv_output($output);
            }

        }
        else {
            redirect('/index.php/user/');
        }

    }

    public function _callback_webpage_url($value)
    {
        return "<a href='".base_url('assets/temp_csv'.$value)."'>Download Link</a>";
    }

}
