<?php

/**
 * Class My_account
 */
class My_account extends ESE_Controller
{
    /**
     * My_account constructor.
     */
    function __construct()
    {
        parent::__construct();
        $this->load->model('user_modal');
        $this->load->model('curl/curl_requests_junifer');
        $this->load->model('curl/curl_requests_redfish');
        $this->mysql = $this->load->database('mysql', true);
    }

    /**
     * Get information required to populate My Account screen
     *
     * @return string
     */
    function index()
    {
        // Response headers needs
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: X-Customer-Token");

        if(in_array('junifer',$this->customer_type) ) {
            $data = $this->get_junifer_customer_data();
        }
        else if(in_array('dyball',$this->customer_type) ) {
            $data = $this->get_dyball_customer_data();
        }
        else if(in_array('redfish',$this->customer_type) ) {
            $data = $this->get_redfish_customer_data();
        }


        // Check its not an empty array
        if (!empty($data)) {
            // return success and data
            echo json_encode(['success' => true, 'error' => false, 'data' => $data, 'token' => $this->get_latest_token() ]);
        } else {
            // Otherwise, return fail
            echo json_encode(['success' => false, 'error' => 'No customer information found']);
        }

    }

    /**
     * Get Junifer customer data
     *
     * @param null
     * @return array
     */
    function get_junifer_customer_data()
    {
        // Get the required information for my account page
        $customer_data = $this->user_modal->get_basic_customer_info($this->account_id->junifer);
        $Balance = $this->curl_requests_junifer->junifer_get_balance($this->account_id->junifer, 'array');
        $MyDetails = $this->curl_requests_junifer->junifer_customer_data($customer_data['customer_id'], 'array');
        $Address = $this->curl_requests_junifer->junifer_get_billing_address($this->account_id->junifer, 'array');
        $Tariff = $this->user_modal->get_customer_tariff_info($this->account_id->junifer);

        $MyDetails['id'] = $customer_data['id'];
        $MyDetails['account_id'] = $this->account_id->junifer;
        $MyDetails['forename'] == '' ? $MyDetails['forename'] = $customer_data['forename'] : '';
        $MyDetails['surname'] == '' ? $MyDetails['surname'] = $customer_data['surname'] : '';
        $MyDetails['email'] == '' ? $MyDetails['email'] = $customer_data['email'] : '';
        $MyDetails['phone1'] == '' ? $MyDetails['phone1'] = $customer_data['phone1'] : '';


        if($Tariff['billingMethod'] == 'directdebit'){
            $Tariff['billingMethod'] = 'Direct Debit';
        }
        else if ($Tariff['billingMethod'] == 'prepay') {
            $Tariff['billingMethod'] = 'Prepay';
        }

        // Merge the 4 arrays into 1
        return array_merge(array_merge(array_merge($MyDetails, $Address), $Tariff), $Balance);
    }

    /**
     * Get Dyabll customer data
     *
     * @param null
     * @return array
     */
    function get_dyball_customer_data()
    {
        $DyballCustomer = $this->user_modal->get_basic_customer_info(NULL, $this->account_id->dyball);

        // Don't need these so don't return them in response
        unset($DyballCustomer['id']);           unset($DyballCustomer['customer_id']);
        unset($DyballCustomer['dyball_account_id']);
        unset($DyballCustomer['intercom_id']);  unset($DyballCustomer['admin_role_type_id']);
        unset($DyballCustomer['title']);        unset($DyballCustomer['billingMethod']);

        $DyballTariff = $this->user_modal->get_customer_tariff_info(NULL, $this->account_id->dyball);

        return array_merge($DyballCustomer, $DyballTariff);
    }

    /**
     * Get Redfish customer data and refresh token
     *
     * @param null
     * @return array
     */
    function get_redfish_customer_data()
    {

        // Make the API call
        $account = $this->curl_requests_redfish->redfish_my_account($this->account_id->redfish);
        $response = $this->parseHttpResponse($account);

        $data = array();

        // If the call was success full
        if($response['body']['Message'] == 'SUCCESS') {

            // If we have a token returned in the header
            if(isset($response['headers']['Set-Cookie'])) {

                // Turn header into something usable
                $cookie = $this->parseCookieHeader($response['headers']['Set-Cookie']);

                // Assign to the public variable
                $this->redfish_token = $cookie['account'];

                // Refresh JWT token with latest Redfish token
                $this->refresh_token();
            }


            // Now get the data we need to load my account page
            $response = $response['body']['Data'];

            $data['customer_id'] = $response['AccountNumber'];

            $name = explode(' ',$response['Name']);
            $data['title'] = $name[0];
            $data['forename'] = $name[1];
            $data['surname'] = $name[2];

            $data['billingMethod'] = $response['PaymentMethod']['Name'];
            $data['email'] = $response['Email'];
            $data['phone1'] = $response['Telephone'];

            $address = explode(',',$response['Address']);
            $data['first_line_address'] = $address[0];
            $data['address_postcode'] = $address[3];
            $data['town_address'] = $address[1] ;
            $data['city_address'] = $address[2];
            $data['balance'] = $response['Balance'];

        }

        return $data;
    }



    /**
     * Updates customer record in Junifer and database with posted data
     *
     * @param null
     * @return string
     */
    function update_account()
    {
        $data = json_decode( file_get_contents( 'php://input' ), true );

        $phone_number = $data['phonenum'];

        // Junifer data array
        $j_data = array();
        $phone_number != '' ? $j_data['phoneNumber1'] = $phone_number : '';

        // DB data array
        $db_data = array();
        $phone_number != '' ? $db_data['phone1'] = $phone_number : '';

        if (!empty($j_data) && !empty($db_data)) {
            // Update DB
            $this->user_modal->update_customer_info( $db_data, NULL, $this->account_id );

            // Update Junifer
            $Response = $this->curl_requests_junifer->junifer_update_customer_data( $this->account_id, $j_data, 'array' );
        }

        // Empty response means success, just make sure we posted something
        if (empty($Response) && !empty($j_data)) {
            // return success and data
            echo json_encode(['success' => true, 'error' => false]);
        } else {
            // Otherwise, return fail
            echo json_encode(['success' => false, 'error' => 'Customer information failed to update']);
        }
    }
}
