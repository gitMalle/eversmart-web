<?php
class Asperato extends ESE_Controller {

    private $payment_url, $payment_id, $payment_ref;

    function __construct()
    {
        parent::__construct();
        $this->load->library('asperato');
    }

    function get_iframe()
    {
        $this->asperato->authorization();
    }
}