<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vision extends ESE_Controller {

  function index(){
    // adding additional styles and scripts to the landing-page.php veiw
		$data['stylesheets'] = [""];

		$data['content'] = 'new-world/about/vision';
    $data['bodyClasses'] = $this->body_classes;
    $this->load->view($this->template, $data);
  }

}
