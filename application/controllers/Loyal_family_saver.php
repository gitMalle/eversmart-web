<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Loyal_family_saver extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->database();
        $this->load->helper('url');

        $this->load->library('Grocery_CRUD');
        $this->load->library('session');
    }

    public function _example_output($output = null)
    {
        $admin_data['page_ref'] = 'loyal_family_saver';
        $admin_data['page_title'] = 'Loyal Family Saver - Call list';
        $admin_data['content'] = (array)$output;
        $this->load->view('layout/admin_template',$admin_data);
    }

    public function offices()
    {
        $output = $this->grocery_crud->render();

        $this->_example_output($output);
    }

    public function index()
    {
        $this->_example_output((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
    }

    public function call_list()
    {


        $crud = new grocery_CRUD();
        $crud->set_theme('twitter-bootstrap');
        $crud->set_table('tariff_change_notification');
        $crud->where('outcome_type_id','NULL');
        $crud->columns('account_number', 'customer_name', 'mpan', 'elec_status', 'elec_start_date', 'elec_end_date', 'meterid', 'meter_type', 'mprn', 'gas_status', 'gas_start_date', 'gas_end_date', 'meter_serial_number', 'meter_mechanism', 'email', 'telephone', 'contact_name', 'contact_telephone', 'contact_mobile', 'contact_address_1', 'contact_address_2', 'contact_address_3', 'contact_address_4', 'contact_address_5', 'contact_post_code', 'e_address_line1', 'e_address_line2', 'e_address_line3', 'e_address_line4', 'e_address_line5', 'e_address_line6', 'e_address_line7', 'e_address_line8', 'e_address_line9', 'building_number', 'sub_building', 'building_name', 'principle_street', 'dep_locality', 'posttown', 'postcode', 'gas_tariff_name', 'electricity_tariff_name', 'letter', 'mpan_gsp', 'mpan_duos', 'mpan_pc', 'rate_type', 'gsp_from_postcode', 'duos_from_postcode', 'duostouse', 'has_elec', 'has_gas', 'current_elec_tariff_lookup', 'current_gas_tariff_lookup', 'current_elec_sc', 'current_elec_dur', 'current_elec_nur', 'current_gas_sc', 'current_gas_ur', 'new_elec_tariff', 'new_gas_tariff', 'new_electricity_tariff_lookup', 'new_gas_tariff_lookup', 'new_elec_sc', 'new_elec_dur', 'new_elec_nr', 'new_gas_sc', 'new_gas_ur', 'elec_on_supply', 'gas_on_supply', 'on_supply', 'elec_product_type', 'gas_product_type', 'elec_payment_type', 'gas_payment_type', 'elec_fix_end', 'gas_fix_end', 'elec_fix_ended', 'gas_fix_ended', 'elec_to_include', 'gas_to_include', 'price_rise', 'has_email', 'valid_email', 'dyball_eac', 'dyball_aq', 'missing_eac', 'missing_aq', 'elec_cost_lookup', 'gas_cost_lookup', 'elec_cost_sc', 'elec_cost_ur', 'gas_cost_sc', 'gas_cost_ur', 'df_cost', 'cost_error', 'old_revenue', 'new_revenue', 'old_gm', 'old_gm_percentage', 'new_gm', 'new_gm_percentage', 'omit_for_result', 'redfish_aq', 'redfish_eac', 'missing_redfish_aq', 'missing_redfish_eac', 'aq_difference', 'eac_difference', 'outcome_type_id');

        $crud->set_subject('Customer Record ');
        $crud->unset_delete();
        $crud->unset_add();
        $crud->unset_print();
        $crud->order_by('account_number','ASC');
        $output = $crud->render();


        if ( empty($this->session->userdata('login_data')) ) {

            redirect('/index.php/user/login/');

        }
        elseif( in_array($this->session->userdata('login_data')['admin_role_type_id'], array(1,2))  ){

            if (isset($savedsearch)) {
                $this->_example_output($output, $savedsearch);
            }
            else {
                $this->_example_output($output);
            }


        }
        else {
            redirect('/index.php/user/');
        }

    }

}
