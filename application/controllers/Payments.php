<?php

/**
 * Class Payments
 */
class Payments extends ESE_Controller
{
    /**
     * Payments constructor.
     */
    function __construct()
    {
        parent::__construct();
        $this->load->model('curl/curl_requests_junifer');
        $this->load->model('curl/curl_requests_dyball');
        $this->load->model('user_modal');
        $this->load->model('curl/curl_requests_sagepay');
    }

    /**
     * Retrieves all customer payments from Junifer
     *
     * @return string
     */
    function index()
    {
        // Response headers needs
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: X-Customer-Token");

        $data = array();

        if(in_array('junifer',$this->customer_type) ) {
            $data = $this->junifer_payments($this->account_id->junifer);
        }

        if(in_array('dyball',$this->customer_type) ) {
            $data = $this->dyball_payments($this->account_id->dyball);
        }

        // Check its not an empty array
        if ( !empty($data) && !isset($data['ErrorMessage']) ) {
            // return success and data
            echo json_encode(['success' => true, 'error' => false, 'data' => $data, 'token' => $this->get_latest_token() ]);
        } else {
            // Otherwise, return fail
            echo json_encode(['success' => false, 'error' => $data['ErrorMessage']]);
        }
    }


    /**
     * Retrieves all customer payments from Junifer
     *
     * @return array
     */
    function junifer_payments()
    {

        $junifer_results = $this->curl_requests_junifer->junifer_get_payments($this->account_id->junifer);

        // Just get the data we need
        $junifer_payments = array();

        $i = 0;
        foreach ($junifer_results['results'] as $data) {

            $date = explode('T', $data["createdDttm"]);
            $date = $date[0];

            $junifer_payments[$i] = [
                'amount' => $data["amount"],
                'payment_type' => str_replace('Go Cardless ', '', $data["paymentMethodType"]),
                'timestamp' => strtotime($date),
                'insert_date' => $date
            ];
            $i++;
        }

        $junifer_payments = $this->curl_requests_junifer->junifer_payment_details($this->account_id->junifer);
        $balance = $this->curl_requests_junifer->junifer_get_balance($this->account_id->junifer, 'array');
        $payment = $this->user_modal->get_payment_details($this->account_id->junifer);
        $payment['next_bill_date'] = isset($junifer_payments['results']) && !empty($junifer_payments['results'])?$junifer_payments['results'][0]['nextPaymentDt']:'';

        // Go check DB for any additional payment - if they exist merge them with the Junifer ones
        $payment_log = $this->user_modal->get_customer_payment($this->account_id->junifer);

        $merged_payments = array_merge($junifer_payments, $payment_log);

        $ready_for_merging['payments'] = $merged_payments;

        // Sort by timestamp field - descending
        function cmp($a, $b)
        {
            return strcmp($b["timestamp"], $a["timestamp"]);
        }

        usort($ready_for_merging['payments'], "cmp");

        unset($balance['date']);

        return array_merge(array_merge($balance, $ready_for_merging), $payment);

    }


    /**
     * Retrieves all customer payments from Dyball
     *
     * @return string
     */
    function dyball_payments()
    {
        $dyb_data =[
            "AccountNumber" => $this->account_id->dyball,
            "AuthKey"=> "1234",
            "StartDate"=> date('Y-m-d',strtotime("-6 month")),
            "EndDate"=> date('Y-m-d')
        ];

        return $this->curl_requests_dyball->dyball_payments($dyb_data);
    }


    function create_payment()
    {

        // Response headers needs
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: X-Customer-Token");

        $data = json_decode( file_get_contents( 'php://input' ), true );

        // Get Merchant session key
        $msk = $this->curl_requests_sagepay->sagepay_generate_msk();

        // Get card identifier
        $body = [
            'forename' => $data['forename'],
            'surname' => $data['surname'],
            'card_num' => $data['card_num'],
            'card_exp' => $data['card_exp'],
            'card_cvc' => $data['card_cvc'],
        ];
        $ci = $this->curl_requests_sagepay->sagepay_generate_ci($body, $msk);

        // Process transaction
        $body += [
            'msk' => $msk,
            'ci' => $ci,
            'id' => $this->account_id->junifer,
            'first_line_address' => $data['first_line_address'],
            'town_address' => $data['town_address'],
            'address_postcode' => $data['address_postcode'],
            'amount' => $data['amount'],
        ];
        $response = $this->curl_requests_sagepay->sagepay_process_transaction($body);

        if( $response && $response['statusCode'] == '2007' ) {

            // Update junifer balance
            $this->curl_requests_junifer->junifer_adjust_balance($this->account_id->junifer, 'debit', $data['amount']/100);

            echo json_encode(['success' => true, 'error' => false, 'transaction_id' => $response['transactionId']]);
        }
        else {
            echo json_encode(['success' => false, 'error' => 'Payment failed.']);
        }
    }
}
