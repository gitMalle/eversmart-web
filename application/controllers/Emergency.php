<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Emergency extends ESE_Controller  {


    function index()
    {
		$data['stylesheets'] = ["assets/css/new-world/help/emergency-page.css"];
		// $data['scripts'] = [];
		$data['content'] = 'new-world/help/emergency-page';
        $this->load->view( 'new-world/master', $data );
    }
}