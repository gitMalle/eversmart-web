<?php

/**
 * Class Home_services_model
 */
class Home_services_model extends CI_Model
{
    /**
     * Home_services_model constructor.
     */
    function __construct()
    {
        parent::__construct();
        $this->mysql_hs = $this->load->database('mysql_hs',true);
    }

    /**
     * Returns id and names of specified hs table
     * Options are: 'house', 'fuel', 'bedroom_number', 'heating_system', 'boiler', 'boiler_manufacturer', 'boiler_age', 'ownership', 'payment_schedule', 'preferred_contact', 'property'
     *
     * @param $type
     * @return mixed
     */
    function get_data_type($type)
    {
        return $this->mysql_hs->select($type.'_id, name')->from($type)->where('active', '1')->get()->result_array();
    }

    /**
     * Inserts passed data into customer table of eversmart_hs_staging
     * Returns customer_id
     *
     * @return mixed
     */
    function insert_customer($data)
    {
        $data = json_decode($data);
        $this->mysql_hs->insert('customer',$data);
        return $this->mysql_hs->insert_id();
    }

    /**
     * Inserts passed data into billing_address table of eversmart_hs_staging
     */
    function insert_billing_address($data)
    {
        $data = json_decode($data);
        $this->mysql_hs->insert('billing_address',$data);
    }

    /**
     * Inserts passed data into risk_address table of eversmart_hs_staging
     */
    function insert_risk_address($data)
    {
        $data = json_decode($data);
        $this->mysql_hs->insert('risk_address',$data);
    }

    /**
     * Inserts request body into web_to_lead_log table of eversmart_hs_staging
     */
    function web_to_lead_log()
    {
        $request_body = file_get_contents('php://input');
        $data = json_decode($request_body);
        $this->mysql_hs->insert('web_to_lead_log',$data);
    }
}