<?php

Class Salesforce_model extends CI_Model
{

    private $api_url,
            $client_id = '3MVG9HxRZv05HarQf.QLNZawpjbhFbOr97svenEyAmPvT5KkuzbJtM4VE0G99XxSPGMVGKSFY80KsKlLipXKY',
            $client_secret = '9927645EDBB7FE434C5C6584EA2B49703FFB6AABE21B7B2A926D741507EBF3CE',
            $username = 'apiuser@eversmartenergy.co.uk',
            $password = '£v£rsm@rt1!';
    public  $api_version = 'v1.0', $access_token;

    function __construct()
    {
        parent::__construct();
        $this->api_url = 'https://eversmartenergy.my.salesforce.com/services/apexrest/' . $this->api_version . '/';
        $this->get_oauth_token();
    }

    /**
    * Create an new Asperato Authorization in Salesforce
    *
    * @return String
    */
    function create_auth()
    {
        $data = $this->request($this->api_url. '/payments/createauth', 'POST');
        echo json_decode($data);
    }

    /**
    * Make a CURL Request to a URI & handle the exception
    *
    * @param $uri
    * @param $method
    * @return String
    */
    function request($uri, $method)
    {
        $headers[] = 'Accept: application/json';
        $headers[] = 'Content-Length: 0';
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Authorization: Bearer ' . $this->access_token;

        try {
            $handle = curl_init();
            if ($method === 'GET') {
                curl_setopt_array($handle,
                    [
                        CURLOPT_URL => $uri,
                        CURLOPT_POST => 0,
                        CURLOPT_HTTPHEADER => $headers,
                        CURLOPT_RETURNTRANSFER => 1
                    ]
                );
            } else {
                curl_setopt_array($handle,
                    [
                        CURLOPT_URL => $uri,
                        CURLOPT_POST => 1,
                        CURLOPT_HTTPHEADER => $headers,
                        CURLOPT_RETURNTRANSFER => 1
                    ]
                );
            }
            $data = curl_exec($handle);
        } catch (Exception $exception) {
            log_message('error', $exception);
        }
        return json_encode($data);
    }

    /**
    * Get the OAuth Token & refresh per new request to the Salesforce API
    *
    * @returns $token (string)
    */
    function get_oauth_token()
    {
        $token_url = 'https://login.salesforce.com/services/oauth2/token?grant_type=password&client_id=' .
            $this->client_id . '&client_secret='.
            $this->client_secret . '&username=' .
            $this->username . '&password=' .
            $this->password;
        $token = $this->refresh_oauth_token($token_url);
        return $token;
    }

    /**
    * Refresh the Access Token for Salesforce API
    *
    * @param $request
    * @return Boolean (successful run)
    */
    function refresh_oauth_token($request)
    {
        $handle = curl_init();
        curl_setopt_array($handle,
            array(
                CURLOPT_URL => $request,
                CURLOPT_POST => 1,
                CURLOPT_RETURNTRANSFER => 1
            )
        );
        $data = curl_exec($handle);
        if ($data) {
            $decoded = json_decode($data, true);
            $this->access_token = isset($decoded['access_token']) ? $decoded['access_token'] : '';
            return true;
        }
        return false;
    }

    /**
    * Get a list of insurance products from Salesforce
    *
    * @return Boolean
    */
    function get_insurance_products()
    {
       return $this->request($this->api_url . 'products/families/Insurance+Based+Products', 'GET');
    }

    /**
    * Get a IPID Data from Product
    *
    * @param $id
    * @return Boolean
    */
    function get_ipid_data($id)
    {
        if (isset($_GET)){
            return $this->request($this->api_url . 'products/byid/'.strtok($id, '?').'/ipiddata', 'GET');
        } else {
            return $this->request($this->api_url . 'products/byid/'.$id.'/ipiddata', 'GET');
        }
    }

    /**
    * Get a specific SF product by ID or Code
    *
    * @param $id
    * @param $code
    * @return Boolean
    */
    function get_product($id = '', $code = '')
    {
        if ($id) {
            $request = $this->request($this->api_url . '/products/byid/' . $id, 'GET');
        } else {
            $request = $this->request($this->api_url . '/products/' . $code, 'GET');
        }
        $encoded = json_encode($request);
        return $encoded;
    }
}