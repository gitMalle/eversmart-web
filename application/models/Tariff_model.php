<?php

class Tariff_model extends CI_Model
{
    private $str_table = 'tariff';

    public function __construct()
    {
        parent::__construct();
        $this->load->database('mysql', true);
    }

    /**
     * Create tariff to insert into the database
     *
     * @return $this
     * @param array $data
     */
    public function create($data)
    {
        if (isset($data)) {
            $this->db->insert('tariff', $data);
            return 'Tariff created successfully';

        } else {
            return 'Data cannot be empty';
        }
    }

    /**
     * Get all tariffs from the tariff table in the DB
     *
     * @return array
     */
    public function all()
    {
        $query = $this->db->select('*')
            ->from($this->str_table)
            ->order_by('tariff_id', 'ASC')
            ->get()->result_array();
        echo json_encode($query, true);
    }


    /**
     * Get Tariff from the DB
     *
     * @param $id
     * @return mixed
     */
    public function get($id)
    {
        $query = $this->db->select('*')
            ->from($this->str_table)
            ->where('tariff_id', $id)
            ->get()->row();
        echo json_encode($query, true);
    }

    /**
     *
     */
    public function get_visible()
    {
        $query = $this->db->select('tariff_name')
            ->from($this->str_table)
            ->where('is_visible', 1)
            ->get()->result_array();
        return json_encode($query, true);
    }

    /**
     * Get all tariffs from the tariff table in the DB
     *
     * @param $id (default: null)
     * @return boolean
     */
    public function get_type($id = null)
    {
        if ($id) {
            $query = $this->mysql->select('tariff_type')->from($this->str_table)
                ->where('id', $id)
                ->get()->result();
            echo $query;
        }
        return false;
    }

    /**
     * Check if Tariff should be visible on the front facing website
     *
     * @param $id (integer)
     * @return boolean
     */
    public function is_visible($id = null)
    {
        $query = $this->mysql->select('is_visible')->from($this->str_table)
            ->where('is_visible', '1')
            ->where('tariff_id', $id)
            ->get()->result();
        return $query ? true : false;
    }


    /**
     * Update Tariff record in DB
     *
     * @param string $id
     * @param array $data
     * @return boolean
     */
    public function update($id, $data)
    {
        if (isset($id) && !empty($data)) {
            $this->db->set([
                'tariff_name' => $data['tariff_name'],
                'tariff_code' => $data['tariff_code'],
                'tariff_type' => $data['tariff_type'],
                'is_visible' => $data['is_visible']
            ]);
            $this->db->where('tariff_id', $id);
            $this->db->update('tariff',$data);

            return 'Successfully updated tariff with ID of ' . $id;
        } else {
            return 'You need to specify the tariff you wish to update or provide correct tariff data';
        }
    }

}
