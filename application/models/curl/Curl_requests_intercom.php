<?php

require APPPATH.'/models/curl/Curl_model_intercom.php';

/**
 * Class Curl_requests_intercom
 */
Class Curl_requests_intercom extends Curl_model_intercom {

    /**
     * Curl_requests_intercom constructor.
     */
    function __construct()
    {
        parent::__construct();
    }

    /**
     * creates intercom user and returns the intercom id
     *
     * @param $id
     * @param $email
     * @param $name
     * @param string $return_type
     * @return array|string
     */
    function intercom_create_user($id, $email, $name, $return_type = 'json')
    {
        $data = json_encode(['user_id' => $id, 'name' => $name, 'email' => $email]);

        $intercom = $this->intercom_curl_request('POST', 'https://api.intercom.io/users', $data);
        $intercom = json_decode($intercom, true);

        $IntercomID = isset($intercom['id']) ? $intercom['id'] : '';

        if($return_type == 'array'){
            return $IntercomID;
        }
        else {
            echo json_encode(['error' => false, 'data' => $IntercomID]);
        }
    }
}