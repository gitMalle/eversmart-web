<?php

require_once APPPATH.'/models/curl/Curl_model.php';

/**
 * Class Curl_model_ecoes
 */
Class Curl_model_ecoes extends Curl_model {

    /**
     * Curl_model_ecoes constructor.
     */
    function __construct()
    {
        parent::__construct();
    }

    /**
     * Defines base Ecoes curl request
     *
     * @param $method
     * @param $url
     * @param $param
     * @param $data
     * @param string $type
     * @return bool|string
     */
    function ecoes_curl_request($method, $url, $param, $data, $type = 'json')
    {
        $auth = file(APPPATH.'/config/credentials/Ecoes.txt');
        
        $p = [];
        foreach($data as $key => $v){
            $p[] = [
                "Key" => $key,
                "Value" => $v
            ];
        }

        $obj = [
            "Authentication" => [
                "Key" => $auth[0]
            ],
            "ParameterSets" => [[
                "Parameters" => $p
            ]]
        ];
        
        if ($type=='json') {
            $optional_headers = [
                'Content-Type: application/json',
                'Accept: application/json'
            ];
        }

        return $this->callAPI($method, $url.$param, json_encode($obj), [], $optional_headers);
    }
}