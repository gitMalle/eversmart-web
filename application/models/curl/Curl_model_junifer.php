<?php

require_once APPPATH.'/models/curl/Curl_model.php';

/**
 * Class Curl_model_junifer
 */
Class Curl_model_junifer extends Curl_model {

    /**
     * @var string
     */
    private $cred;

    /**
     * Curl_model_junifer constructor.
     */
    function __construct()
    {
        parent::__construct();
        if ($_SERVER['HTTP_HOST'] == 'localhost' || $_SERVER['HTTP_HOST'] == '35.177.2.247' || $_SERVER['HTTP_HOST'] == 'jwtstaging.eversmartenergy.co.uk' || $_SERVER['HTTP_HOST'] == 'staging.eversmartenergy.co.uk' ) {
            $this->cred = 'UAT';
        } else {
            $this->cred = 'LIVE';
        }
    }

    /**
     * Defines base Junifer curl request
     * 
     * @param $method
     * @param $url
     * @param $param
     * @param $data
     * @param string $type
     * @return bool|string
     */
    function junifer_curl_request($method, $url, $param, $data, $type = 'json')
    {
        $credentials = [];
        $names = file(APPPATH.'/config/credentials/JuniferCred'.$this->cred.'.txt');

        foreach ($names as $name) {
            $credentials[] = trim($name);
        }

        if ($type=='json') {
            $optional_headers = [ 'Api-Type: Website',
                'Content-Type: application/json',
                'Accept: application/json'];

        } else if ($type=='pdf') {
            $optional_headers = [ 'CURLOPT_ENCODING: ""',
                'CURLOPT_MAXREDIRS: 10',
                'CURLOPT_TIMEOUT: 30',
                'CURLOPT_HTTP_VERSION: CURL_HTTP_VERSION_1_1'];
        }

        return $this->callAPI($method, $credentials[2].$url.$param, $data, $credentials, $optional_headers);
    }

}