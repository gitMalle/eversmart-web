<?php

require APPPATH.'/models/curl/Curl_model_dyball.php';

/**
 * Class Curl_requests_dyball
 */
Class Curl_requests_dyball extends Curl_model_dyball
{
    
    
    /**
     * Curl_requests_dyball constructor.
     */
    function __construct()
    {
        parent::__construct();
        $this->load->model('user_modal');
    }

    
    /**
     * Creates new account in Dyball
     *
     * @param $title
     * @param $forename
     * @param $surname
     * @param $dob
     * @param $address1
     * @param $postcode
     * @param $phone
     * @param $eac
     * @param $mpan
     * @param string $fuel
     * @param string $aq
     * @param string $mprn
     */
    function dyball_create_account($title, $forename, $surname, $dob, $address1, $postcode, $phone, $eac, $mpan, $fuel = 'elec', $aq = '', $mprn = '')
    {
        $customer_data = ['forename' => $forename, 'signup_type' => '3'];

        $id = $this->user_modal->insert_customer_info($customer_data);

        if (empty($insert_customer_info) || $insert_customer_info == 0) {
            echo json_encode(['error' => '1', 'msg' => 'Server Error']);
            exit;
        }

        $format_dob = date('Y-m-d', strtotime($dob));

        $data = [
            'CustomerReference' => 'DYB_'.$id,
            'SaleMadeBy' => 'Eversmart Website',
            'SaleMadeOn' => date('Y-m-d'),
            'SaleType' => 'CoS',
            'CustomerTitle' => $title,
            'CustomerFirstName' => $forename,
            'CustomerLastName' => $surname,
            'CustomerDateOfBirth' => $format_dob,
            'CustomerAddressLine1' => $address1,
            'CustomerPostcode' => $postcode,
            'CustomerPhoneNumber' => $phone,
            'PaymentMethod' => 'PrePay',
            'ElectricityTariffName' => 'Safeguard PAYG',
            'ElectricEAC' => $eac,
            'MPANs' => [
                [
                    'MPAN'=> $mpan,
                    'UseIndustryDataLookup'=> true
                ]
            ]
        ];

        if ($fuel == 'dual') {
            $data['GasEAC'] = $aq;
            $data['MPRNs'] = [
                [
                    'MPRN'=> $mprn,
                    'UseIndustryDataLookup'=> true
                ]
            ];
        }

        $this->dyball_curl_request('POST', '/customerquote/', 'salemade', $data);
    }

    
    /**
     * Dyball account lookup
     *
     * @param $data
     */
    function dyball_get_account($data)
    {
        $this->dyball_curl_request('POST', '/customerquote/', 'getquoteaccount', $data);
    }
    

    /**
     * Get dyball payments
     *
     * @param $id string
     * @param $data array
     *
     * @return array php
     */
    function dyball_payments($data)
    {
        
        $payments = $this->dyball_curl_request('POST', '/billing/GetCustomerElectricityPayments', NULL, $data);
        $payments_response = json_decode($payments,true);

        return $payments_response;
    }




}
