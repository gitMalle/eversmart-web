<?php
$this->set_css($this->default_theme_path.'/twitter-bootstrap/css/bootstrap.min.css');
$this->set_css($this->default_theme_path.'/twitter-bootstrap/css/bootstrap-responsive.min.css');
$this->set_css($this->default_theme_path.'/twitter-bootstrap/css/style.css');
$this->set_css($this->default_theme_path.'/twitter-bootstrap/css/jquery-ui/flick/jquery-ui-1.9.2.custom.css');

$this->set_js_lib($this->default_javascript_path.'/'.grocery_CRUD::JQUERY);

//	JAVASCRIPTS - JQUERY-UI
$this->set_js($this->default_theme_path.'/twitter-bootstrap/js/jquery-ui/jquery-ui-1.9.2.custom.js');

//	JAVASCRIPTS - JQUERY LAZY-LOAD
$this->set_js_lib($this->default_javascript_path.'/common/lazyload-min.js');

if (!$this->is_IE7()) {
	$this->set_js_lib($this->default_javascript_path.'/common/list.js');
} 
//	JAVASCRIPTS - TWITTER BOOTSTRAP
$this->set_js($this->default_theme_path.'/twitter-bootstrap/js/libs/bootstrap/bootstrap.min.js');
$this->set_js($this->default_theme_path.'/twitter-bootstrap/js/libs/bootstrap/application.js');
//	JAVASCRIPTS - MODERNIZR
$this->set_js($this->default_theme_path.'/twitter-bootstrap/js/libs/modernizr/modernizr-2.6.1.custom.js');
//	JAVASCRIPTS - TABLESORTER
$this->set_js($this->default_theme_path.'/twitter-bootstrap/js/libs/tablesorter/jquery.tablesorter.min.js');
//	JAVASCRIPTS - JQUERY-COOKIE
$this->set_js($this->default_theme_path.'/twitter-bootstrap/js/cookies.js');
//	JAVASCRIPTS - JQUERY-FORM
$this->set_js($this->default_theme_path.'/twitter-bootstrap/js/jquery.form.js');
//	JAVASCRIPTS - JQUERY-NUMERIC
$this->set_js($this->default_javascript_path.'/jquery_plugins/jquery.numeric.min.js');
//	JAVASCRIPTS - JQUERY-PRINT-ELEMENT
$this->set_js($this->default_theme_path.'/twitter-bootstrap/js/libs/print-element/jquery.printElement.min.js');
//	JAVASCRIPTS - JQUERY FANCYBOX
$this->set_js($this->default_javascript_path.'/jquery_plugins/jquery.fancybox-1.3.4.js');
//	JAVASCRIPTS - JQUERY EASING
$this->set_js($this->default_javascript_path.'/jquery_plugins/jquery.easing-1.3.pack.js');

//	JAVASCRIPTS - JQUERY UI
//$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/ui/'.grocery_CRUD::JQUERY_UI_JS);
//$this->load_js_jqueryui();

//	JAVASCRIPTS - twitter-bootstrap - CONFIGURAÇÕES
$this->set_js($this->default_theme_path.'/twitter-bootstrap/js/app/twitter-bootstrap-edit.js');
//	JAVASCRIPTS - JQUERY-FUNCTIONS
$this->set_js($this->default_theme_path.'/twitter-bootstrap/js/jquery.functions.js');
?>
<style>
.modal-header .close {
    padding: 0;
    margin: 0;
    position: absolute;
    left: 20px;
    top: 12px;
}
.modal.fade.in {
    top: 30%;
    height: 95%;
}
span.responsive-side-menu {
    display: none;
}
.modal-body {
    max-height: 400px;
    padding: 15px;
    overflow-y: auto;
    text-align: center;
    font-size: 20px;
    line-height: 1.4;
}
.modal-footer {
    display: block;
    display: block;
    display: block;
	text-align: center;
}
.modal-header {
    display: block;
    display: block;
    display: block;
}
.modal-header .close {
    padding: 0;
    margin: 0;
}
.modal-header h3 {
    margin: 0;
    line-height: 30px;
    text-align: center;
}


form#crudForm input {
    font-size: 17px;
    padding: 16px;
	width: 100%;
}  
.form-field-box {
    margin: 18px 0px;
}
.chosen-search {
    display: none;
}
input#save-and-go-back-button {
    background: #f44a7d!important;
    color: #fff!important;
    margin-bottom: 20px;
    margin-top: 20px;
}
.ui-state-hover, .ui-widget-content .ui-state-hover, .ui-widget-header .ui-state-hover, .ui-state-focus, .ui-widget-content .ui-state-focus, .ui-widget-header .ui-state-focus {
    border: 1px solid #f44a7d;
    background: #f44a7d;
    font-weight: bold;
    color: #ffffff!important;
}
.chosen-container {
    width: 100%!important;
}
input.btn.btn-large.btn-primary.submit-form {
    DISPLAY: NONE;
}
form#crudForm {
    max-width: 600px;
    margin: auto;
    float: none;
}
h2.span12 {
    text-align: center;
    max-width: 600px;
    float: none;
    margin: auto;
    margin-bottom: 20px;
}
#note{display:none!important;}
</style>
<div class="twitter-bootstrap crud-form">

	<h2 class="span12"><?php echo $this->l('form_edit'); ?> <?php echo $subject?></h2>

	<!-- CONTENT FOR ALERT MESSAGES -->
	<div id="message-box" class="span12"></div>

	<div id="main-table-box span12">
		<?php
		echo form_open( $update_url, 'method="post" id="crudForm" class="form-div span12"  enctype="multipart/form-data"');
			foreach($fields as $field){

                if($field->field_name == 'password'){
                    continue; // Skips password field - this should not be visible / editable
                }

                ?>
				<div class="form-field-box" id="<?php echo $field->field_name; ?>_field_box">
					<div class="form-display-as-box" id="<?php echo $field->field_name; ?>_display_as_box">
						<?php echo $input_fields[$field->field_name]->display_as?><?php echo ($input_fields[$field->field_name]->required)? '<span class="required">*</span>' : ""?> :
					</div>
					<div class="form-input-box" id="<?php echo $field->field_name; ?>_input_box">
						<?php echo $input_fields[$field->field_name]->input?>
					</div>
					<div class="clear"></div>
				</div>
				<?php
			}
			//	Hidden Elements
			if(!empty($hidden_fields)){
				foreach($hidden_fields as $hidden_field){
					echo $hidden_field->input;
				}
			}?>
			<input type="button" value="<?php echo $this->l('form_update_changes'); ?>" class="btn btn-large btn-primary submit-form"/>
			<?php 	if(!$this->unset_back_to_list) { ?>
				<input type="button" value="<?php echo $this->l('form_update_and_go_back'); ?>" id="save-and-go-back-button" class="btn btn-large btn-primary"/>
				<input type="button" value="<?php echo $this->l('form_cancel'); ?>" class="btn btn-large return-to-list" />
			<?php 	} ?>

			<div class="hide loading" id="ajax-loading"><?php echo $this->l('form_update_loading'); ?></div>

		<?php echo form_close(); ?>
	</div>
</div>
<script>
	var validation_url = "<?php echo $validation_url?>",
		list_url = "<?php echo $list_url?>",
		message_alert_edit_form = "<?php echo $this->l('alert_edit_form')?>",
		message_update_error = "<?php echo $this->l('update_error')?>";
</script>