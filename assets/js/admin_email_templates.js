if(window.location.hostname === 'localhost' || window.location.host === '52.56.247.49')
{
    var base_url = 'http://'+window.location.host+'/';
}
if( window.location.hostname === 'www.eversmartenergy.co.uk' || window.location.hostname === 'eversmartenergy.co.uk' || window.location.hostname === '52.56.76.183')
{
    var base_url = 'https://'+window.location.hostname+'/';
}

function logout_user()
{
    $.ajax({
        url: base_url +'index.php/user/logout',
        type: 'post',
        dataType: 'json',
        success: function (response) {
            if (response.error == '0') {
                window.location.href = base_url+'index.php/user/login';
            }
        }
    });
}

// validation - only numbers
function isNumberKey(evt)
{
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

    return true;
}

// allow only numbers and decimals
function numberDecimals(evt)
{
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode != 46 && charCode > 31
        && (charCode < 48 || charCode > 57))
        return false;

    return true;
}

$( document ).ready(function()
{
    $('#bill_num').hide();
    $('#new_add').hide();
    $('#post_code').hide();
    $('#phone_num').hide();
    $('#old_dd').hide();
    $('#new_dd').hide();

    $('#email_type').on( 'change', function(){
        var selected = $(this).find('option:selected').val();

        if (selected === 'preview_moving_home_email') {
            $('#new_add').show();
            $('#address_param').prop('required',true);
        } else {
            $('#new_add').hide();
            $('#address_param').prop('required',false);
        }

        if (selected === 'preview_direct_debit_confirmation') {
            $('#old_dd').show();
            $('#new_dd').show();
            $('#old_dd_param').prop('required',true);
            $('#new_dd_param').prop('required',true);
        } else {
            $('#old_dd').hide();
            $('#new_dd').hide();
            $('#old_dd_param').prop('required',false);
            $('#new_dd_param').prop('required',false);
        }

        if (selected === 'preview_book_meter') {
            $('#post_code').show();
            $('#phone_num').show();
            $('#pcod_param').prop('required',true);
            $('#phone_param').prop('required',true);
        } else {
            $('#post_code').hide();
            $('#phone_num').hide();
            $('#pcod_param').prop('required',false);
            $('#phone_param').prop('required',false);
        }

        if (selected === 'preview_balance_overdue_1' || selected === 'preview_balance_overdue_2' || selected === 'preview_balance_overdue_3' || selected === 'preview_direct_debit_changes' ||
            selected === 'preview_leaving' || selected === 'preview_new_payment_plan' || selected === 'preview_new_payment_schedule' || selected === 'preview_objection' || selected === 'preview_previous_supplier_objection') {
            $('#bill_num').show();
            $('#bill_param').prop('required',true);
        } else {
            $('#bill_num').hide();
            $('#bill_param').prop('required',false);
        }
    });
});

$('#id-lookup').click(function(e){
    e.preventDefault();

    var selected = $('#email_type').find('option:selected').val();
    var Errors = [];
    if($('#id_param').val() === '') { Errors += ' - Please enter a customer id\n'; }
    if(selected === '') { Errors += ' - Please select an email type\n'; }

    if(Errors.length >0) {
        alert('Please check the following:\n\n'+Errors);
    } else {

        $.ajax({
            url: base_url + 'Admin/admin_id_lookup',
            dataType: 'json',
            type: 'post',
            data: {id_param: $('#id_param').val()},
            success: function (response) {
                $('#pcod_param').val(response.postcode);
                $('#phone_param').val(response.phone);
            }
        });
    }
});

$('#send-email').click(function(e){
    e.preventDefault();

    $('html,body').animate({
        scrollTop: $('#email_outcome').offset().top+30},
    'slow');

    var selected = $('#email_type').find('option:selected').val();
    var Errors = [];
    if($('#name_param').val() === '') { Errors += ' - Please enter a customer name\n'; }
    if($('#new_address').css('display') === 'block') {
        if($('#address_param').val() === '') { Errors += ' - Please the customers new address\n'; }
    }
    if($('#post_code').css('display') === 'block') {
        if($('#pcod_param').val() === '') { Errors += ' - Please the customers post code\n'; }
    }
    if($('#user_phone').css('display') === 'block') {
        if($('#phone_param').val() === '') { Errors += ' - Please the customers phone number\n'; }
    }
    if($('#bill_number').css('display') === 'block') {
        if($('#bill_param').val() === '') { Errors += ' - Please enter the bill number\n'; }
    }
    if($('#old_dd').css('display') === 'block') {
        if($('#old_dd_param').val() === '') { Errors += ' - Please enter an old Direct Debit amount\n'; }
    }
    if($('#new_dd').css('display') === 'block') {
        if($('#new_dd_param').val() === '') { Errors += ' - Please enter a new Direct Debit amount\n'; }
    }

    if(Errors.length >0) {
        alert('Please check the following:\n\n'+Errors);
    } else {

        var data = {
            customer_number: $('#id_param').val(),
            forename: $('#name_param').val(),
            selected: selected
        };

        if (selected === 'preview_balance_overdue_1' || selected === 'preview_balance_overdue_2' || selected === 'preview_balance_overdue_3' || selected === 'preview_direct_debit_changes' || selected === 'preview_leaving' || selected === 'preview_new_payment_plan' || selected === 'preview_new_payment_schedule' || selected === 'preview_objection' || selected === 'preview_previous_supplier_objection') {
            data.customer_number = $('#id_param').val();
            data.bill_number = $('#bill_param').val();
        } else if (selected === 'preview_moving_home_email') {
            data.new_address = $('#address_param').val();
        } else if (selected === 'preview_book_meter') {
            data.user_phone = $('#phone_param').val();
            data.post_code = $('#pcod_param').val();
        } else if (selected === 'preview_direct_debit_confirmation') {
            data.old_dd = $('#old_dd_param').val();
            data.new_dd = $('#new_dd_param').val();
        }

        $.ajax({
            type: 'post',
            dataType: 'json',
            cache: false,
            url: base_url + 'Admin/preview_email',
            data: data,
            success: function (response) {
                $('#email_outcome').html(response.success);
            }
        });
    }
});
