$(function(){

    var careers = {
        $wrap : $('#job-roles-wrap')
    };



    function evtClick_submitCV( evt ){
        evt.preventDefault();

        var $form = $( this ).parents('form');
        var filename = $form.find('input[type="file"]').val().split('\\');
        var data = $form.serialize() +'&CV='+ filename[ filename.length-1 ];

        togglePreloader();
        $.ajax({
            url     : '/career/email',
            type    : 'post',
            data    : data,
            success : function(){
                togglePreloader();
                closeModal();
            },
            error : function(){
                ERROR("failed to upload your CV");
            }
        });

    };

    function evtClick_uploadCV( evt ){
        evt.preventDefault();

        var $this = $( this );

        togglePreloader();
        REQUEST( '/templates/about/cv-upload.hbs', 'text' )
            .then(function( template ){
                togglePreloader();
                openModal(function( $modal ){
                    $modal.find('.inner').append( Handlebars.compile( template )({}) );
                    $modal.find('[name="role"]').val( $this.attr('data-id') );
                    $modal.find('.submit').on( 'click', evtClick_submitCV );
                });
            });
    };

    function evtClick_questionExpand( evt ){
        evt.preventDefault();

        var $parent = $( this ).parents( '.job-wrap' );
        var condition = $parent.hasClass('expanded');
        condition ? $parent.removeClass('expanded') : $parent.addClass('expanded') ;
        TweenMax.to( $parent.find('.job-body'), 0.3, { autoAlpha : condition ? 0 : 1, display : condition ? 'none' : 'block', ease : Power2.easeInOut } );
    };

    function buildBody( data ){
        return $('<div>', { class : 'job-body' })
            .append( $.parseHTML( data.acf.jbbrief ).concat($.parseHTML( data.acf.jbdesc )) )
            .append( $('<a>', { id : 'more-info-link', class: "btn-eversmart-primary", href : data.link }).text( "More Info" ) )
            .append( $('<a>', { id : 'upload-cv', class: "btn-eversmart-primary", href : "#", "data-id" : data.id }).text( "Upload CV" ) );
    };

    function buildLink( data ){
        return $('<a>', { class : 'job-expand', href : '#' }).text( data.acf.jbtitle );
    };

    function appendJobs( data ){
        data.forEach(function( job ){
            careers.$wrap.append( $('<div>', { class : 'job-wrap' }).append([ buildLink( job ), buildBody( job ) ]));
        });
        $('.job-expand').on( 'click', evtClick_questionExpand );
        $('#upload-cv').on( 'click', evtClick_uploadCV );
    };

    togglePreloader();
    REQUEST( '/career/jobs' )
        .then(function( res ){
            togglePreloader();
            appendJobs( res );
        });
});