$(function () {

    var quotation = {
        products: null,
        campaign: null,
        excessess: [0, 50, 95],
        paymentType: ['Annual', 'Monthly'],
        addresses: null,
        currentIndex: -1,
        currentStep: null,
        $wrap: $('#quote-step'),
        $title: $('#quote-step-title'),
        templates: {},
        form: {
            o_id: '00D0Y000000YWqc',
            record_type: 'Home Emergency',
            lead_source: 'Website',
            country: 'GB',
            policy_duration: '12',
            retailer: 'Eversmart Limited',
            broker: 'Eversmart Limited',
            asperato_reference: 'placeholder',
            payment_reference: 'ESE124',
            authorisation_id: '',
            lead_status: 'Complete'
        },
        wtc: [
            {
                database: "o_id",
                salesforce: "oid",
                value: null
            },
            {
                database: null,
                salesforce: "Status",
                value: "New"
            },
            {
                database: "record_type",
                salesforce: "recordType",
                value: null
            },
            {
                database: "lead_source",
                salesforce: "lead_source",
                value: "Website"
            },
            {
                database: "sales_date",
                salesforce: "00N1n00000ScSN1",
                value: null
            },
            {
                database: "premium",
                salesforce: "00N1n00000ScSLu",
                value: null
            },
            {
                database: "title",
                salesforce: "title",
                value: null
            },
            {
                database: "first_name",
                salesforce: "first_name",
                value: null
            },
            {
                database: "middle_name",
                salesforce: "00N1n00000ScSLi",
                value: null
            },
            {
                database: "last_name",
                salesforce: "last_name",
                value: null
            },
            {
                database: "company",
                salesforce: "company",
                value: ""
            },
            {
                database: "dob",
                salesforce: "00N0Y00000S9MH9",
                value: null
            },
            {
                database: "email",
                salesforce: "email",
                value: null
            },
            {
                database: "phone1",
                salesforce: "mobile",
                value: null
            },
            {
                database: "phone2",
                salesforce: "phone",
                value: null
            },
            {
                database: "risk_address_line_1",
                salesforce: "00N1n00000ScSLw",
                value: null
            },
            {
                database: "risk_address_line_2",
                salesforce: "00N1n00000ScSLx",
                value: null
            },
            {
                database: "risk_address_line_3",
                salesforce: "00N1n00000ScSLy",
                value: null
            },
            {
                database: "risk_address_town",
                salesforce: "00N1n00000ScSLz",
                value: null
            },
            {
                database: "risk_address_county",
                salesforce: "00N1n00000ScSLv",
                value: null
            },
            {
                database: "risk_address_postcode",
                salesforce: "00N1n00000ScSM1",
                value: null
            },
            {
                database: "ownership",
                salesforce: "00N1n00000ScSLl",
                value: null
            },
            {
                database: "property_type",
                salesforce: "00N1n00000ScSLt",
                value: null
            },
            {
                database: "detachment",
                salesforce: "00N1n00000ScSLT",
                value: null
            },
            {
                database: "number_bedrooms",
                salesforce: "00N1n00000ScSLj",
                value: null
            },
            {
                database: "fuel",
                salesforce: "00N1n00000ScSLU",
                value: null
            },
            {
                database: "heating_system",
                salesforce: "00N1n00000ScSLS",
                value: null
            },
            {
                database: "boiler_type",
                salesforce: "00N1n00000ScSLR",
                value: null
            },
            {
                database: "boiler_age",
                salesforce: "00N1n00000ScSLM",
                value: null
            },
            {
                database: "billing_address_street",
                salesforce: "street",
                value: null
            },
            {
                database: "billing_address_town",
                salesforce: "city",
                value: null
            },
            {
                database: "billing_address_city",
                salesforce: "state",
                value: null
            },
            {
                database: "billing_address_postcode",
                salesforce: "zip",
                value: null
            },
            {
                database: "country",
                salesforce: "country",
                value: null
            },
            {
                database: "product_code",
                salesforce: "00N1n00000ScSLp",
                value: null
            },
            {
                database: "product_id",
                salesforce: "00N1n00000ScSLq",
                value: null
            },
            {
                database: "product_name",
                salesforce: "00N1n00000ScSLr",
                value: null
            },
            {
                database: "policy_start_date",
                salesforce: "00N1n00000ScSIf",
                value: null
            },
            {
                database: "policy_duration",
                salesforce: "00N1n00000ScSJY",
                value: null
            },
            {
                database: "payment_schedule",
                salesforce: "00N1n00000ScX6k",
                value: null
            },
            {
                database: "asperato_reference",
                salesforce: "00N1l000000cIvV",
                value: null
            },
            {
                database: "preferred_contact",
                salesforce: "00N1n00000ScSLo",
                value: null
            },
            {
                database: "email_opt_in",
                salesforce: "00N1n00000ScSLe",
                value: null
            },
            {
                database: "phone_opt_in",
                salesforce: "00N1n00000ScSLf",
                value: null
            },
            {
                database: "post_opt_in",
                salesforce: "00N1n00000ScSLg",
                value: null
            },
            {
                database: "campaign_ref",
                salesforce: "campaign_ref",
                value: null
            },
            {
                database: "text_opt_in",
                salesforce: "00N1n00000ScSLh",
                value: null
            },
            {
                database: "retailer",
                salesforce: "00N1n00000ScSMw",
                value: null
            },
            {
                database: "broker",
                salesforce: "00N1n00000ScSMr",
                value: null
            },
            {
                database: "boiler_manufacturer",
                salesforce: "00N1n00000ScSLP",
                value: null
            },
            {
                database: "payment_reference",
                salesforce: "00N1l000000ceCs",
                value: null
            },
            {
                database: "authorisation_id",
                salesforce: "00N1n00000ScSLO",
                value: null
            },
            {
                database: "lead_status",
                salesforce: "00N1n00000ScSLd",
                value: null
            },
            {
                database: 'debug',
                salesforce: 'debug',
                value: null
            },
            {
                database: 'debugEmail',
                salesforce: 'debugEmail',
                value: null
            }
        ],
        steps: [
            {
                index: 0,
                progress: {
                    include: true,
                    label: "Overview"
                },
                title: "Customize your Cover",
                type: 'chosen',
                productId: window.location.hash.replace('#', '').split("?")[0]
            },
            {
                index: 1,
                progress: {
                    include: true,
                    label: "Details"
                },
                title: "Enter your postcode to personalise your quote",
                type: 'postcode',
                input: {
                    id: uuid(),
                    type: 'text',
                    name: 'postcode',
                    placeholder: 'POSTCODE',
                    className: 'postcode-field'
                }
            },
            {
                index: 2,
                progress: {
                    include: true,
                    label: "Details"
                },
                title: "Choose your address",
                type: 'address',
                asterix: true,
                asterixContent: "The address you wish to insure",
                both: {
                    id: uuid(),
                    name: 'gas-and-elec',
                    className: 'gas-and-elec-field'
                },
                gas: {
                    id: uuid(),
                    name: 'gas',
                    className: 'gas-field'
                },
                mpan: {
                    id: uuid(),
                    name: 'mpan',
                    className: 'mpan-field'
                },
                serial: {
                    id: uuid(),
                    type: 'text',
                    name: 'serial',
                    placeholder: 'Enter Your Gas Serial Number',
                    className: 'serial-field'
                }
            },
            {
                index: 3,
                progress: {
                    include: true,
                    label: "Details"
                },
                title: "Signup",
                type: 'signup'
            },
            {
                index: 4,
                progress: {
                    include: true,
                    label: "Payment"
                },
                title: "Payment",
                type: 'payment'
            },
            {
                index: 5,
                progress: {
                    include: true,
                    label: "Complete"
                },
                title: 'Welcome to the family',
                type: 'welcome',
                card: {
                    title: "Your welcome pack is on its way.",
                    message: "our executive will call you shortly."
                }
            },
            {
                index: 6,
                goBackTo: 1,
                progress: {
                    include: false
                },
                title: 'Cant find your property?',
                type: 'contact',
                card: {
                    title: "Please get in touch",
                    options: [
                        {
                            id: uuid(),
                            img: '/assets/images/new-world/icons/HS_phone.svg',
                            href: "tel:01613320019",
                            classes: "error-quote-section",
                            imgAlt: '0161 332 0019',
                            info: "lines are open 8am - 9pm, 7 days a week"
                        },
                        {
                            id: uuid(),
                            img: '/assets/images/new-world/icons/HS_mail.svg',
                            href: "mailto:insurance@eversmartenergy.co.uk",
                            classes: "error-quote-section",
                            imgAlt: 'for general queries email us at',
                            info: "insurance@eversmartenergy.co.uk"
                        }
                    ]
                }
            }
        ],
        FAQs: {
            data: [
                {
                    page: "chosen",
                    title: "What does an annual service include?",
                    body: [
                        {
                            tag: "p",
                            text: "This is a visit we carry out in each contract year* to check your *boiler and *central heating system or *gas appliance is safe and in good working order. *(depending on what is included in your agreement) these policies do not cover an annual service however we can arrange an annual service from £75.00 inc VAT by calling 0161 332 0019"
                        }
                    ]
                },
                {
                    page: "chosen",
                    title: "Where can I find what my home emergency policy covers?",
                    body: [
                        {
                            tag: "p",
                            text: "Your policy information will have been emailed to you in your welcome pack when you took out your policy. If you have misplaced your welcome pack, you can email or call us and we can send it to you again. You can also refer to terms and conditions to understand what your policy covers."
                        }
                    ]
                },
                {
                    page: "chosen",
                    title: "Do you cover LPG, Oil and Electric boilers?",
                    body: [
                        {
                            tag: "p",
                            text: "No, unfortunately we do not cover boilers fueled by LPG, Oil or Electric. Please refer to page 7 of your T&C’s."
                        }
                    ]
                },
                {
                    page: "chosen",
                    title: "Is there an age limit for boilers?",
                    body: [
                        {
                            tag: "p",
                            text: "There is no age limit on the boilers that we cover but please refer to basis of cover on page 3 of your t&c’s."
                        }
                    ]
                },
                {
                    page: "chosen",
                    title: "Can I manage my account online?",
                    body: [
                        {
                            tag: "p",
                            text: "Our online portal & app will be launching in spring. Until this time our dedicated customer service team can assist you with any requirements you have."
                        }
                    ]
                },
                {
                    page: "chosen postcode address signup payment",
                    title: "Can I speak to someone if I have a question?",
                    body: [
                        {
                            tag: "p",
                            text: "Speak to a member of our friendly team in one of the following ways:"
                        },
                        {
                            tag: "ul",
                            items: [
                                'Chat to us online at &lt;a target=&quot;_blank&quot; class=&quot;faq-body-link&quot;  href=&quot;http://www.eversmartenergy.co.uk&quot; title=&quot;Go to eversmartenergy&quot;&gt;eversmartenergy.co.uk&lt;/a&gt;',
                                'Give us a call on &lt;a target=&quot;_blank&quot; class=&quot;faq-body-link&quot;  href=&quot;tel:01613320019&quot; title=&quot;call 01613320019&quot;&gt;0161 332 0019&lt;/a&gt;',
                                'Email us on &lt;a target=&quot;_blank&quot; class=&quot;faq-body-link&quot;  href=&quot;mailto:insurance@eversmartenergy.co.uk&quot; title=&quot;Email insurance@eversmartenergy.co.uk&quot;&gt;insurance@eversmartenergy.co.uk&lt;/a&gt;',
                                'Write to us at 10 Empress Business Park, Manchester, M16 9EB.'
                            ]
                        }
                    ]
                },
                {
                    page: "postcode address",
                    title: "Do you cover house shares, HMOs, flats or lodgers?",
                    body: [
                        {
                            tag: "p",
                            text: "Yes, as long as you are not in breach of definitions of coverage which can be found in pages 2 and 3 of your t&c’s"
                        }
                    ]
                },
                {
                    page: "postcode address",
                    title: "Do you cover holiday homes or properties abroad?",
                    body: [
                        {
                            tag: "p",
                            text: "Please refer to geographical limits and general definitions on pages 2 and 3 of your t&c’s."
                        }
                    ]
                },
                {
                    page: "signup",
                    title: "Can I manage my account online?",
                    body: [
                        {
                            tag: "p",
                            text: "Our online portal & app will be launching in spring. Until this time our dedicated customer service team can assist you with any requirements you have."
                        }
                    ]
                },
                {
                    page: "signup",
                    title: "How can I make a claim?",
                    body: [
                        {
                            tag: "p",
                            text: "In order to make a claim by contacting our claims partner preferred management services on 0191 466 1298 or email info@preferredmanagement.co.uk."
                        }
                    ]
                },
                {
                    page: "signup payment",
                    title: "Where can I find the terms & conditions of my home emergency policy?",
                    body: [
                        {
                            tag: "p",
                            text: "The terms and conditions can be found &lt;a target=&quot;_blank&quot; class=&quot;faq-body-link&quot; href=&quot;https://eversmartenergy--qa.cs108.my.salesforce.com/sfc/p/#1l0000008d2T/a/1l0000004JRl/dBl.xsbO.RRVlHfIukbrGA.TCxcn2P4HgXp9EI1vbtw&quot; title=&quot;T&amp;C's&quot;&gt;here&lt;/a&gt;."
                        }
                    ]
                },
                {
                    page: "signup",
                    title: "Where can I find what my home emergency policy covers?",
                    body: [
                        {
                            tag: "p",
                            text: "Your policy information will have been emailed to you in your welcome pack when you took out your policy. If you have misplaced your welcome pack, you can email or call us and we can send it to you again. You can also refer to terms and conditions to understand what your policy covers."
                        }
                    ]
                },
                {
                    page: "signup",
                    title: "Who will attend a home emergency?",
                    body: [
                        {
                            tag: "p",
                            text: "A qualified representative will be appointed by approved management services in the event of an emergency."
                        }
                    ]
                },
                {
                    page: "signup",
                    title: "What is the definition of an emergency?",
                    body: [
                        {
                            tag: "p",
                            text: "A sudden or unexpected event which if not dealt with quickly may:"
                        },
                        {
                            tag: "ul",
                            items: [
                                'lead to further damage.',
                                'leave your home unsafe or unsecured.',
                                'or cause danger to you or any other permanent resident of your home.',
                                'Please refer to your t&c’s for policy definitions.'
                            ]
                        }
                    ]
                },
                {
                    page: "signup",
                    title: "How can I change the contact details you have for me?",
                    body: [
                        {
                            tag: "p",
                            text: "If you wish to change your contact details, please contact us in one of the following ways:"
                        },
                        {
                            tag: "ul",
                            items: [
                                'Chat to us online at &lt;a target=&quot;_blank&quot; class=&quot;faq-body-link&quot;  href=&quot;http://www.eversmartenergy.co.uk&quot; title=&quot;Go to eversmartenergy&quot;&gt;eversmartenergy.co.uk&lt;/a&gt;',
                                'Give us a call on &lt;a target=&quot;_blank&quot; class=&quot;faq-body-link&quot;  href=&quot;tel:01613320019&quot; title=&quot;call 01613320019&quot;&gt;0161 332 0019&lt;/a&gt;.',
                                'Email us on &lt;a target=&quot;_blank&quot; class=&quot;faq-body-link&quot;  href=&quot;mailto:insurance@eversmartenergy.co.uk&quot; title=&quot;Email insurance@eversmartenergy.co.uk&quot;&gt;insurance@eversmartenergy.co.uk&lt;/a&gt;',
                                'Write to us at 10 Empress Business Park, Manchester, M16 9EB.'
                            ]
                        }
                    ]
                },
                {
                    page: "signup",
                    title: "What happens if I move to a new house?",
                    body: [
                        {
                            tag: "p",
                            text: "If you move home, please contact us in one of the following ways:"
                        },
                        {
                            tag: "ul",
                            items: [
                                'Chat to us online at &lt;a target=&quot;_blank&quot; class=&quot;faq-body-link&quot;  href=&quot;http://www.eversmartenergy.co.uk&quot; title=&quot;Go to eversmartenergy&quot;&gt;eversmartenergy.co.uk&lt;/a&gt;',
                                'Give us a call on &lt;a target=&quot;_blank&quot; class=&quot;faq-body-link&quot;  href=&quot;tel:01613320019&quot; title=&quot;call 01613320019&quot;&gt;0161 332 0019&lt;/a&gt;',
                                'Email us on &lt;a target=&quot;_blank&quot; class=&quot;faq-body-link&quot;  href=&quot;mailto:insurance@eversmartenergy.co.uk&quot; title=&quot;Email insurance@eversmartenergy.co.uk&quot;&gt;insurance@eversmartenergy.co.uk&lt;/a&gt;',
                                'Write to us at 10 Empress Business Park, Manchester, M16 9EB.'
                            ]
                        }
                    ]
                },
                {
                    page: "signup",
                    title: "What will happen if my home becomes uninhabitable?",
                    body: [
                        {
                            tag: "p",
                            text: "If your policy covers this and If the emergency responder determines and deems your home uninhabitable in line with the t&c’s, emergency polices cover alternative accommodation. Please refer to t&c’s for more information."
                        }
                    ]
                },
                {
                    page: "payment",
                    title: "Can I pay for my policy using a credit / debit card?",
                    body: [
                        {
                            tag: "p",
                            text: "No, we only accept Direct Debit as a payment facility however this may change in future."
                        }
                    ]
                },
                {
                    page: "payment",
                    title: "My Direct Debit has failed or I missed a payment? What should i do?",
                    body: [
                        {
                            tag: "p",
                            text: "You will receive a confirmation that a payment has been missed. We will automatically re-attempt to take payment within 24 hours. If the payment is still unsuccessful you can contact us in one of the following ways:"
                        },
                        {
                            tag: "ul",
                            items: [
                                'Chat to us online at &lt;a target=&quot;_blank&quot; class=&quot;faq-body-link&quot;  href=&quot;http://www.eversmartenergy.co.uk&quot; title=&quot;Go to eversmartenergy&quot;&gt;eversmartenergy.co.uk&lt;/a&gt;',
                                'Give us a call on &lt;a target=&quot;_blank&quot; class=&quot;faq-body-link&quot;  href=&quot;tel:01613320019&quot; title=&quot;call 01613320019&quot;&gt;0161 332 0019&lt;/a&gt;',
                                'Email us on &lt;a target=&quot;_blank&quot; class=&quot;faq-body-link&quot;  href=&quot;mailto:insurance@eversmartenergy.co.uk&quot; title=&quot;Email insurance@eversmartenergy.co.uk&quot;&gt;insurance@eversmartenergy.co.uk&lt;/a&gt;'
                            ]
                        },
                        {
                            tag: "p",
                            text: "Failing this, we will contact you. If the policy remains unpaid for 30 day period we reserve the right to cancel your policy."
                        }
                    ]
                },
                {
                    page: "payment",
                    title: "When will my first payment be taken?",
                    body: [
                        {
                            tag: "p",
                            text: "Payment is usually taken 14 days after taking out the policy online."
                        }
                    ]
                },
                {
                    page: "payment",
                    title: "How can I change when my Direct Debit is taken?",
                    body: [
                        {
                            tag: "p",
                            text: "If you wish to amend your direct debit date please contact us in one of the following ways:"
                        },
                        {
                            tag: "ul",
                            items: [
                                'Chat to us online at &lt;a target=&quot;_blank&quot; class=&quot;faq-body-link&quot;  href=&quot;http://www.eversmartenergy.co.uk&quot; title=&quot;Go to eversmartenergy&quot;&gt;eversmartenergy.co.uk&lt;/a&gt;',
                                'Give us a call on &lt;a target=&quot;_blank&quot; class=&quot;faq-body-link&quot;  href=&quot;tel:01613320019&quot; title=&quot;call 01613320019&quot;&gt;0161 332 0019&lt;/a&gt;',
                                'Email us on &lt;a target=&quot;_blank&quot; class=&quot;faq-body-link&quot;  href=&quot;mailto:insurance@eversmartenergy.co.uk&quot; title=&quot;Email insurance@eversmartenergy.co.uk&quot;&gt;insurance@eversmartenergy.co.uk&lt;/a&gt;',
                                'Write to us at 10 Empress Business Park, Manchester, M16 9EB.'
                            ]
                        }
                    ]
                },
                {
                    page: "payment",
                    title: "What happens to my renewal price after the first year?",
                    body: [
                        {
                            tag: "p",
                            text: "The price you'll pay at renewal is likely to increase as your introductory price will have ended, but it will be tailored to your specific details. If you're paying by direct debit, you will automatically renew; however, you can still choose to change or cancel your contract. We'll contact you 30 days before your renewal date to remind you and provide you with a renewal comparison quote."
                        }
                    ]
                },
                {
                    page: "payment",
                    title: "Do I have to pay an excess if I claim on my policy?",
                    body: [
                        {
                            tag: "p",
                            text: "Excess amounts depend on the policy you have taken. Please refer to your policy schedule which can be found in your welcome pack."
                        }
                    ]
                },
                {
                    page: "payment",
                    title: "Are call-out charges, labour and parts included?",
                    body: [
                        {
                            tag: "p",
                            text: "Call out charges depend on your policy, labour and parts are included within your policy up to the policy cover limits."
                        }
                    ]
                }
            ]
        }
    };

    // Load Helpers
    Handlebars.registerHelper('addDataAttributes', function (data) {
        return data.populate instanceof Function ? '' : ' ' + data.populate.map(function (item) {
            return (item.field + '="' + item.val + '"')
        }).join(' ');
    });

    Handlebars.registerHelper('paymentStatement', function (product) {
        return product.PaymentSchedule === "Annual" ? product.AnnualRetailPremiumIncIPT + ' for the year' : product.MonthlyPremiumIncIPT + ' a month';
    });

    // Initialize Error Messages
    $(window).on('message', function (evt) {
        if (evt.originalEvent.origin.indexOf('protectedpayments') > -1) {
            switch (evt.originalEvent.data) {
                case "asp--error-screen":
                case "asp--error":
                case "asp--cancel":
                    ERROR("Something went wrong processing your payment, please try again or visit the faqs section of the page for our contact info to get in touch with us");
                    break;
                case "asp--complete":
                    togglePreloader();
                    Promise.all([
                        REQUEST('/user/web_to_lead_log', 'none', { // send to our database
                            method: 'post',
                            headers: new Headers({'Content-Type': 'application/json'}),
                            body: JSON.stringify(quotation.form)
                        }),
                        REQUEST('https://login.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8', 'none', { // Send the web to lead form
                            method: 'post',
                            mode: 'no-cors',
                            headers: new Headers({'Content-Type': 'application/x-www-form-urlencoded'}),
                            body: buildWTL(true)
                        })
                    ])
                        .then(function () {
                            togglePreloader();
                            cycleSteps(1);
                            window.scrollTo({
                                top: 0,
                                behavior: 'smooth'
                            });
                        });
                    break;
            }
            ;
        }
    });

    // Initialize onLoad
    Promise.all([
        REQUEST('/templates/quotation/buttons.hbs', 'text'),
        REQUEST('/templates/quotation/postcode.hbs', 'text'),
        REQUEST('/templates/quotation/address.hbs', 'text'),
        REQUEST('/templates/quotation/contact.hbs', 'text'),
        REQUEST('/templates/quotation/welcome.hbs', 'text'),
        REQUEST('/templates/quotation/signup.hbs', 'text'),
        REQUEST('/templates/quotation/summary.hbs', 'text'),
        REQUEST('/templates/quotation/payment.hbs', 'text'),
        REQUEST('/templates/quotation/chosen.hbs', 'text')
    ]).then(function (templates) {
        [
            'buttons',
            'postcode',
            'address',
            'contact',
            'welcome',
            'signup',
            'summary',
            'payment',
            'chosen'
        ]
            .forEach(function (item, i) {
                quotation.templates[item] = templates[i]
            });
        cycleSteps(1);
        addProgressBar();
    });

    $('#quote-step-back-button').on('click', evtClick_backButton);

    // Begin Home Services Specific Quote Functions
    function addId(item) {
        item.id = uuid();
        return item;
    }

    function getAdressNameOrNum(data) {
        var result = '';
        data.bnum !== "" && (result += data.bnum + ' ');
        data.subb !== "" && (result += data.subb + (data.bnam !== "" ? " " : ""));
        data.bnam !== "" && (result += data.bnam);
        return result;
    }

    function getAddressPostcode(data) {
        return data.tpco !== "" ? data.tpco : data.ppco !== "" ? data.ppco : data.pcod;
    }

    // TODO: This is fucking retarded and needs to be re-done in a more dynamic way -AH
    function constructAddressString(data) {
        return [getAdressNameOrNum(data), data.thor, data.town, getAddressPostcode(data)].join(' ');
    }

    function addOptions(selector, arr, placeholder, address) {
        $(selector).html(
            [$('<option>', {value: '0'}).text(placeholder)].concat(arr.map(function (item, i) {
                return $('<option>', {value: item.id || uuid()}).text(address ? constructAddressString(item) : item)
            }))
        );
    }

    function getPostcodeData(postcode) {
        return REQUEST(window.APIUrl ? window.APIUrl : window.location.origin + '/homeservices/get_address_list', 'json', {
            method: 'post',
            headers: new Headers({'Content-Type': 'application/json'}),
            body: JSON.stringify({postcode: postcode})
        });
    }

    function validateEmail(email) {
        return email.indexOf('@') > -1 && email.indexOf('.') && email.length > 5;
    }

    function validateFields($elements) {
        $elements.each(function (i, el) {

            var $el = $(el);
            var value = $el.val();

            switch (el.nodeName === "SELECT" ? 'select' : $el.attr('type')) {
                case 'select':
                    var condition = value === null || value == 0;
                    condition ? $el.addClass('invalid') : $el.removeClass('invalid');
                    condition && ERROR('Invalid select fields needs to be populated');
                    break;
                case 'text':
                case 'password':
                case 'date':
                    var condition = value.length <= 2;
                    condition ? $el.addClass('invalid') : $el.removeClass('invalid');
                    condition && ERROR('Invalid text|date field needs to be populated');
                    break;
                case 'email':
                    var condition = !validateEmail(value);
                    condition ? $el.addClass('invalid') : $el.removeClass('invalid');
                    condition && ERROR('Invalid email field needs to be populated and valid');
                    break;
                case 'tel':
                    var condition = !value.length || !/^[0-9 ]*$/.test(value);
                    condition ? $el.addClass('invalid') : $el.removeClass('invalid');
                    condition && ERROR('Invalid number field, it needs to be populated with numbers only');
                    break;
            }
        });
    }

    function buildWTL(serialize) {
        var form = {};
        quotation.wtc.forEach(function (item) {
            if (item.database in quotation.form) {
                form[item.salesforce] = quotation.form[item.database];
            }
        });

        form.company = 'Eversmart Limited';
        return serialize ? serializeJSON(form) : form;
    }

    function priceCaption(product) {
        return product.PaymentSchedule === "Annual" ? 'for the year' : 'a month';
    }

    function evtClick_backButton(evt) {
        evt.preventDefault();
        if (quotation.currentStep.index === 0) {
            window.location.href = window.location.origin + '/homeservices';
            return;
        }
        clearCurrentStepData();
        quotation.currentStep.goBackTo instanceof Function ? cycleSteps(null, quotation.currentStep.goBackTo()) : quotation.currentStep.goBackTo ? cycleSteps(null, quotation.currentStep.goBackTo) : cycleSteps(-1);
    }

    function evtClick_button(evt) {

        let $this = $(this);
        let option = evt.data.options.find(function (item) {
            return item.id === $this.attr('id')
        });
        if (!option) {
            ERROR('Option Not Found');
            return;
        }

        switch (Array.isArray(option.populate)) {
            case true:
                option.populate.forEach(function (item) {
                    quotation.form[item.field] = item.val;
                });
                break;
            case false:
                option.populate(option);
                break;
        }

        option.goToAfter ? cycleSteps(null, option.goToAfter) : cycleSteps(1);

    }

    function evtClick_submitPostcode(evt) {
        evt.preventDefault();

        var $postcode = $('#' + evt.data.input.id);
        if ($postcode.val().length < 5) {
            ERROR('Please Provide A Valid Postcode');
            return;
        }

        quotation.form.risk_address_postcode = $postcode.val();
        cycleSteps(1);
    }

    function evtClick_submitAddress(evt) {
        evt.preventDefault();
        cycleSteps(1);
    }

    /*
    function evtClick_signupLogin(evt) {
        evt.preventDefault();

        var $form = $(this).parents('form');
        validateFields($form.find('input.required'));

        if (!$form.find('.invalid').length) {
            // submit login details here

            togglePreloader();
            REQUEST(window.APIUrl ? window.APIUrl : window.location.origin + '/homeservices/login_customer_details', 'json', {
                method: "post",
                headers: new Headers({
                    "Content-Type": "application/json"
                }),
                body: JSON.stringify({
                    user_email: $form.find('[name="email"]').val(),
                    user_password: $form.find('[name="password"]').val()
                })
            })
                .then(function (res) {
                    togglePreloader();
                    if (!res.success) {
                        ERROR('Your email or password is incorrect');
                        return;
                    }

                    quotation.$wrap.find('.inner[data-type="signup"]').addClass('not-existing');
                    var $signupForm = $form.parents('.inner').find('.form-wrap[data-form-type="signup"] form');

                    $signupForm.find('[name="dob"]').val(res.data.dateOfBirth);
                    $signupForm.find('[name="email"]').val(res.data.email);
                    $signupForm.find('[name="forename"]').val(res.data.forename);
                    $signupForm.find('[name="telephone"]').val(res.data.phone1);
                    $signupForm.find('[name="surname"]').val(res.data.surname);
                    $signupForm.find('[name="title"]').val(res.data.title);
                });
        }
    }
    */

    function evtClick_signupSubmit(evt) {
        evt.preventDefault();

        var $form = $(this).parents('form');
        validateFields($form.find('input:not(.hidden).required, select:not(.hidden)'));

        if (!$form.find('.invalid').length) {

            storeFormValues($form);
            var paymentData = cleanObj(quotation.form);
            paymentData.product = quotation.currentProduct;
            paymentData.paymentDay = (new Date()).addDays(14).getDate();
            paymentData.aboutOurInsurance = 'http://eversm.art/hpabout';
            paymentData.emergencyPolicy = 'http://eversm.art/hppolicy';
            paymentData.TcsAndCsDocument = 'http://eversm.art/hppolicy';
            quotation.steps.find(function (step) {
                return step.type === "payment"
            }).data = paymentData;

            togglePreloader();
            REQUEST(window.APIUrl ? window.APIUrl : window.location.origin + +'/user/web_to_lead_log', 'none', {
                method: 'post',
                headers: new Headers({'Content-Type': 'application/json'}),
                body: JSON.stringify(quotation.form)
            })
                .then(function () {
                    togglePreloader();
                    cycleSteps(1);
                    window.scrollTo({
                        top: 0,
                        behavior: 'smooth'
                    });
                });
        }
    }

    function evtClick_billingPostcode(evt) {
        evt.preventDefault();
        var $el = $(this).parent().find('[name="billing-postcode"]');
        evtClick_submitBillingPostcode.call($el, {target: $el[0]});
    }

    function evtClick_billingCheckbox(evt) {
        try {
            evt.preventDefault()
        } catch (err) {
        }
        ;

        var $parent = $(this).parents('.billing');
        var $body = $parent.find('.billing-body');

        if ($parent.hasClass('expanded')) {
            $body.empty();
            $parent.removeClass('expanded');
            return;
        }
        ;
        $parent.addClass('expanded');

        $body.empty().append([
            $('<input>', {
                type: "text",
                name: "billing-postcode",
                class: "billing-field required",
                placeholder: "Billing Postcode",
                "data-wtc": "billing_address_postcode"
            }),
            $('<a>', {href: "#", class: "submit-billing-postcode"})
        ]);
        $body.find('.submit-billing-postcode').on('click', evtClick_billingPostcode);
        $parent.on('keyup', evtKeyup_billingPostcode);
    }

    function evtClick_submitBillingPostcode(evt) {
        var $target = $(evt.target);
        quotation.form.billing_address_postcode = $target.val();
        togglePreloader();
        getPostcodeData($target.val()).then(function (res) {
            togglePreloader();

            if (!res.elec.length) {
                ERROR("Billing address not found");
                $target.addClass('invalid');
                return;
            } else {
                $target.removeClass('invalid');
            }
            ;

            // add id
            quotation.billingAddressess = res.elec.map(addId);
            addBillingAddressSelect();
        });
    };

    function addBillingAddressSelect() {
        var id = uuid();
        var $body = quotation.$wrap.find('.billing-body');
        $body.find('select').remove();
        $body.append($('<select>', {id: id, class: 'required billing-field'}));

        // add options to the select fields
        addOptions('#' + id, quotation.billingAddressess, 'Please Select Billing Address', true);
        $('#' + id).on('change', evtChange_billingAddress);
    };

    function evtClick_changeAddress(evt) {
        evt.preventDefault();
        cycleSteps(null, 1);
        window.scrollTo({
            top: 0,
            behavior: 'smooth'
        });
    };

    function evtClick_excessChosen(evt) {
        evt.preventDefault();
        var $this = $(this);
        changeProduct($this.attr('data-type'), null);
        $this.parents('.excess-list').find('.excess-option').removeClass('chosen');
        $this.addClass('chosen');
    }

    function evtClick_paymentTypeChosen(evt) {
        evt.preventDefault();
        var $this = $(this);
        changeProduct(null, $(this).attr('data-type'));
        $this.parents('.payment-type-list').find('.payment-option').removeClass('chosen');
        $this.addClass('chosen');
    }

    function evtClick_faqExpand(evt) {
        evt.preventDefault();

        var $parent = $(this).parents('.faq-wrap');
        var condition = $parent.hasClass('expanded');
        condition ? $parent.removeClass('expanded') : $parent.addClass('expanded');
        TweenMax.to($parent.find('.faq-body'), 0.3, {
            autoAlpha: condition ? 0 : 1,
            display: condition ? 'none' : 'block',
            ease: Power2.easeInOut
        });

    }

    function asperatoIframe(url) {
        $('.footnote').html('<iframe ' +
            'src="' + url + '' +
            '&LabelFrequencyOptions=' + quotation.form.payment_schedule + '' +
            '&DefaultPaymentOption=dd' +
            '&DLforename=' + quotation.form.first_name + '' +
            '&DLlastname=' + quotation.form.last_name + '' +
            '&DLemail=' + quotation.form.email + '' +
            '&DLaddress1=' + quotation.form.billing_address_street +
            '&DLaddress2=' + quotation.form.billing_address_town +
            '&DLcity=' + quotation.form.billing_address_city +
            '&DLpostcode=' + quotation.form.billing_address_postcode +
            '&DLcountry=' + quotation.form.country +
            '" frameborder="0" width="100%" height="600px" scrolling="yes" id="asperato_frame">' +
            '</iframe>');
    }

    function evtClick_payInject(evt) {
        evt.preventDefault();
        console.log('-->', quotation.$wrap.find('.info-wrap .checkbox'));

        privacy_checkbox = quotation.$wrap.find('.info-wrap .privacy');
        terms_checkbox = quotation.$wrap.find('.info-wrap .terms');
        if (privacy_checkbox.hasClass('checked')
            && terms_checkbox.hasClass('checked')) {
            togglePreloader();
            REQUEST(window.APIUrl ? window.APIUrl : window.location.origin + '/salesforce/create_auth')
                .then(function (res) {
                    togglePreloader();
                    quotation.form.authorisation_id = res.Authinfo.AuthorisationId;
                    quotation.form.full_url = res.Authinfo.FullURL;
                    $('.footnote').css('text-align', 'center');
                    asperatoIframe(quotation.form.full_url);
                });
        } else {
            ERROR("You must agree to the terms of this product & the privacy policy to continue.");
        }
    }

    function evtClick_changeDetails(evt) {
        evt.preventDefault();
        cycleSteps(null, 3);
    }

    function evtKeyup_postcode(evt) {
        (evt.keyCode === 13 || evt.which === 13) && evtClick_submitPostcode(evt);
    }

    function evtKeyup_billingPostcode(evt) {
        if (evt.keyCode === 13 || evt.which === 13) {
            evtClick_submitBillingPostcode(evt);
        }
    }

    function evtChange_address(evt) {

        var $this = $(this);
        if ($this.val() === '0') {
            return;
        }

        TweenMax.set(quotation.$wrap.find('.submit'), {display: 'block'});
        quotation.selectedAddress = quotation.addresses.elec.find(function (item) {
            return item.id === $this.val()
        });
        console.log(quotation.selectedAddress);
        quotation.form.risk_address_line_1 = quotation.selectedAddress.bnum + quotation.selectedAddress.subb;
        quotation.form.risk_address_line_2 = quotation.selectedAddress.thor;
        quotation.form.risk_address_county = quotation.selectedAddress.cnty;
        quotation.form.risk_address_town = quotation.selectedAddress.town;
    }

    function evtChange_billingAddress(evt) {
        var $this = $(this);

        if ($this.val() === '0') {
            return;
        }

        var address = quotation.billingAddressess.find(function (item) {
            return item.id === $this.val()
        });

        quotation.form.billing_address_street = quotation.selectedAddress.bnum + quotation.selectedAddress.subb;
        quotation.form.billing_address_town = address.town;
        quotation.form.billing_address_city = address.cnty;
    }

    function evtChange_heatingSystem(evt) {
        quotation.$wrap.find('[name="boiler_type"], [name="boiler_age"], [name="boiler_manufacturer"]')[$(this).val().toLowerCase().indexOf('boiler') > -1 ? 'removeClass' : 'addClass']('hidden');
    }

    function storeFormValues($form) {

        var now = (new Date()).formatted();

        quotation.form.sales_date = now;
        quotation.form.policy_start_date = now;

        quotation.form.premium = quotation.currentProduct.TotalPaidByConsumer;
        quotation.form.product_id = quotation.currentProduct.ProductId;
        quotation.form.product_code = quotation.currentProduct.ProductCode;
        quotation.form.product_name = quotation.currentProduct.ProductName;
        quotation.form.payment_schedule = quotation.currentProduct.PaymentSchedule;

        $form.find('input:not(.readonly), select:not(.billing-field)').toArray().forEach(function (el) {
            var $el = $(el);
            quotation.form[$el.attr('data-wtc')] = $el.val();
        });

        quotation.form.dob = quotation.form.dob.split('-').reverse().join('/');

        if (!$form.find('.billing .checkbox').hasClass('checked')) {
            quotation.form.billing_address_postcode = quotation.form.risk_address_postcode;
            quotation.form.billing_address_street = quotation.form.risk_address_line_1;
            quotation.form.billing_address_town = quotation.form.risk_address_town;
            quotation.form.billing_address_city = quotation.form.risk_address_county;
        }

        $form.find('.opt-in-wrap').toArray().forEach(function (el) {
            var $el = $(el);
            quotation.form[$el.attr('data-type')] = $el.find('checkbox').hasClass('checked') ? 1 : 0;
        });
    }

    function updatePrice(simple) {

        if (!simple) {
            var $wrap = quotation.$wrap.find('.customize .overview');
            var $header = quotation.$wrap.find('header');
        } else {
            var $wrap = quotation.$wrap;
        }

        var condition = quotation.currentProduct.PaymentSchedule === 'Annual';
        var $annual = $wrap.find('.breakdown .annual');
        $wrap[condition ? 'addClass' : 'removeClass']('annual');

        var equivilent = quotation.products.find(function (item) {
            return item.ExcessLevel === quotation.currentProduct.ExcessLevel && item.ItemsCovered.length === quotation.currentProduct.ItemsCovered.length && item.PaymentSchedule !== quotation.currentProduct.PaymentSchedule;
        });
        quotation.$wrap.find('.excess-value').text(quotation.currentProduct.ExcessLevel);
        discount = Math.max(equivilent.AnnualRetailPremiumIncIPT, quotation.currentProduct.AnnualRetailPremiumIncIPT) - Math.min(equivilent.AnnualRetailPremiumIncIPT, quotation.currentProduct.AnnualRetailPremiumIncIPT);
        quotation.$wrap.find('.annual-diff-value').text(discount.toFixed(2));

        if (condition) {
            if (!simple) {
                $header.find('.price').text('£' + quotation.currentProduct.AnnualRetailPremiumIncIPT.toFixed(2) + '*');
                $header.find('.caption').text('for the year');
            }
            $annual.find('.breakdown .label').text('Annual:');
            $annual.find('.price').text('£' + quotation.currentProduct.AnnualRetailPremiumIncIPT.toFixed(2) + '*');
        } else {
            if (!simple) {
                $header.find('.price').text('£' + quotation.currentProduct.MonthlyPremiumIncIPT.toFixed(2) + '*');
                $header.find('.caption').text('a month');
            }
            $wrap.find('.breakdown .first-month .price').text('£' + quotation.currentProduct.FirstPayment + '*');
            $wrap.find('.breakdown .monthly .price').text('£' + quotation.currentProduct.MonthlyPremiumIncIPT.toFixed(2) + '*');
            $annual.find('.label').text('Which will equate annually to:');
            $annual.find('.price').text('(£' + quotation.currentProduct.AnnualRetailPremiumIncIPT.toFixed(2) + ')*');
        }

        var date = new Date();
        var string = constructPaymentDayText(condition, date.addDays(14).getDate(), date.ordinal());
        quotation.$wrap.find('.payment-date-text').remove();

        if (quotation.currentStep.type !== "payment") {
            quotation.$wrap.find('.breakdown').after($('<p>', {class: 'payment-date-text'}).html(string));
        } else {
            quotation.$wrap.find('.footnote').append($('<p>', {class: 'payment-date-text'}).html(string));
        }
    }

    function changeProduct(excess, payment) {

        var excess = excess || quotation.currentProduct.ExcessLevel;
        var coverage = quotation.currentProduct.ItemsCovered;
        var payment = payment || quotation.currentProduct.PaymentSchedule;
        var product = quotation.products.find(function (item) {
            return item.ExcessLevel === +excess && coverage.filter(function (opt) {
                return item.ItemsCovered.indexOf(opt) > -1 && coverage.length === item.ItemsCovered.length
            }).length === coverage.length && item.PaymentSchedule === payment;
        });

        if (!product) {
            ERROR('no product found matching the options specified');
        }

        quotation.currentProduct = product;
        updatePrice();
    }

    function displayChosen() {
        quotation.currentProduct = quotation.products.find(function (item) {
            return item.ProductId === quotation.currentStep.productId
        });
        console.log(quotation.currentStep.productId);
        if (quotation.currentProduct === undefined) {
            ERROR('Cannot find the product selected, please go back and try selecting another product');
            $('.quote-step').html('Please press the arrow above');
            return;
        }

        function matches(field, value) {
            switch (field) {
                case 'PaymentSchedule':
                    return quotation.currentProduct[field] === value ? ' chosen' : quotation.currentProduct[field] === value ? ' chosen' : '';
                default:
                    return quotation.currentProduct[field] === value ? ' chosen' : '';
            }
        }

        quotation.$wrap.find('.excess-list').append(
            quotation.excessess.map(function (item) {
                return $('<li>').append($('<a>', {
                    href: '#',
                    class: 'excess-option' + matches('ExcessLevel', item),
                    'data-type': item
                }).text('£' + item))
            })
        );
        quotation.$wrap.find('.excess-option').on('click', evtClick_excessChosen);

        quotation.$wrap.find('.payment-type-list').append(
            quotation.paymentType.map(function (item) {
                return $('<li>').append($('<a>', {
                    href: '#',
                    class: 'payment-option' + matches('PaymentSchedule', item),
                    'data-type': item
                }).text(item))
            })
        );
        quotation.$wrap.find('.payment-option').on('click', evtClick_paymentTypeChosen);

        quotation.$wrap.find('header .product-name').text(quotation.currentProduct.ProductName);
        quotation.$wrap.find('header .product-name').text(quotation.currentProduct.ProductName);
        quotation.$wrap.find('header .caption').text(priceCaption(quotation.currentProduct));

        updatePrice();

        var includes = {
            included: [],
            excluded: []
        };

        function buildIncludes($list, data) {
            $list.append(data.map(function (item) {
                return $('<li>', {class: item.FragmentName}).text(item.Content)
            }));
        }

        quotation.includes.forEach(function (item) {
            item.FragmentName === "Excluded" ? includes.excluded.push(item) : includes.included.push(item)
        });
        buildIncludes(quotation.$wrap.find('section.includes .included > ul'), includes.included);
        buildIncludes(quotation.$wrap.find('section.includes .excluded > ul'), includes.excluded);

        var date = new Date();
        var string = constructPaymentDayText(quotation.currentProduct.PaymentSchedule === 'Annual', date.addDays(14).getDate(), date.ordinal());
        quotation.$wrap.find('.payment-date-text').remove();
        quotation.$wrap.find('.breakdown').after($('<p>', {class: 'payment-date-text'}).html(string));
    }

    function addPDFs(templateStr) {
        if ($('.pdf-wrap').length) {
            return;
        }
        ;
        var template = Handlebars.compile(templateStr);
        var $asterixs = $('.asterixs');
        ($asterixs.length ? $asterixs : $('#quotation-page')).after(template([
            {
                title: 'Open PDF in new tab',
                name: quotation.currentProduct.ProductName + ' PDF',
                link: quotation.currentProduct.IPIDDocument
            },
            {
                title: 'Open PDF in new tab',
                name: 'General Terms & Conditions',
                link: 'http://eversm.art/hppolicy'
            },
            {
                title: 'Open PDF in new tab',
                name: 'About our insurance services',
                link: 'http://eversm.art/hpabout'
            }
        ]));
        if (quotation.currentStep.type === "chosen") {
            quotation.$wrap.find('.agree .tncs').attr('href', 'http://eversm.art/hppolicy');
        }
    }

    function parseContent(tag, txt) {
        return $(tag).html(txt).text();
    }

    function buildFAQsBody(data) {
        return data.map(function (item) {
            return item.tag !== 'ul' ? $('<' + item.tag + '>').html(parseContent('<' + item.tag + '>', item.text)) : $('<ul>').append(item.items.map(function (entry) {
                return $('<li>').html(parseContent('<li>', entry))
            }));
        });
    }

    function buildFAQs($wrap) {
        var questions = [];
        quotation.FAQs.data.filter(function (item) {
            return item.page.split(' ').indexOf(quotation.currentStep.type) > -1
        }).forEach(function (q) {
            questions.push(
                $('<div>', {class: "faq-wrap"}).append([
                    $('<a>', {class: "faq-link", href: '#'}).text(q.title),
                    $('<div>', {class: "faq-body"}).append(buildFAQsBody(q.body))
                ])
            );
        });

        $wrap.append(questions);
        $wrap.find('.faq-link').on('click', evtClick_faqExpand);
        TweenMax.set($('.faqs-section'), {display: !questions.length ? 'none' : 'block'});
    }

    function updateFaqs() {
        var $wrap = $('.faqs-wrap');
        if ($wrap.length) {
            $wrap.empty();
            buildFAQs($wrap);
        } else {
            REQUEST('/templates/quotation/faqs.hbs', 'text')
                .then(function (templateStr) {
                    var template = Handlebars.compile(templateStr);
                    $('#preloader-wrap').before(template());
                    buildFAQs($('.faqs-wrap'));
                });
        }
    }

    function populateSignupForm() {

        Object.keys(quotation.form).forEach(function (item) {
            quotation.$wrap.find('[data-wtc="' + item + '"]').val(item !== 'dob' ? quotation.form[item] : quotation.form[item].split('/').reverse().join('-'))
        });

        if (quotation.form.billing_address_street !== quotation.form.risk_address_line_1) {
            var $billing = quotation.$wrap.find('.billing');
            checkboxHandler.call($billing.find('.checkbox')[0]);
            evtClick_billingCheckbox.call($billing.find('.checkbox')[0]);
            $billing.find('[name="billing-postcode"]').val(quotation.form.billing_address_postcode);
            addBillingAddressSelect();

            quotation.billingAddressess.forEach(function (item) {
                if (constructAddressString(item) === quotation.form.billing_address_street) {
                    $billing.find('select').val(item.id);
                }
            });
        }
    }

    function constructPaymentDayText(isAnnual, date, ordinal) {
        var str = 'Your first payment will be taken on or soon after <strong>14 days</strong> from when you press the "Buy Now" at the bottom of the payment page';

        if (!isAnnual) {
            str += ', that means all your following payments will be on or around the <strong class="payment-day"> ' + ordinal + '</strong> of each month.';
        } else {
            str += '.';
        }

        return str;
    }

    function addProgressBar() {
        REQUEST('/templates/quotation/progress-bar.hbs', 'text')
            .then(function (templateStr) {
                var template = Handlebars.compile(templateStr);
                $('#quote-step-wrap').prepend(template());
                calculateProgress();
            });
    }

    function calculateProgress() {
        var index = {
            current: quotation.currentStep.index + 1,
            length: Math.max.apply(null, quotation.steps.filter(function (item) {
                return item.progress.include
            }).map(function (item) {
                return item.index
            })) + 1
        };
        setProgress(index.current / index.length * 100);
    }

    function setProgress(amount) {

        var $wrap = $('#progress-bar-wrap');
        var $header = $wrap.find('.header');
        var labelAmount = amount + '%';
        var TL = new TimelineMax({
            paused: true,
            onComplete: function () {
                $wrap.find('.heading').text(quotation.currentStep.progress.label);
                $header[amount >= 50 ? 'addClass' : 'removeClass']('past-mid-point');
                $header[amount >= 90 ? 'addClass' : 'removeClass']('end');
            }
        });

        TL.to($wrap.find('.progress'), 0.3, {width: amount + '%', ease: Power2.easeInOut}, 'l');
        TL.to($header, 0.3, {left: labelAmount, ease: Power2.easeInOut}, 'l');
        TL.play();
    }

    function addDynamicContent(data) {
        switch (data.type) {
            case "address":
                togglePreloader();
                getPostcodeData(quotation.form.risk_address_postcode).then(function (json) {
                    togglePreloader();

                    if (!json.elec.length) {
                        cycleSteps(null, 6);
                        return;
                    }
                    ;

                    quotation.addresses = {elec: json.elec};
                    quotation.addresses.elec = quotation.addresses.elec.map(addId);
                    // add options to the select fields
                    addOptions('#' + data.both.id, quotation.addresses.elec, 'Please Select Address', true);
                });
                break;
            case "chosen":
                togglePreloader();
                if (!quotation.currentStep.productId) {
                    ERROR("Please return to " + window.location.origin + "/homeservices and click a quote button on one of the products to proceed to this step.");
                    return;
                }
                ;
                Promise.all([REQUEST(window.APIUrl ? window.APIUrl : window.location.origin + '/homeservices/get_salesforce_products'), REQUEST(window.APIUrl ? window.APIUrl : window.location.origin + '/salesforce/get_product_info/' + quotation.currentStep.productId), REQUEST('/templates/quotation/pdf-list.hbs', 'text')])
                    .then(function (res) {
                        togglePreloader();
                        quotation.products = res[0].Products;
                        quotation.includes = res[1].IPIDData;
                        displayChosen();
                        addPDFs(res[2]);
                    });
                break;
            case "signup":
                if (quotation.campaign) {
                    $('.campaign_ref').val(quotation.campaign);
                }
                quotation.currentStep.passedThisStepBefore && populateSignupForm();
                quotation.$wrap.find('.address').text(quotation.form.risk_address_line_1 + ' ' + quotation.form.risk_address_line_2);
                $('.phone_opt_in').on('click', function () {
                    $(this).find('input').val(1);
                });
                $('.post_opt_in').on('click', function () {
                    $(this).find('input').val(1);
                });
                $('.email_opt_in').on('click', function () {
                    $(this).find('input').val(1);
                });
                $('.text_opt_in').on('click', function () {
                    $(this).find('input').val(1);
                });
                togglePreloader();
                REQUEST(window.APIUrl ? window.APIUrl : window.location.origin + '/homeservices/existing_customer_check', 'json', {
                    method: "post",
                    headers: new Headers({"Content-Type": "application/json"}),
                    body: JSON.stringify({postcode: quotation.form.risk_address_postcode})
                })
                    .then(function (res) {
                        togglePreloader();
                        if (!res.customer_exists) {
                            quotation.$wrap.find('.inner[data-type="signup"]').addClass('not-existing')
                        }
                        ;
                    });
                break;
            case "payment":
                quotation.$wrap.find('.breakdown')[quotation.currentProduct.PaymentSchedule === 'Annual' ? 'addClass' : 'removeClass']('annual');
                updatePrice(true);
                break;
        }
    }

    function clearCurrentStepData() {
        switch (quotation.currentStep.type) {
            default:
        }
    }

    function addListeners(data) {
        switch (data.type) {
            case "buttons":
                quotation.$wrap.find('.step-option').on('click', data, evtClick_button);
                break;
            case "postcode":
                quotation.$wrap.find('.submit').on('click', data, evtClick_submitPostcode);
                quotation.$wrap.find('#' + data.input.id).on('keyup', data, evtKeyup_postcode);
                break;
            case "address":
                quotation.$wrap.find('#' + data.both.id).on('change', data, evtChange_address);
                quotation.$wrap.find('.submit').on('click', data, evtClick_submitAddress);
                break;
            case "signup":
                quotation.$wrap.find('.change-address').on('click', evtClick_changeAddress);
                //quotation.$wrap.find('.login').on('click', evtClick_signupLogin);
                quotation.$wrap.find('.submit').on('click', evtClick_signupSubmit);
                resetCheckboxs();
                quotation.$wrap.find('.billing .checkbox').on('click', evtClick_billingCheckbox);
                quotation.$wrap.find('[name="heating_system"]').on('change', evtChange_heatingSystem);
                break;
            case "summary":
                quotation.$wrap.find('.submit').on('click', function (evt) {
                    evt.preventDefault();
                    cycleSteps(1);
                });
                break;
            case "payment":
                resetCheckboxs();
                $('.submit').on('click', evtClick_payInject);
                $('.change-details').on('click', evtClick_changeDetails);
                break;
            case "chosen":
                resetCheckboxs();
                quotation.$wrap.find('.submit').on('click', function (evt) {
                    evt.preventDefault();
                    if (quotation.$wrap.find('.agree .checkbox').hasClass('checked')) {
                        cycleSteps(1);
                        window.scrollTo({
                            top: 0,
                            behavior: 'smooth'
                        });
                    } else {
                        ERROR("You must agree to the terms and conditions to proceed");
                    }
                });
                break;
        }
    }

    function changeStep(next) {
        quotation.$wrap.empty();

        if (quotation.currentStep !== null) {
            quotation.currentStep.passedThisStepBefore = true;
        }
        quotation.currentStep = next;
        next.type === 'welcome' ? TweenMax.set($('#quote-step-back-button'), {display: 'none'}) : TweenMax.set($('#quote-step-back-button'), {display: 'inline-block'});

        var template = Handlebars.compile(quotation.templates[next.type]);
        quotation.$wrap.append(template(next));
        quotation.$title.text(next.title);
        addDynamicContent(next);
        addListeners(next);
        // updateFaqs();
    }

    function cycleSteps(increment, skipTo) {

        var index = skipTo ? skipTo : quotation.currentIndex + increment;
        var step = quotation.steps.find(function (item) {
            return item.index === index
        });

        if (!step) {
            ERROR('Quotation Step Not Recognised.');
            return;
        }

        changeStep(step);
        calculateProgress();
        quotation.currentIndex = index;
        console.log(quotation);
    }
});
