if (window.location.hostname === 'localhost') {
    var base_url = 'http://'+window.location.host+'/';
}
if (window.location.hostname === 'www.eversmartenergy.co.uk' || window.location.hostname === 'staging.eversmartenergy.co.uk') {
    var base_url = 'https://'+window.location.hostname+'/';
}


function logout_user()
{
    $.ajax({
        url: base_url +'index.php/user/logout',
        type: 'post',
        dataType: 'json',
        success: function (response) {
            if (response.error == '0') {
                window.location.href = base_url+'index.php/user/login';
            }
        }
    });
}

function load_tariffs()
{

    console.log('load tariffs')

    $("#tariff_table tbody").empty();

    $.ajax({
        url: base_url + 'Tariff/get_all',
        dataType: 'json',
        type: 'get',
        success: function (data) {

            let tbody = $("<tbody />"),tr;
            $.each(data,function(_,obj) {
                tr = $("<tr />");
                $.each(obj,function(key,text) {
                    if(key == 'tariff_id') return;
                    key == 'is_visible' ?  text == '0' ? tr.append("<td>No</td>") : tr.append("<td>Yes</td>") : tr.append("<td>"+text+"</td>") ;

                });
                tr.append("<td><a href='#' class='edit-tariff' onclick='edit_tariff("+obj.tariff_id+")'>Edit</a></td>")
                tr.appendTo(tbody);
            });
            tbody.appendTo("#tariff_table");

        }
    });
}


function edit_tariff(tariff_id)
{
    console.log('edit tariff ' + tariff_id)

    $.ajax({
        url: base_url + 'Tariff/get/' + tariff_id,
        dataType: 'json',
        type: 'get',
        success: function (response) {
            console.log(response);

            $('form#edit-tariff-form #tariff_id').val(response.tariff_id);
            $('form#edit-tariff-form #tariff_code').val(response.tariff_code);
            $('form#edit-tariff-form #tariff_name').val(response.tariff_name);
            $('form#edit-tariff-form #tariff_type').val(response.tariff_type);
            $('form#edit-tariff-form #is_visible').attr('checked', response.is_visible == '1' ? true : false);
        }
    });

    $('#add-tariff').hide();
    $('#edit-tariff').show();
    $('#response').text('');
}


function toJSONString( form )
{
    var obj = {};
    var elements = document.getElementById(form).querySelectorAll( "input, select" );

    for( var i = 0; i < elements.length; ++i ) {
        var element = elements[i];
        var name = element.name;
        var value = element.value;
        if( name ) {
            obj[ name ] = value;
        }
    }

    return JSON.stringify( obj );
}


$( document ).ready(function() {

    load_tariffs();

    $('#add-new-tariff').click(function(e){

        e.preventDefault();
        console.log('add new tariff')

        $('#add-tariff-form').find("input[type=text]").val("");

        $('#add-tariff').show();
        $('#edit-tariff').hide();
        $('#response').text('');
    });


    $('button.close-tariff').click(function(e){

        e.preventDefault();
        console.log('hide')

        $('#add-tariff').hide();
        $('#edit-tariff').hide();
        $('#response').text('');
    });


    $('#save-new-tariff').click(function(e){

        e.preventDefault();
        console.log('save-new-tariff');

        Promise.all([
            REQUEST( base_url + 'Tariff/create', '', { // Log web to case form
                method : 'post',
                headers : new Headers({ 'Content-Type' : 'application/json' }),
                body : toJSONString('add-tariff-form')
            })
        ])
        .then(function(response){

            load_tariffs();

            $('#response').text(response[0].msg);
            $('#add-tariff').hide();
            $('#edit-tariff').hide();
        })
    });


    $('#save-edit-tariff').click(function(e){

        e.preventDefault();
        console.log('save-edit-tariff');

        $('#edit-tariff-form #is_visible').attr('checked') ? $('#edit-tariff-form #is_visible').val(0) : $('#edit-tariff-form #is_visible').val(1)

        Promise.all([
            REQUEST( base_url + 'Tariff/update/'+$('#tariff_id').val(), '', { // Log web to case form
                method : 'post',
                headers : new Headers({ 'Content-Type' : 'application/json' }),
                body : toJSONString('edit-tariff-form')
            })
        ])
        .then(function(response){

            load_tariffs();

            $('#response').text(response[0].msg);
            $('#add-tariff').hide();
            $('#edit-tariff').hide();
        })
    });


    $('.edit-tariff').click(function(){

        console.log('edit tariff ' + $(this).attr("data-val"))

        $.ajax({
            url: base_url + 'Tariff/get/' + $(this).attr("data-val"),
            dataType: 'json',
            type: 'get',
            success: function (response) {
                console.log(response);
            }
        });

        $('#add-tariff').hide();
        $('#edit-tariff').show();
        $('#response').text('');
    });

});
