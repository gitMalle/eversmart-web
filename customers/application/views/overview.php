<?php require_once 'header.php';
   // debug($profile,1);
 ?>
 
<style>
.dash_footer{position:absolute;}
</style>
                            <div class="col-md-6">
                                <a href="<?php echo base_url() ?>index.php/user/billing"
                                   class="gas_card dash dash-hover">
                                    <div class="gas_box_icon gap_icon_dashboard">
                                        <div class="">
                                            <div class="row">
                                                <div class="col-sm-12"><span class="dash-over-text">Billing</span></div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-4"><p class="dash-p">Last Bill:<br><strong class="dash-s">N/A</strong></p></div>
                                                <div class="col-sm-4"><span class="switch_icon_dashboard"><img src="<?php echo base_url(); ?>images/dashboard/Overview/billing.svg" alt=""></span></div>
                                                <div class="col-sm-4"><p class="dash-p">Next payment due:<br><strong class="dash-s"> N/A</strong></p></div>
                                            </div>
                                        </div>

                                    </div>
                                </a>

                            </div>

                           

                            <div class="col-md-6">
                                <a href="<?php echo base_url() ?>index.php/user/meter_reading"
                                   class="gas_card dash dash-hover">
                                    <div class="gas_box_icon gap_icon_dashboard">

                                        <div class="">
                                            <div class="row">
                                                <div class="col-sm-12"><span class="dash-over-text">Meter Reading</span>
                                                </div>
                                                <div class="col-sm-4">

                                                    <p class="dash-p">
                                                        <?php
                                                        if (!empty($last_reading)) {
                                                            echo "Last Elec Reading: <br>";
                                                            echo '<strong class="dash-s">' . json_encode($last_reading['cumulative']) . '</strong>';
                                                        } else {
                                                            echo "Last Elec Reading: <br>";
                                                            echo '<strong class="dash-s">N/A</strong>';

                                                            ?>
                                                            <br>
                                                            <?php
                                                        } ?>
                                                    </p>

                                                </div>
                                                <div class="col-sm-4"><span class="switch_icon_dashboard"><img
                                                                src="<?php echo base_url(); ?>images/dashboard/Overview/meterreading.svg"
                                                                alt=""></span></div>
                                                <div class="col-sm-4">

                                                    <p class="dash-p">
                                                        <?php
                                                        if (!empty($last_reading_gas)) {
                                                            echo "Last Gas Reading: <br>";
                                                            echo '<strong class="dash-s">' . json_encode($last_reading_gas['cumulative']) . '</strong>';
                                                        } else {
                                                            echo "Last Gas Reading: <br>";
                                                            echo '<strong class="dash-s">N/A</strong>';
                                                            ?>
                                                            <br>
                                                            <?php
                                                        }
                                                        ?>
                                                    </p>


                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </a>
                            </div>

                            <div class="col-md-6">
                                <a href="<?php echo base_url() ?>index.php/user/referral"
                                   class="gas_card dash dash-hover">
                                    <div class="gas_box_icon gap_icon_dashboard">

                                        <div class="">
                                            <div class="row">
                                                <div class="col-sm-12"><span
                                                            class="dash-over-text">Friend Referral</span></div>
                                                <div class="col-sm-4"><p class="dash-p">Friends Referred:
                                                        <?php
                                                        if (!isset($ref) || $ref == 0) {
                                                            echo '<strong class="dash-s">0</strong>';
                                                        } else {
                                                            echo '<br><strong class="dash-s">' . $ref . '</strong>';
                                                        }
                                                        ?>
                                                    </p></div>
                                                <div class="col-sm-4"><span class="switch_icon_dashboard"><img src="<?php echo base_url(); ?>images/dashboard/Overview/refer.svg" alt=""></span></div>
                                                <div class="col-sm-4">
                                                    <p class="dash-p">Credit Accrued: 
                                                        <?php
                                                        if (!isset($format_bal) || $format_bal == 0) {
                                                            echo '<br><strong class="dash-s">£0</strong>';
                                                        } else {
                                                            echo '<br><strong class="dash-s">£' . $format_bal . '</strong>';
                                                        }
                                                        ?>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>

                           

                           

                        <?php require_once 'footer.php'; ?>