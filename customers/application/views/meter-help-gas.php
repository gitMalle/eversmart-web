<div class="container meter-help" id="meter-detail-gas" style="background:#f2b862">
    <div class="row">
        <div class="col-md-6">
            <img class="meter-images" src="<?php echo base_url(); ?>dashboard/images/dashboard/gas-meter-detail.png" alt="">
        </div>
        <div class="col-md-6 detail">
            <h4>Gas Meter</h4>
            <p>Simply write down the numbers from left to right, including any zeros at the beginning. You can ignore the numbers in red or any number after a decimal point.<br><br>
        
            If you have an older imperial gas meter, it might also have a dial. You can ignore this - we just need the numbers on the digital display.<br><br></p>
        </div>
    </div>
</div>