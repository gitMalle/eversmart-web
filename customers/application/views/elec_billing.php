<?php require_once 'header.php'; ?>

        
        <div class="box">
            <div class="box1">
            <div class="boxUpper">
                <div class="boxInner"><img src="<?= base_url(); ?>img/bulb.svg"></div>
                <div class="text"><b>Electricity</b></br> <?= $this->session->userdata('elec_mpan'); ?></div>
            </div>  
                <div class="greyLine"></div>
                <div class="boxText"><a href="<?= base_url() ?>index.php/portal/elec_reading">View Reading</a></div>
            </div>
            
            <?php  if( !empty( $this->session->userdata('gas_mprn') )  ) { ?>
           <div class="box1 box2">
            <div class="boxUpper">
                <div class="boxInner"><img src="<?= base_url(); ?>img/gas.svg"></div>
                <div class="text"><b>Gas</b></br><?= $this->session->userdata('gas_mprn'); ?></div>
            </div> 
                <div class="greyLine"></div>
                <div class="boxText"><a href="<?= base_url() ?>index.php/portal/gas_reading">View Reading</a></div>
            </div>
            <?php } ?>
            
        </div>
        
        <div class="dashboardTable dashboardTable2">
            <div class="imageTop imageElec"> <img src="http://54.218.78.31:50/portal/img/bulb.svg"> </div>
            <div class="tableHeading"><b>Invoice</b></div>
            
            <table cellpadding="1" cellspacing="1" width="100%">
                <thead></thead>
                <tr>
                    <th width="30%">Invoice Number</th>
                    <th width="30%">Invoice Date</th>
                    <th width="15%">Electricity Gross</th>
                    <th width="15%">Gas Gross</th>
                    <th width="15%">Gross</th>
                    <th width="55%">Net</th>
                    <th width="55%">Download</th>
                </tr>
                <?php

                    if( !empty($invoice) )
                    {
                        for( $i=0; $i<count($invoice); $i++ )
                        {
                        ?>
                            <tr>
                                <td><b><?= '#'.$invoice[$i]->InvoiceNumber; ?></b></td>
                                <td><?= $invoice[$i]->InvoiceDate; ?></td>
                                <td><?= $invoice[$i]->ElectricityGrossValue; ?></td>
                                <td><?= $invoice[$i]->GasGrossValue; ?></td>
                                <td><?= $invoice[$i]->GrossValue; ?></td>
                                <td><?= $invoice[$i]->NetValue; ?></td>
                                <td><a target="_blank" href="<?= base_url() ?>index.php/portal/downloadInvoice/<?= $invoice[$i]->InvoiceNumber ?>" data-invoiceID="<?= $invoice[$i]->InvoiceNumber ?>" class="downloadPDF"><center><img src="<?= base_url() ?>img/download.svg" width="30px" height="30px"></center></td>
                            </tr>
                        <?php
                        }
                    }
                    else if( !empty($error) )
                    { 
                        ?>
                        <tr>
                            <td colspan="6" class="text-center"><b><?php echo $error; ?><b></td>
                        </tr>
                        <?php
                    }

                ?>
                <tr>
                </tr>           
            </table>
        </div>
<?php require_once 'footer.php'; ?>
<script type="text/javascript">
	$(document).ready(function()
	{
		$('.dashboard').removeClass('dashboard3');
	});
</script>