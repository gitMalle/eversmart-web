<?php require_once 'header.php';
   // debug($profile,1);
 ?>
 <style type="text/css">
     .dashboard.dashboard3 .dashboardTable {  max-width: 800px; }
     .dashboardTable table { text-align: left; }
     .dashboardTable .imageTop { border-radius: 10px; }
 </style>
        
        <div class="box">
            <div class="box1">
            <div class="boxUpper">
                <div class="boxInner"><img src="<?= base_url(); ?>img/bulb.svg"></div>
                <div class="text"><b>Electricity</b></br> <?= $elec_mpan; ?></div>
            </div>  
                <div class="greyLine"></div>
                <div class="boxText" style="visibility: hidden;"><a href="#">View Statement</a></div>
            </div>
            
            
            <div class="box1 box2">
            <div class="boxUpper">
                <div class="boxInner"><img src="<?= base_url(); ?>img/gas.svg"></div>
                <div class="text"><b>Gas</b></br><?= $gas_mprn; ?></div>
            </div> 
                <div class="greyLine"></div>
                <div class="boxText" style="visibility: hidden;"><a href="#">View Statement</a></div>
            </div>
            
        </div>
        
        <div class="dashBoardTableOuter">
        <div class="dashboardTable dashboardTable2">
            <div class="imageTop"> <img src="<?= base_url(); ?>img/billingIconHover.png"> </div>
            <div class="tableHeading"><b>Profile</b></div>
            <div class="tableHeading" style="float: right;  margin-left:18px;margin-top: 15px;margin-right: 22px;"><b>Status</b> : <span style="color: green; font-weight: bold; font-size: 16px;"><?= $profile->Status->State; ?></span></div>
            
            <table cellpadding="1" cellspacing="1" width="100%">
                <thead></thead>
                <tr>
                    <td><b>Name</b></td>
                    <td><?= ucfirst($profile->Name); ?></td>
                </tr>
                <tr>
                    <td><b>Account Number</b></td>
                    <td><?= ucfirst($profile->AccountNumber); ?></td>
                </tr>
                 <tr>
                    <td><b>Address</b></td>
                    <td><?= ucfirst($profile->Address); ?></td>
                </tr>
                 <tr>
                    <td><b>Email</b></td>
                    <td><?= $profile->Email; ?></td>
                </tr>
                <tr>
                    <td><b>Telephone</b></td>
                    <td><?= $profile->Telephone; ?></td>
                </tr>
                <tr>
                    <td><b>Mobile</b></td>
                    <td><?= $profile->Telephone; ?></td>
                </tr>
                <tr>
                    <td><b>Payment Method</b></td>
                    <td><?= ucfirst($profile->PaymentMethod->Name); ?></td>
                </tr>
           
            </table>
            </div>
        </div>
<?php require_once 'footer.php'; ?>
