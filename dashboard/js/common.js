var redirect_url, group;

if(window.location.hostname == 'localhost')
{
	var base_url = 'http://'+window.location.hostname+'/';
}
if( window.location.hostname == '18.191.137.119')
{
	var base_url = 'http://'+window.location.hostname+'/';
}
if(window.location.hostname == 'www.eversmartenergy.co.uk' )
{
	var base_url = 'https://'+window.location.hostname+'/';
}

function isNumberKey(evt){
   var charCode = (evt.which) ? evt.which : evt.keyCode
   if (charCode > 31 && (charCode < 48 || charCode > 57)){
	  return false;
   };

   return true;
}

function REQUEST( url, resType, opts ){
	opts = opts || {};
	return fetch( url, opts )
		.then(function( res ){
			return resType === 'none' ? res : res[ resType || 'json' ]() ;
		})
		.catch(console.error);
};



function webToCaseBody(){
	return {
		org_id: $('#orgid').val(),
		name: $('#00N1n00000SB9PS').val(), // Name
		email: $('#wtc_email').val(),
		phone: $('#phone').val(),
		dob: $('#00N1n00000SB9PN').val(), // DOB
		address: $('#00N1n00000SB9PI').val(), // Address line 1
		postcode: $('#00N1n00000SB9Ph').val(), // Postcode
		subject: $('#subject').val(),
		description: $('#description').val(),
		junifer_customer_id: $('#00N1n00000SB9OK').val(), // Junifer customer id
		dyball_account_id: $('#00N1n00000SB9NR').val(), // dyball account number
		external_system_registration_status: $('#00N1n00000SB9Pc').val(), // External System Registration Status

		contact_name: $('#name').val(), // Contact Name
		discount_elec: $('#00N1n00000SakSP').val(), // Discount Elec
		discount_gas: $('#00N1n00000SakSU').val(), // Discounts Gas
		supply_end_date_elec: $('#00N1n00000SakSF').val(), // Supply End Date Electricity
		supply_end_date_gas: $('#00N1n00000SakSK').val(), // Supply End Date Gas
		tariff_comparison_rate_gas: $('#00N1n00000SajO2').val(), // Tariff comparison rate Gas
		tariff_comparison_rate_elec: $('#00N1n00000SajO1').val(), // Tariff comparison rate Electricity
		expected_supply_start_date_gas: $('#00N1n00000SajNw').val(), // Expected supply start date Gas
		expected_supply_start_date_elec: $('#00N1n00000SajNv').val(), // Expected supply start date Electricity
		estimated_annual_cost_elec: $('#00N1n00000SajNt').val(), // Estimated annual cost Electricity
		estimated_annual_cost_gas: $('#00N1n00000SajNu').val(), // Estimated annual cost Gas
		gas_unit_rate: $('#00N1n00000SajNs').val(), // Energy unit rate (p/kwh) Gas
		elec_unit_rate: $('#00N1n00000SajNr').val(), // Energy unit rate (p/kwh) Electricity
		daily_standing_charge_elec: $('#00N1n00000SajNp').val(), // Daily Standing charge (p/day) Electricity
		daily_standing_charge_gas: $('#00N1n00000SajNq').val(), // Daily Standing charge (p/day) Gas
		assumed_annual_consumption_elec: $('#00N1n00000SajNn').val(), // Assumed annual consumption Electricity
		assumed_annual_consumption_gas: $('#00N1n00000SajNo').val(), // Assumed annual consumption Gas
		record_type: $('#recordType').val(), // Case Record Type
		company: $('#company').val(),
		case_send_date: $('#00N1n00000SB9ST').val(), // Case send date & time

		meter_supply_number_elec: $('#00N1n00000Saz7d').val(), // Meter Supply Number Electricity
		meter_supply_number_gas: $('#00N1n00000Saz7e').val(), // Meter Supply Number Gas
		product_name_gas: $('#00N1n00000Saz7g').val(), // Product Name Gas
		product_name_elec: $('#00N1n00000Saz7f').val(), // Product Name ELECTRICITY
		meter_serial_number_elec: $('#00N1n00000Saz7b').val(), // Meter Serial Number Electricity
		meter_serial_number_gas: $('#00N1n00000Saz7c').val(), // Meter Serial Number Gas
	};
};

function webToCaseSalesforceBody(){
	return {
		orgid: $('#orgid').val(),
		'00N1n00000SB9PS': $('#00N1n00000SB9PS').val(), // Name
		email: $('#wtc_email').val(),
		phone: $('#phone').val(),
		'00N1n00000SB9PN': $('#00N1n00000SB9PN').val(), // DOB
		'00N1n00000SB9PI': $('#00N1n00000SB9PI').val(), // Address line 1
		'00N1n00000SB9Ph': $('#00N1n00000SB9Ph').val(), // Postcode
		subject: $('#subject').val(),
		description: $('#description').val(),
		'00N1n00000SB9OK': $('#00N1n00000SB9OK').val(), // Junifer customer id
		'00N1n00000SB9NR': $('#00N1n00000SB9NR').val(), // dyball account number
		'00N1n00000SB9Pc': $('#00N1n00000SB9Pc').val(), // External System Registration Status

		name: $('#name').val(), // Contact Name
		'00N1n00000SakSP': $('#00N1n00000SakSP').val(), // Discount Elec
		'00N1n00000SakSU': $('#00N1n00000SakSU').val(), // Discounts Gas
		'00N1n00000SakSF': $('#00N1n00000SakSF').val(), // Supply End Date Electricity
		'00N1n00000SakSK': $('#00N1n00000SakSK').val(), // Supply End Date Gas
		'00N1n00000SajO2': $('#00N1n00000SajO2').val(), // Tariff comparison rate Gas
		'00N1n00000SajO1': $('#00N1n00000SajO1').val(), // Tariff comparison rate Electricity
		'00N1n00000SajNw': $('#00N1n00000SajNw').val(), // Expected supply start date Gas
		'00N1n00000SajNv': $('#00N1n00000SajNv').val(), // Expected supply start date Electricity
		'00N1n00000SajNt': $('#00N1n00000SajNt').val(), // Estimated annual cost Electricity
		'00N1n00000SajNu': $('#00N1n00000SajNu').val(), // Estimated annual cost Gas
		'00N1n00000SajNs': $('#00N1n00000SajNs').val(), // Energy unit rate (p/kwh) Gas
		'00N1n00000SajNr': $('#00N1n00000SajNr').val(), // Energy unit rate (p/kwh) Electricity
		'00N1n00000SajNp': $('#00N1n00000SajNp').val(), // Daily Standing charge (p/day) Electricity
		'00N1n00000SajNq': $('#00N1n00000SajNq').val(), // Daily Standing charge (p/day) Gas
		'00N1n00000SajNn': $('#00N1n00000SajNn').val(), // Assumed annual consumption Electricity
		'00N1n00000SajNo': $('#00N1n00000SajNo').val(), // Assumed annual consumption Gas
		recordType: $('#recordType').val(), // Case Record Type
		company: $('#company').val(),
		'00N1n00000SB9ST': $('#00N1n00000SB9ST').val(), // Case send date & time

		'00N1n00000Saz7d': $('#00N1n00000Saz7d').val(), // Meter Supply Number Electricity
		'00N1n00000Saz7e': $('#00N1n00000Saz7e').val(), // Meter Supply Number Gas
		'00N1n00000Saz7g': $('#00N1n00000Saz7g').val(), // Product Name Gas
		'00N1n00000Saz7f': $('#00N1n00000Saz7f').val(), // Product Name ELECTRICITY
		'00N1n00000Saz7b': $('#00N1n00000Saz7b').val(), // Meter Serial Number Electricity
		'00N1n00000Saz7c': $('#00N1n00000Saz7c').val(), // Meter Serial Number Gas

	};
};







$(document).ready(function()
{

    $("#monthly_breakdown_button").click(function(){
        $('body').addClass('noscroll');
        $('#popup-container').removeAttr('class').addClass('reveal');
        $('body').addClass('popup-active');
        return false;
    });

    $('#close-interest-layer').click(function(){
        $("body").removeClass("noscroll");
        $('#popup-container').addClass('out');
        $('body').removeClass('popup-active');
        $('.email-modal').css("display","none");
    });

    $('#close').click(function(){
		hide_all_detail();
		$('#popup-container').addClass('out');
		$('body').removeClass('popup-active');
		
		$('#meter-info').text("Pick the meter that looks most like yours...");
		$('#help-index').show('slow');
	});

	$('#gas_card_selected').click(function(){

		// Main borders
        $('#select_elec').removeClass('white-blue');
        $('#select_elec').addClass('white-border');
        $('#gas_card_selected').addClass('white-gold');

        // Field wrapper styles
        $('#gas_field_wrapper').css('background-color','#f2b862');
        $('#elec_field_wrapper').css('background-color','#CCC');

        // Show / hide buttons
        $('#gas_button').css('visibility','inherit');
        $('#elec_button').css('visibility','hidden');

        // Toggle icon image
        $("#gas_icon").attr("src", base_url+"dashboard/images/dashboard/Reading/gas/gas-gold.svg");
        $("#elec_icon").attr("src", base_url+"dashboard/images/dashboard/Reading/gas/elec-grey.svg");

        // Show table of results below
        $('#elecReadingTable').hide();
		$('#gasReadingTable').show();
		$('#heading').text('Gas');

        $('#error_msg_elec').empty();


	});

	$('#select_elec').click(function(){

        // Main borders
        $('#gas_card_selected').removeClass('white-gold');
        $('#gas_card_selected').addClass('white-border');
        $('#select_elec').addClass('white-blue');

        // Field wrapper styles
        $('#elec_field_wrapper').css('background-color','#62c6f2');
		$('#gas_field_wrapper').css('background-color','#CCC');

        // Show / hide buttons
        $('#elec_button').css('visibility','inherit');
        $('#gas_button').css('visibility','hidden');

        // Toggle icon image
        $("#gas_icon").attr("src", base_url+"dashboard/images/dashboard/Reading/gas/gas.svg");
        $("#elec_icon").attr("src", base_url+"dashboard/images/dashboard/Reading/Elec/elec.svg");

        // Show table of results below
		$('#gasReadingTable').hide();
		$('#elecReadingTable').show();
		$('#heading').text('Electricity');

        $('#error_msg_gas').empty();

    });
	
	// electricity smart meter
	$("#DialSeventh").keypress(function () {
        $("#DialEight").focus();
    });

	$("#DialSixth").keypress(function () {
        $("#DialSeventh").focus();
    });

	$("#DialFive").keypress(function () {
        $("#DialSixth").focus();
    });

	$("#DialFourth").keypress(function () {
        $("#DialFive").focus();
    });

	$("#DialThird").keypress(function () {
        $("#DialFourth").focus();
    });

	$("#DialSecond").keypress(function () {
        $("#DialThird").focus();
    });

	$("#DialFirst").keypress(function () {
        $("#DialSecond").focus();
	});
	

	$("#DialSeventh_two").keypress(function () {
        $("#DialEight_two").focus();
    });

	$("#DialSixth_two").keypress(function () {
        $("#DialSeventh_two").focus();
    });

	$("#DialFive_two").keypress(function () {
        $("#DialSixth_two").focus();
    });

	$("#DialFourth_two").keypress(function () {
        $("#DialFive_two").focus();
    });

	$("#DialThird_two").keypress(function () {
        $("#DialFourth_two").focus();
    });

	$("#DialSecond_two").keypress(function () {
        $("#DialThird_two").focus();
    });

	$("#DialFirst_two").keypress(function () {
        $("#DialSecond_two").focus();
    });

	$("#DialEight").keypress(function () {
        $("#DialFirst_two").focus();
    });

	// gas smart meter

	$("#Dial_Gas_Seventh").keypress(function () {
        $("#Dial_Gas_Eight").focus();
    });

	$("#Dial_Gas_Sixth").keypress(function () {
        $("#Dial_Gas_Seventh").focus();
    });

	$("#Dial_Gas_Five").keypress(function () {
        $("#Dial_Gas_Sixth").focus();
    });

	$("#Dial_Gas_Fourth").keypress(function () {
        $("#Dial_Gas_Five").focus();
    });

	$("#Dial_Gas_Third").keypress(function () {
        $("#Dial_Gas_Fourth").focus();
    });

	$("#Dial_Gas_Second").keypress(function () {
        $("#Dial_Gas_Third").focus();
    });

	$("#Dial_Gas_First").keypress(function () {
        $("#Dial_Gas_Second").focus();
    });





setTimeout(function(){ $('.loading-page').hide();
 $('#main_content').show() },1000);
  //postcode quotation page
  $.ajax({
      url: base_url+'index.php/quotation/postcode',
      type: 'get',
      success:function(response)
      {
        $('#response').html(response);
      }
  });


  //postcode quotation page
  $.ajax({
      url: base_url+'index.php/quotation/postcode',
      type: 'get',
      success:function(response)
      {
        $('#response').html(response);
      }
  });

  $(document).on('submit','#form-postcode-simple',function(e)
  {
		e.preventDefault();
		var form_postcode = $('#form-postcode').val();
		$.ajax({
			url: base_url+'index.php/quotation/quotefor',
			type: 'post',
			data: { postcode: form_postcode },
			success:function(response)
			{
				//console.log(response);
				if( response == '2000' ) {
					$('#error_message').text('Input postcode did not match validation regex');
					$('.error_msg').show();
				}
				else {
					$('#quotation_form #quotation_postcode').val( form_postcode );
					$('#response').html(response);
				}
			},
      error: function(jqXHR, textStatus, errorThrown){
          alert(textStatus);
      }
		});
  });

	$(document).on( 'click', '#back_postcode', function(){
		//alert($('#quotation_postcode').val());
		//return false;
		var postcode = $('#quotation_postcode').val();
		$.ajax({
			url: base_url+'index.php/quotation/postcode',
			type: 'get',
			success:function(response)
			{
				$('#response').html(response);
				$('.md-form label').addClass('active');
				$('#form-postcode').val( postcode );
			}
		});
	});


//submit gas reading
	$(document).on('submit','#submit_reading_meter_gas', function(e){
		e.preventDefault();
	
		//alert( $('#submit_reading_meter_gas').serialize() );
		if( $('#gas_signup_type').val() == '3' )
		{
			$.ajax({
				url: base_url+'index.php/user/dyball_gas_submit_reading',
				type: 'post',
				dataType: 'json',
				data: $('#submit_reading_meter').serialize(),
				beforeSend:function()
				{
					$('.loading-page').show();
					$('#main_content').hide()
				},
				complete:function(){
					$('.loading-page').hide();
					$('#main_content').show();
				},
				success:function(response){
					console.log(response);
					if( response.error == '0' ){
						$('#error_msg').html('<div class="alert alert-success" role="alert">Reading Submitted!</div>');
						$('#submit_reading_meter')[0].reset();
						setTimeout(function(){ $('#error_msg').empty() }, 1000);
					}
					else{
						$('#error_msg').html('<div class="alert alert-danger" role="alert">Error! while submitting reading</div>');
					}
				}
			});
		}
	});


	$(document).on('click', '.quotefor', function(){

		var select_quote = $(this).data('quotefor');

		$.ajax({
			url: base_url+'index.php/quotation/address_list',
			type: 'get',
			success:function(response)
			{
				$('#quotation_form #quotation_for').val( select_quote );
				$('#response').html(response);
			}
		});
	});


	// BACK TO QUOTE FOR SECTION
	$(document).on( 'click', '#back_quotefor', function(){
		var form_postcode = $('#quotation_postcode').val();
		$.ajax({
			url: base_url+'index.php/quotation/quotefor',
			type: 'post',
			data: { postcode: form_postcode },
			success:function(response)
			{
					$('#response').html(response);
			}
		});
	});

	$(document).on('click', '.cashoutlink', function(){
		$.ajax({
			url: base_url+'index.php/user/saasDebit',
			type: 'post',
			dataType: 'json',
			beforeSend: function() {

				$('.cashoutlink ').css('visibility', 'hidden');
				$('#responsecash').css('visibility', 'visible');

			},
			success:function(response)
			{
				if ( response.status == 'success')
				{

					var lastrewardsamount = $(".lastrewardsamount span").text().replace(/,/g, '');

					var lastrewardsamount  =  parseInt(lastrewardsamount);

					var lastrewardsamountplusfifty = parseInt(lastrewardsamountplusfifty);

					var lastrewardsamountplusfifty = lastrewardsamount  + 50 ;

					var totalrewardsamount = $(".totalrewardsamount span").text().replace(/,/g, '');

					var totalrewardsamount  =  parseInt(totalrewardsamount);

					var totalrewardsamountminusfifty = parseInt(totalrewardsamountminusfifty);

					var totalrewardsamountminusfifty = totalrewardsamount  - 50 ;


					$('.lastrewardsamount span').easy_number_animate({
					  start_value:  lastrewardsamount,
					  end_value: lastrewardsamountplusfifty,
					  duration: 1000,

					});

					$('.totalrewardsamount span').easy_number_animate({
					  start_value:  totalrewardsamount,
					  end_value: totalrewardsamountminusfifty,
					  duration: 1000,

					});


					$('#responsecashnotice').css('display', 'block');

					$('#responsecashnotice').html('Congratulations, we have credited your account');

					$('#responsecashnotice').delay(5000).fadeOut('slow');

				}
				//$('.cashoutlink').html(response);
				else {

					$('#responsecashnotice').css('display', 'block');

					$('#responsecashnotice').html('You do not currently have any rewards...');

					$('#responsecashnotice').delay(5000).fadeOut('slow');

				}
			},
			complete: function() {

			$('.cashoutlink ').css('visibility', 'visible');
			$('#responsecash').css('visibility', 'hidden');
			},
		});
	});

	

	$('#edit_account').click(function(){
		$('#main_div').fadeOut();
		$('#edit_div').fadeIn();
	});
 
	$("#payScroll").click(function() {
		$('html, body').animate({
			scrollTop: $(".col-md-12").offset().top+250
		}, 500);
	}); 


	$('.updateaccount').click(function(){
		$('#updateCustomerDetail').trigger('submit');
	});

	$('#updateCustomerDetail').submit(function(e){
		e.preventDefault();
		$.ajax({
			url: base_url+'index.php/user/update_account',
			type: 'post',
			dataType: 'json',
			data: $('#updateCustomerDetail').serialize(),

			success:function(response){
				console.log(response);
			}
		}) 
	});

	

	$('.booked_meters').click(function(){
		var booked_meter = $(this).data('book_meter');
		//alert(booked_meter);


        $.ajax({
			url: base_url+'index.php/user/booked_meter',
			type: 'post',
            dataType: 'json',
            data: { user_id: $(this).data('userid'), meter: $(this).data('book_meter')  },
			success:function(response){

				// If there were no errors booking
				if(response.error == '0'){

                    // Log web to case form
                    $.ajax({
                        url: base_url + 'index.php/user/web_to_case_log',
                        type: 'post',
                        data: {
                            org_id: $('#orgid').val(),
                            name: $('#00N1n00000SB9PS').val(), // Name
                            email: $('#wtc_email').val(),
                            phone: $('#phone').val(),
                            dob: $('#00N1n00000SB9PN').val(), // DOB
                            address: $('#00N1n00000SB9PI').val(), // Address line 1
                            postcode: $('#00N1n00000SB9Ph').val(), // Postcode
                            subject: $('#subject').val(),
                            description: $('#description').val(),
                            junifer_customer_id: $('#00N1n00000SB9OK').val(), // Junifer customer id
                            dyball_account_id: $('#00N1n00000SB9NR').val(), // dyball account number
                            external_system_registration_status: $('#00N1n00000SB9Pc').val(), // External System Registration Status

                            contact_name: $('#name').val(), // Contact Name
                            discount_elec: $('#00N1n00000SakSP').val(), // Discount Elec
                            discount_gas: $('#00N1n00000SakSU').val(), // Discounts Gas
                            supply_end_date_elec: $('#00N1n00000SakSF').val(), // Supply End Date Electricity
                            supply_end_date_gas: $('#00N1n00000SakSK').val(), // Supply End Date Gas
                            tariff_comparison_rate_gas: $('#00N1n00000SajO2').val(), // Tariff comparison rate Gas
                            tariff_comparison_rate_elec: $('#00N1n00000SajO1').val(), // Tariff comparison rate Electricity
                            expected_supply_start_date_gas: $('#00N1n00000SajNw').val(), // Expected supply start date Gas
                            expected_supply_start_date_elec: $('#00N1n00000SajNv').val(), // Expected supply start date Electricity
                            estimated_annual_cost_elec: $('#00N1n00000SajNt').val(), // Estimated annual cost Electricity
                            estimated_annual_cost_gas: $('#00N1n00000SajNu').val(), // Estimated annual cost Gas
                            gas_unit_rate: $('#00N1n00000SajNs').val(), // Energy unit rate (p/kwh) Gas
                            elec_unit_rate: $('#00N1n00000SajNr').val(), // Energy unit rate (p/kwh) Electricity
                            daily_standing_charge_elec: $('#00N1n00000SajNp').val(), // Daily Standing charge (p/day) Electricity
                            daily_standing_charge_gas: $('#00N1n00000SajNq').val(), // Daily Standing charge (p/day) Gas
                            assumed_annual_consumption_elec: $('#00N1n00000SajNn').val(), // Assumed annual consumption Electricity
                            assumed_annual_consumption_gas: $('#00N1n00000SajNo').val(), // Assumed annual consumption Gas
                            record_type: $('#recordType').val(), // Case Record Type
                            company: $('#company').val(),
                            case_send_date: $('#00N1n00000SB9ST').val(), // Case send date & time

                            meter_supply_number_elec: $('#00N1n00000Saz7d').val(), // Meter Supply Number Electricity
                            meter_supply_number_gas: $('#00N1n00000Saz7e').val(), // Meter Supply Number Gas
                            product_name_gas: $('#00N1n00000Saz7g').val(), // Product Name Gas
                            product_name_elec: $('#00N1n00000Saz7f').val(), // Product Name ELECTRICITY
                            meter_serial_number_elec: $('#00N1n00000Saz7b').val(), // Meter Serial Number Electricity
                            meter_serial_number_gas: $('#00N1n00000Saz7c').val(), // Meter Serial Number Gas

                        }
                    });
                    // End log web to case

                    // Send the web to case form
                    $.ajax({
                        url: 'https://webto.salesforce.com/servlet/servlet.WebToCase?encoding=UTF-8',
                        type: 'post',
                        data: {
                            orgid: $('#orgid').val(),
                            '00N1n00000SB9PS': $('#00N1n00000SB9PS').val(), // Name
                            email: $('#wtc_email').val(),
                            phone: $('#phone').val(),
                            '00N1n00000SB9PN': $('#00N1n00000SB9PN').val(), // DOB
                            '00N1n00000SB9PI': $('#00N1n00000SB9PI').val(), // Address line 1
                            '00N1n00000SB9Ph': $('#00N1n00000SB9Ph').val(), // Postcode
                            subject: $('#subject').val(),
                            description: $('#description').val(),
                            '00N1n00000SB9OK': $('#00N1n00000SB9OK').val(), // Junifer customer id
                            '00N1n00000SB9NR': $('#00N1n00000SB9NR').val(), // dyball account number
                            '00N1n00000SB9Pc': $('#00N1n00000SB9Pc').val(), // External System Registration Status

                            name: $('#name').val(), // Contact Name
                            '00N1n00000SakSP': $('#00N1n00000SakSP').val(), // Discount Elec
                            '00N1n00000SakSU': $('#00N1n00000SakSU').val(), // Discounts Gas
                            '00N1n00000SakSF': $('#00N1n00000SakSF').val(), // Supply End Date Electricity
                            '00N1n00000SakSK': $('#00N1n00000SakSK').val(), // Supply End Date Gas
                            '00N1n00000SajO2': $('#00N1n00000SajO2').val(), // Tariff comparison rate Gas
                            '00N1n00000SajO1': $('#00N1n00000SajO1').val(), // Tariff comparison rate Electricity
                            '00N1n00000SajNw': $('#00N1n00000SajNw').val(), // Expected supply start date Gas
                            '00N1n00000SajNv': $('#00N1n00000SajNv').val(), // Expected supply start date Electricity
                            '00N1n00000SajNt': $('#00N1n00000SajNt').val(), // Estimated annual cost Electricity
                            '00N1n00000SajNu': $('#00N1n00000SajNu').val(), // Estimated annual cost Gas
                            '00N1n00000SajNs': $('#00N1n00000SajNs').val(), // Energy unit rate (p/kwh) Gas
                            '00N1n00000SajNr': $('#00N1n00000SajNr').val(), // Energy unit rate (p/kwh) Electricity
                            '00N1n00000SajNp': $('#00N1n00000SajNp').val(), // Daily Standing charge (p/day) Electricity
                            '00N1n00000SajNq': $('#00N1n00000SajNq').val(), // Daily Standing charge (p/day) Gas
                            '00N1n00000SajNn': $('#00N1n00000SajNn').val(), // Assumed annual consumption Electricity
                            '00N1n00000SajNo': $('#00N1n00000SajNo').val(), // Assumed annual consumption Gas
                            recordType: $('#recordType').val(), // Case Record Type
                            company: $('#company').val(),
                            '00N1n00000SB9ST': $('#00N1n00000SB9ST').val(), // Case send date & time

                            '00N1n00000Saz7d': $('#00N1n00000Saz7d').val(), // Meter Supply Number Electricity
                            '00N1n00000Saz7e': $('#00N1n00000Saz7e').val(), // Meter Supply Number Gas
                            '00N1n00000Saz7g': $('#00N1n00000Saz7g').val(), // Product Name Gas
                            '00N1n00000Saz7f': $('#00N1n00000Saz7f').val(), // Product Name ELECTRICITY
                            '00N1n00000Saz7b': $('#00N1n00000Saz7b').val(), // Meter Serial Number Electricity
                            '00N1n00000Saz7c': $('#00N1n00000Saz7c').val(), // Meter Serial Number Gas

                        }
                    });
                    // End send web to case form

				}

				console.log(response);

				if( booked_meter == 3 ) 
				{
					$('.done_right').hide();
					$('.cross_red').show();
					$('.cross').hide();
				}

				if( booked_meter == 2 ) 
				{
					$('.done_right').show();
					$('.cross_red').hide();
					$('.cross').hide();

				}
				if( booked_meter == 1 )
				{
					$('.done_right').hide();
					$('.cross_red').show();
					$('.cross').hide();
				}
			}


		})
	});

	// $('#payment-form').submit(function(e){
	// 	alert( $('#payment-form').serialize() );
	// });


	// user/pay_dashboard billing address toggle
    $('#toggle_new_card_address').click(function() {
        $("#billing_address_wrapper").toggle(this.checked);
        $("#existing_billing_address").toggle(!this.checked);
    });



    $('.pay').keyup(function() {
        var val = $(this).val();
        if (isNaN(val)) {
            val = val.replace(/[^0-9]/g, '');
            if (val.split('.').length > 2)
                val = val.replace(/\.+$/, "");
        }
        $(this).val(val);
	});

	$('.faq-contact-icons').hide();
	
	$('.faq-contact-help').click(function() {
		$('.faq-contact-help').hide();
		$('.faq-contact-icons').addClass("slide-icons");
		$('.faq-contact-icons').show();
		
	});

});


if(window.location.origin == 'https://www.eversmartenergy.co.uk'){
	var stripe = Stripe('pk_live_t7wcBhtnypxJcda14IRE5QZA');
}
else {
	var stripe = Stripe('pk_test_1iPyX53CapEIo0TAkpNwR8hb');
}

// Create an instance of Elements.
var elements = stripe.elements();

// Custom styling can be passed to options when creating an Element.
// (Note that this demo uses a wider set of styles than the guide below.)
var style = {
  base: {
    color: '#32325d',
    lineHeight: '18px',
    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
    fontSmoothing: 'antialiased',
    fontSize: '16px',
    '::placeholder': {
      color: '#aab7c4'
    }
  },
  invalid: {
    color: '#fa755a',
    iconColor: '#fa755a'
  }
};

// for dd
// Create an instance of the card Element.
var card = elements.create('cardNumber', {style: style});
var dd_ccv = elements.create('cardCvc', {style: style});
var dd_expire = elements.create('cardExpiry', {style: style});
// Add an instance of the card Element into the `card-element` <div>.
card.mount('#card-element');
dd_ccv.mount('#card-dd_ccv');
dd_expire.mount('#card-dd_expire');


// Handle real-time validation errors from the card Element.
card.addEventListener('change', function(event) {
	console.log(event);
	//alert(event);
  var displayError = document.getElementById('card-errors_dd');
  if (event.error) {
    displayError.textContent = event.error.message;
  	displayError.style.display = "block";
  } else {
  	displayError.style.display = "none";
    displayError.textContent = '';
  }
});

var form_dd = document.getElementById('payment-form-dd');

if(form_dd){

	form_dd.addEventListener('submit', function(event) {

		event.preventDefault();
		stripe.createToken(card).then(function(result) {

			if (result.error) {

				// Inform the user if there was an error.
				var errorElement = document.getElementById('card-errors_dd');
				errorElement.textContent = result.error.message;

			}
			else {

				// Validate the pay amount
				var pay_errors = new Array();

				if (document.getElementById('amount_pay').value == '') {
					pay_errors += '<br>Please enter the amount you would like to pay';
				}
				if (document.getElementById('amount_pay').value != '' && document.getElementById('amount_pay').value < 1) {
					pay_errors += '<br>The amount must be over £1';
				}
				// Are there any errors?
				if ($(pay_errors).toArray().length > 0) {
					$('#pay_error_msg').html('<div class="alert alert-danger" role="alert"><strong>Error! </strong>' + pay_errors + '</div>');
					$('html, body').animate({scrollTop: $('#pay_error_msg').offset().top -100 }, 'fast');
					return false;
				}
				else {
					// Clear errors an d send the token to your server.
					$('#pay_error_msg').empty();
				}


				// Are we adding a new address
				if(document.getElementById('toggle_new_card_address').checked == true) {

					// Set up errors array
					var errors = new Array();

					// Validate the required fields
					if (document.getElementById('ba_line1').value == '') {
						errors += '<br>Please enter address line 1 information';
					}
					if (document.getElementById('ba_line2').value == '') {
						errors += '<br>Please enter address line 2 information';
					}
					if (document.getElementById('ba_postcode').value == '') {
						errors += '<br>Please enter postcode';
					}

					// Are there any errors?
					if($(errors).toArray().length>0){
						$('#billing_error_msg').html('<div class="alert alert-danger" role="alert"><strong>Error! </strong>'+errors+'</div>');
						$('html, body').animate({scrollTop: $('#billing_error_msg').offset().top -100 }, 'fast');
						return false;
					}
					else {
						// Clear errors an d send the token to your server.
						$('#billing_error_msg').empty();
						stripeTokenHandler(result.token);
					}

				}
				else {
					// Clear errors and send the token to your server.
					$('#billing_error_msg').empty();
					stripeTokenHandler(result.token);
				}

			}
		});
	});
}

var form_yp = document.getElementById('payment-form-yp');
if(form_yp){
form_yp.addEventListener('submit', function(event) {

	event.preventDefault();
	stripe.createToken(card).then(function(result) {

		if (result.error) {

		  // Inform the user if there was an error.
		  var errorElement = document.getElementById('card-errors_dd');
		  errorElement.textContent = result.error.message;

		} else {

            // Validate the pay amount
            var pay_errors = new Array();

            if (document.getElementById('amount_pay').value == '') {
                pay_errors += '<br>Please enter the amount you would like to pay';
            }
            if (document.getElementById('amount_pay').value != '' && document.getElementById('amount_pay').value < 1) {
                pay_errors += '<br>The amount must be over £1';
            }
            // Are there any errors?
            if ($(pay_errors).toArray().length > 0) {
                $('#pay_error_msg').html('<div class="alert alert-danger" role="alert"><strong>Error! </strong>' + pay_errors + '</div>');
                $('html, body').animate({scrollTop: $('#pay_error_msg').offset().top -100 }, 'fast');
                return false;
        	}
            else {
                // Clear errors an d send the token to your server.
                $('#pay_error_msg').empty();
            }


            // Are we adding a new address
            if(document.getElementById('toggle_new_card_address').checked == true) {

				// Set up errors array
				var errors = new Array();

				// Validate the required fields
				if (document.getElementById('ba_line1').value == '') {
					errors += '<br>Please enter address line 1 information';
				}
				if (document.getElementById('ba_line2').value == '') {
					errors += '<br>Please enter address line 2 information';
				}
				if (document.getElementById('ba_postcode').value == '') {
					errors += '<br>Please enter postcode';
				}

				// Are there any errors?
				if($(errors).toArray().length>0){
					$('#billing_error_msg').html('<div class="alert alert-danger" role="alert"><strong>Error! </strong>'+errors+'</div>');
                    $('html, body').animate({scrollTop: $('#billing_error_msg').offset().top -100 }, 'fast');
                    return false;
                }
				else {
                    // Clear errors an d send the token to your server.
                    $('#billing_error_msg').empty();
					stripeTokenHandleryp(result.token);
				}

			}
			else {
                // Clear errors and send the token to your server.
                $('#billing_error_msg').empty();
				stripeTokenHandleryp(result.token);
			}

		}
	});
});
}


var form_ypp = document.getElementById('current-newjunifer');
if(form_ypp){
form_ypp.addEventListener('submit', function(event) {

	event.preventDefault();
	stripe.createToken(card).then(function(result) {

		if (result.error) {

		  // Inform the user if there was an error.
		  var errorElement = document.getElementById('card-errors_dd');
		  errorElement.textContent = result.error.message;

		} else {

            // Validate the pay amount
            var pay_errors = new Array();

            if (document.getElementById('amount_pay').value == '') {
                pay_errors += '<br>Please enter the amount you would like to pay';
            }
            if (document.getElementById('amount_pay').value != '' && document.getElementById('amount_pay').value < 1) {
                pay_errors += '<br>The amount must be over £1';
            }
            // Are there any errors?
            if ($(pay_errors).toArray().length > 0) {
                $('#pay_error_msg').html('<div class="alert alert-danger" role="alert"><strong>Error! </strong>' + pay_errors + '</div>');
                $('html, body').animate({scrollTop: $('#pay_error_msg').offset().top -100 }, 'fast');
                return false;
        	}
            else {
                // Clear errors an d send the token to your server.
                $('#pay_error_msg').empty();
            }


            // Are we adding a new address
            if(document.getElementById('toggle_new_card_address').checked == true) {

				// Set up errors array
				var errors = new Array();

				// Validate the required fields
				if (document.getElementById('ba_line1').value == '') {
					errors += '<br>Please enter address line 1 information';
				}
				if (document.getElementById('ba_line2').value == '') {
					errors += '<br>Please enter address line 2 information';
				}
				if (document.getElementById('ba_postcode').value == '') {
					errors += '<br>Please enter postcode';
				}

				// Are there any errors?
				if($(errors).toArray().length>0){
					$('#billing_error_msg').html('<div class="alert alert-danger" role="alert"><strong>Error! </strong>'+errors+'</div>');
                    $('html, body').animate({scrollTop: $('#billing_error_msg').offset().top -100 }, 'fast');
                    return false;
                }
				else {
                    // Clear errors an d send the token to your server.
                    $('#billing_error_msg').empty();
					stripeTokenHandlerypy(result.token);
				}

			}
			else {
                // Clear errors and send the token to your server.
                $('#billing_error_msg').empty();
				stripeTokenHandlerypy(result.token);
			}

		}
	});
});
}
// //for cc
// var card_cc = elements.create('cardNumber', {style: style});
// var cc_ccv = elements.create('cardCvc', {style: style});
// var cc_expire = elements.create('cardExpiry', {style: style});
// // Add an instance of the card Element into the `card-element` <div>.
// card_cc.mount('#card-element-cc');
// cc_ccv.mount('#card-cc_ccv');
// cc_expire.mount('#card-cc_expire');

// // Handle real-time validation errors from the card Element.
// card_cc.addEventListener('change', function(event) {
//   var displayError = document.getElementById('card-errors_cc');
//   if (event.error) {
//     displayError.textContent = event.error.message;
//   	displayError.style.display = "block";
//   } else {
//   	displayError.style.display = "none";
//     displayError.textContent = '';
//   }
// });


// // Handle form submission.
// var form = document.getElementById('payment-form');
// form.addEventListener('submit', function(event) {
//   event.preventDefault();

//   stripe.createToken(card_cc).then(function(result) {
//     if (result.error) {
//       // Inform the user if there was an error.
//       var errorElement = document.getElementById('card-errors_cc');
//       errorElement.textContent = result.error.message;
//     } else {
//       // Send the token to your server.
//       console.log(result.token);
//       stripeTokenHandler(result.token);
//     }
//   });
// });




function stripeTokenHandler(token) {

	// Insert the token ID into the form so it gets submitted to the server
	var form = document.getElementById('payment-form-dd');

	var hiddenInput = document.createElement('input');
	hiddenInput.setAttribute('type', 'hidden');
	hiddenInput.setAttribute('name', 'token');
	hiddenInput.setAttribute('value', token.id);

	var amount = document.createElement('input');
	amount.setAttribute('type', 'hidden');
	amount.setAttribute('name', 'amount');
	amount.setAttribute('value', document.getElementById('amount_pay').value);

	var userid = document.createElement('input');
	userid.setAttribute('type', 'hidden');
	userid.setAttribute('name', 'account_id');
	userid.setAttribute('value', document.getElementById('user_id').value);

	var juniferid = document.createElement('input');
	juniferid.setAttribute('type', 'hidden');
	juniferid.setAttribute('name', 'junifer_account_id');
	juniferid.setAttribute('value', document.getElementById('junifer_account_id').value);

	var existing_line1 = document.createElement('input');
    existing_line1.setAttribute('type', 'hidden');
    existing_line1.setAttribute('name', 'existing_line1');
    existing_line1.setAttribute('value', document.getElementById('existing_line1').value);

	var existing_postcode = document.createElement('input');
    existing_postcode.setAttribute('type', 'hidden');
    existing_postcode.setAttribute('name', 'existing_postcode');
    existing_postcode.setAttribute('value', document.getElementById('existing_postcode').value);

	var email = document.createElement('input');
    email.setAttribute('type', 'hidden');
    email.setAttribute('name', 'email');
    email.setAttribute('value', document.getElementById('email').value);

	var customer_id = document.createElement('input');
    customer_id.setAttribute('type', 'hidden');
    customer_id.setAttribute('name', 'customer_id');
    customer_id.setAttribute('value', document.getElementById('customer_id').value);

	var billing_address_id = document.createElement('input');
    billing_address_id.setAttribute('type', 'hidden');
    billing_address_id.setAttribute('name', 'billing_address_id');
    billing_address_id.setAttribute('value', document.getElementById('billing_address_id').value);

	// Are they using a new billing address
	if(document.getElementById('toggle_new_card_address').checked){

		var ba_line1 = document.createElement('input');
		ba_line1.setAttribute('type', 'hidden');
		ba_line1.setAttribute('name', 'ba_line1');
		ba_line1.setAttribute('value', document.getElementById('ba_line1').value);

		var ba_line2 = document.createElement('input');
		ba_line2.setAttribute('type', 'hidden');
		ba_line2.setAttribute('name', 'ba_line2');
		ba_line2.setAttribute('value', document.getElementById('ba_line2').value);

		var ba_line3 = document.createElement('input');
		ba_line3.setAttribute('type', 'hidden');
		ba_line3.setAttribute('name', 'ba_line3');
		ba_line3.setAttribute('value', document.getElementById('ba_line3').value);

		var ba_line4 = document.createElement('input');
		ba_line4.setAttribute('type', 'hidden');
		ba_line4.setAttribute('name', 'ba_line4');
		ba_line4.setAttribute('value', document.getElementById('ba_line4').value);

		var ba_postcode = document.createElement('input');
		ba_postcode.setAttribute('type', 'hidden');
		ba_postcode.setAttribute('name', 'ba_postcode');
		ba_postcode.setAttribute('value', document.getElementById('ba_postcode').value);


		form.appendChild(ba_line1);
		form.appendChild(ba_line2);
		form.appendChild(ba_line3);
		form.appendChild(ba_line4);
		form.appendChild(ba_postcode);

	}

	form.appendChild(hiddenInput);
	form.appendChild(amount);
	form.appendChild(userid);
	form.appendChild(juniferid);
	form.appendChild(existing_line1);
	form.appendChild(existing_postcode);
	form.appendChild(email);
	form.appendChild(customer_id);
	form.appendChild(billing_address_id);


	// Submit the form
	$.ajax({

	url: base_url+'index.php/api/clear_payment',
	type: 'post',
	dataType: 'json',
	  data: $('#payment-form-dd').serialize(),

	beforeSend:function(){
		$('.loading-page').show();
		$('#main_content').hide();
	},
	complete:function(){
		$('.loading-page').hide();
		  $('#main_content').show();
		  console.log($('#payment-form-dd').serialize())
	},
	success:function(res)
	{
		console.log(res);
		if(res.api_status == 1)
		{
			$('#Transaction_status').text('Successful');
			$('#Transaction').text(res.data.transaction_id);

			$('#pay_card').fadeOut();
			$('#thank_you').fadeIn();

			$(".gas_card").css("display","none");
			$(".payment-details").css("display","inline-block");

            $('html, body').animate({scrollTop: $('#Transaction_status').offset().top -100 }, 'fast');

		}
		else{
			$('#card-errors').text(res.message);
		}
	}

	})
}

function stripeTokenHandleryp(token) {

	// Insert the token ID into the form so it gets submitted to the server
	var form = document.getElementById('payment-form-yp');

	var hiddenInput = document.createElement('input');
	hiddenInput.setAttribute('type', 'hidden');
	hiddenInput.setAttribute('name', 'token');
	hiddenInput.setAttribute('value', token.id);

	var amount = document.createElement('input');
	amount.setAttribute('type', 'hidden');
	amount.setAttribute('name', 'amount');
	amount.setAttribute('value', document.getElementById('amount_pay').value);

	var userid = document.createElement('input');
	userid.setAttribute('type', 'hidden');
	userid.setAttribute('name', 'account_id');
	userid.setAttribute('value', document.getElementById('user_id').value);

	var juniferid = document.createElement('input');
	juniferid.setAttribute('type', 'hidden');
	juniferid.setAttribute('name', 'junifer_account_id');
	juniferid.setAttribute('value', document.getElementById('junifer_account_id').value);

	var existing_line1 = document.createElement('input');
    existing_line1.setAttribute('type', 'hidden');
    existing_line1.setAttribute('name', 'existing_line1');
    existing_line1.setAttribute('value', document.getElementById('existing_line1').value);

	var existing_postcode = document.createElement('input');
    existing_postcode.setAttribute('type', 'hidden');
    existing_postcode.setAttribute('name', 'existing_postcode');
    existing_postcode.setAttribute('value', document.getElementById('existing_postcode').value);

	var email = document.createElement('input');
    email.setAttribute('type', 'hidden');
    email.setAttribute('name', 'email');
    email.setAttribute('value', document.getElementById('email').value);

	var customer_id = document.createElement('input');
    customer_id.setAttribute('type', 'hidden');
    customer_id.setAttribute('name', 'customer_id');
    customer_id.setAttribute('value', document.getElementById('customer_id').value);

	if(billing_address_id){
	var billing_address_id = document.createElement('input');
    billing_address_id.setAttribute('type', 'hidden');
    billing_address_id.setAttribute('name', 'billing_address_id');
	billing_address_id.setAttribute('value', document.getElementById('billing_address_id').value);
	}

	// Are they using a new billing address
	if(document.getElementById('toggle_new_card_address').checked){

		var ba_line1 = document.createElement('input');
		ba_line1.setAttribute('type', 'hidden');
		ba_line1.setAttribute('name', 'ba_line1');
		ba_line1.setAttribute('value', document.getElementById('ba_line1').value);

		var ba_line2 = document.createElement('input');
		ba_line2.setAttribute('type', 'hidden');
		ba_line2.setAttribute('name', 'ba_line2');
		ba_line2.setAttribute('value', document.getElementById('ba_line2').value);

		var ba_line3 = document.createElement('input');
		ba_line3.setAttribute('type', 'hidden');
		ba_line3.setAttribute('name', 'ba_line3');
		ba_line3.setAttribute('value', document.getElementById('ba_line3').value);

		var ba_line4 = document.createElement('input');
		ba_line4.setAttribute('type', 'hidden');
		ba_line4.setAttribute('name', 'ba_line4');
		ba_line4.setAttribute('value', document.getElementById('ba_line4').value);

		var ba_postcode = document.createElement('input');
		ba_postcode.setAttribute('type', 'hidden');
		ba_postcode.setAttribute('name', 'ba_postcode');
		ba_postcode.setAttribute('value', document.getElementById('ba_postcode').value);


		form.appendChild(ba_line1);
		form.appendChild(ba_line2);
		form.appendChild(ba_line3);
		form.appendChild(ba_line4);
		form.appendChild(ba_postcode);

	}

	form.appendChild(hiddenInput);
	form.appendChild(amount);
	form.appendChild(userid);
	form.appendChild(juniferid);
	form.appendChild(existing_line1);
	form.appendChild(existing_postcode);
	form.appendChild(email);
	form.appendChild(customer_id);

	if(billing_address_id){
		form.appendChild(billing_address_id);
	};

	REQUEST( base_url+'index.php/api/create_yearly_customer', 'json', {
		method : 'post',
		body : $('#payment-form-yp').serialize()
	})
	.then(function( res ){
		switch( res.api_status ){
			case 1:
				$('#Transaction').text(res.message);

				$('#pay_card').fadeOut();
				$('#thank_you').fadeIn();

				$(".gas_card").css("display","none");
				$(".payment-details").css("display","inline-block");

				$('html, body').animate({scrollTop: $('#Transaction_status').offset().top -100 }, 'fast');
			break;
			default:
				$('#card-errors').text(res.message);
			break;
		}
	})
	.then(function(){
		$('.loading-page').hide();
		$('#main_content').show();
	});

	$('.loading-page').show();
	$('#main_content').hide();

};


function stripeTokenHandlerypy(token) {

	// Insert the token ID into the form so it gets submitted to the server
	var form = document.getElementById('current-newjunifer');

	var hiddenInput = document.createElement('input');
	hiddenInput.setAttribute('type', 'hidden');
	hiddenInput.setAttribute('name', 'token');
	hiddenInput.setAttribute('value', token.id);

	var amount = document.createElement('input');
	amount.setAttribute('type', 'hidden');
	amount.setAttribute('name', 'amount');
	amount.setAttribute('value', document.getElementById('amount_pay').value);

	var userid = document.createElement('input');
	userid.setAttribute('type', 'hidden');
	userid.setAttribute('name', 'account_id');
	userid.setAttribute('value', document.getElementById('user_id').value);

	var juniferid = document.createElement('input');
	juniferid.setAttribute('type', 'hidden');
	juniferid.setAttribute('name', 'junifer_account_id');
	juniferid.setAttribute('value', document.getElementById('junifer_account_id').value);

	var existing_line1 = document.createElement('input');
    existing_line1.setAttribute('type', 'hidden');
    existing_line1.setAttribute('name', 'existing_line1');
    existing_line1.setAttribute('value', document.getElementById('existing_line1').value);

	var existing_postcode = document.createElement('input');
    existing_postcode.setAttribute('type', 'hidden');
    existing_postcode.setAttribute('name', 'existing_postcode');
    existing_postcode.setAttribute('value', document.getElementById('existing_postcode').value);

	var email = document.createElement('input');
    email.setAttribute('type', 'hidden');
    email.setAttribute('name', 'email');
    email.setAttribute('value', document.getElementById('email').value);

	var customer_id = document.createElement('input');
    customer_id.setAttribute('type', 'hidden');
    customer_id.setAttribute('name', 'customer_id');
    customer_id.setAttribute('value', document.getElementById('customer_id').value);

	if(billing_address_id){
	var billing_address_id = document.createElement('input');
    billing_address_id.setAttribute('type', 'hidden');
    billing_address_id.setAttribute('name', 'billing_address_id');
	billing_address_id.setAttribute('value', document.getElementById('billing_address_id').value);
	}

	// Are they using a new billing address
	if(document.getElementById('toggle_new_card_address').checked){

		var ba_line1 = document.createElement('input');
		ba_line1.setAttribute('type', 'hidden');
		ba_line1.setAttribute('name', 'ba_line1');
		ba_line1.setAttribute('value', document.getElementById('ba_line1').value);

		var ba_line2 = document.createElement('input');
		ba_line2.setAttribute('type', 'hidden');
		ba_line2.setAttribute('name', 'ba_line2');
		ba_line2.setAttribute('value', document.getElementById('ba_line2').value);

		var ba_line3 = document.createElement('input');
		ba_line3.setAttribute('type', 'hidden');
		ba_line3.setAttribute('name', 'ba_line3');
		ba_line3.setAttribute('value', document.getElementById('ba_line3').value);

		var ba_line4 = document.createElement('input');
		ba_line4.setAttribute('type', 'hidden');
		ba_line4.setAttribute('name', 'ba_line4');
		ba_line4.setAttribute('value', document.getElementById('ba_line4').value);

		var ba_postcode = document.createElement('input');
		ba_postcode.setAttribute('type', 'hidden');
		ba_postcode.setAttribute('name', 'ba_postcode');
		ba_postcode.setAttribute('value', document.getElementById('ba_postcode').value);


		form.appendChild(ba_line1);
		form.appendChild(ba_line2);
		form.appendChild(ba_line3);
		form.appendChild(ba_line4);
		form.appendChild(ba_postcode);

	}

	form.appendChild(hiddenInput);
	form.appendChild(amount);
	form.appendChild(userid);
	form.appendChild(juniferid);
	form.appendChild(existing_line1);
	form.appendChild(existing_postcode);
	form.appendChild(email);
	form.appendChild(customer_id);

	if(billing_address_id){
		form.appendChild(billing_address_id);
	};

	REQUEST( base_url+'index.php/notfound/junifer_enrol', 'json', {
		method : 'post',
		body : $('#current-newjunifer').serialize()
	})
	.then(function( res ){
		switch( res.api_status ){
			case 1:

				// JUNIFER - SET THE WEB TO CASE FIELDS
                $('#subject').val('Tariff Switch Success - Junifer Customer ID:' + res.data.junifer_id + ' - Success');
                $('#description').val('Tariff Switch Success for ' + $('#00N1n00000SB9PS').val());
                $('#00N1n00000SB9OK').val(res.data.junifer_id); // Junifer customer id
                $('#00N1n00000SB9Pc').val('Tariff Switch Success');
				// JUNIFER - END THE WEB TO CASE FIELDS

				$('#Transaction').text(res.message);
                $('#error_msg').text(res.msg);

                $('#pay_card').fadeOut();
                $('#thank_you').fadeIn();

                $(".gas_card").css("display","none");
                $(".payment-details").css("display","inline-block");

                $('html, body').animate({scrollTop: $('#Transaction_status').offset().top -100 }, 'fast');

			break;
			default:

				$('#card-errors').text(res.message);
				$('.error_msg').text(res.msg);

				// JUNIFER - SET THE WEB TO CASE FIELDS
				if (res.message != '') { // Stripe payment issue
					// Success
					$('#subject').val('Tariff Switch Pending - Junifer Customer ID:' + res.junifer_id + ' - Pending');
					$('#description').val(res.message);
					$('#00N1n00000SB9OK').val(res.junifer_id); // Junifer customer id
					$('#00N1n00000SB9Pc').val('Tariff Switch Pending');
				}

				if(res.msg != '') { // Junifer issue
					$('#subject').val('Tariff Switch Failure - Junifer Customer - Failure');
					$('#description').val(res.msg);
					$('#00N1n00000SB9OK').val(''); // Junifer customer id
					$('#00N1n00000SB9Pc').val('Tariff Switch Failure');
				}
				// JUNIFER - END THE WEB TO CASE FIELDS

			break;
		};

		// Log web to case form
		REQUEST( base_url + 'index.php/user/web_to_case_log', 'none', {
			method : 'post',
			headers : new Headers({ 'Content-type' : 'application/json' }),
			body : JSON.stringify( webToCaseBody() )
		});
		
		// Send the web to case form
		REQUEST( 'https://webto.salesforce.com/servlet/servlet.WebToCase?encoding=UTF-8', 'none', {
			method : 'post',
			headers : new Headers({ 'Content-type' : 'application/json' }),
			body : JSON.stringify( webToCaseSalesforceBody() )
		});

	})
	.then(function(){
		$('.loading-page').hide();
        $('#main_content').show();
	});

	$('.loading-page').show();
	$('#main_content').hide();
	
};























function showTab(n) // This function will display the specified tab of the form ...
{
    var x = document.getElementsByClassName("tab");
    x[n].style.display = "block";   // ... and fix the Previous/Next buttons
    if (n == 0) 
    {
        document.getElementById("prevBtn").style.display = "none";
    } 
    else 
    {
        document.getElementById("prevBtn").style.display = "inline";
    }

    if (n == (x.length - 1)) 
    {
        document.getElementById("nextBtn").innerHTML = "Submit";
    } 
    else 
    {
        document.getElementById("nextBtn").innerHTML = "Next";
    }
}

function nextPrev(n)    // This function will figure out which tab to display
{

	// Clear all errors initially 
	$('#error_msg_date').empty();
	$('#error_msg_elec').empty();
	$('#error_msg_gas').empty();
	$('#error_msg_add').empty();


    var x = document.getElementsByClassName("tab");
    x[currentTab].style.display = "none";   // Hide the current tab
    currentTab = currentTab + n;    // Increase or decrease the current tab by 1

    // if you have reached the end of the form... 
    if(currentTab >= x.length) 
    {
        $('.en_select').css('display', 'none');
        $('.tabend').css('display', 'block');
        $('#btns').css('display', 'none');
        $('#4').removeClass('is-active');
        $('#5').addClass('is-active');

		$.ajax({
			url: base_url+'index.php/user/save_moving_home',
			data: $('#moveHomeForm').serialize(),
			dataType: 'json',
			type: 'get',
			success:function(response)
			{			
				$('#mov_response').html(response.msg);
			},
			
		});

        return false;
    }
    if(currentTab == 0)
    {
		$('.en_select').css('display', 'none');
		$('#1').removeClass('is-active');
		$('#2').addClass('is-active');
    }
    if(currentTab == 1)
    {
        $('#2').removeClass('is-active');
        $('#3').addClass('is-active');
        $('.en_select').css('display', 'inline-flex');
    }
    if(currentTab == 2)
    {
        $('.en_select').css('display', 'none');
        $('#3').removeClass('is-active');
        $('#4').addClass('is-active');
    }
    showTab(currentTab);    // Otherwise, display the correct tab
}

function toggleGas()
{
	$('#gas_card').css('display','none');
	$('#select_elec').hide();
	$('#select_gas').show();
	$('#gas_card_selected').show();
	$('#main_electricity_card').hide();
	$('#main_gas_card').show();
	$('#submit_meter').addClass('gold_bg');
	$('#elecReadingTable').hide();
	$('#gasReadingTable').show();
	$('#heading').text('Gas');
}

function toggleElec()
{
	$('#select_gas').hide();
	$('#select_elec').show();
	$('#gas_card').show();
	$('#gas_card_selected').hide();
	$('#main_electricity_card').show();
	$('#main_gas_card').hide();
	$('#submit_meter').removeClass('gold_bg');
	$('#gasReadingTable').hide();
	$('#elecReadingTable').show();
	$('#heading').text('Electricity');
}

function validateForm() {
	if(currentTab == 0)
	{
		$date = document.forms["moveHomeForm"]["date"].value;
	}

	if(currentTab == 1)
	{
		//alert("set elec/gas vars");
		$elec = document.forms["moveHomeForm"]["elec_submit_reds"].value;
		$gas = document.forms["moveHomeForm"]["gas_submit_reds"].value;
	}

	if(currentTab == 2)
	{
		$add1 = document.forms["moveHomeForm"]["add1"].value;
		$add2 = document.forms["moveHomeForm"]["add2"].value;
		$add3 = document.forms["moveHomeForm"]["add3"].value;
		$add4 = document.forms["moveHomeForm"]["add4"].value;
		//$new_add = document.forms["moveHomeForm"]["new_address"].value;
	}

	if($date == "") 
	{
		$('#error_msg_date').html('<div class="alert alert-danger" role="alert" style="margin-top:10px;text-align:center"><strong>Error! </strong>Please enter a date</div>');
        return false;
	}
	else
	{	
		if(currentTab == 0)
		{
			nextPrev(1);
		}
	}

	if($elec == "" || $gas == "")
	{

		if($elec == "")
		{
			$('#error_msg_elec').html('<div class="alert alert-danger"  role="alert" style="margin-top:10px;text-align:center"><strong>Error! </strong>Please enter your electricity reading</div>');
		}
		else
		{
			$('#error_msg_elec').empty();
			if($gas == "")
			{
				toggleGas();
			}
		}
		if($gas == "")
		{
			$('#error_msg_gas').html('<div class="alert alert-danger" role="alert" style="margin-top:10px;text-align:center"><strong>Error! </strong>Please enter your gas reading</div>');
			
		}
		else
		{
			$('#error_msg_gas').empty();
			if($elec == "")
			{
				toggleElec();
			}
		}
		return false;
        
	}else
	{	
		if(currentTab == 1)
		{
			nextPrev(1);
		}
	}

	if($add1 == "" || $add2 == "" || $add3 == "" || $add4 == "")
	{
		if($add1 == "")
		{
			$("#add1").focus();
			$("#add1").select();
			$('#error_msg_add').html('<div class="alert alert-danger" role="alert" style="margin-top:10px;text-align:center"><strong>Error! </strong>Please enter the first line of your new address</div>');
        	return false;
		}
		else
		{
			$('#error_msg_add').empty();
		}
		
		if($add2 == "")
		{
			$("#add2").focus();
			$("#add2").select();
			$('#error_msg_add').html('<div class="alert alert-danger" role="alert" style="text-align:center"><strong>Error! </strong>Please enter the second line of your new address</div>');
        	return false;
		}
		else
		{
			$('#error_msg_add').empty();
		}

		if($add3 == "")
		{
			$("#add3").focus();
			$("#add3").select();
			$('#error_msg_add').html('<div class="alert alert-danger" role="alert" style="text-align:center"><strong>Error! </strong>Please enter the third line of your new address</div>');
        	return false;
		}
		else
		{
			$('#error_msg_add').empty();
		}

		if($add4 == "")
		{
			$("#add4").focus();
			$("#add4").select();
			$('#error_msg_add').html('<div class="alert alert-danger" role="alert" style="text-align:center"><strong>Error! </strong>Please enter the postcode of your new address</div>');
        	return false;
		}
		else
		{
			$('#error_msg_add').empty();
		}
		
	}else
	{	
		if(currentTab == 2)
		{
			nextPrev(1);
		}
	}

	//if($new_add == "" || $new_add == ", , , ")
}


function logout_user() {

    $.ajax({
        url: base_url + 'index.php/user/logout',
        type: 'post',
        dataType: 'json',
        success: function (response) {
            if (response.error == '0') {
                window.location.href =  base_url + 'index.php/user/login';
            }
        }
    });

}